package org.dimensinfin.eveonline.neocom.datasource;

import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.android.mvc.datasource.AMVCDataSource;
import org.dimensinfin.android.mvc.support.SeparatorController;
import org.dimensinfin.android.mvc.core.AppCompatibilityUtils;
import org.dimensinfin.android.mvc.datasource.DataSourceManager;
import org.dimensinfin.android.mvc.datasource.IDataSource;
import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.android.mvc.interfaces.ICollaboration;
import org.dimensinfin.android.mvc.support.Separator;

import org.junit.Assert;
import org.junit.Test;

public class TimingDataSourceTest {
	/**
	 * This test should check the correct chaining of background tasks when initialising a data source.
	 * The Data Source creation process starts on the Fragment <code>generateDataSource</code> that should create a new data source that is then
	 * registered on the <code>DataSourceRepository</code>. At this point if the new data source is accepted it should start the initialization process
	 * with the call <code>prepareModel</code>. This runs as a thread task and should start after the Fragment <code>onCreateView</code> completes.
	 *
	 * After the <code>prepareModel</code> then the Fragment starts the model generation by calling the data source adapter method <code>collaborateData</code>
	 * that should then call the data source initialisation on the <code>collaborate2Model</code>. But this call should wait for the <code>prepareModel</code> task
	 * to complete because is then when the model structures can be read to generate the right view contents lists.
	 *
	 * After the initialisation of the model that is also run on another thread task, there comes the Header view generation that should obtain the list
	 * of header controllers from the list of header model elements. This is the call <code>getHeaderSectionContents</code> to the data source
	 * instance. With his list the Fragment can populate the header view. But to be able to read that list the previous data source call to generate
	 * the model should have completed so the rendering will show the complete list of elements and when the progress spinner is removed there are no more
	 * render items pending.
	 *
	 * This synchronization is done with a <b>monitor</b> synchronized call.
	 */
	public class TimingControllerFactory extends ControllerFactory {

		public TimingControllerFactory(final String selectedVariant) {
			super(selectedVariant);
		}

		@Override
		public IAndroidController createController(final ICollaboration node) {
			//		if (node instanceof RootControllerTest.TestNode) {
			//			return new SeparatorController((RootControllerTest.TestNode) node, this);
			//		}
			if (node instanceof Separator) {
				return new SeparatorController((Separator) node, this);
			}
			return super.createController(node);
		}
	}

	public static class TimingDataSource extends AMVCDataSource {
		/**
		 * This is the action to signal the data source to start searching for its data to process it and generate
		 * the contents for the root model list. The model generated is still not processed and no controllers are created until
		 * the viewer requires them. This call is to create any internal data source data structures that are to be used when extracting the
		 * header and data section models.
		 */
		@Override
		public void prepareModel() {
			synchronized (this.monitor) {
				this.addModelContents(new Separator("PREPAREMODEL"));
			}
		}

		@Override
		public void collaborate2Model() {
			synchronized (this.monitor) {
				this.addModelContents(new Separator("COLLABORATE2MODEL"));
			}
		}

		public static class Builder extends BaseBuilder<TimingDataSource, TimingDataSource.Builder> {
			@Override
			protected TimingDataSource getActual() {
				return new TimingDataSource();
			}

			@Override
			protected Builder getActualBuilder() {
				return this;
			}
		}
	}

	@Test
	public void initializationTimingTest() {
		// Given
		final ControllerFactory factory = new TimingControllerFactory("TEST");
		final IDataSource timingDS = new TimingDataSource.Builder()
				                             .addIdentifier("TEST")
				                             .withVariant("TEST")
				                             .withFactory(factory)
				                             .withCacheStatus(false)
				                             .build();

		// - S T E P 0 1 . E M P T Y
		// Test.
		//		final List<IAndroidController> headerContents = timingDS.getHeaderSectionContents();
		//		final List<IAndroidController> dataContents = timingDS.getDataSectionContents();
		// Asserts
		Assert.assertEquals("Empty header contents should not have contents.", 0, timingDS.getHeaderSectionContents().size());
		Assert.assertEquals("Empty data contents should not have contents.", 0, timingDS.getDataSectionContents().size());
		//		Assert.assertEquals("The task executor is empty.", 0, AppCompatibilityUtils.backgroundExecutor.);

		// - S T E P 0 2 . P R E P A R E M O D E L
		// Test.
		DataSourceManager.registerDataSource(timingDS);
		// Asserts.
		Assert.assertEquals("During initialisation the header contents should not have contents.", 0, timingDS.getHeaderSectionContents().size());
		Assert.assertEquals("During initialisation the data contents should not have contents.", 0, timingDS.getDataSectionContents().size());

		// - S T E P 0 3 . C O L L A B O R A T E 2 M O D E L
		// Test.
		AppCompatibilityUtils.backgroundExecutor.submit(() -> {
			timingDS.collaborate2Model();
			// Asserts.
			Assert.assertEquals("Tasks are pending execution until the current task completes.", 0, timingDS.getHeaderSectionContents().size());
			Assert.assertEquals("Tasks are pending execution until the current task completes.", 0, timingDS.getDataSectionContents().size());
		});

		// - S T E P 0 4 . T A S K   C O M P L E T E D
		// Test.
		AppCompatibilityUtils.backgroundExecutor.submit(() -> {
//			timingDS.collaborate2Model();
			// Asserts.
			Assert.assertEquals("Tasks are pending execution until the current task completes.", 0, timingDS.getHeaderSectionContents().size());
			Assert.assertEquals("Tasks are pending execution until the current task completes.", 0, timingDS.getDataSectionContents().size());
		});
	}
}
package org.dimensinfin.eveonline.neocom.features.planetaryinteraction;

import org.dimensinfin.eveonline.neocom.esiswagger.model.GetUniverseTypesTypeIdOk;
import org.dimensinfin.eveonline.neocom.features.universe.adapters.EveItemProvider;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class StructureTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void getStructureType_noinit() {
		// Create a structure but with no item.
		final Structure struc = new Structure.Builder()
				                        .build();
		final PlanetaryStructureType obtained = struc.getStructureType();
		final PlanetaryStructureType expected = PlanetaryStructureType.COMMAND_CENTER;

		// Asserts
		Assert.assertEquals("Planetary type should match to default.", expected, obtained);
	}
	@Test
	public void getStructureType_init() {
		// Given
		final EveItemProvider itemProvider = EveItemProvider.getInstance();
		final EveItemProvider itemProviderock = Mockito.mock(EveItemProvider.class);
		final GetUniverseTypesTypeIdOk defaultType = new GetUniverseTypesTypeIdOk();
		defaultType.setTypeId(2848);
		defaultType.setGroupId(1063);
		defaultType.setName("Barren Command Center");

		// When
//		Mockito.when(EveItemProvider.getInstance()).thenReturn(itemProvider);
		Mockito.when(itemProvider.search(Mockito.any(Integer.class))).thenReturn(defaultType);

		// Create a structure but with no item.
		final Structure struc = new Structure.Builder()
										.withTypeId(2848)
				                        .build();
		final PlanetaryStructureType obtained = struc.getStructureType();
		final PlanetaryStructureType expected = PlanetaryStructureType.EXTRACTOR_CONTROL_UNIT;

		// Asserts
		Assert.assertEquals("Planetary type should match to default.", expected, obtained);
	}

	@Test
	public void getContentsVolume () {

	}
	@Test
	public void getItemDelegate() {
	}

	@Test
	public void getPlanetType() {
	}

	@Test
	public void getCommandCenter() {
	}

	@Test
	public void setCommandCenter() {
	}

	@Test
	public void getGeoPosition() {
	}

	@Test
	public void getIconReferenceId() {
	}

	@Test
	public void getBackgroundColor() {
	}

	@Test
	public void getLastCycleStart() {
	}

	@Test
	public void setLastCycleStart() {
	}

	@Test
	public void getLatitude() {
	}

	@Test
	public void getLongitude() {
	}

	@Test
	public void collaborate2Model() {
	}

	@Test
	public void compareTo() {
	}
}
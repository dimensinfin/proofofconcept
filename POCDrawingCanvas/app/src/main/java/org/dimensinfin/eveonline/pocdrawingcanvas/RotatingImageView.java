package org.dimensinfin.eveonline.pocdrawingcanvas;

import java.util.logging.Logger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class RotatingImageView extends AppCompatImageView {
	protected static Logger logger = Logger.getLogger("RotatingImageView");

	// Initial position.
	private int drawableId;
	private int coordX;
	private int coordY;
	private int color;
	private float scale = 1.0F;
	private Bitmap sourceBitmap;

	private int rotationDegrees = 0;
	private int directionScale;

	public RotatingImageView(Context context) {
		super(context);
		//		init();
	}

	public RotatingImageView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		//		init();
	}

	public RotatingImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		//		init();
	}

	public RotatingImageView build() {
		sourceBitmap = BitmapFactory.decodeResource(getResources(), drawableId);
		setImageBitmap(sourceBitmap);
		return this;
	}


	protected void onDraw(Canvas canvas) {
		// Change the color
		final Bitmap mFinalBitmap =  Util.changeImageColor(sourceBitmap, color);
		setImageBitmap(mFinalBitmap);
		// Move to position
		canvas.translate((canvas.getWidth() / 2) - 40, (canvas.getHeight() / 2) - 40);
		canvas.translate(coordX, coordY);
		// Scale the image
		canvas.scale(scale, scale);

		postInvalidateOnAnimation();
		super.onDraw(canvas);
	}

	protected void onDrawOld(Canvas canvas) {
		// Rotate the image
		// Translate rotation axe to the center.
		//		canvas.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);
		//		// Rotate!
		//		canvas.rotate(rotation(3));
		//		canvas.translate(-canvas.getWidth() / 3, -canvas.getHeight() / 3);

		// Change the color
		final Bitmap mFinalBitmap =  Util.changeImageColor(sourceBitmap, color);
		setImageBitmap(mFinalBitmap);

		canvas.translate(canvas.getWidth() / 2, canvas.getHeight() / 2);
		//	canvas.translate(canvas.getWidth() / 2 + coordX, canvas.getHeight() / 2 + coordY);
		//		canvas.translate(coordX, coordY);

		// Scale the image
		//				float scaleFactor = scale(0.5F);
		//				scaleFactor = 0.1F;
		canvas.scale(scale, scale);


		//		// Change background color
		//		final Drawable image = getResources().getDrawable(drawableId, getContext().getTheme());
		//		image.mutate().setColorFilter(getResources().getColor(R.color.appblack, getContext().getTheme()), PorterDuff.Mode.SRC_IN);
		//
		//		mFinalBitmap.setColorFilter(getResources().getColor(R.color.appblack, getContext().getTheme()), PorterDuff.Mode.SRC_IN);


		// Invalidate the view.
		postInvalidateOnAnimation();
		//	super.onDraw(canvas);
	}

	private float scale(float delta) {
		scale = (scale + delta * directionScale);
		if (scale <= 0) {
			directionScale = 1;
			scale = 0;
		} else if (scale >= 1) {
			directionScale = -1;
			scale = 1;
		}
		return scale;
	}

	private int rotation(int delta) {
		rotationDegrees = (rotationDegrees + delta) % 360;
		return rotationDegrees;
	}

	public Bitmap convertToBitmap(Drawable drawable, int widthPixels, int heightPixels) {
		Bitmap mutableBitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(mutableBitmap);
		drawable.setBounds(0, 0, widthPixels, heightPixels);
		drawable.draw(canvas);

		return mutableBitmap;
	}

	public RotatingImageView setDrawable(int resourceid) {
		drawableId = resourceid;
		return this;
	}

	public RotatingImageView setLocation(final int cx, final int cy) {
		coordX = cx;
		coordY = cy;
		return this;
	}

	public RotatingImageView setColor(final int colorcode) {
		color = colorcode;
		return this;
	}

	public RotatingImageView setScale(final float newscale) {
		scale = newscale;
		return this;
	}
}

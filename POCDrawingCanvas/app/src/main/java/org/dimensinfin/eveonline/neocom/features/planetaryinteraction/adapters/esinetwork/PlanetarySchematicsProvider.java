package org.dimensinfin.eveonline.neocom.features.planetaryinteraction.adapters.esinetwork;

import org.dimensinfin.eveonline.neocom.POCDrawingCanvasApplication;
import org.dimensinfin.eveonline.neocom.components.ESINetworkManagerMock;
import org.dimensinfin.eveonline.neocom.esiswagger.model.GetUniverseSchematicsSchematicIdOk;

public class PlanetarySchematicsProvider {
	private static PlanetarySchematicsProvider singleton = new PlanetarySchematicsProvider();

	public static PlanetarySchematicsProvider getInstance() {
		return singleton;
	}

	private PlanetarySchematicsProvider() {
	}

	public GetUniverseSchematicsSchematicIdOk search(final int schematicId) {
		return ESINetworkManagerMock.getUniversePlanetarySchematicsById("tranquility"
				, schematicId);
	}
}

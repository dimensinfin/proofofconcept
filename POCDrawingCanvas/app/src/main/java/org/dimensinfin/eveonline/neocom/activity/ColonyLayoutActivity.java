package org.dimensinfin.eveonline.neocom.activity;

import android.os.Bundle;

import org.dimensinfin.android.mvc.activity.MVCMultiPageActivity;
import org.dimensinfin.android.mvc.activity.IPagerFragment;

/**
 * This Proof Of Concept project will show the Planetary Colony detailed structure inside a two component view using the standard MVC
 * developed for the NeoCom projects. It will only show a single colony for a predefined character so all that identification and parametrisation
 * should be declared as constants input to the Activity in the form of Extras, that is the real way the Activity would receive such
 * data in a final application implementation.
 *
 * Then we should define the character unique <b>account identifier</b> to be used to locate the Credential authorization structure, we should also
 * set the target ESI server to use on the requests but probably that definition should be something set on configuration and not
 * selectable to the application interface and finally we should select one of the planetary colonies as if that was the planet
 * selected by the user on the UI.
 *
 * @author Adam Antinoo on 05/01/2018.
 */
public class ColonyLayoutActivity extends MVCMultiPageActivity {
	public static final String CHARACTER_ACCOUNT_IDENTIFIER = "CHARACTER_ACCOUNT_IDENTIFIER";
	public static final String ESI_TARGET = "ESI_TARGET";
	public static final String COLONY_IDENTIFIER = "COLONY_IDENTIFIER";

	/** During the Activity creation just create the pages that should compose this feature UI pages. */
	public void onCreate(final Bundle savedInstanceState) {
		logger.info(">> [ColonyLayoutActivity.onCreate]");
		super.onCreate(savedInstanceState);

		// Load into the extras the parameters as if this intent was processed from another UI page.
		final Bundle extras = this.getExtras();
		extras.putLong(CHARACTER_ACCOUNT_IDENTIFIER, 92002067);
		extras.putString(ESI_TARGET, "tranquility");
		extras.putLong(COLONY_IDENTIFIER, 40237774);

		final IPagerFragment fragment = new ColonyLayoutFragment();
		this.addPage(fragment);
		logger.info("<< [ColonyLayoutActivity.onCreate]");
	}
}

package org.dimensinfin.eveonline.neocom.features.planetaryinteraction.controllers;

import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.eveonline.neocom.features.planetaryinteraction.Structure;
import org.dimensinfin.eveonline.neocom.features.planetaryinteraction.StructureGroup;

public class PlanetaryInteractionControllerFactory extends ControllerFactory {
	public PlanetaryInteractionControllerFactory( final String selectedVariant ) {
		super(selectedVariant);
	}

	@Override
	public IAndroidController createController( final ICollaboration node ) {
		if (node instanceof StructureGroup) {
			return new StructureGroupController((StructureGroup) node, this);
		}
//		if (node instanceof ColonyMap) {
//			return new ColonyMapController((ColonyMap) node, this);
//		}
		if (node instanceof Structure) {
			//			final PlanetaryStructureType type = ((Structure) node).getStructureType();
			//			if (type == PlanetaryStructureType.BASIC_INDUSTRY)
			return new BasicIndustryStructureController((Structure) node, this);
			//			else
			//				return new BasicIndustryStructureController((Structure) node, this);
		}
		return super.createController(node);
	}
}

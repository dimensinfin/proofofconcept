package org.dimensinfin.eveonline.neocom.factory;

import android.app.Application;
import android.content.Context;

import org.dimensinfin.eveonline.neocom.conf.IPreferencesProvider;
import org.dimensinfin.eveonline.neocom.components.ESINetworkManagerMock;
import org.dimensinfin.eveonline.neocom.features.credential.adapters.persistence.CredentialRepository;
import org.dimensinfin.eveonline.neocom.interfaces.IConfigurationProvider;
import org.dimensinfin.eveonline.neocom.interfaces.IFileSystem;

public class POCAndroidComponentFactory {
	// -  S I N G L E T O N
	private static POCAndroidComponentFactory singleton = new POCAndroidComponentFactory();

	public static POCAndroidComponentFactory getInstance() {
		return singleton;
	}

	public static void linkSingleton( final POCAndroidComponentFactory single ) {
		singleton = single;
	}

	public static void clean() {
		singleton = null;
	}

	public static <T> T assertNotNull( T target ) {
		if (null == target) {
			throw new InvalidComponentException("Component Factory not initialised or some of the components not already linked.");
		} else {
			return target;
		}
	}

	// - C O N T R U C T O R S
	private POCAndroidComponentFactory() {
	}

	public POCAndroidComponentFactory( final POCAndroidComponentFactory mockPOCAndroidComponentFactory ) {
		linkSingleton(mockPOCAndroidComponentFactory);
	}

	// - C O M P O N E N T S
	private Application appSingleton;
	private Context appContext;
	private CredentialRepository credentialRepository;
	private IPreferencesProvider preferencesProvider;
	private IFileSystem fileSystem;
	private IConfigurationProvider configurationProvider;
	private ESINetworkManagerMock esiNetwork;
	// - C H A N G E A B L E   C O M P O N E N T S

	public CredentialRepository getCredentialRepository() {
		return credentialRepository;
	}

	// - B U I L D E R
	public static class Builder {
		private final POCAndroidComponentFactory object;

		public Builder() {
			this.object = new POCAndroidComponentFactory();
		}

		public Builder linkAppSingleton( final Application appSingleton ) {
			this.object.appSingleton = appSingleton;
			return this;
		}

		public Builder linkAppContext( final Context appContext ) {
			this.object.appContext = appContext;
			return this;
		}

		public Builder linkCredentialRepository( final CredentialRepository credentialRepository ) {
			this.object.credentialRepository = credentialRepository;
			return this;
		}

		public Builder linkPreferencesManager( final IPreferencesProvider preferencesManager ) {
			this.object.preferencesProvider = preferencesManager;
			return this;
		}

		public Builder linkFileSystem( final IFileSystem fileSystem ) {
			this.object.fileSystem = fileSystem;
			return this;
		}

		public Builder linkConfigurationProvider( final IConfigurationProvider configurationProvider ) {
			this.object.configurationProvider = configurationProvider;
			return this;
		}

		public Builder linkESINetwork( final ESINetworkManagerMock esiNetwork ) {
			this.object.esiNetwork = esiNetwork;
			return this;
		}

		public POCAndroidComponentFactory build() {
			// Before returning the singleton be sure all components are linked.
			assertNotNull(this.object.appSingleton);
			assertNotNull(this.object.appContext);
			assertNotNull(this.object.credentialRepository);
			assertNotNull(this.object.preferencesProvider);
			assertNotNull(this.object.fileSystem);
			assertNotNull(this.object.configurationProvider);
//			assertNotNull(this.object.esiNetwork);
			linkSingleton(this.object);
			return POCAndroidComponentFactory.getInstance();
		}
	}
}

final class InvalidComponentException extends RuntimeException {

	public InvalidComponentException( final String message ) {
		super(message);
	}
}

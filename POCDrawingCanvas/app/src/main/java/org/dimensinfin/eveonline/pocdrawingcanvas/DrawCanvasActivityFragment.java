package org.dimensinfin.eveonline.pocdrawingcanvas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

import org.dimensinfin.eveonline.neocom.R;
import org.dimensinfin.eveonline.neocom.features.planetaryinteraction.ui.FacilityImageView;

import java.util.List;
import java.util.Vector;
import java.util.logging.Logger;

import androidx.fragment.app.Fragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class DrawCanvasActivityFragment extends Fragment {
	protected static Logger logger = Logger.getLogger("DrawCanvasActivityFragment");

	// - U I    F I E L D S
	private ViewGroup _container = null;
	private ImageView _imagecanvas = null;
	private List<FrameLayout> structures = new Vector();

	public DrawCanvasActivityFragment () {
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
	                          Bundle savedInstanceState) {
		logger.info(">> [DrawCanvasActivityFragment.onCreateView]");
		super.onCreateView(inflater, container, savedInstanceState);
		try {
			_container = (ViewGroup) inflater.inflate(R.layout.fragment_colonylayout, container, false);
			//			_imagecanvas = (ImageView) _container.findViewById(R.id.imageCanvas);
		} catch (final RuntimeException rtex) {
			logger.severe("RTEX [MVCPagerFragment.onCreateViewSuper]> " + rtex.getMessage());
			rtex.printStackTrace();
		}
		logger.info("<< [DrawCanvasActivityFragment.onCreateView]");
		return _container;
	}

	@Override
	public void onStart () {
		logger.info(">> [DrawCanvasActivityFragment.onStart]");
		super.onStart();

		double center_longitude;
		double center_latitude;
		double longitude;
		double latitude;
		double posx;
		double posy;
		FrameLayout frame;
		LayoutParams layoutParams;
		ColonyStructureImageView structureImage;
		int conversionLongitudeMultiplier = 7000;
		int conversionLatitudeMultiplier = 9000;

		drawColony();

		//		// Command center location.
		//		center_longitude = -4.74107389672607;
		//		center_latitude = 0.808934460793102;
		//
		//		// Special test
		//		longitude=-4.71122230433;
		//		latitude=0.809178342394;
		//		longitude = longitude - center_longitude;
		//		latitude = latitude - center_latitude;
		//		posx = longitude * CONVERSION_LONGITUDE_MULTIPLIER;
		//		posy = latitude * CONVERSION_LATITUDE_MULTIPLIER;
		//		frame = new FrameLayout(this.getContext());
		//		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		//		structureImage = new ColonyStructureImageView(this.getActivity())
		//				.setDrawable(R.drawable.commandcenter80_white)
		//				.setColor(0xE79434)
		//				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
		//				.setScale(0.1F)
		//				.build();
		//		frame.addView(structureImage);
		//		structures.add(frame);
		//		_container.addView(frame);

		logger.info("<< [DrawCanvasActivityFragment.onStart]");
	}

	private void drawColony () {
		double center_longitude;
		double center_latitude;
		double longitude;
		double latitude;
		double posx;
		double posy;
		FrameLayout frame;
		LayoutParams layoutParams;
		RotatingImageView structureImage;
		FacilityImageView facilityImage;
		int conversionLongitudeMultiplier = 10500 / 9 * 7;
		int conversionLatitudeMultiplier = 10500;

		// Command center location.
		center_longitude = -4.74107389672607;
		center_latitude = 0.808934460793102;

		// Draw the command center
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//		facilityImage = new FacilityImageView(this.getActivity())
//				.setDrawable(R.drawable.commandcenter80_white)
//		//		.setColor(0xFFFFFF)
//				.setLocation(0, 0);
////				.setScale(0.1F)
////				.build();
//		frame.addView(facilityImage);
//		structures.add(frame);
//		_container.addView(frame);

		// Storage Facility
		longitude = -4.74084864085;
		latitude = 0.796796216569;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.storage80_white)
				.setColor(0x004764)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
//		structures.add(frame);
		_container.addView(frame);

		// Launchpad
		longitude = -4.74050339883;
		latitude = 0.820994655635;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.launchpad80_white)
				.setColor(0x00A0D7)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Extractor
		longitude = -4.74103527972;
		latitude = 0.784725840572;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.extractor80_white)
				.setColor(0x00A0D7)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Basic Industry
		longitude = -4.75563428247;
		latitude = 0.802738796915;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.facility80_white)
				.setColor(0xE79434)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Basic Industry
		longitude = -4.755343046;
		latitude = 0.81560809073;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.facility80_white)
				.setColor(0xE79434)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Basic Industry
		longitude = -4.72601355333;
		latitude = 0.803082801349;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.facility80_white)
				.setColor(0xE79434)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Basic Industry
		longitude = -4.7256722388;
		latitude = 0.815408799205;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		structureImage = new  RotatingImageView(this.getActivity())
				.setDrawable(R.drawable.facility80_white)
				.setColor(0xE79434)
				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue())
				.setScale(0.1F)
				.build();
		frame.addView(structureImage);
		structures.add(frame);
		_container.addView(frame);

		// Advanced Industry
		longitude = -4.71122230433;
		latitude = 0.809178342394;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//		facilityImage = new FacilityImageView(this.getActivity())
//				.setDrawable(R.drawable.storage80_white)
//				.setColor(0xE79434)
//				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue());
////				.setScale(0.1F)
////				.build();
//		frame.addView(facilityImage);
//		structures.add(frame);
//		_container.addView(frame);

		// Advanced Industry
		longitude = -4.77027205957;
		latitude = 0.809148885737;
		longitude = longitude - center_longitude;
		latitude = latitude - center_latitude;
		posx = longitude * conversionLongitudeMultiplier;
		posy = latitude * conversionLatitudeMultiplier;
		frame = new FrameLayout(this.getContext());
		layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//		 facilityImage = new FacilityImageView(this.getActivity())
//				.setColor(0xE79434)
//				.setLocation(Long.valueOf(Math.round(posx)).intValue(), Long.valueOf(Math.round(posy)).intValue());
//		//				.setScale(0.1F)
//		//				.build();
//		frame.addView(facilityImage);
//		structures.add(frame);
//		_container.addView(frame);

	}
}

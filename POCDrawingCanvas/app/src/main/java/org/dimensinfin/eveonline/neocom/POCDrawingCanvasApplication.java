package org.dimensinfin.eveonline.neocom;

import android.app.Application;

import org.dimensinfin.eveonline.neocom.conf.GlobalAndroidConfigurationProvider;
import org.dimensinfin.eveonline.neocom.conf.GlobalAndroidPreferencesProvider;
import org.dimensinfin.eveonline.neocom.datamngmt.AndroidGlobalDataManager;
import org.dimensinfin.eveonline.neocom.datamngmt.FileSystemAndroidImplementation;
import org.dimensinfin.eveonline.neocom.factory.POCAndroidComponentFactory;
import org.dimensinfin.eveonline.neocom.features.credential.adapters.persistence.CredentialRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class POCDrawingCanvasApplication extends Application {
	private static Logger logger = LoggerFactory.getLogger(POCDrawingCanvasApplication.class);

	// - A P P L I C A T I O N   L I F E C Y C L E
	@Override
	public void onCreate() {
		super.onCreate();
		// Create the component and singleton factory.
		logger.info("-- [NeoComApp.onCreate]> Creating Component Factory and loading singletons...");
		logger.info("-- [NeoComApp.onCreate]> Connecting the Context to the Global...");
		AndroidGlobalDataManager.setApplicationContext(this);
		AndroidGlobalDataManager.installFileSystem(new FileSystemAndroidImplementation(
				AndroidGlobalDataManager.getApplication().getResources().getString(R.string.appname)
		));
		AndroidGlobalDataManager.connectConfigurationManager(new GlobalAndroidConfigurationProvider("properties"));
		AndroidGlobalDataManager.connectPreferencesManager(new GlobalAndroidPreferencesProvider());

		// Initializing the ESI api network controller.
		//		ESINetworkManagerMock.initialize();
		new POCAndroidComponentFactory.Builder()
				.linkAppSingleton(this)
				.linkAppContext(this.getApplicationContext())
				.linkCredentialRepository(new CredentialRepository(this.getApplicationContext()))
				.linkFileSystem(new FileSystemAndroidImplementation(
						this.getApplicationContext().getResources().getString(R.string.appname)
				))
				.linkPreferencesManager(new GlobalAndroidPreferencesProvider())
				.linkConfigurationProvider(new GlobalAndroidConfigurationProvider("properties"))
				//				.linkESINetwork(ESINetworkManagerMock.ii)
				.build();
	}
}

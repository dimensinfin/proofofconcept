package org.dimensinfin.eveonline.neocom.model;

import java.util.List;
import java.util.Vector;

import org.dimensinfin.core.interfaces.ICollaboration;

public class Pin implements ICollaboration {
	public enum EPlanetaryStructureType {
		COMMAND_CENTER, STORAGE, LAUNCHPAD, EXTRACTOR, BASIC_INDUSTRY, ADVANCED_INDUSTRY, DEFAULT
	}

	public static class PinContent {

		private long typeId;
		private String typeName;
		private double amount;

		public long getTypeId() {
			return typeId;
		}

		public String getTypeName() {
			return typeName;
		}

		public double getAmount() {
			return amount;
		}

		public void setTypeName(final String typeName) {
			this.typeName = typeName;
		}

		public void setTypeId(final long typeId) {
			this.typeId = typeId;
		}

		public void setAmount(final double amount) {
			this.amount = amount;
		}
	}

	public EPlanetaryStructureType getStructureTypeCode() {
		if (getTypeId() == 2524) return EPlanetaryStructureType.COMMAND_CENTER;
		if (getTypeId() == 2525) return EPlanetaryStructureType.COMMAND_CENTER;
		if (getTypeId() == 2526) return EPlanetaryStructureType.COMMAND_CENTER;
		if (getTypeId() == 2541) return EPlanetaryStructureType.STORAGE;
		if (getTypeId() == 2544) return EPlanetaryStructureType.LAUNCHPAD;
		if (getTypeId() == 2848) return EPlanetaryStructureType.EXTRACTOR;
		if (getTypeId() == 2473) return EPlanetaryStructureType.BASIC_INDUSTRY;
		if (getTypeId() == 2474) return EPlanetaryStructureType.ADVANCED_INDUSTRY;
		return EPlanetaryStructureType.DEFAULT;
	}

	private long id;
	private long typeId;

	private double longitude;
	private double latitude;

	private long schematicsId;
//	private String schematicsName;

	private long expiryTime;
	private long installTime;
	private long lastCycleStart;

	private List<PinContent> content = new Vector();
	//	private PinExtractor extractor;

	public List<ICollaboration> collaborate2Model(final String s) {
		List<ICollaboration> results = new Vector();
		return results;
	}

	public long getId() {
		return id;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public long getTypeId() {
		return typeId;
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public List<PinContent> getContent() {
		return content;
	}

	public long getLastCycleStart() {
		return lastCycleStart;
	}

	public void setLastCycleStart(final long lastCycleStart) {
		this.lastCycleStart = lastCycleStart;
	}

	public void setLongitude(final double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(final double latitude) {
		this.latitude = latitude;
	}

	public void setTypeId(final long typeId) {
		this.typeId = typeId;
	}

	public void setSchematicsId(final long schematicsId) {
		this.schematicsId = schematicsId;
	}

	public void addContent(final PinContent content) {
		this.content.add(content);
	}
//	@Override
	public int compareTo(final Object o) {
		return 0;
	}

}

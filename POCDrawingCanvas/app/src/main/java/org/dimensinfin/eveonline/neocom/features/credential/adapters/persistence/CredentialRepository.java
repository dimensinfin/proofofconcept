package org.dimensinfin.eveonline.neocom.features.credential.adapters.persistence;

import android.content.Context;

import org.dimensinfin.eveonline.neocom.POCDrawingCanvasApplication;
import org.dimensinfin.eveonline.neocom.datamngmt.ESINetworkManager;
import org.dimensinfin.eveonline.neocom.features.credential.CredentialMock;
//import org.dimensinfin.eveonline.neocom.persistence.PersistenceHelper;

public class CredentialRepository {
	private static final String DATABASE_NAME = "POCDrawingCanvas";
	private static final int DATABASE_VERSION = 100;
//	private static PersistenceHelper persistenceHelper;
	//	private Dao<Credential, String> credentialDao = null;

	public CredentialRepository(final Context context) {
//		persistenceHelper = new PersistenceHelper(context, DATABASE_NAME, DATABASE_VERSION);
	}

	public CredentialMock findById(final String uniqueId) {
		CredentialMock cred = new CredentialMock(92002067,  "tranquility");
		cred.setAccessToken("D8KEMVvY2zcbRXMh6B7ldShWWM2rnoqMomH-PbPegPZH00vfC9yeMXMWo-Nl94LBDQwcl76LOgoDdl2F3qQfZA2")
				.setRefreshToken("veGsthIl6AWifDZHEjqmZFBrxwu0ZGOEPgLtfrAnzKJxrbD9HEAploBalAS40AgJeM5siWM1oEQu-At790COHTSpSzITYLJoN_BFpe337bGEU3r9HPDFtka5_rKRvFlG1kF1GawXQOgL0pZ6lxC6CsDEscUGLwSaxe1KG9cJbWK1KDC024fyoTmYZeVyUZMnNV3AX064E4eak7jOfPiijEPc2jKMefI0zJwZl_g3nhE1pVzJA_Cexlb-YEnh1zl0xsiLhPCfjdLs3blFoX-Y1IeVLV9iordxd_llrc54z2-Rvz2R8atpM1tN2NI7GIuKX21HEp2fVP6m4TRy854oFz1Bw1StaLzS2XprLEzPjIc1gHTlaC5GysdQROxY57VHyyEQqCtxsBDzbJZ3k4WaaO-QuWEE-by3V_N_I0vX6LdxFwfhw15eSUbHE2Zm2uOvgdJrwotks7kXmKWL9erOGxcBK1Q7W-ckpRgYCY3yClGG9ptEq9fTEhvDupcgbaff70yTtYX_VAqysbD35KKdpFpnVu76EuVkrdyqWD1CAJbZgUhbZe_CYk5lbBQfjziQ6GYRSdt_6AVjrxN08_oTqbXdzH8f5Vtn6BRUKr0DtVw1")
				.setDataSource("tranquility")
				.setScope(ESINetworkManager.getStringScopes())
				.setAccountName("Adam Antinoo");
		return cred;
	}
}

package org.dimensinfin.eveonline.neocom.features.planetaryinteraction;

import org.dimensinfin.eveonline.neocom.R;

import com.google.gson.annotations.SerializedName;

public enum PlanetType {
	BARREN( R.color.pi_barren_backgroundcolor ),
	TEMPERATE( R.color.pi_barren_backgroundcolor ),
	OCEANIC( R.color.pi_barren_backgroundcolor ),
	ICE( R.color.pi_barren_backgroundcolor ),
	GAS( R.color.pi_barren_backgroundcolor ),
	LAVA( R.color.pi_barren_backgroundcolor ),
	STORM( R.color.pi_barren_backgroundcolor ),
	PLASMA( R.color.pi_barren_backgroundcolor );

	private final int backgroundColor;

	private PlanetType( final int backgroundColor ) {
		this.backgroundColor = backgroundColor;
	}

	public int getBackgroundColor() {
		return this.backgroundColor;
	}
}

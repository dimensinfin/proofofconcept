package org.dimensinfin.eveonline.pocdrawingcanvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.AttributeSet;

import org.dimensinfin.eveonline.neocom.R;

import java.util.logging.Logger;

import androidx.appcompat.widget.AppCompatImageView;

public class Image2 extends AppCompatImageView {
	protected static Logger logger = Logger.getLogger("Image2");

	private static final int widthPixels = 64;
	private static final int heightPixels = 64;
	private static final int barren_backgroundcolor=0xFF392514;
	private static final int facility_fillcolor=0xFFE79434;
	private static final int facility_emptycolor=0xFFA15929;
	private static final int storage_fillcolor=0xFF019FE5;
	private static final int storage_emptycolor=0xFF0C3F5F;
	private static final int extractor_cyclecolor=0xFF039392;
	private static final int cycle_advancecolor=Color.WHITE;


	private Bitmap bitmap;

	// Initial position.
	private int color=Color.WHITE;

	private int drawableId;
	private int coordX;
	private int coordY;
	private Bitmap sourceBitmap;

	private int rotationDegrees = 0;
	private float scale = 1.0F;
	private int directionScale;

	public Image2 (Context context) {
		super(context);
		setImageBitmap(createBitmap());
	}

	public Image2 (Context context,  AttributeSet attrs) {
		super(context, attrs);
		setImageBitmap(createBitmap());
	}

	public Image2 (Context context,  AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setImageBitmap(createBitmap());
	}

	public Image2 build () {
		sourceBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.extractor80_white);
		setImageBitmap(sourceBitmap);
		return this;
	}

	public Image2 setDrawable (int resourceid) {
		drawableId = resourceid;
		return this;
	}
	public Image2 setColor (final int colorcode) {
		color = colorcode;
		return this;
	}

	protected void onDraw (Canvas canvas) {
		logger.info(">> [Image2.onDraw]");
		//		Canvas work = new Canvas(createBitmap());
		canvas.translate((canvas.getWidth() / 2), (canvas.getHeight() / 2));
		canvas.drawColor(0x00000000);
		Paint painter = new Paint(Paint.ANTI_ALIAS_FLAG);
		painter.setColor(facility_fillcolor);
		canvas.drawCircle(0.0f, 0.0f, widthPixels / 2, painter);
		painter.setColor(barren_backgroundcolor);
		painter.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
		canvas.drawCircle(0.0f, 0.0f, (widthPixels / 2) - 15, painter);
		canvas.drawRect(-12, -heightPixels / 2, 12, heightPixels / 2, painter);

		painter.setColor(Color.WHITE);
		painter.setXfermode(new PorterDuffXfermode(Mode.OVERLAY));
		canvas.drawArc(-heightPixels / 2.0f + 30, -heightPixels / 2.0f + 30, heightPixels / 2.0f - 30, heightPixels / 2.0f - 30, -90.0f, 210.0f, true, painter);
		painter.setColor(0xFF392514);
		painter.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
		canvas.drawCircle(0.0f, 0.0f, (widthPixels / 2) - 40, painter);

		painter.setXfermode(new PorterDuffXfermode(Mode.OVERLAY));
		sourceBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.extractor80_white);

		Rect source = new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight());
		int dimension = Float.valueOf(heightPixels / 2.0f).intValue();
		Rect destination = new Rect(-dimension + 50, -dimension + 50, dimension - 50, dimension - 50);
		canvas.drawBitmap(sourceBitmap, source, destination, painter);
		canvas.scale(scale/2, scale/2);
		super.onDraw(canvas);
		logger.info("<< [Image2.onDraw]");
	}

	private Bitmap createBitmap () {
		bitmap = Bitmap.createBitmap(widthPixels, heightPixels, Bitmap.Config.ARGB_8888);
		return bitmap;
	}
}

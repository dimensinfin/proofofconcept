package org.dimensinfin.eveonline.neocom.features.planetaryinteraction.ui;

import org.dimensinfin.eveonline.neocom.features.planetaryinteraction.Structure;
import org.dimensinfin.eveonline.neocom.features.planetaryinteraction.StructureGeoPosition;

public interface IPlanetaryImageView {
	void setDeltaPosition( final StructureGeoPosition deltaPosition );

	void setStructure( Structure structure );
}

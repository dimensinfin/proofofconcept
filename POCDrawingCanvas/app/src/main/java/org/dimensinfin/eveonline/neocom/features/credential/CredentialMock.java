package org.dimensinfin.eveonline.neocom.features.credential;

import org.dimensinfin.eveonline.neocom.entities.Credential;

public class CredentialMock extends Credential {
	public CredentialMock(final int newAccountIdentifier, final String dataSource) {
		this.accountId = newAccountIdentifier;
		this.dataSource = dataSource;
		this.uniqueCredential = Credential.createUniqueIdentifier(this.dataSource, this.accountId);
	}

	public CredentialMock store() {
		return this;
	}
}

package org.dimensinfin.eveonline.neocom.features.universe.adapters;

import org.dimensinfin.eveonline.neocom.POCDrawingCanvasApplication;
import org.dimensinfin.eveonline.neocom.components.ESINetworkManagerMock;
import org.dimensinfin.eveonline.neocom.esiswagger.model.GetUniverseTypesTypeIdOk;

public class EveItemProvider {
	private static EveItemProvider ourInstance = new EveItemProvider();

	public static EveItemProvider getInstance() {
		return ourInstance;
	}

	private EveItemProvider() {
	}

	public GetUniverseTypesTypeIdOk search(final Integer typeId) {
		return ESINetworkManagerMock.getUniverseTypeById("tranquility"
				, typeId);
	}
}

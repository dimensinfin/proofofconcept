package org.dimensinfin.eveonline.pocdrawingcanvas;

import java.util.logging.Logger;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

public class ColonyStructureImageView extends AppCompatImageView {
	protected static Logger logger = Logger.getLogger("RotatingImageView");

	// Initial position.
	private int drawableId;
	private int coordX;
	private int coordY;
	private int color;
	private float scale = 1.0F;
	private Bitmap sourceBitmap;

	private static final int STROKE_WIDTH_DP = 6;
	private Paint paintBorder;


	private int strokeWidthPx;
	private RectF rectF;
	private RadialGradient radialGradient;

	public ColonyStructureImageView(Context context) {
		super(context);
	}

	public ColonyStructureImageView(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	public ColonyStructureImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public ColonyStructureImageView build() {
		sourceBitmap = BitmapFactory.decodeResource(getResources(), drawableId);
		setImageBitmap(sourceBitmap);
		return this;
	}

	protected void onDraw(Canvas canvas) {


		// Change the color
		final Bitmap mFinalBitmap = Util.changeImageColor(sourceBitmap, color);
		setImageBitmap(mFinalBitmap);
		// Move to position
		canvas.translate((canvas.getWidth() / 2) - 40, (canvas.getHeight() / 2) - 40);


		// make the bitmap bigger
		int totalWidth = canvas.getWidth() / 2;
		int totalHeight = canvas.getHeight() / 2;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, totalWidth, totalHeight);
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.parseColor("#E79434"));
		canvas.drawCircle(totalWidth / 2 + 0.7f, totalHeight / 2 + 0.7f, totalWidth / 2 + 0.1f, paint);


		canvas.translate(coordX, coordY);
		// Scale the image
		canvas.scale(scale / 2, scale / 2);

		//		postInvalidateOnAnimation();
		//		super.onDraw(canvas);


		//		canvas.drawRoundRect(rectF, 40, 40, paintBorder);
		//		canvas.drawBitmap(sourceBitmap,strokeWidthPx, strokeWidthPx, null);
		postInvalidateOnAnimation();
		super.onDraw(canvas);
	}

	public ColonyStructureImageView setDrawable(int resourceid) {
		drawableId = resourceid;
		return this;
	}

	public ColonyStructureImageView setLocation(final int cx, final int cy) {
		coordX = cx;
		coordY = cy;
		return this;
	}

	public ColonyStructureImageView setColor(final int colorcode) {
		color = colorcode;
		return this;
	}

	public ColonyStructureImageView setScale(final float newscale) {
		scale = newscale;
		return this;
	}
}

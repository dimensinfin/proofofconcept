#if (${PACKAGE_NAME} && ${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end
#parse("File Header.java")
public class ${NAME}{
    private static ${NAME} singleton = new ${NAME}();

    public static ${NAME} getInstance() {
        return singleton;
    }
    public static void linkSingleton( final ${NAME} single ) {
		singleton = single;
	}
    public static void clean(  ) {
		singleton = null;
	}
    private ${NAME}() {
    }
    public ${NAME}( final ${NAME} mock${NAME} ) {
		linkSingleton(mock${NAME});
	}
}

package org.dimensinfin.eveonline.neocom.datamngmt;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.dimensinfin.eveonline.neocom.conf.IGlobalPreferencesManager;
import org.dimensinfin.eveonline.neocom.exception.NeoComRuntimeException;
import org.dimensinfin.eveonline.neocom.interfaces.IConfigurationProvider;
import org.dimensinfin.eveonline.neocom.interfaces.IFileSystem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalDataManagerMock extends GlobalDataManager {
	private static Logger logger = LoggerFactory.getLogger(GlobalDataManagerMock.class);

	// - P U B L I C   E N U M E R A T O R S
	public enum EDataUpdateJobs {
		READY, CHARACTER_CORE, CHARACTER_FULL, ASSETDATA, BLUEPRINTDATA, INDUSTRYJOBS, MARKETORDERS, MININGEXTRACTIONS, COLONYDATA, SKILL_DATA
	}

	// - P R I V A T E   E N U M E R A T O R S
	private enum EManagerCodes {
		PLANETARY_MANAGER, ASSETS_MANAGER
	}

	// - P R I M A R Y    K E Y   C O N S T R U C T O R S
	public static String constructModelStoreReference(final GlobalDataManager.EDataUpdateJobs type, final long
			                                                                                                identifier) {
		return new StringBuffer("TS/")
				       .append(type.name())
				       .append("/")
				       .append(identifier)
				       .toString();
	}

	public static String constructJobReference(final GlobalDataManagerConfiguration.EDataUpdateJobs type, final long identifier) {
		return new StringBuffer("JOB:")
				       .append(type.name())
				       .append("/")
				       .append(identifier)
				       .toString();
	}

	public static String constructPlanetStorageIdentifier(final int characterIdentifier, final int planetIdentifier) {
		return new StringBuffer("CS:")
				       .append(Integer.valueOf(characterIdentifier).toString())
				       .append(":")
				       .append(Integer.valueOf(planetIdentifier).toString())
				       .toString();
	}

	// - F I L E   S Y S T E M   S E C T I O N
	private static IFileSystem fileSystemIsolation = null;

	public static void installFileSystem(final IFileSystem newfileSystem) {
		fileSystemIsolation = newfileSystem;
	}

	protected static IFileSystem getFileSystem() {
		if (null != fileSystemIsolation) return fileSystemIsolation;
		else throw new NeoComRuntimeException("File System isolation layer is not installed.");
	}

	public static InputStream openResource4Input(final String filePath) throws IOException {
		return getFileSystem().openResource4Input(filePath);
	}

	public static OutputStream openResource4Output(final String filePath) throws IOException {
		return getFileSystem().openResource4Output(filePath);
	}

	public static InputStream openAsset4Input(final String filePath) throws IOException {
		return getFileSystem().openAsset4Input(filePath);
	}

	public static String accessAsset4Path(final String filePath) throws IOException {
		return getFileSystem().accessAsset4Path(filePath);
	}

	public static String accessResource4Path(final String filePath) {
		return getFileSystem().accessResource4Path(filePath);
	}

	public static String accessAppStorage4Path(final String filePath) {
		return getFileSystem().accessResource4Path(filePath);
	}

	public static boolean checkAssetFile(final String resourceString) {
		try {
			final InputStream toCheck = GlobalDataManager.openAsset4Input(resourceString);
			return true;
		} catch (IOException ioe) {
			return false;
		}
	}

	public static boolean checkStorageResource(final String fileName) {
		File toCheck = new File(fileName);
		return toCheck.exists();
	}

	// - C O N F I G U R A T I O N   S E C T I O N
	public static String TRANQUILITY_DATASOURCE = "tranquility";

	private static IConfigurationProvider configurationManager = null/*new GlobalConfigurationProvider(null)*/;

	private static IConfigurationProvider accessConfigurationManager() {
		// If the Configuration is not already loaded then connect a default configuration provider.
		if (null == configurationManager)
			throw new RuntimeException("No configuration manager present. Running with no configuration.");
		return configurationManager;
	}

	public static void connectConfigurationManager(final IConfigurationProvider newconfigurationProvider) {
		configurationManager = newconfigurationProvider;
	}

	public static String getResourceString(final String key) {
		return accessConfigurationManager().getResourceString(key);
	}

	public static String getResourceString(final String key, final String defaultValue) {
		return accessConfigurationManager().getResourceString(key, defaultValue);
	}

	public String getResourcePropertyString(final String key) {
		return accessConfigurationManager().getResourceString(key);
	}

	public Integer getResourcePropertyInteger(final String key) {
		return getResourceInt(key);
	}

	public static int getResourceInt(final String key) {
		try {
			return Integer.valueOf(accessConfigurationManager().getResourceString(key)).intValue();
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public static int getResourceInt(final String key, final int defaultValue) {
		try {
			return Integer.valueOf(accessConfigurationManager().getResourceString(key
					, Integer.valueOf(defaultValue).toString())).intValue();
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public static long getResourceLong(final String key) {
		try {
			return Long.valueOf(accessConfigurationManager().getResourceString(key)).longValue();
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public static long getResourceLong(final String key, final long defaultValue) {
		try {
			return Long.valueOf(accessConfigurationManager().getResourceString(key
					, Long.valueOf(defaultValue).toString())).longValue();
		} catch (NumberFormatException nfe) {
			return 0;
		}
	}

	public static float getResourceFloat(final String key) {
		try {
			return Float.valueOf(accessConfigurationManager().getResourceString(key)).floatValue();
		} catch (NumberFormatException nfe) {
			return 0.0f;
		}
	}

	public static float getResourceFloat(final String key, final float defaultValue) {
		try {
			return Float.valueOf(accessConfigurationManager().getResourceString(key
					, Float.valueOf(defaultValue).toString())).floatValue();
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	public static boolean getResourceBoolean(final String key) {
		return Boolean.valueOf(accessConfigurationManager().getResourceString(key)).booleanValue();
	}

	public static boolean getResourceBoolean(final String key, final boolean defaultValue) {
		return Boolean.valueOf(accessConfigurationManager().getResourceString(key
				, Boolean.valueOf(defaultValue).toString())).booleanValue();
	}

	// - P R E F E R E N C E S   S E C T I O N
	private static IGlobalPreferencesManager preferencesprovider = null;

	public static void connectPreferencesManager(final IGlobalPreferencesManager newPreferencesProvider) {
		preferencesprovider = newPreferencesProvider;
	}

	public static IGlobalPreferencesManager getDefaultSharedPreferences() {
		if (null != preferencesprovider) return preferencesprovider;
		else
			throw new NeoComRuntimeException("[GlobalDataManagerConfiguration.getDefaultSharedPreferences]> Preferences provider not " +
					                                 "configured " +
					                                 "into the Global area.");
	}

	// - B A C K G R O U N D   T A S K S
	/** Background executor to use for off the main thread jobs. */
	private static final ExecutorService jobExecutor = Executors.newSingleThreadExecutor();

	public void shutdownExecutors() {
		try {
			logger.info("-- [GlobalDataManager.shutdownExecutor]> Attempt to shutdown jobExecutor");
			jobExecutor.shutdown();
			jobExecutor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (final InterruptedException iee) {
			logger.info("W- [GlobalDataManager.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
		} finally {
			if (!jobExecutor.isTerminated()) {
				logger.info("W- [GlobalDataManager.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
			}
			jobExecutor.shutdownNow();
			logger.info("-- [GlobalDataManager.shutdownExecutor]> Shutdown completed.");
		}
	}

	public static void suspendThread(final long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException ie) {
		}
	}

	public static Future<?> submitBackgroundJob(final Runnable task) {
		return jobExecutor.submit(task);
	}
}

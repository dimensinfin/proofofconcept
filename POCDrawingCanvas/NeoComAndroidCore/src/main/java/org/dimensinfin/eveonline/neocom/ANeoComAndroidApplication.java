package org.dimensinfin.eveonline.neocom;

import java.sql.SQLException;

import android.app.Application;

import org.dimensinfin.android.neocom.androidcore.R;
import org.dimensinfin.eveonline.neocom.conf.GlobalAndroidConfigurationProvider;
import org.dimensinfin.eveonline.neocom.conf.GlobalAndroidPreferencesProvider;
import org.dimensinfin.eveonline.neocom.database.NeoComAndroidDBHelper;
import org.dimensinfin.eveonline.neocom.datamngmt.AndroidGlobalDataManager;
import org.dimensinfin.eveonline.neocom.datamngmt.ESINetworkManager;
import org.dimensinfin.eveonline.neocom.datamngmt.FileSystemAndroidImplementation;
import org.dimensinfin.eveonline.neocom.model.ANeoComEntity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ANeoComAndroidApplication extends Application {
	private static Logger logger = LoggerFactory.getLogger(ANeoComAndroidApplication.class);
	@Override
	public void onCreate() {
		super.onCreate();
		// Connect the application to the global context to be able to use all the resources.
		logger.info("-- [NeoComApp.onCreate]> Connecting the Context to the Global...");
		AndroidGlobalDataManager.setApplicationContext(this);

		// Connect the file system to be able to read the assets and other application resources stored externally.
		logger.info("-- [NeoComApp.onCreate]> Connecting the File System to Global...");
		AndroidGlobalDataManager.installFileSystem(new FileSystemAndroidImplementation(
				AndroidGlobalDataManager.getApplication().getResources().getString(R.string.appname)
		));

		// Connect the Configuration manager. Do this after the file system because it reads configuration files.
		logger.info("-- [NeoComApp.onCreate]> Connecting the Configuration Manager...");
		AndroidGlobalDataManager.connectConfigurationManager(new GlobalAndroidConfigurationProvider("properties"));

		logger.info("-- [NeoComApp.onCreate]> Connecting the Preferences Manager...");
		AndroidGlobalDataManager.connectPreferencesManager(new GlobalAndroidPreferencesProvider());

		// Initialize the Model with the current global instance.
		logger.info("-- [NeoComApp.onCreate]> Connecting Global to Model...");
//		ANeoComEntity.connectGlobal(new AndroidGlobalDataManager());

		// Initializing the ESI api network controller.
//		ESINetworkManager.initialize();

		// Connect the NeoCom database.
//		logger.info("-- [NeoComApp.onCreate]> Connecting NeoCom private database...");
//		try {
//			AndroidGlobalDataManager.connectNeoComDBConnector(new NeoComAndroidDBHelper.NeoComAndroidDBHelperBuilder()
//					                                                  .setDatabaseLocation(AndroidGlobalDataManager.getApplication().getResources().getString(R.string.appname))
//					                                                  .setDatabaseName(AndroidGlobalDataManager.getApplication().getResources().getString(R.string.appdatabasefilename))
//					                                                  .setDatabaseVersion(Integer.valueOf(AndroidGlobalDataManager.getApplication().getResources().getString(
//							                                                  R.string.appdatabaseversion)))
//					                                                  .build()
//			);
//		} catch (SQLException sqle) {
//			AndroidGlobalDataManager.registerException("NeoComApp.onCreate", sqle, GlobalDataManagerExceptions.EExceptionSeverity.SEVERE);
//			sqle.printStackTrace();
//		}
	}
}

//  PROJECT:     POC.AccessDB(SP.POC.ADB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Java JRE 1.8 Specification. Android compatible
//  DESCRIPTION: Proff of Concept project to develop a set of SpringCloud controllers to
//                 provide with Json serialized data to Angular 5 POC application. The main
//                 idea of this POC is to develop the core for the final Decorators to be
//                 used on the NeoCom project.
//               Coding will be extended to use Hystrix and any other libraries that could
//                 be used on the final Backend NeoCom project.
package org.dimensinfin.eveonline.neocom.interfaces;

// - INTERFACE IMPLEMENTATION ...............................................................................
public interface INeoComApplicationConnector {
	public void connect ( final INeoComApplicationImplementation implementer );
	//	public String toString();
}

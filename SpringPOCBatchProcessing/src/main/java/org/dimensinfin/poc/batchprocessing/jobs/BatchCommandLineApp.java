package org.dimensinfin.poc.batchprocessing.jobs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.dimensinfin.logging.LogWrapper;

@SpringBootApplication
public class BatchCommandLineApp {
	public static void main( final String[] args ) {
		LogWrapper.enter();
		try {
			System.exit( SpringApplication.exit( SpringApplication.run( BatchCommandLineApp.class, args ) ) );
		} finally {
			//		new LogoPrinter().print();
			LogWrapper.exit();
		}
	}
}

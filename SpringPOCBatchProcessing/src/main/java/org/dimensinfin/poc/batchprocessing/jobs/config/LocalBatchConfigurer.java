package org.dimensinfin.poc.batchprocessing.jobs.config;

import java.util.Properties;
import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.interceptor.TransactionProxyFactoryBean;

@Configuration
@EnableBatchProcessing
public class LocalBatchConfigurer extends DefaultBatchConfigurer {
	private final DataSource dataSource;
//	private final PlatformTransactionManager transactionManager;

	@Autowired
	public LocalBatchConfigurer( final DataSource dataSource
//			,
//	                             final PlatformTransactionManager transactionManager
	) {
		this.dataSource = dataSource;
//		this.transactionManager = transactionManager;
	}

	@Override
	protected JobRepository createJobRepository() throws Exception {
		JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
		factory.setDataSource( dataSource );
		factory.setTransactionManager( getTransactionManager() );
		factory.setIsolationLevelForCreate( "ISOLATION_SERIALIZABLE" );
		factory.setTablePrefix( "BATCH_" );
		factory.setMaxVarCharLength( 1000 );
		return factory.getObject();
	}

	@Bean
	public TransactionProxyFactoryBean baseProxy() {
		TransactionProxyFactoryBean transactionProxyFactoryBean = new TransactionProxyFactoryBean();
		Properties transactionAttributes = new Properties();
		transactionAttributes.setProperty( "*", "PROPAGATION_REQUIRED" );
		transactionProxyFactoryBean.setTransactionAttributes( transactionAttributes );
		transactionProxyFactoryBean.setTarget( getJobRepository() );
		transactionProxyFactoryBean.setTransactionManager( getTransactionManager() );
		return transactionProxyFactoryBean;
	}
}

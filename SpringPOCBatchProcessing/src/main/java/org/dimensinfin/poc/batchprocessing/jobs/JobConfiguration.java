package org.dimensinfin.poc.batchprocessing.jobs;

import javax.batch.api.listener.JobListener;
import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

public class JobConfiguration {
	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory steps;

	//	private final JobBuilderFactory jobBuilders;
	private final JobRepository jobRepository;
	private final JobListener preProcessingListener;

	@Autowired
	public JobConfiguration( final JobBuilderFactory jobBuilders,
	                         final JobRepository jobRepository,
	                         final JobListener preProcessingListener ) {
		//		this.jobBuilders = jobBuilders;
		this.jobRepository = jobRepository;
		this.preProcessingListener = preProcessingListener;
	}

	@Bean
	public Job preProcessingJob() {
		return this.jobs.get( "initialPreProcessingJob" )
				.listener( preProcessingListener )
				.start( validateInputFile() )
				//				.next( processTransactions )
				//				.next( transactionSummarization )
				.build();
	}

	@Bean
	protected Step validateInputFile( /*ItemReader<FileSpec> reader,
	                                  ItemProcessor<FileSpec, FileRecord> processor,
	                                  ItemWriter<FileRecord> writer */) {
		return this.steps.get( "ValidateInputFile" )
				.tasklet( ( contribution, chunkContext ) -> null )
				.build();
	}@Bean
	public FlatFileItemReader<Person> reader() {
		return new FlatFileItemReaderBuilder<Person>()
				.name("personItemReader")
				.resource(new ClassPathResource("sample-data.csv"))
				.delimited()
				.names(new String[]{"firstName", "lastName"})
				.fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
					setTargetType(Person.class);
				}})
				.build();
	}

	@Bean
	public PersonItemProcessor processor() {
		return new PersonItemProcessor();
	}

	@Bean
	public JdbcBatchItemWriter<Person> writer( DataSource dataSource) {
		return new JdbcBatchItemWriterBuilder<Person>()
				.itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
				.sql("INSERT INTO people (first_name, last_name) VALUES (:firstName, :lastName)")
				.dataSource(dataSource)
				.build();
	}
}
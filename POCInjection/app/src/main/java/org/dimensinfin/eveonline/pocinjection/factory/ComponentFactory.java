package org.dimensinfin.eveonline.pocinjection.factory;

import org.dimensinfin.eveonline.pocinjection.components.CredentialRepository;
import org.dimensinfin.eveonline.pocinjection.singletons.MockableSingleton;

public class ComponentFactory {
    private static ComponentFactory singleton;

    public static ComponentFactory getInstance() {
        if (null == singleton) singleton = new ComponentFactory.Builder().build();
        return singleton;
    }

    public static void clean() {
        singleton = null;
    }

    private static void linkSingleton(final ComponentFactory single) {
        singleton = single;
    }

    public static <T> T assertNotNull(T target) {
        if (null == target) {
            throw new InvalidComponentException("Component Factory not initialised or some of the components not already linked.");
        } else {
            return target;
        }
    }

    // - C O M P O N E N T S
    private CredentialRepository credentialRepository;
    private MockableSingleton mockableSingleton;
    // - C H A N G E A B L E   C O M P O N E N T S
    private IFileSystem fileSystem;

    // - C O N S T R U C T O R S
    private ComponentFactory() {
    }

    public ComponentFactory(final ComponentFactory mockFactory) {
        linkSingleton(mockFactory);
    }

    // - C O M P O N E N T   A C C E S S
    public CredentialRepository getCredentialRepository() {
        return this.credentialRepository;
    }

    public IFileSystem getFileSystem() {
        return this.fileSystem;
    }

    public MockableSingleton getMockableSingleton() {
        return this.mockableSingleton;
    }

    // - B U I L D E R
    public static class Builder {
        private final ComponentFactory object;

        public Builder() {
            this.object = new ComponentFactory();
        }

        public Builder linkCredentialRepository(final CredentialRepository credentialRepository) {
            this.object.credentialRepository = credentialRepository;
            return this;
        }
        public Builder linkMockableSingleton(final MockableSingleton mockableSingleton) {
            this.object.mockableSingleton = mockableSingleton;
            return this;
        }

        public Builder linkFileSystem(final IFileSystem fileSystem) {
            this.object.fileSystem = fileSystem;
            return this;
        }

        public ComponentFactory build() {
            // Before returning the singleton be sure all components are linked.
            assertNotNull(this.object.credentialRepository);
            assertNotNull(this.object.mockableSingleton);
            assertNotNull(this.object.fileSystem);
            linkSingleton(this.object);
            return ComponentFactory.getInstance();
        }
    }
}

final class InvalidComponentException extends RuntimeException {

    public InvalidComponentException(final String message) {
        super(message);
    }
}

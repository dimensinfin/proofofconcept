package org.dimensinfin.eveonline.pocinjection.components;

public class CredentialRepository {
    private static CredentialRepository singleton = new CredentialRepository();

    public static CredentialRepository getInstance() {
        if (null == singleton) singleton = new CredentialRepository();
        return singleton;
    }

    public static void clean() {
        singleton = null;
    }

    private static void linkSingleton(final CredentialRepository single) {
        singleton = single;
    }

    private String name;

    private CredentialRepository() {
        this.name = this.getClass().getSimpleName();
    }

    public CredentialRepository(final CredentialRepository mockCredentialRepository) {
        linkSingleton(mockCredentialRepository);
    }

    public String getName() {
        return this.name;
    }
}

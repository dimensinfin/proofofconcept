package org.dimensinfin.eveonline.pocinjection.singletons;

public class SingletonUser {
    public int singletonCounter (){
        return  MockableSingleton.getInstance().getCounter ();
    }
}

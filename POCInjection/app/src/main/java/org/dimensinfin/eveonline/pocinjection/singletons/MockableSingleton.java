package org.dimensinfin.eveonline.pocinjection.singletons;

public class MockableSingleton {
    private static MockableSingleton singleton = new MockableSingleton();

    public static MockableSingleton getInstance() {
        if (null == singleton) singleton = new MockableSingleton();
        return singleton;
    }

    public static void clean() {
        singleton = null;
    }

    private static void linkSingleton(final MockableSingleton single) {
        singleton = single;
    }

    private MockableSingleton() {
    }

    public MockableSingleton(final MockableSingleton mockSingleton) {
        linkSingleton(mockSingleton);
    }


    public int getCounter() {
        return 10;
    }
}

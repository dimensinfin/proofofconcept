package org.dimensinfin.eveonline.pocinjection.singletons;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SingletonUserTest {
    @Before
    public void clearSingletons()  {
        MockableSingleton.clean();
    }

    @Test
    public void singletonCounter() {
        final SingletonUser user = new SingletonUser();
        final int expected = 10;
        final int obtained = user.singletonCounter();

        // Asserts
        Assert.assertEquals("Values should match.", expected, obtained);
    }

    @Test
    public void singletonCounterMocked() {
        // Given
        final MockableSingleton singleton = Mockito.mock(MockableSingleton.class);
        // Fix the singleton to use.
        new MockableSingleton(singleton);

        // When
        Mockito.when(singleton.getCounter()).thenReturn(20);

        final SingletonUser user = new SingletonUser();
        final int expected = 10;
        final int obtained = user.singletonCounter();

        // Asserts
        Assert.assertNotEquals("Values should match.", expected, obtained);
    }
}
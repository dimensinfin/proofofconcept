package org.dimensinfin.eveonline.pocinjection.factory;

import org.dimensinfin.eveonline.pocinjection.components.AndroidFileSystem;
import org.dimensinfin.eveonline.pocinjection.components.CredentialRepository;
import org.dimensinfin.eveonline.pocinjection.singletons.MockableSingleton;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Singleton tests should have setup cleqnun of the singletons to run the code successfully independent of test order.
 */
public class ComponentFactoryTest {
    @Before
    public void setupSingletons() {
        ComponentFactory.clean();
        CredentialRepository.clean();
        MockableSingleton.clean();
    }

    @Test(expected = InvalidComponentException.class)
    public void useWithoutInitialisation() {
        ComponentFactory.getInstance();
    }

    @Test(expected = InvalidComponentException.class)
    public void useWithoutAllComponents() {
        new ComponentFactory.Builder().build();
    }

    @Test
    public void useAllComponents() {
        new ComponentFactory.Builder()
                .linkCredentialRepository(CredentialRepository.getInstance())
                .linkMockableSingleton(MockableSingleton.getInstance())
                .linkFileSystem(new AndroidFileSystem())
                .build();
    }

    @Test
    public void testFactoryComponent() {
        // Check the default factory component.
        new ComponentFactory.Builder()
                .linkCredentialRepository(CredentialRepository.getInstance())
                .linkMockableSingleton(MockableSingleton.getInstance())
                .linkFileSystem(new AndroidFileSystem())
                .build();
        final MockableSingleton single = ComponentFactory.getInstance().getMockableSingleton();
        final int expected = 10;
        final int obtained = single.getCounter();
        Assert.assertEquals(expected, obtained);
    }

    @Test
    public void testMockComponent() {
        // Replace a singleton by mock and then factory by mock.
        final MockableSingleton mockSingleton = Mockito.mock(MockableSingleton.class);
        ComponentFactory mockFactory = new ComponentFactory.Builder()
                .linkCredentialRepository(CredentialRepository.getInstance())
                .linkMockableSingleton(mockSingleton)
                .linkFileSystem(new AndroidFileSystem())
                .build();
        Mockito.when(mockSingleton.getCounter()).thenReturn(20);
        new ComponentFactory(mockFactory);

        // Check the mocked factory component.
        final MockableSingleton single = ComponentFactory.getInstance().getMockableSingleton();
        final int expected = 20;
        final int obtained = single.getCounter();
        Assert.assertEquals(expected, obtained);
    }
}
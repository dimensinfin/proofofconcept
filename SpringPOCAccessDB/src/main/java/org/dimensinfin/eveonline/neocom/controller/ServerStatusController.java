//  PROJECT:     POC.AccessDB(SP.POC.ADB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Java JRE 1.8 Specification. Android compatible
//  DESCRIPTION: Proff of Concept project to develop a set of SpringCloud controllers to
//                 provide with Json serialized data to Angular 5 POC application. The main
//                 idea of this POC is to develop the core for the final Decorators to be
//                 used on the NeoCom project.
//               Coding will be extended to use Hystrix and any other libraries that could
//                 be used on the final Backend NeoCom project.
package org.dimensinfin.eveonline.neocom.controller;

import org.dimensinfin.core.util.Chrono;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

// - CLASS IMPLEMENTATION ...................................................................................
@RestController
public class ServerStatusController {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("ServerStatusController");

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public ServerStatusController() {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	@CrossOrigin()
	@RequestMapping(value = "/api/v1/serverstatus", method = RequestMethod.GET, produces = "application/json")
	public String serverstatusEntryPoint() {
		logger.info(">>>>>>>>>>>>>>>>>>>>NEW REQUEST: " + "/api/v1/serverstatus");
		logger.info(">> [ServerStatusController.serverstatusEntryPoint]");
		Chrono fullRunTime = new Chrono();
		try {
			//			NeoComMSConnector.getSingleton().startChrono();
			//			// If we receive a force command we should clear data before executing the request.
			//			if (force != null) if (force.equalsIgnoreCase("true")) AppModelStore.getSingleton().clearLoginList();
			//			return AppModelStore.getSingleton().accessLoginList();
			//		} catch (RuntimeException rtex) {
			//			return new Hashtable<String, Login>();
		} finally {
			logger.info("<< [ServerStatusController.serverstatusEntryPoint]> [TIMING] Processing Time: "
					+ fullRunTime.printElapsed(Chrono.ChonoOptions.SHOWMILLIS));
		}
		return "{server:ONLINE}";
	}
}

// - UNUSED CODE ............................................................................................

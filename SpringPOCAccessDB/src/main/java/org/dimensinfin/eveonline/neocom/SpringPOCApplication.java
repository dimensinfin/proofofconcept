//  PROJECT:     POC.AccessDB(SP.POC.ADB)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Java JRE 1.8 Specification. Android compatible
//  DESCRIPTION: Proff of Concept project to develop a set of SpringCloud controllers to
//                 provide with Json serialized data to Angular 5 POC application. The main
//                 idea of this POC is to develop the core for the final Decorators to be
//                 used on the NeoCom project.
//               Coding will be extended to use Hystrix and any other libraries that could
//                 be used on the final Backend NeoCom project.
package org.dimensinfin.eveonline.neocom;

import org.dimensinfin.eveonline.neocom.interfaces.INeoComApplicationConnector;
import org.dimensinfin.eveonline.neocom.interfaces.INeoComApplicationImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// - CLASS IMPLEMENTATION ...................................................................................
//@EnableCircuitBreaker
//@EnableScheduling
@SpringBootApplication
public class SpringPOCApplication implements INeoComApplicationImplementation {
	public static class Principal{
		public String name="-NAME-";

		public Principal(){
		}

		public String getName () {
			return name;
		}

		public void setName (final String name) {
			this.name = name;
		}
	}
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger(SpringPOCApplication.class);
	private static final SpringPOCApplication _singleton = new SpringPOCApplication();
	//			.connect(new NeoComMSConnector());
	private static INeoComApplicationConnector _connector = null;

	// - M A I N   E N T R Y P O I N T ........................................................................

	/**
	 * Just create the Spring application and launch it to run.
	 */
	public static void main (final String[] args) {
		// Instance and connect the Adaptors.
		SpringApplication.run(SpringPOCApplication.class, args);
	}

	// - F I E L D - S E C T I O N ............................................................................

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public SpringPOCApplication () {
		super();
		logger.info(">> [NeocomMicroServiceApplication.<constructor>]");
		// Connect to the database on initialization.

		// Create and connect the adapters.
		//		if (null == singleton) {
		//			logger.info("-- [NeocomMicroServiceApplication.<constructor>]> Instantiating the singleton.");
		//			singleton = this;
		//		}
		//		_connector = new NeoComMSConnector(this);
		logger.info("<< [NeocomMicroServiceApplication.<constructor>]");
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	private SpringPOCApplication connect (final INeoComApplicationConnector connectorChain) {
		_connector = connectorChain;
		_connector.connect(this);
		return this;
	}
	@RestController
	public class UserController {
		@GetMapping("/user/me")
		public Principal user(Principal principal) {
			return principal;
		}
	}
	@Configuration
	@EnableOAuth2Sso
	public class UiSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		public void configure (HttpSecurity http) throws Exception {
			http.antMatcher("/**")
					.authorizeRequests()
					.antMatchers("/", "/login**")
					.permitAll()
					.anyRequest()
					.authenticated();
		}
	}

	@Override
	public String toString () {
		StringBuffer buffer = new StringBuffer("SpringPOCApplication [");
		buffer.append("name: ").append("SpringPOCApplication");
		buffer.append("]");
		buffer.append("->").append(super.toString());
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
//[01]

package org.dimensinfin.eveonline.poccanvas.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.FrameLayout;
import android.widget.ImageView;

import org.dimensinfin.android.mvc.controller.AAndroidController;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.android.mvc.interfaces.IControllerFactory;
import org.dimensinfin.android.mvc.interfaces.IRender;
import org.dimensinfin.android.mvc.render.AMVCRender;
import org.dimensinfin.eveonline.poccanvas.R;
import org.dimensinfin.eveonline.poccanvas.StorageData;
import org.dimensinfin.eveonline.poccanvas.ui.StorageImageView;

public class StorageDataController extends AAndroidController<StorageData> {
	public StorageDataController( StorageData model, IControllerFactory factory ) {
		super(model, factory);
	}

	@Override
	public IRender buildRender( Context context ) {
		return new StorageDataRender(this, context);
	}

	public static class StorageDataRender extends AMVCRender {
		private ImageView nodeIcon;
		private FrameLayout mapCanvas;

		public StorageDataRender( IAndroidController controller, Context context ) {
			super(controller, context);
		}

		@Override
		public StorageDataController getController() {
			return (StorageDataController) super.getController();
		}

		@Override
		public int accessLayoutReference() {
			return R.layout.storage4canvas;
		}

		@Override
		public void initializeViews() {
			this.nodeIcon = this.getView().findViewById(R.id.nodeIcon);
			this.mapCanvas = this.getView().findViewById(R.id.mapCanvas);
		}

		@Override
		public void updateContent() {
			final Handler _handler = new Handler(Looper.getMainLooper());;
			this.nodeIcon = this.getView().findViewById(R.id.nodeIcon);
			this.mapCanvas = this.getView().findViewById(R.id.mapCanvas);
			this.nodeIcon.setImageResource(R.drawable.planet_barren_102_128_3);
			_handler.post(()->{
				final ImageView storageImage = this.generateStorageImage();
				this.mapCanvas.addView((ImageView) storageImage);
			});
		}

		private ImageView generateStorageImage() {
			return new StorageImageView.Builder(this.getContext())
					       .build();
		}
	}
}

package org.dimensinfin.eveonline.poccanvas.activity;

import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.eveonline.poccanvas.StorageData;
import org.dimensinfin.eveonline.poccanvas.controller.StorageDataController;

public class POCExperimentalFactory extends ControllerFactory {
    public POCExperimentalFactory(String selectedVariant) {
        super(selectedVariant);
    }

    @Override
    public IAndroidController createController(ICollaboration node) {
        if ( node instanceof StorageData)
            return new StorageDataController((StorageData) node, this);
        return super.createController(node);
    }
}

package org.dimensinfin.eveonline.poccanvas.datasource;

import org.dimensinfin.android.mvc.datasource.AMVCDataSource;
import org.dimensinfin.eveonline.poccanvas.StorageData;

public class StorageContentDataSource extends AMVCDataSource {
    @Override
    public void prepareModel() {

    }

    @Override
    public void collaborate2Model() {
        this.addModelContents(new StorageData(1234, 22500));
    }

    // - B U I L D E R
    public static class Builder extends AMVCDataSource.BaseBuilder<StorageContentDataSource, Builder> {
        private StorageContentDataSource onConstruction;

        @Override
        protected StorageContentDataSource getActual() {
            if (null == this.onConstruction) this.onConstruction = new StorageContentDataSource();
            return this.onConstruction;
        }

        @Override
        protected Builder getActualBuilder() {
            return this;
        }

//        public Builder withName( final String name ) {
//            this.onConstruction.name = name;
//            return this;
//        }
//
//        public Builder withVersion( final String version ) {
//            this.onConstruction.version = version;
//            return this;
//        }
//
//        //			public Builder withApplicationContext( final Context applicationContext ) {
//        //				this.getActual().setApplicationContext(applicationContext);
//        //				return this;
//        //			}
//
//        public Builder withEsiDataAdapter( final ESIDataAdapter esiDataAdapter ) {
//            this.onConstruction.esiDataAdapter = esiDataAdapter;
//            return this;
//        }
//
//        public Builder withCredentialRepository( final CredentialRepository credentialRepository ) {
//            this.onConstruction.credentialRepository = credentialRepository;
//            return this;
//        }
    }
}

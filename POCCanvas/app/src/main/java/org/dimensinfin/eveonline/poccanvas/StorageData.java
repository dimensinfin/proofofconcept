package org.dimensinfin.eveonline.poccanvas;

import org.dimensinfin.core.interfaces.ICollaboration;

import java.util.ArrayList;
import java.util.List;

public class StorageData implements ICollaboration {
    private Float capacity;
    private Float inUse;

    public StorageData(Integer inUse, Integer capacity) {
        this.capacity = capacity.floatValue();
        this.inUse = inUse.floatValue();
    }

    @Override
    public List<ICollaboration> collaborate2Model(String variation) {
        return new ArrayList<>();
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}

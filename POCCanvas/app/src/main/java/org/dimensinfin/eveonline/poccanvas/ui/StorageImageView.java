package org.dimensinfin.eveonline.poccanvas.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.widget.ImageView;

import org.dimensinfin.eveonline.poccanvas.R;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageImageView extends ImageView {
	protected static Logger logger = LoggerFactory.getLogger(StorageImageView.class);

	private Context context;
	private Paint paint;

	public StorageImageView( final Context context ) {
		super(context);
	}

	@Override
	protected void onDraw( Canvas canvas ) {
		logger.info(">> [FacilityImageView.onDraw]");
		// Move canvas to center to allow drawing symmetries. Paint all area transparent.
		try {
			this.centerCanvas(canvas);
			//			this.drawFacilityRing(canvas);
			//			this.drawFacilityFill(canvas);
			this.drawCycleRing(canvas);
			//			this.drawStructureIcon(canvas);
		} catch (Exception ex) {
			super.onDraw(canvas);
		}
		super.onDraw(canvas);
		logger.info("<< [FacilityImageView.onDraw]");
	}

	// - D R A W I N G
	protected void centerCanvas( final Canvas canvas ) {
		canvas.translate((canvas.getWidth() / 2), (canvas.getHeight() / 2));
		//		canvas.translate(posx, posy);
		//		canvas.drawColor(0xFF000000);
		// Restart the paint.
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
	}

	protected void drawCycleRing( Canvas canvas ) {
		final Integer dimension = (canvas.getWidth() - 20 * 3) / 2;
		paint.setColor(getResources().getColor(R.color.appblue));
		paint.setStrokeWidth(20);
		paint.setAntiAlias(true);
		paint.setStrokeCap(Paint.Cap.BUTT);
		paint.setStyle(Paint.Style.STROKE);
		paint.setPathEffect(new DashPathEffect(new float[]{5, 3}, 5));
		canvas.drawArc(-dimension, -dimension, dimension, dimension, -90.0F
				, this.getCycleAngle(), false, paint);
		paint.setColor(getResources().getColor(R.color.appred));
		canvas.drawArc(-dimension, -dimension, dimension, dimension, this.getCycleAngle()-90.0F
				,  30, false, paint);
	}

	public Float getCycleAngle() {
		final float angle = (64.0F / 100.0F) * 360.0F;
		logger.info("-- [getCycleAngle]> Angle: {}", angle);
		if (angle > 360.0F) return 360.0F;
		else return angle;
	}

	// - B U I L D E R
	public static class Builder {
		private StorageImageView onConstruction;

		public Builder( final Context context ) {
			this.onConstruction = new StorageImageView(context);
			this.onConstruction.context = context;
		}

		public StorageImageView build() {
			return this.onConstruction;
		}
	}
}

//  PROJECT:     NeoCom.Android (NEOC.A)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2013-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API22.
//  DESCRIPTION: Android Application related to the Eve Online game. The purpose is to download and organize
//                 the game data to help capsuleers organize and prioritize activities. The strong points are
//                 help at the Industry level tracking and calculating costs and benefits. Also the market
//                 information update service will help to identify best prices and locations.
//               Planetary Interaction and Ship fittings are point under development.
//               ESI authorization is a new addition that will give continuity and allow download game data
//                 from the new CCP data services.
//               This is the Android application version but shares libraries and code with other application
//                 designed for Spring Boot Angular 4 platform.
//               The model management is shown using a generic Model View Controller that allows make the
//                 rendering of the model data similar on all the platforms used.
#if (${PACKAGE_NAME} != "")package ${PACKAGE_NAME};#end

#if (${IMPORT_BLOCK} != "")${IMPORT_BLOCK}
import java.util.Hashtable;
import java.util.Hashtable;
import java.util.Hashtable;
import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
#end
#parse("File Header.java")

// - INTERFACE IMPLEMENTATION ...............................................................................
#if (${VISIBILITY} == "PUBLIC")public #end interface ${NAME} #if (${INTERFACES} != "")extends ${INTERFACES} #end {
	public String toString();
}

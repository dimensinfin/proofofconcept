package org.dimensinfin.eveonline.pocanimation;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toolbar tb = findViewById(R.id.toolbar);
//        setSupportActionBar(tb);
//        tb.setSubtitle("Login");

        findViewById(R.id.changeActivity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleRegistrationClick();
            }
        });
    }

    private void handleRegistrationClick() {
        Intent i = new Intent();
        i.setClass(this, Main2Activity.class);
        startActivity(i);
//        overridePendingTransition(R.anim.slideup, R.anim.slideup);
        overridePendingTransition(R.anim.slide_up, R.anim.no_animation); // remember to put it after startActivity, if you put it to above, animation will not working
    }

    @Override
    public void finishActivity(int requestCode) {
        super.finishActivity(requestCode);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition
//            Window.setEnterTransition(new Transition());
        } else {
            // Swap without transition
        }
    }
}

//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.adapter;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Adam Antinoo on 30/12/2017.
 */
// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IStorageAdapter {
	public String getApplicationBaseDir ();

	public String getApplicationCacheDir ();

	public InputStream accessStorageResource (final String resourcePath) throws IOException;

	public InputStream accessAssetStorageResource (final String resourceName) throws IOException;
}

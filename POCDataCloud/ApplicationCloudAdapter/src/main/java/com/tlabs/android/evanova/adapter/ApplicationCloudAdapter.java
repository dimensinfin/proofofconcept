//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Adam on 29/12/2017.
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class ApplicationCloudAdapter implements ICoreCloudAdapter {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger(ApplicationCloudAdapter.class);

	private static final ApplicationCloudAdapter _singleton = new ApplicationCloudAdapter();

	public static ApplicationCloudAdapter getSingleton () {
		return _singleton;
	}

	public static void submit2downloadExecutor (final Runnable task) {
		_singleton.getDownloadExecutor().submit(task);
	}
	public static void submit2uiExecutor (final Runnable task) {
		_singleton.getUIExecutor().submit(task);
	}

	// - F I E L D - S E C T I O N ............................................................................
	private final ExecutorService _downloadExecutor = Executors.newSingleThreadExecutor();
	private final ExecutorService _uiExecutor = Executors.newSingleThreadExecutor();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	protected ApplicationCloudAdapter () {
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	public ExecutorService getDownloadExecutor () {
		return _downloadExecutor;
	}

	public ExecutorService getUIExecutor () {
		return _uiExecutor;
	}

	public void shutdownExecutors () {
		try {
			logger.info("-- [ApplicationCloudAdapter.shutdownExecutor]> Attempt to shutdown _downloadExecutor");
			_downloadExecutor.shutdown();
			_downloadExecutor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (final InterruptedException iee) {
			logger.info("W- [ApplicationCloudAdapter.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
		} finally {
			if ( !_downloadExecutor.isTerminated() ) {
				logger.info("W- [ApplicationCloudAdapter.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
			}
			_downloadExecutor.shutdownNow();
			logger.info("-- [ApplicationCloudAdapter.shutdownExecutor]> Shutdown completed.");
		}
		try {
			logger.info("-- [ApplicationCloudAdapter.shutdownExecutor]> Attempt to shutdown _uiExecutor");
			_uiExecutor.shutdown();
			_uiExecutor.awaitTermination(1, TimeUnit.MINUTES);
		} catch (final InterruptedException iee) {
			logger.info("W- [ApplicationCloudAdapter.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
		} finally {
			if ( !_uiExecutor.isTerminated() ) {
				logger.info("W- [ApplicationCloudAdapter.shutdownExecutor]> Cancelling tasks. Grace time elapsed.");
			}
			_uiExecutor.shutdownNow();
			logger.info("-- [ApplicationCloudAdapter.shutdownExecutor]> Shutdown completed.");
		}
	}
}
// - UNUSED CODE ............................................................................................
//[01]

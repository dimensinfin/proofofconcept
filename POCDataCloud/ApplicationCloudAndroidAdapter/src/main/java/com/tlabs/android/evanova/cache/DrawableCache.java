//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import com.tlabs.android.evanova.adapter.ApplicationCloudAndroidAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

/**
 * Created by Adam Antinoo on 30/12/2017.
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class DrawableCache {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger(DrawableCache.class);
	private static final String DEFAULT_CACHE_PATH = "./";
	private static final String DEFAULT_CACHE_NAME = "DiskCache";

	// - F I E L D - S E C T I O N ............................................................................
	private SimpleDiskCache cacheDrawables = null;
	private boolean memoryActive = false;
	private Hashtable<String, Drawable> _memoryCacheDrawables = new Hashtable<String, Drawable>();
	private File cacheFolder = null;
	private volatile int loads = 0;
	private final CompressFormat mCompressFormat = CompressFormat.PNG;
	private final int mCompressQuality = 90;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public DrawableCache () {
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	/**
	 * There is a new implementation allowing different cache types so we can simplify configurations and setup.
	 * The default configuration is MEMORY so we do not use the file system.
	 * Another configuration is INTERNAL so the data is tored on the INTERNAL STORAGE that does not require additional
	 * configuration steps.
	 * And finally and that was the last defautl configuration is DISK that stores the data on the developer configured
	 * sdcard destination.
	 */
	public DrawableCache setCacheLocation (final String cacheType, final String path, final String folderName) {
		if ( cacheType.equalsIgnoreCase("MEMORY") ) {
			activateMemoryCache();
		}
		if ( cacheType.equalsIgnoreCase("INTERNAL") ) {
			cacheFolder = ApplicationCloudAndroidAdapter.getSingleton().getContext().getDir(path, Context.MODE_APPEND);
			try {
				cacheDrawables = SimpleDiskCache.open(cacheFolder, 1, 100 * 1024 * 1024);
			} catch (final IOException ioe) {
				logger.error("ER [DrawableCache.setCacheLocation]> Unable to create the cache. " + ioe.getMessage());
				ioe.printStackTrace();
			}
		}
		if ( cacheType.equalsIgnoreCase("DISK") ) {
			cacheFolder = new File(path, folderName);
			try {
				cacheDrawables = SimpleDiskCache.open(cacheFolder, 1, 100 * 1024 * 1024);
			} catch (final IOException ioe) {
				logger.error("ER [DrawableCache.setCacheLocation]> Unable to create the cache. " + ioe.getMessage());
				ioe.printStackTrace();
				return setCacheLocation("INTERNAL", path, folderName);
			}
		}
		return this;
	}

	private void activateMemoryCache () {
		cacheDrawables = null;
		memoryActive = true;
	}

	public synchronized void add (final String key, final Drawable target) {
		logger.info("-- [DrawableCache.add]> Storing Drawable instance [" + key + "] ");
		if ( memoryActive ) _memoryCacheDrawables.put(key, target);
		else this.writeDrawableToCache(key, target);
	}

	public synchronized Drawable getByURL (final String url) {
		if ( memoryActive ) {
			return _memoryCacheDrawables.get(url);
		} else {
			final String hash = new Integer(Math.abs(new Integer(url.hashCode()))).toString();
			try {
				final SimpleDiskCache.BitmapEntry bit = getCache().getBitmap(hash);
				if ( null == bit ) return null;
				final Drawable draw = new BitmapDrawable(ApplicationCloudAndroidAdapter.getSingleton().getResources(), bit.getBitmap());
				return draw;
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

	public void loads () {
		loads++;
	}

	@Override
	public String toString () {
		StringBuffer buffer = new StringBuffer("DrawableCache [");
		if ( null != cacheFolder ) buffer.append("cache location: ").append(cacheFolder.getAbsolutePath());
		buffer.append("]");
		return buffer.toString();
	}

	private void writeDrawableToCache (final String urlname, final Drawable data) {
		logger.info(">> [DrawableCache.writeDrawableToDisk]");
		OutputStream out = null;
		try {
			// Create a valid hash key from the resource URL
			final String hash = new Integer(Math.abs(new Integer(urlname.hashCode()))).toString();
			if ( data instanceof BitmapDrawable ) {
				final Bitmap bit = ((BitmapDrawable) data).getBitmap();
				out = getCache().openStream(hash, null);
				bit.compress(mCompressFormat, mCompressQuality, out);
			}
			logger.info("<< [DrawableCache.writeDrawableToDisk] [true]"); //$NON-NLS-1$
			return;
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				out.flush();
				out.close();
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		logger.info("<< [DrawableCache.writeDrawableToDisk] [false]"); //$NON-NLS-1$
	}

	private SimpleDiskCache getCache () {
		// If the cache is null result to default cache.
		if ( null == cacheDrawables ) setCacheLocation("INTERNAL", DEFAULT_CACHE_PATH, DEFAULT_CACHE_NAME);
		return cacheDrawables;
	}
}
// - UNUSED CODE ............................................................................................
//[01]

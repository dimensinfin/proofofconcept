//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

import com.tlabs.android.evanova.R;
import com.tlabs.android.evanova.cache.DrawableCache;
import com.tlabs.android.evanova.cache.DrawableDownloaderTask;
import com.tlabs.android.evanova.configuration.CoreCloudAndroidConfiguration;
import com.tlabs.android.evanova.configuration.ICoreCloudAndroidConfiguration;
import com.tlabs.android.evanova.interfaces.IConfigurationProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Adam Antinoo on 29/12/2017.
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class ApplicationCloudAndroidAdapter extends ApplicationCloudAdapter implements IAndroidCoreAdapter, ICacheAdapter {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger(ApplicationCloudAndroidAdapter.class);

	private static final ApplicationCloudAndroidAdapter _singleton = new ApplicationCloudAndroidAdapter();
	//	static{
	//		// WARNING- This is required to allow complete initialization of the singleton before use.
	//		_singleton.injectConfiguration(CoreCloudAndroidConfiguration.getSingleton());
	//	}

	public static ApplicationCloudAndroidAdapter getSingleton () {
		return _singleton;
	}

	// - F I E L D - S E C T I O N ............................................................................
	private Activity _activity = null;
	private ICoreCloudAndroidConfiguration _configuration = null;
	private DrawableCache _cacheDrawables = null;

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	protected ApplicationCloudAndroidAdapter () {
		super();
	}

	// - M E T H O D - S E C T I O N ..........................................................................
	private ICoreCloudAndroidConfiguration getConfiguration () {
		if ( null == _configuration ) injectConfiguration(CoreCloudAndroidConfiguration.getSingleton());
		return _configuration;
	}

	public void injectConfiguration (ICoreCloudAndroidConfiguration configuration) {
		_configuration = configuration;
	}

	public void registerActivity (final Activity newcontext) {
		_activity = newcontext;
		// Load the configuration from this activity into the configuration place.
		if ( newcontext instanceof IConfigurationProvider ) {
			injectConfiguration(((IConfigurationProvider) newcontext).getCoreCloudAndroidConfiguration());
		}
	}

	@Override
	public Context getContext () {
		// Check if we have an Activity registered.
		if ( null == _activity )
			throw new RuntimeException("RTEX [ApplicationCloudAndroidAdapter.getResourceString]> No Activity registered on Cloud.");
		else return _activity.getApplicationContext();
	}

	@Override
	public boolean checkNetworkAvailability () {
		return false;
	}

	@Override
	public boolean checkWiFiAvailability () {
		return false;
	}

	@Override
	public String getResourceString (final int identifier) {
		// Check if we have an Activity registered.
		if ( null == _activity )
			throw new RuntimeException("RTEX [ApplicationCloudAndroidAdapter.getResourceString]> No Activity registered on Cloud.");
		return _activity.getResources().getString(identifier);
	}

	public Drawable getResourceDrawable (final int identifier) {
		// Check if we have an Activity registered.
		if ( null == _activity )
			throw new RuntimeException("RTEX [ApplicationCloudAndroidAdapter.getResourceString]> No Activity registered on Cloud.");
		if ( Build.VERSION.SDK_INT >= 21 )
			return _activity.getResources().getDrawable(identifier, _activity.getTheme());
		else return _activity.getResources().getDrawable(identifier);
	}

	@Override
	public Resources getResources () {
		// Check if we have an Activity registered.
		if ( null == _activity )
			throw new RuntimeException("RTEX [ApplicationCloudAndroidAdapter.getResourceString]> No Activity registered on Cloud.");
		return _activity.getResources();
	}

	/**
	 * Gets a drawable by its URL. Most of the Eve icons can be reached though an URL and also this is valid for
	 * the pilot avatar. The process checks if the image is available at the cache. If the image is not there
	 * then it will try to locate it on the cache filesystem. If not found there then it will open a request to
	 * get it form the internet location once the network is available.
	 * <p>
	 * Removed all call to the cache accounting because not used.
	 *
	 * @param urlString the location of the resource. This is already developed by the caller and it is treated as a
	 *                  black box name.
	 * @param target    the UI object where we have to write the drawable once we get it to replace the dummy image that
	 *                  will be shown while we retrieve the not cache image. This will be kept on a list because there
	 *                  may be more that one pending call for the same image resource.
	 * @return the cached image or a dummy is still not available.
	 */
	public synchronized Drawable getCacheDrawable (final String urlString, final ImageView target) {
		// Try to get a hit from the memory cache.
		Drawable hit = this.getCache().getByURL(urlString);
		if ( null == hit ) {
			synchronized (this) {
				hit = this.getCache().getByURL(urlString);
				if ( null == hit ) {
					try {
						// hit = readDrawableFromDisk(urlString);
						// if (null == hit) {
						// No luck. We have to download the data from the
						// network.
						this.postDrawableRequest(urlString, target);
						//						this.getCache().miss();
						// }
					} catch (final Exception rtex) {
						logger.info("E> [ApplicationCloudAndroidAdapter.getCacheDrawable]> Exception reading cached data. " + rtex.getMessage());
						//						this.getCache().fault(urlString);
					}
				}
			}
		}
		//		this.getCache().access();
		if ( null == hit ) {
			hit = getResourceDrawable(R.drawable.defaulticoncacheplaceholder);
		}
		return hit;
	}

	//@TargetApi(12)
	public void addDrawableToCache (final String key, final Drawable image) {
		this.getCache().add(key, image);
		this.getCache().loads();
	}

	/**
	 * Accessor to the cache reference to allow working in conditions that thre is not configuration present. In that case
	 * we default to memory cache. Also isolate from configuration properties that should be defined on this modeule but
	 * on the container application.
	 *
	 * @return the image drawable cache implementation.
	 */
	private DrawableCache getCache () {
		if ( null == _cacheDrawables ) {
			_cacheDrawables = new DrawableCache()
					.setCacheLocation(getConfiguration().drawablecache_type()
							, getConfiguration().drawablecache_foldername()
							, getConfiguration().drawablecache_foldername());
		}
		return _cacheDrawables;
	}

	/**
	 * Keeps track of the requests to download data that are pending and of all the images that are waiting to
	 * replace their content by the new downloaded content. The real download process is done on an asych task
	 * as the writing on disk of the downloaded data for later accesses.<br>
	 * The tasks will kept the request list updated with the downloaded data and theirs download states.
	 *
	 * @param urlString url of the resource to access.
	 * @param target    UI image that is waiting for this drawable to replace the dummy we have set while we download
	 *                  the data.
	 */
	private void postDrawableRequest (final String urlString, final ImageView target) {
		// Launch a background task to get the image.
		final DrawableDownloaderTask task = new DrawableDownloaderTask();
		task.setImageTarget(target);
		task.execute(urlString);
	}
}
// - UNUSED CODE ............................................................................................
//[01]

//  PROJECT:     Evanova.Planetary (EVAN.PLAN)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2013-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API16.
//  DESCRIPTION: Android Library Modules to develop the Planetary Interaction activities and processes.
//               The initial idea is to develop this as an standalone module so it can be plugged on Evanova
//                 but also pluggable on NeoCom with minimal configuration changes.
//               The module will have Activities to show a Character Colony configuration and all colony
//                 assets and also adding the listing for Planetary assets and the possible processing
//                 transformations to get the most profitable solutions.
package com.tlabs.android.evanova.interfaces;

import com.tlabs.android.evanova.configuration.ICoreCloudAndroidConfiguration;

import java.util.Hashtable;
import java.util.Hashtable;
import java.util.Hashtable;

import org.joda.time.Instant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Adam on 30/12/2017.
 */
// - INTERFACE IMPLEMENTATION ...............................................................................
public interface IConfigurationProvider {
	public ICoreCloudAndroidConfiguration getCoreCloudAndroidConfiguration();
}

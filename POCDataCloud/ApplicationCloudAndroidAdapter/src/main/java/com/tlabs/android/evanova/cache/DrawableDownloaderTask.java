//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.cache;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.tlabs.android.evanova.adapter.ApplicationCloudAndroidAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Adam Antinoo on 30/12/2017.
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class DrawableDownloaderTask extends AsyncTask<String, Void, Drawable> {
	// - S T A T I C - S E C T I O N ..........................................................................

	// - F I E L D - S E C T I O N ............................................................................
	private ImageView targetImage = null;
	private Drawable source = null;

	// - M E T H O D - S E C T I O N  ..........................................................................
	public void setImageTarget (final ImageView target) {
		targetImage = target;
	}

	@Override
	protected Drawable doInBackground (final String... reference) {
		InputStream is = null;
		URLConnection urlConn = null;
		try {
			urlConn = new URL(reference[0]).openConnection();
			is = urlConn.getInputStream();
			source = Drawable.createFromStream(is, "src");
			ApplicationCloudAndroidAdapter.getSingleton().addDrawableToCache(reference[0], source);
			return source;
		} catch (final Exception ex) {
		} finally {
			try {
				if ( is != null ) {
					is.close();
				}
			} catch (final IOException e) {
			}
		}
		return null;
	}

	@Override
	protected void onPostExecute (final Drawable result) {
		// Invalidate the view to force a refresh.
		if ( null != result ) if ( null != targetImage ) {
			targetImage.setImageDrawable(source);
			targetImage.invalidate();
			super.onPostExecute(result);
		}
	}
}
// - UNUSED CODE ............................................................................................
//[01]

//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.configuration;

/**
 * Created by Adam Antinoo on 30/12/2017.
 */
// - INTERFACE IMPLEMENTATION ...............................................................................
public interface ICoreCloudAndroidConfiguration {
	public String drawablecache_type ();

	public String drawablecache_path ();

	public String drawablecache_foldername ();
}

//  PROJECT:     POC Singleton (POC.SING)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Android API14.
//  DESCRIPTION: Define the structure of static classes and Interfaces to allow a global
//                 access to application broad methods and services and that the implementation
//                 is delayed until the compilation and execution time.
//               This POC is developed under the new Evanova collaboration so the target
//                 is to make the code as independen from NeoCom as possible but at the
//                 same time be usable from Spring Boot microservices environments.
package com.tlabs.android.evanova.configuration;

import com.tlabs.android.evanova.R;
import com.tlabs.android.evanova.adapter.ApplicationCloudAndroidAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Adam Antinoo on 30/12/2017.
 */
// - CLASS IMPLEMENTATION ...................................................................................
public class CoreCloudAndroidConfiguration implements ICoreCloudAndroidConfiguration {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger(CoreCloudAndroidConfiguration.class);

	private static final CoreCloudAndroidConfiguration _singleton = new CoreCloudAndroidConfiguration();

	public static ICoreCloudAndroidConfiguration getSingleton () {
		return _singleton;
	}

	// - F I E L D - S E C T I O N ............................................................................
	// WARNING- ApplicationCloudAndroidAdapter still not completely initialized when calling this point.
	private String drawablecache_type = "";
	private String drawablecache_path = "";
	private String drawablecache_foldername = "";

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	public CoreCloudAndroidConfiguration () {
		super();
		initialize();
	}

	// - M E T H O D - S E C T I O N ..........................................................................

	public String drawablecache_type () {
		return drawablecache_type;
	}

	public String drawablecache_path () {
		return drawablecache_path;
	}

	public String drawablecache_foldername () {
		return drawablecache_foldername;
	}

	/**
	 * Load the configuration values form the configuration files so we can compile dependent modules.
	 */
	private void initialize () {
		drawablecache_type = ApplicationCloudAndroidAdapter.getSingleton().getResourceString(R.string.drawablecache_type);
		drawablecache_path = ApplicationCloudAndroidAdapter.getSingleton().getResourceString(R.string.drawablecache_path);
		drawablecache_foldername = ApplicationCloudAndroidAdapter.getSingleton().getResourceString(R.string.drawablecache_foldername);
	}

	@Override
	public String toString () {
		StringBuffer buffer = new StringBuffer("CoreCloudAndroidConfiguration [");
		buffer.append("drawablecache_type: ").append(drawablecache_type).append(" ");
		buffer.append("drawablecache_path: ").append(drawablecache_path).append(" ");
		buffer.append("drawablecache_foldername: ").append(drawablecache_foldername).append(" ");
		buffer.append("]");
		//		buffer.append("->").append(super.toString());
		return buffer.toString();
	}
}
// - UNUSED CODE ............................................................................................
//[01]

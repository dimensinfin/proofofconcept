package org.dimensinfin.poc.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
//@Component
public class LogStats {
	private static Logger logger = LoggerFactory.getLogger(Logger.class);
	private static final String LOG_METHOD_STATS = "execution(@org.dimensinfin.poc.annotations.MethodStats * *(..))";

	@Pointcut(value = LOG_METHOD_STATS)
	public void LogMethodStats() {
	}

	@Around("LogMethodStats()")
	public Object log( ProceedingJoinPoint point ) throws Throwable {
		long start = System.currentTimeMillis();
		Object result = point.proceed();
		logger.info("className={}, methodName={}, timeMs={},threadId={}", new Object[]{
				MethodSignature.class.cast(point.getSignature()).getDeclaringTypeName(),
				MethodSignature.class.cast(point.getSignature()).getMethod().getName(),
				System.currentTimeMillis() - start,
				Thread.currentThread().getId() }
		);
		return result;
	}
}
package org.dimensinfin.poc.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class ConnectionAspect {
	private static Logger logger = LoggerFactory.getLogger(ConnectionAspect.class);
	private static final String RESTRICT_TO_INTERNET = "execution(@org.dimensinfin.poc.annotations.RunWithInternet * * (..))";


	@Pointcut(value = RESTRICT_TO_INTERNET)
	public void RestrictToInternetAnnotationMethod() {

	}

	@Around("RestrictToInternetAnnotationMethod()")
	public void checkMethodConnection( ProceedingJoinPoint joinPoint ) throws Throwable {
		logger.info(">> [ConnectionAspect.checkMethodConnection]");
//		if(MyNetworkManager.isConnected()){ // A class i made to check for the Internet connection.
//			joinPoint.proceed();
//		}//if there is no Internet do nothing

	}
}
package org.dimensinfin.poc.annotations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

/**
 * @author Adam Antinoo
 */
public class DemoClass {
    private static Logger logger = LoggerFactory.getLogger(DemoClass.class);
    private static DemoClass singleton;

//    public static void main(String[] args) {
//        singleton = new DemoClass();
//        try {
//            singleton.awesomeMethod();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        }
//    }

    // - F I E L D - S E C T I O N

    // - C O N S T R U C T O R - S E C T I O N

    // - G E T T E R S   &   S E T T E R S
    @MethodStats
    @MethodInfo(author = "John Snow", revision = 2, comments = "Hey!")
    public void awesomeMethod() throws NoSuchMethodException {
        Method method = getClass().getMethod("awesomeMethod");
        MethodInfo methodInfo = method.getAnnotation(MethodInfo.class);

        logger.info("MethodInfo {}", methodInfo.author());
        logger.info("MethodInfo {}", methodInfo.revision());
        logger.info("MethodInfo {}", methodInfo.comments());
    }

    // - M E T H O D - S E C T I O N
    // - B U I L D E R
//    public static class Builder {
//
//        private final DemoClass instance;
//
//        public Builder() {
//            instance = new DemoClass();
//        }
//
//        public Builder withHHH(final HHH HHH) {
//            this.instance.HHH = HHH;
//            return this;
//        }
//
//        public DemoClass build() {
//            return instance;
//        }
//    }

//    public static class Log extends Logger {
//        @Override
//        public String getName() {
//            return super.getName();
//        }
//
//        public void d(final String format, final Object... arguments) {
//            return super.info(format, arguments);
//        }
//}
}

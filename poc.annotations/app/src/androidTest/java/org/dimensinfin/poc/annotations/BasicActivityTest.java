package org.dimensinfin.poc.annotations;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.SmallTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class BasicActivityTest {
	@Rule
	public ActivityTestRule<BasicActivity> mActivityRule =
			new ActivityTestRule<BasicActivity>(BasicActivity.class);

	@Test
	public void activitySetup() {
		Assert.assertNotNull(this.mActivityRule.getActivity());
	}
}
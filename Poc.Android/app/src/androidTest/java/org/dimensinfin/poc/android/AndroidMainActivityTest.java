package org.dimensinfin.poc.android;

import android.view.View;
import android.widget.TextView;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;

import org.dimensinfin.poc.android.activity.MainActivity;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

//@RunWith(AndroidJUnitRunner.class)
public class AndroidMainActivityTest {
	static {
		System.setProperty("org.mockito.android.target", ApplicationProvider.getApplicationContext().getCacheDir().getPath());
	}

	@Rule
	public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

	@Test
	public void checkMessageContents() {
		final View content = this.mActivityRule.getActivity().findViewById(R.id.content);
		Assert.assertNotNull(content);
	}

	@Test
	public void checkCounterContents() {
		final TextView counter = this.mActivityRule.getActivity().findViewById(R.id.counter);
		Assert.assertNotNull(counter);
		Assert.assertEquals("-", counter.getText());
	}

	@Test
	public void checkCounterIncrement() {
		final TextView counter = this.mActivityRule.getActivity().findViewById(R.id.counter);
		Assert.assertNotNull(counter);
		Assert.assertEquals("-", counter.getText());
		final TextView button = this.mActivityRule.getActivity().findViewById(R.id.adderButton);
		Assert.assertNotNull(button);
		onView(withId(R.id.adderButton)).perform(click());
		Assert.assertEquals("1", counter.getText());
	}
}
package org.dimensinfin.poc.android.test;

import android.widget.TextView;

import androidx.test.rule.ActivityTestRule;

import org.dimensinfin.poc.android.activity.MainActivity;
import org.dimensinfin.poc.android.R;
import org.junit.Assert;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class MainActivitySteps {
	private MainActivity activity;
	ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class, false, false);

	@Before
	public void launchActivity() throws Exception {
		rule.launchActivity(null);
	}

	@After
	public void finishActivity() throws Exception {
		this.rule.getActivity().finish();
	}

	@Given("the main activity")
	public void theMainActivity() {
		this.activity = this.rule.getActivity();
		Assert.assertNotNull(this.activity);
	}

	@And("having a button on the main activity")
	public void havingAButtonOnTheMainActivity() {
		onView(withId(R.id.adderButton)).check(matches(withText("Adder")));
	}

	@When("clicking on the count button")
	public void clickingOnTheCountButton() {
		onView(withId(R.id.adderButton)).perform(click());
	}

	@Then("increment the click counter")
	public void incrementTheClickCounter() {
		final String obtained = this.activity.entryPoint4Acceptance();
		final String expected = ((TextView) this.activity.findViewById(R.id.counter)).getText().toString();
		Assert.assertEquals(expected, obtained);
	}
}

package org.dimensinfin.poc.android.controller;

import android.content.Context;

import androidx.annotation.NonNull;

import org.dimensinfin.android.mvc.controller.AndroidController;
import org.dimensinfin.android.mvc.interfaces.IControllerFactory;
import org.dimensinfin.android.mvc.interfaces.IRender;
import org.dimensinfin.poc.android.AppTitle;
import org.dimensinfin.poc.android.render.AppTitleRender;

public class AppTitleController extends AndroidController<AppTitle> {
	public AppTitleController( @NonNull final AppTitle model, @NonNull final IControllerFactory factory ) {
		super(model, factory);
	}

	@Override
	public IRender buildRender( final Context context ) {
		return new AppTitleRender(this, context);
	}

}

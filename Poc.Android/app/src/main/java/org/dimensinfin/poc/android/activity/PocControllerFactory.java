package org.dimensinfin.poc.android.activity;

import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.poc.android.AppTitle;
import org.dimensinfin.poc.android.controller.AppTitleController;

public class PocControllerFactory extends ControllerFactory {
	public PocControllerFactory( final String selectedVariant ) {
		super(selectedVariant);
	}

	@Override
	public IAndroidController createController( final ICollaboration node ) {
		if ( node instanceof AppTitle)
			return new AppTitleController((AppTitle) node, this).setRenderMode(this.getVariant());

		return super.createController(node);
	}
}

package org.dimensinfin.poc.android.activity;

import android.os.Bundle;

import org.dimensinfin.android.mvc.activity.MVCMultiPageActivity;
import org.dimensinfin.android.mvc.activity.MVCPagerFragment;
import org.dimensinfin.android.mvc.datasource.IDataSource;
import org.dimensinfin.android.mvc.datasource.MVCDataSource;
import org.dimensinfin.android.mvc.interfaces.IControllerFactory;
import org.dimensinfin.poc.android.AppTitle;

public class MainActivity extends MVCMultiPageActivity {
	@Override
	protected void onCreate( final Bundle savedInstanceState ) {
		super.onCreate(savedInstanceState);
		this.addPage(new MainFragment().);
	}

	public static class MainFragment extends MVCPagerFragment {

		@Override
		public IControllerFactory createFactory() {
			return new PocControllerFactory(this.getVariant());
		}

		@Override
		public IDataSource createDS() {
			return
		}
		public static class MainDataSource extends  MVCDataSource {
			@Override
			public void prepareModel() {

			}

			@Override
			public void collaborate2Model() {
				final String appName
				this.addModelContents(new AppTitle())
			}
			public static class Builder extends MVCDataSource.BaseBuilder
		}
	}
}

package org.dimensinfin.poc.android;

import org.dimensinfin.poc.android.activity.MainActivity;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class MainActivityTest {

	@Test
	public void adder() {
		final MainActivity activity = Mockito.mock(MainActivity.class, Mockito.CALLS_REAL_METHODS);
		final Integer obtained = activity.adder(34);
		Assert.assertEquals(Integer.valueOf(35), obtained);
	}
}
package org.dimensinfin.poc.modulea;

import com.annimon.stream.Stream;

import java.util.List;

public class ConcatComponent {
	public String concatenate( final List<String> data ) {
		final String result = "";
		Stream.of(data)
		      .forEach(name -> result.concat(name).concat(" "));
		return result;
	}

	// - B U I L D E R
	public static class Builder {
		private ConcatComponent onConstruction;

		public Builder() {
			this.onConstruction = new ConcatComponent();
		}

		public ConcatComponent build() {
			return this.onConstruction;
		}
	}
}

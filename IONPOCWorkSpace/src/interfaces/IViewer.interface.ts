//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- INTERFACES
// import { IDataSource } from '../interfaces/IDataSource.interface';
// import { INeoComNode } from '../interfaces/INeoComNode.interface';

export interface IViewer {
  getViewer(): IViewer;
  notifyDataChanged(): void;
  redirectPage(route: any): void;
}

//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// import { NgZone } from '@angular/core';
// import { Observable } from 'rxjs';
// import { tap } from 'rxjs/operators';
// import { map } from 'rxjs/operators';
// import { flatMap } from 'rxjs/operators';
// import { Injectable } from '@angular/core';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
//--- SERVICES
import { BackendService } from '../../services/backend.service';
//--- INTERFACES
import { IViewer } from '../../interfaces/IViewer.interface';
//--- MODELS
import { Node } from '../../models/Node.model';
import { Centro } from '../../models/Centro.model';
import { Especialidad } from '../../models/Especialidad.model';
import { Medico } from '../../models/Medico.model';
//--- PAGES
import { ListAppointmentsPage } from '../../pages/list-appointments/list-appointments.page';

@Component({
  selector: 'available-doctors-page',
  templateUrl: 'availabledoctors.page.html'
})
export class AvailableDoctorsPage implements OnInit, IViewer {
  /** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  public dataModelRoot: Node[] = [];
  /** The real time updated list of nodes to render. */
  public renderNodeList: Node[] = [];

  private loader: any;

  //--- C O N S T R U C T O R
  constructor(
    protected navCtrl: NavController,
    protected storage: Storage,
    protected loadingCtrl: LoadingController,
    protected backendService: BackendService) {
    console.log('>>[AvailableDoctorsPage.constructor]');
  }

  ngOnInit() {
    console.log('>>[AvailableDoctorsPage.ngOnInit]');
    // Call the backend to retrieve the list of Doctors to be presented to the Patient
    this.presentLoading();
    // let specialitiesList = new Map<string, Speciality>();
    this.backendService.getMedicalCenters()
      .subscribe((centroList: Centro[]) => {
        // Variable to collect the complete list of Medicos.
        let medicosList: Medico[] = [];
        // Iterate on all the centers to collect the data.
        for (let centro of centroList) {
          console.log('--[AvailableDoctorsPage.ngOnInit]> Centro: ' + JSON.stringify(centro));
          let especialidadList = new Map<string, Especialidad>();
          // Get the list pf service providers.
          this.backendService.getMedicalServiceProviders(centro.getId())
            .subscribe((medicoList) => {
              // Process the medical serfice providers and set them on the hierarchy.
              for (let medico of medicoList) {
                // Aggregate providers by specialty.
                let spec = medico.getEspecialidad();
                let hit = especialidadList.get(spec);
                if (null == hit) {
                  hit = new Especialidad().setNombre(spec);
                  // hit.expand();
                  especialidadList.set(spec, hit);
                  centro.addEspecialidad(hit);
                }
                hit.addMedico(medico);
                medicosList.push(medico)
                console.log('--[AvailableDoctorsPage.ngOnInit]> Medico: ' + JSON.stringify(medico));
              }
              // Cache the list of providers into the service.
              this.backendService.setMedicos(medicosList)

              // When all the providers are processed we can add the center to the list.
              this.dataModelRoot.push(centro);
              this.notifyDataChanged();
            });
        }
        this.dismissLoading();
      });
  }
  public presentLoading(): void {
    this.loader = this.loadingCtrl.create({
      content: "Por favor espere..."
    });
    this.loader.present();
  }
  public dismissLoading(): void {
    this.loader.dismiss();
  }
  public itemSelected(item: any) {
    console.log('>>[AvailableDoctorsPage.itemSelected]> Item: ' + item);
  }
  //--- IVIEWER INTERFACE
  /**
	Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
	*/
  public getViewer(): IViewer {
    return this;
  }
  /**
    Reconstructs the list of nodes to be rendered from the current DataRoot and their collaborations to the view.
    */
  public notifyDataChanged(): void {
    console.log(">>[BasePageComponent.notifyDataChanged]");
    // Clear the current list while reprocessing the new nodes.
    // TODO This should be improved to change only the nodes that change (replaced, removed, added)
    // this.renderNodeList = [];
    let copyList = [];
    // Get the initial list by applying the policies defined at the page to the initial root node contents. Policies may be sorting or filtering actions.
    // let initialList = this.dataModelRoot;
    // Generate the contents by collaborating to the view all the nodes.
    for (let node of this.dataModelRoot) {
      let nodes = node.collaborate2View(this.backendService);
      console.log("--[BasePageComponent.notifyDataChanged]> Collaborating " + nodes.length + " nodes.");
      // Add the collaborated nodes to the list of nodes to return.
      for (let childNode of nodes) {
        copyList.push(childNode);
      }
    }
    this.renderNodeList = copyList;
    console.log("<<[BasePageComponent.notifyDataChanged]");
  }
  public redirectPage(route: any): void {
    // this.navCtrl.push(route);
  }
  public medicClick(target: Node): void {
    // Move to the list of appointments with the selected provider.
    let provider = target as Medico;
    this.navCtrl.push(ListAppointmentsPage, { 'providerIdentifier': provider.id });
  }
  public inDevelopment(): boolean {
    return true;
  }
}

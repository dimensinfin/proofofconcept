//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
//--- SERVICES
import { BackendService } from '../../services/backend.service';
//--- INTERFACES
import { IViewer } from '../../interfaces/IViewer.interface';
//--- MODELS
import { Node } from '../../models/Node.model';
import { Medico } from '../../models/Medico.model';
import { Cita } from '../../models/Cita.model';
import { PatientCredential } from '../../models/PatientCredential.model';
//--- PAGES
import { AppointmentReservationPage } from '../../pages/appointment-reservation/appointment-reservation.page';

const weekDays = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado"
];
const monthNames = [
  "enero",
  "febrero",
  "marzo",
  "abril",
  "mayo",
  "junio",
  "julio",
  "agosto",
  "septiembre",
  "octubre",
  "noviembre",
  "diciembre"
];


@Component({
  selector: 'list-appointments-page',
  templateUrl: 'list-appointments.page.html',
})
export class ListAppointmentsPage implements OnInit, IViewer {
  /** This is the single pointer to the model data that is contained on this page. This is the first element than when processed with the collaborate2View process will generate the complete list of nodes to render and received by the factory from the getBodyComponents().
	This variable is accessed directly (never been null) and it if shared with all descendans during the generation process. */
  public dataModelRoot: Node[] = [];
  /** The real time updated list of nodes to render. */
  public renderNodeList: Node[] = [];
  public providerIdentifier: string = '';
  public downloading: boolean = true;
  private medicos: Medico[] = [];
  public medicoSeleccionado: Medico;

  //--- C O N S T R U C T O R
  constructor(
    protected navCtrl: NavController,
    protected navParams: NavParams,
    protected storage: Storage,
    protected loadingCtrl: LoadingController,
    protected backendService: BackendService) {
    console.log('>>[ListAppointmentsPage.constructor]');
    this.providerIdentifier = this.navParams.get('providerIdentifier');
  }

  ngOnInit() {
    console.log('>>[ListAppointmentsPage.ngOnInit]');
    // Get the identifier for the selected provider.
    let providerIdentifier = this.navParams.get('providerIdentifier');
    // Access the last version of the list of Medics to locate the selected provider card.
    this.backendService.medicos.subscribe((data) => {
      this.medicos = data;
      for (let medico of this.medicos) {
        if (medico.getId() === providerIdentifier) {
          this.medicoSeleccionado = medico;
        }
      }
    });

    // Call the backend to retrieve the list of Doctors to be presented to the Patient
    this.backendService.getAppoinmentList(this.navParams.get('providerIdentifier'))
      .subscribe((citaList: Cita[]) => {
        // Iterate on all the centers to collect the data.
        for (let cita of citaList) {
          console.log('--[ListAppointmentsPage.ngOnInit]> Cita: ' + JSON.stringify(cita));
          this.dataModelRoot.push(cita);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
  }
  /**
   * Start the process to block and reserve the appointment. At this point we have the Medico and the Cita ready so this is the right place to block the appointment and them move to the confirmation page.
   * @param cita the Cita clicked and selected for blocking.
   */
  public citaClick(cita: Cita): void {
    console.log('>>[ListAppointmentsPage.citaClick]');
    // Block the appointment. If the block is confirmed then move to the information page.
    this.storage.get(BackendService.CREDENTIAL_KEY)
      .then((data) => {
        if (null != data) {
          console.log('--[PatientRecordPage.ngOnInit]> Patient data: ' + data);
          let patientRecord = new PatientCredential(JSON.parse(data));
          this.backendService.blockAppointment(patientRecord, cita)
            .subscribe((blockedCita) => {
              console.log('>>[ListAppointmentsPage.citaClick.blockAppointment]> blockedCita: ' + blockedCita);
              // Start the reservation process. Show the reservation page.
              // Store the data in the store for other page access.
              this.backendService.currentMedico = this.medicoSeleccionado;
              this.backendService.currentPaciente = patientRecord;
              this.backendService.currentCita = cita;
              this.navCtrl.push(AppointmentReservationPage, { 'citaIdentifier': cita.id });
            });
        }
      });
    console.log('<<[ListAppointmentsPage.citaClick]');
  }
  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return weekDays[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return monthNames[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }

  //--- IVIEWER INTERFACE
  /**
	Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
	*/
  public getViewer(): IViewer {
    return this;
  }
  /**
    Reconstructs the list of nodes to be rendered from the current DataRoot and their collaborations to the view.
    */
  public notifyDataChanged(): void {
    console.log(">>[BasePageComponent.notifyDataChanged]");
    // Clear the current list while reprocessing the new nodes.
    // TODO This should be improved to change only the nodes that change (replaced, removed, added)
    // this.renderNodeList = [];
    let copyList = [];
    // Get the initial list by applying the policies defined at the page to the initial root node contents. Policies may be sorting or filtering actions.
    // let initialList = this.dataModelRoot;
    // Generate the contents by collaborating to the view all the nodes.
    for (let node of this.dataModelRoot) {
      let nodes = node.collaborate2View(this.backendService);
      console.log("--[BasePageComponent.notifyDataChanged]> Collaborating " + nodes.length + " nodes.");
      // Add the collaborated nodes to the list of nodes to return.
      for (let childNode of nodes) {
        copyList.push(childNode);
      }
    }
    this.renderNodeList = copyList;
    console.log("<<[BasePageComponent.notifyDataChanged]");
  }
  public redirectPage(route: any): void {
    // this.navCtrl.push(route);
  }
}

//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
//--- SERVICES
import { BackendService } from '../../services/backend.service';
//--- MODELS
import { PatientCredential } from '../../models/PatientCredential.model';
//--- PAGES
import { PatientRecordPage } from '../../pages/patientrecord/patientrecord.page';
import { AvailableDoctorsPage } from '../../pages/availabledoctors/availabledoctors.page';

@Component({
  selector: 'home-page',
  templateUrl: 'home.page.html'
})
export class HomePage implements OnInit {
  public credentialValid: boolean = false;
  public credential: PatientCredential = new PatientCredential();

  constructor(
    protected navCtrl: NavController,
    protected storage: Storage) {
  }
  ngOnInit() {
    console.log('>>[HomePage.ngOnInit]');
    // Check if there is Patient data available. If not hide the card and show the creation button.
    this.storage.get(BackendService.CREDENTIAL_KEY)
      .then((val) => {
        console.log('--[HomePage.ngOnInit]> Credential data: ', JSON.stringify(val));
        if (null == val) {
          // Hide the card and show the button.
          this.credentialValid = false;
        } else {
          // Load the credential data into the rendering model.
          // console.log('--[PatientRecordPage.ngOnInit]> Patient data: '+JSON.stringify(this.patientRecord));
          this.credential = new PatientCredential(JSON.parse(val));
          // Chgeck if event the retrieved credential is valid.
          this.credentialValid = this.credential.isValid();
          console.log('--[HomePage.ngOnInit]> Credential state: ', this.credentialValid);
        }
      });
    console.log('<<[HomePage.ngOnInit]');
  }

  //--- I N T E R F A C E   A C T I O N S
  public openCredentialForm(): void {
    console.log('>>[HomePage.openCredentialForm]');
    this.navCtrl.push(PatientRecordPage);
    console.log('<<[HomePage.openCredentialForm]');
  }
  public newAppointment(): void {
    console.log('>>[HomePage.newAppointment]');
    this.navCtrl.push(AvailableDoctorsPage);
    console.log('<<[HomePage.newAppointment]');
  }
  public clearCredential(): void {
    console.log('>>[HomePage.clearCredential]');
    this.storage.set(BackendService.CREDENTIAL_KEY, JSON.stringify(new PatientCredential()));
    this.credential = new PatientCredential();
    this.credentialValid = false;
    console.log('<<[HomePage.clearCredential]');
  }
}

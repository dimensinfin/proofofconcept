//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- IONIC
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
//--- SERVICES
import { BackendService } from '../../services/backend.service';
//--- INTERFACES
// import { IViewer } from '../../interfaces/IViewer.interface';
//--- MODELS
// import { Node } from '../../models/Node.model';
// import { Centro } from '../../models/Centro.model';
// import { Especialidad } from '../../models/Especialidad.model';
import { Medico } from '../../models/Medico.model';
import { Cita } from '../../models/Cita.model';
import { PatientCredential } from '../../models/PatientCredential.model';
//--- PAGES
import { HomePage } from '../../pages/home/home.page';

const weekDays = [
  "Domingo",
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado"
];
const monthNames = [
  "enero",
  "febrero",
  "marzo",
  "abril",
  "mayo",
  "junio",
  "julio",
  "agosto",
  "septiembre",
  "octubre",
  "noviembre",
  "diciembre"
];


@Component({
  selector: 'appointment-reservation-page',
  templateUrl: 'appointment-reservation.page.html',
})
export class AppointmentReservationPage implements OnInit {
  public currentMedico: Medico;
  public currentPaciente: PatientCredential;
  public currentCita: Cita;

  //--- C O N S T R U C T O R
  constructor(
    protected navCtrl: NavController,
    public navParams: NavParams,
    protected storage: Storage,
    protected loadingCtrl: LoadingController,
    protected backendService: BackendService) {
    console.log('><[AppointmentReservationPage.constructor]');
    this.currentMedico = this.backendService.currentMedico;
    this.currentPaciente = this.backendService.currentPaciente;
    this.currentCita = this.backendService.currentCita;
  }

  ngOnInit() {
    console.log('>>[AppointmentReservationPage.ngOnInit]');
    this.currentMedico = this.backendService.currentMedico;
    this.currentPaciente = this.backendService.currentPaciente;
    this.currentCita = this.backendService.currentCita;
  }
  public dataReady(): boolean {
    if (null == this.currentMedico) return false;
    if (null == this.currentPaciente) return false;
    if (null == this.currentCita) return false;
    return true;
  }
  public getWeekDay(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return weekDays[theDate.getDay()];
  }
  public getDateNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getDate();
  }
  public getYearNumber(cita: Cita): number {
    let theDate = new Date(cita.fecha);
    return theDate.getFullYear();
  }
  public getMonthName(cita: Cita): string {
    let theDate = new Date(cita.fecha);
    return monthNames[theDate.getMonth()];
  }
  public getDateDate(cita: Cita): Date {
    let theDate = new Date(cita.huecoId * 60 * 1000);
    return theDate;
  }
  public confirmAppointment(): void {
    console.log('>>[AppointmentReservationPage.confirmAppointment]');
    this.backendService.reserveAppointment(this.currentPaciente, this.currentCita)
      .subscribe((reservedCita) => {
        console.log('>>[ListAppointmentsPage.citaClick.confirmAppointment]> blockedCita: ' + reservedCita);
        // Store the appointment on the list of reservations.

        this.navCtrl.push(HomePage);
      });

    console.log('<<[AppointmentReservationPage.confirmAppointment]');
  }
  public cancelAppointment(): void {
    console.log('>>[AppointmentReservationPage.cancelAppointment]');
    this.backendService.cancelAppointment(this.currentPaciente, this.currentCita)
      .subscribe((reservedCita) => {
        console.log('>>[ListAppointmentsPage.citaClick.cancelAppointment]> blockedCita: ' + reservedCita);
        this.navCtrl.push(HomePage);
      });
    console.log('<<[AppointmentReservationPage.cancelAppointment]');
  }
}

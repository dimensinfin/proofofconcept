//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- FORMS
import { FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
//--- IONIC
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
//--- SERVICES
import { BackendService } from '../../services/backend.service';
//--- MODELS
import { PatientCredential } from '../../models/PatientCredential.model';
//--- PAGES
// import { CommonPage } from '../../pages/common/common';
import { HomePage } from '../../pages/home/home.page';

@Component({
  selector: 'patient-record-page',
  templateUrl: 'patientrecord.page.html'
})
export class PatientRecordPage implements OnInit {
  public patientRecord: PatientCredential = new PatientCredential();
  public credentialForm: FormGroup;
  // private identificadorControl;
  // private nombreControl;

  constructor(
    protected navCtrl: NavController,
    protected storage: Storage,
    protected backendService: BackendService) {
  }

  ngOnInit() {
    console.log('>>[PatientRecordPage.ngOnInit]');
    // Initialize form controls and validation structures.
    this.credentialForm = new FormGroup({
      identificador: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]),
      tefContacto: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(14)]),
      nombre: new FormControl('', [Validators.minLength(6)])
    });
    // Read the current state of the patient record.
    this.storage.get(BackendService.CREDENTIAL_KEY)
      .then((data) => {
        if (null != data) {
          console.log('--[PatientRecordPage.ngOnInit]> Patient data: ' + data);
          this.patientRecord = new PatientCredential(JSON.parse(data));
        }
      });
  }
  //--- F O R M   V A L I D A T I O N
  public blurIdentificador(): void {
    console.log('>>[PatientRecordPage.blurIdentificador]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurIdentificador]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.storage.set(BackendService.CREDENTIAL_KEY, JSON.stringify(this.patientRecord));
    console.log('<<>[PatientRecordPage.blurIdentificador]');
  }
  public blurTefContacto(): void {
    console.log('>>[PatientRecordPage.blurTefContacto]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurTefContacto]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.storage.set(BackendService.CREDENTIAL_KEY, JSON.stringify(this.patientRecord));
    console.log('<<>[PatientRecordPage.blurTefContacto]');
  }
  public blurNombre(): void {
    console.log('>>[PatientRecordPage.blurNombre]');
    // Save the current data on the storage and check if the record is valid.
    console.log('--[PatientRecordPage.blurNombre]> Patient data: ' + JSON.stringify(this.patientRecord));
    this.storage.set(BackendService.CREDENTIAL_KEY, JSON.stringify(this.patientRecord));
    console.log('<<[PatientRecordPage.blurNombre]');
  }

  public savePatientData(): void {
    this.navCtrl.push(HomePage);
  }
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
  public isValid(): boolean {
    if (null != this.patientRecord) return this.patientRecord.isValid();
    else return false;
  }
  public test4Valid(field: string): boolean {
    if (!this.isNonEmptyString(field)) return false;
    else return true;
  }
}

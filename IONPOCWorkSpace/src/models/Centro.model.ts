//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- SERVICES
import { BackendService } from '../services/backend.service';
//--- MODELS
import { Node } from '../models/Node.model';
import { Especialidad } from '../models/Especialidad.model';

export class Centro extends Node {
  public id: number = -1;
  public nombre: string;
  public _especialidades: Especialidad[] = [];

  //--- C O N S T R U C T O R
  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Centro";
  }

  public addEspecialidad(esp: Especialidad): number {
    this._especialidades.push(esp);
    return this._especialidades.length;
  }

  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  public collaborate2View(backendService: BackendService): Node[] {
    let collab: Node[] = [];
    collab.push(this);
    for (let node of this._especialidades) {
      let partialcollab = node.collaborate2View(backendService);
      for (let partialnode of partialcollab) {
        collab.push(partialnode);
      }
    }
    return collab;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): number {
    return this.id;
  }
  public getNombre(): string {
    return this.nombre;
  }
}

//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- SERVICES
import { BackendService } from '../services/backend.service';

export class Node {
  public jsonClass: string = "Node";
  public expanded: boolean = false;
  public renderWhenEmpty: boolean = true;
  public selected: boolean = false;

  constructor(values: Object = {}) {
    Object.assign(this, values);
    this.jsonClass = "Node";
  }
  //--- I C O L L A B O R A T I O N   I N T E R F A C E
  public collaborate2View(backendService: BackendService): Node[] {
    let collab: Node[] = [];
    collab.push(this);
    return collab;
  }
  // //--- IEXPANDABLE INTERFACE
  // public isExpanded(): boolean {
  //   return this.expanded;
  // }
  // public collapse(): boolean {
  //   this.expanded = false;
  //   return this.expanded;
  // }
  // public expand(): boolean {
  //   this.expanded = true;
  //   return this.expanded;
  // }
  // public toggleExpanded() {
  //   this.expanded = !this.expanded;
  // }
  // public getContentsCount(): number {
  //   return 0;
  // }
  // //--- ISELECTABLE INTERFACE
  // public toggleSelected(): boolean {
  //   this.selected = !this.selected;
  //   return this.selected;
  // }
  //--- P R I V A T E   S E C T I O N
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }
}

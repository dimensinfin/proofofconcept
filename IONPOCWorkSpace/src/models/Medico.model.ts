//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- MODELS
import { Node } from '../models/Node.model';

export class Medico extends Node {
  public id: string = '';
  public referencia: string;
  public tratamiento: string;
  public nombre: string;
  public apellidos: string;
  public especialidad: string;
  public localidad: string;
  public direccion: string;
  public horario: string;
  public telefono: string;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Medico";
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): string {
    return this.id;
  }
  public getNombre(): string {
    return this.nombre;
  }
  public getEspecialidad(): string {
    if (this.isNonEmptyString(this.especialidad)) return this.especialidad;
    else return "-General-";
  }
}

//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- MODELS
import { Node } from '../models/Node.model';

export class Cita extends Node {
  public id: number = -1;
  public fecha: Date;
  public huecoId: number;
  public estado: string;
  public pacienteId: string;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Cita";
  }

  //--- G E T T E R S   &   S E T T E R S
  public getId(): number {
    return this.id;
  }
  public getHour(): number {
    return Math.round(this.huecoId / 100);
  }
  public getMinutes(): number {
    return this.huecoId - Math.round(this.huecoId / 100) * 100;
  }
}

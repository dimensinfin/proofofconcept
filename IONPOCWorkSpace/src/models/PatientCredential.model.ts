//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- MODELS
import { Node } from '../models/Node.model';

/**
 * This class defines the data attributes that should be collected from the patient and stored on the local srorage for patient identification. The appication currently support a single credential and stores the data for a single patient.
  * @param identificador is a abstract identifier that will serve the attending doctor to relate the patient to its internal records. The content and format is not specified and can be from the NIF or Passport to any other ientifier that is made unique for each patient.
  * @param nombre this is the patient's name. It is not used anywhere and it is only used to identify who is the owner of the abstract identifier on the local application.
 */
export class PatientCredential extends Node {
  public identificador: string;
  public tefContacto: number = 0;
  public nombre: string;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "PatientCredential";
  }

  //--- G E T T E R S   &   S E T T E R S
  public getIdentificador(): string {
    return this.identificador;
  }
  public getTelefonoContacto(): number {
    return this.tefContacto;
  }
  public getNombre(): string {
    if (null != this.nombre) return this.nombre;
    else return "-";
  }
  public isValid(): boolean {
    if (!this.isNonEmptyString(this.identificador)) return false;
    if (!this.isNonEmptyString(this.nombre)) return false;
    return true;
  }
}

//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
// import { tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
// import { flatMap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
//--- IONIC
import { Storage } from '@ionic/storage';
//--- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
//--- MODEL
import { Node } from '../models/Node.model';
import { Centro } from '../models/Centro.model';
import { Medico } from '../models/Medico.model';
import { Cita } from '../models/Cita.model';
import { PatientCredential } from '../models/PatientCredential.model';

/**
This service will store persistent application data and has the knowledge to get to the backend to retrieve any data it is requested to render on the view.
*/
@Injectable()
export class BackendService {
  //--- G L O B A L   C O N S T A N T S
  public static CREDENTIAL_KEY: string = "-CREDENTIAL-KEY-";
  public static RESOURCE_SERVICE_URL: string = "https://backcitamed.herokuapp.com/api/v1";
  // public static RESOURCE_SERVICE_URL: string = "http://localhost:9000/api/v1";

  //--- C A C H E   S T O R E
  private medicosSource = new BehaviorSubject([]);
  public medicos = this.medicosSource.asObservable();
  public currentMedico: Medico;
  public currentPaciente: PatientCredential;
  public currentCita: Cita;

  //--- C O N S T R U C T O R
  constructor(
    protected http: HttpClient,
    protected storage: Storage) { }

  //--- M O C K   S E C T I O N
  public getMockStatus(): boolean {
    return false;
  }
  // Define mock data references to input data on files.
  protected responseTable = {
    // CENTROS
    // '/api/v1/centros':
    //   '/assets/mockData/centros.json',
    // '/api/v1/centros/1000052/medicos':
    //   '/assets/mockData/proveedores-1000052.json'
  }

  //--- G L O B A L   F U C T I O N A L I T I E S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }

  //--- S T O R E   A C C E S S   S E C T I O N
  public setMedicos(medicos: Medico[]): void {
    this.medicosSource.next(medicos);
  }

  //--- B A C K E N D   C A L L S   A C C E S S O R S
  public getMedicalCenters(): Observable<Centro[]> {
    console.log("><[BackendService.getMedicalCenters]");
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/centros";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let centroList = this.transformRequestOutput(data.content) as Centro[];
        console.log("--[BackendService.getMedicalCenters]> Centro.count: " + centroList.length);
        return centroList;
      }));
  }
  /**
   * Gets the complete list of medical service providers from each of the available centers. This is the global list that will cached and shown to the caller services. The list first downloads the list of centers, then for each of them the list of providers and then we coalesce tll this information into a list that is then rendered to the view.
   * @return List of Centers with all the Specialties and then the Doctors.
   */
  public getMedicalServiceProviders(centerIdentifier: number): Observable<Medico[]> {
    console.log("><[BackendService.getMedicalServiceProviders]> centerIdentifier: " + centerIdentifier);
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/centros/" + centerIdentifier + "/medicos";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        let medicoList = this.transformRequestOutput(data) as Medico[];
        console.log("--[BackendService.getMedicalCenters]> Medico.count: " + medicoList.length);
        return medicoList;
      }));
  }
  public getAppoinmentList(providerIdentifier: string): Observable<Cita[]> {
    console.log("><[BackendService.getAppoinmentList]> providerIdentifier: " + providerIdentifier);
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/medicos/" + providerIdentifier + "/citas/nextfree/10";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data.content) as Cita[];
        console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }
  public blockAppointment(patient: PatientCredential, cita: Cita) {
    console.log("><[BackendService.blockAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/citas/" + cita.getId() + "/block/" + patient.getIdentificador();
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }
  public reserveAppointment(patient: PatientCredential, cita: Cita) {
    console.log("><[BackendService.reserveAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/citas/" + cita.getId() + "/reserve/" + patient.getIdentificador();
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[BackendService.reserveAppointment]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }
  public cancelAppointment(patient: PatientCredential, cita: Cita) {
    console.log("><[BackendService.cancelAppointment]> cita: " + JSON.stringify(cita));
    // Construct the request to call the backend.
    let request = BackendService.RESOURCE_SERVICE_URL + "/citas/" + cita.getId() + "/cancel/" + patient.getIdentificador();
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    return this.wrapHttpGETCall(request)
      .pipe(map((data: any) => {
        // Transform received data and return the node list to the caller for aggregation.
        let citaList = this.transformRequestOutput(data) as Cita[];
        console.log("--[BackendService.getMedicalCenters]> Cita.count: " + citaList.length);
        return citaList;
      }));
  }

  //--- P R I V A T E    S E C T I O N
  protected transformRequestOutput(entrydata: any): Node[] {
    let results: Node[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as Node;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      // if (jclass == "Exception") {
      //   let convertedException = new NeoComException(entrydata);
      //   console.log("--[AppModelStoreService.transformRequestOutput]> Exception node: " + convertedException.code);
      //   // Report the exception to the Notification
      //   this.toasterService.pop('error', convertedException.code, convertedException.message);
      //   return [];
      // } else {
      // // Return a single item converted.
      // return this.convertNode(entrydata as INeoComNode);
      // Process the entry data as an array.
      let list = [];
      list.push(entrydata);
      return this.transformRequestOutput(list);
      // }
    }
    return results;
  }
  protected convertNode(node): Node {
    switch (node.jsonClass) {
      case "Centro":
        let convertedCentro = new Centro(node);
        console.log("--[AppModelStoreService.convertNode]> Centro node: " + convertedCentro.getId());
        return convertedCentro;
      case "Medico":
        let convertedMedico = new Medico(node);
        console.log("--[AppModelStoreService.convertNode]> Medico node: " + convertedMedico.getId());
        return convertedMedico;
      case "Cita":
        let convertedCita = new Cita(node);
        console.log("--[AppModelStoreService.convertNode]> Cita node: " + convertedCita.getId());
        return convertedCita;
      // default:
      //   return new NeoComException().setMessage(JSON.stringify(node));
    }
  }
  protected wrapHttpGETCall(request: string): any {
    console.log("><[BackendService.wrapHttpGETCall]> request: " + request);
    // Generate the new headers to limit data access tampering. Time validity timer set to 15 minutes.
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get(request, { headers: headers })
  }
}

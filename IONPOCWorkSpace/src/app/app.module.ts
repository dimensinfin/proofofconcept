//  PROJECT:     CitaMed.paciente(CITM.ION)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Ionic 3.20.
//  DESCRIPTION: CitaMed. Sistema S3. Aplicacion Ionic con estructura muy similar a las aplicaciones Angular
//               que genera paquetes para Android e Ios. Este aplicativo es el que se debe ran instalar los
//               pacientes para poder acceder a la lista de servicios disponibles y poder reservar una cita
//               medica con los proceedores disponibles.
//--- CORE MODULES
import { NgModule } from '@angular/core';
//--- BROWSER
import { BrowserModule } from '@angular/platform-browser';
//--- HTTPCLIENT
import { HttpClientModule } from '@angular/common/http';
//--- IONIC
import { IonicApp } from 'ionic-angular';
import { IonicModule } from 'ionic-angular';
import { IonicErrorHandler } from 'ionic-angular';
//--- IONIC PLUGINS
import { IonicStorageModule } from '@ionic/storage';
//--- IONIC NATIVE COMPONENTS
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//--- SERVICES
import { ErrorHandler } from '@angular/core';
import { BackendService } from '../services/backend.service';
//--- CORE COMPONENTS
import { MyApp } from './app.component';
//--- PAGES
import { HomePage } from '../pages/home/home.page';
import { PatientRecordPage } from '../pages/patientrecord/patientrecord.page';
import { AvailableDoctorsPage } from '../pages/availabledoctors/availabledoctors.page';
import { ListAppointmentsPage } from '../pages/list-appointments/list-appointments.page';
import { AppointmentReservationPage } from '../pages/appointment-reservation/appointment-reservation.page';

@NgModule({
  imports: [
    //--- BROWSER
    BrowserModule,
    //--- HTTPCLIENT
    HttpClientModule,
    //--- IONIC
    IonicModule.forRoot(MyApp, {
      backButtonText: 'Retroceder',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      tabsPlacement: 'bottom',
      pageTransition: 'ios-transition'
    }),
    IonicStorageModule.forRoot(),
  ],
  declarations: [
    //--- CORE COMPONENTS
    MyApp,
    //--- PAGES
    HomePage,
    PatientRecordPage,
    AvailableDoctorsPage,
    ListAppointmentsPage,
    AppointmentReservationPage
  ],
  entryComponents: [
    //--- CORE COMPONENTS
    MyApp,
    //--- PAGES
    HomePage,
    PatientRecordPage,
    AvailableDoctorsPage,
    ListAppointmentsPage,
    AppointmentReservationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    BackendService
  ],
  bootstrap: [IonicApp]
})
export class AppModule { }

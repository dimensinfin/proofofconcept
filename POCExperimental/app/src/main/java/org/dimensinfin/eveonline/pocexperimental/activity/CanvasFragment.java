package org.dimensinfin.eveonline.pocexperimental.activity;

import org.dimensinfin.android.mvc.activity.ASimplePagerFragment;
import org.dimensinfin.android.mvc.datasource.IDataSource;
import org.dimensinfin.android.mvc.interfaces.IControllerFactory;
import org.dimensinfin.eveonline.pocexperimental.datasource.StorageContentDataSource;

public class CanvasFragment extends ASimplePagerFragment {
    @Override
    public IControllerFactory createFactory() {
        return new POCExperimentalFactory(this.getVariant());
    }

    @Override
    public IDataSource createDS() {
        return new StorageContentDataSource.Builder()
                .addIdentifier("STORAGE")
                .withExtras(this.getExtras())
                .withFactory(this.getFactory())
                .withVariant(this.getVariant())
                .build();
    }

    @Override
    public String getSubtitle() {
        return null;
    }

    @Override
    public String getTitle() {
        return null;
    }
}

package org.dimensinfin.eveonline.pocexperimental.activity;

import android.os.Bundle;

import org.dimensinfin.android.mvc.activity.AMVCMultiPageActivity;

public class StorageTestActivity extends AMVCMultiPageActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.addPage(new CanvasFragment());
    }
}

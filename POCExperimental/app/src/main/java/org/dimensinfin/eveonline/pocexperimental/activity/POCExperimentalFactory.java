package org.dimensinfin.eveonline.pocexperimental.activity;

import org.dimensinfin.android.mvc.controller.ControllerFactory;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.core.interfaces.ICollaboration;
import org.dimensinfin.eveonline.pocexperimental.StorageData;
import org.dimensinfin.eveonline.pocexperimental.controller.StorageDataController;

public class POCExperimentalFactory extends ControllerFactory {
    public POCExperimentalFactory(String selectedVariant) {
        super(selectedVariant);
    }

    @Override
    public IAndroidController createController(ICollaboration node) {
        if ( node instanceof StorageData)
            return new StorageDataController((StorageData) node, this);
        return super.createController(node);
    }
}

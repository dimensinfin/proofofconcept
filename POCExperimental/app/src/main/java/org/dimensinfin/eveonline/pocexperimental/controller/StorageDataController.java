package org.dimensinfin.eveonline.pocexperimental.controller;

import android.content.Context;

import org.dimensinfin.android.mvc.controller.AAndroidController;
import org.dimensinfin.android.mvc.controller.IAndroidController;
import org.dimensinfin.android.mvc.interfaces.IControllerFactory;
import org.dimensinfin.android.mvc.interfaces.IRender;
import org.dimensinfin.android.mvc.render.AMVCRender;
import org.dimensinfin.eveonline.pocexperimental.StorageData;

public class StorageDataController extends AAndroidController<StorageData> {
    public StorageDataController(StorageData model, IControllerFactory factory) {
        super(model, factory);
    }

    @Override
    public IRender buildRender(Context context) {
        return new StorageDataRender(this, context);
    }

    public static class StorageDataRender extends AMVCRender {

        public StorageDataRender(IAndroidController controller, Context context) {
            super(controller, context);
        }

        @Override
        public StorageDataController getController() {
            return (StorageDataController) super.getController();
        }

        @Override
        public int accessLayoutReference() {
            return 0;
        }

        @Override
        public void initializeViews() {

        }

        @Override
        public void updateContent() {

        }
    }
}

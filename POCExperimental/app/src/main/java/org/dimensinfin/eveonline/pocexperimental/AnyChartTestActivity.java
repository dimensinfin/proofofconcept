package org.dimensinfin.eveonline.pocexperimental;

import android.app.Activity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adam on 06/06/2018.
 */

public class AnyChartTestActivity extends Activity {
	// - S T A T I C - S E C T I O N ..........................................................................
	private static Logger logger = LoggerFactory.getLogger("AppDashboardActivity");

	@Override
	public void onCreate( Bundle savedInstanceState ) {
		logger.info(">> [BarGraphTestActivity.onCreate]");
		super.onCreate(savedInstanceState);
		// Set the layout to the core activity that defines the background and the fragment container, this time one on top of another and
		// not in different pages.
		try {
			this.setContentView(R.layout.activity_bargraph);

			// Locate the elements of the page and store in global data.
			BarChart chart = (BarChart) findViewById(R.id.chart);
			chart.setBackgroundColor(getResources().getColor(R.color.appblack));
			Description desc = new Description();
			desc.setText("Mining extractions for today.");
			desc.setTextColor(R.color.appwhite);
			chart.setDescription(desc);
//			chart.setDescriptionColor(getResources().getColor(R.color.appwhite));
			List<BarEntry> entries = new ArrayList<BarEntry>();

			entries.add(new BarEntry(1, 1234));
			entries.add(new BarEntry(2, 2345));
			entries.add(new BarEntry(3, 3456));
			entries.add(new BarEntry(4, 4567));

			BarDataSet dataSet = new BarDataSet(entries, "Mining Extractions");
			dataSet.setColor(R.color.colorPrimary);

			BarData lineData = new BarData(dataSet);
			chart.setData(lineData);
			chart.invalidate();
		} catch (RuntimeException rtex) {
			logger.warn("RTEX [BarGraphTestActivity.onCreate]> " + rtex.getMessage());
			rtex.printStackTrace();
		}
		logger.info("<< [BarGraphTestActivity.onCreate]");
	}
}

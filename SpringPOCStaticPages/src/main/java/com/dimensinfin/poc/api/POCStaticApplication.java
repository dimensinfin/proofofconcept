package com.dimensinfin.poc.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class POCStaticApplication {
    public static void main(final String[] args) {
        SpringApplication.run(POCStaticApplication.class, args);
    }
}

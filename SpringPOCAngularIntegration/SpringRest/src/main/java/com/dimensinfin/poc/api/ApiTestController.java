package com.dimensinfin.poc.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ApiTestController {
    @GetMapping(path = "/api/v1/test")
    public ResponseEntity<String> testApicall() {
        return new ResponseEntity("test endpoint called.", HttpStatus.OK);
    }
}

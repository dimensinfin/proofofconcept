package com.dimensinfin.poc.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class POCStaticApplication {
    public static void main(final String[] args) throws Exception {
        SpringApplication.run(POCStaticApplication.class, args);
//        RatpackServer.start(server -> server.handlers(chain -> chain
//                .get(ctx -> ctx.render("Welcome to Baeldung ratpack!!!"))));
    }
}

package org.dimensinfin.poc.android;

import android.view.View;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.rule.ActivityTestRule;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

public class MainActivityTest {
	private static final String MESSAGE_CONTENT = "Test Message";

	static {
		System.setProperty("org.mockito.android.target", ApplicationProvider.getApplicationContext().getCacheDir().getPath());
	}

	@Rule
	public ActivityTestRule<MainActivity> mActivityRule =
			new ActivityTestRule<MainActivity>(MainActivity.class);

	@Test
	public void check_messageContents() {
		final View content = this.mActivityRule.getActivity().findViewById(R.id.content);
		Assert.assertNotNull(content);
//		if (content instanceof TextView) {
//			((TextView) content).setText(MESSAGE_CONTENT);
//			final String expected = MESSAGE_CONTENT;
//			final CharSequence obtained = ((TextView) content).getText();
//			Assert.assertEquals(expected, obtained);
//		}
	}
}
package org.dimensinfin.eveonline.pocdrawerstart.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.dimensinfin.eveonline.pocdrawerstart.R;

/**
 * Created by Adam on 04/01/2018.
 */

public class FirstActivity extends Activity {
	private String[] mPlanetTitles;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;

	private FrameLayout content;
	private TextView destinationAction;

	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.drawerlayout);

		mPlanetTitles = getResources().getStringArray(R.array.planets_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		content = (FrameLayout) findViewById(R.id.content_frame);

		// Instantiate the content for this application from the app layout.
		final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		final View appLayout = inflater.inflate(R.layout.activity_first, null);
		destinationAction = (TextView) appLayout.findViewById(R.id.destinationAction);
		content.addView(appLayout);

		// Set the adapter for the list view
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.draweritem4list, mPlanetTitles));
		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
	}

	private class DrawerItemClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick (AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}

	/** Chnage the content of the Destination action to the drawer item. */
	private void selectItem (int position) {
		// Highlight the selected item, update the title, and close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(mPlanetTitles[position]);
		destinationAction.setText(mPlanetTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	public void setTitle (CharSequence title) {
		final CharSequence mTitle = title;
		getActionBar().setTitle(mTitle);
	}
	public void secondActivity(View v){
		final Intent intent = new Intent(this, SecondActivity.class);
		// Add all the identification chain as in the backend Angular calls. This is Login/Pilot
		startActivity(intent);
	}
}

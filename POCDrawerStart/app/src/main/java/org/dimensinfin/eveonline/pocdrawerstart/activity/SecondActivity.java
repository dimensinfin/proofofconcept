package org.dimensinfin.eveonline.pocdrawerstart.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import org.dimensinfin.eveonline.pocdrawerstart.R;

/**
 * Created by Adam on 04/01/2018.
 */

public class SecondActivity extends Activity {
	@Override
	public void onCreate (@Nullable final Bundle savedInstanceState, @Nullable final PersistableBundle persistentState) {
		super.onCreate(savedInstanceState, persistentState);
		setContentView(R.layout.activity_second);
		getActionBar().setTitle("Second Activity");
	}
}

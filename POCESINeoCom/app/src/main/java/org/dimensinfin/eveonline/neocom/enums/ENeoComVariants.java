//	PROJECT:        NeoCom.model (NEOC.M)
//	AUTHORS:        Adam Antinoo - adamantinoo.git@gmail.com
//	COPYRIGHT:      (c) 2013-2017 by Dimensinfin Industries, all rights reserved.
//	ENVIRONMENT:		Android API16.
//	DESCRIPTION:		Isolated model structures to access and manage Eve Online character data and their
//									available databases.
//									This version includes the access to the latest 6.x version of eveapi libraries to
//									download ad parse the CCP XML API data.
//									Code integration that is not dependent on any specific platform.
package org.dimensinfin.eveonline.neocom.enums;

// - CLASS IMPLEMENTATION ...................................................................................
public enum ENeoComVariants {
	SPLASH_SCREEN, CAPSULEER_LIST, NEW_LOGIN, PILOT_DETAILS, ASSETS_BYLOCATION, PLANETARY_BYLOCATION, PLANETARY_OPTIMIZATION, PILOT_HEADER, PILOT_MANAGERS, ASSETS_BYCATEGORY, ASSETS_MATERIALS
}

// - UNUSED CODE ............................................................................................

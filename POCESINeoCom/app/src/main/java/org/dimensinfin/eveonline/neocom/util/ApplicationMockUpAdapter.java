package org.dimensinfin.eveonline.neocom.util;


import com.j256.ormlite.dao.Dao;

import org.dimensinfin.eveonline.neocom.model.Credential;

import java.util.List;
import java.util.Vector;

/**
 * Created by Adam on 09/01/2018.
 */

public class ApplicationMockUpAdapter {
	public static class DBConnector {

		public List<Credential> accessAllAccounts () {
			return new Vector<Credential>();
		}

		public Dao<Credential, String> getCredentialDao () {
			return null;
		}
	}

	private static final ApplicationMockUpAdapter _singleton = new ApplicationMockUpAdapter();

	public static ApplicationMockUpAdapter getSingleton () {
		return _singleton;
	}

	// - F I E L D - S E C T I O N ............................................................................
	private DBConnector dbConnector = new DBConnector();

	// - C O N S T R U C T O R - S E C T I O N ................................................................
	protected ApplicationMockUpAdapter () {
		super();
	}

	public DBConnector getDBConnector () {
		return dbConnector;
	}
}

import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
//--- TOAST NOTIFICATIONS
// import { ToasterModule } from 'angular5-toaster';
// import { ToasterContainerComponent } from 'angular5-toaster';
// import { ToasterService } from 'angular5-toaster';

import { AppComponent } from './app.component';
import { UIModule } from './modules/ui/ui.module';
import { NeoComModelsModule } from './modules/neocom-models/neocom-models.module';
import { FittingModule } from './modules/fitting/fitting.module';
import { AssetsModule } from './modules/assets/assets.module';
import { MenuBarModule } from './modules/menubar/menubar.module';
import { IndustryModule } from './modules/industry/industry.module';
import { IncubationModule } from './modules/incubation/incubation.module';
import { PlanetaryModule } from './modules/planetary/planetary.module';

//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';

fdescribe('AppComponent', () => {
  let originalTimeout: number;

  beforeEach(function() {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        RouterTestingModule,

        // AppRoutingModule,
        // NgDragDropModule.forRoot(),
        // ToasterModule,
        // OAuthModule.forRoot(),
        // InlineEditorModule,
        //--- APPLICATION MODULES
        UIModule,
        NeoComModelsModule,
        FittingModule,
        AssetsModule,
        MenuBarModule,
        IndustryModule,
        IncubationModule,
        PlanetaryModule
      ],
      providers: [AppModelStoreService]
    }).compileComponents();
  });
  // Change the default respose timeout.
  beforeEach(function() {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  // it(`should have as title 'app'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('app');
  // }));
  // it('should render title in a h1 tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  // }));
  // Set back to original values the attributes changed.
  afterEach(function() {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });
});

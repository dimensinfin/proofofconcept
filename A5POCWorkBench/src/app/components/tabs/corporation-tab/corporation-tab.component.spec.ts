import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporationTabComponent } from './corporation-tab.component';

describe('CorporationTabComponent', () => {
  let component: CorporationTabComponent;
  let fixture: ComponentFixture<CorporationTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporationTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporationTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

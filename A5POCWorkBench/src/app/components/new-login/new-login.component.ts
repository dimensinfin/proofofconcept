//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import { ROUTER_DIRECTIVES } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- INTERFACES
// import { PageComponent } from '../../../classes/PageComponent';
//--- SERVICES
// import { AppModelStoreService } from '../../../services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
// import { ComponentFactoryComponent } from '../../factory/component-factory/component-factory.component';
//--- MODELS
// import { Login } from '../../../models/Login.model';
// import { NeoComNode } from '../../../models/NeoComNode.model';

@Component({
  selector: 'app-new-login',
  templateUrl: './new-login.component.html',
  styleUrls: ['./new-login.component.scss']
  // directives: [ROUTER_DIRECTIVES]
})
export class NewLoginComponent extends NeoComNodeComponent {
}

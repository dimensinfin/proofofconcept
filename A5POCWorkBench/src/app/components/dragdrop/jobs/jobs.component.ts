//  PROJECT:     A5POC (A5POC)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5
//  DESCRIPTION: Proof of concept projects.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- SERVICES
import { AppModelStoreService } from '../../../services/app-model-store.service';
//--- INTERFACES
//--- MODELS
import { Job } from '../../../models/Job.model';

/**
This component will show the list of Industry jobs. It will use a mock up for the list of jobs but will implement the core basics that are in use on the NeoCom project and that define the current Data Source interface.
*/
@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})

export class JobsComponent {
  @Input() dataSource: Job;

  public getPanelComponents(): Job[] {
    // Create some default jobs to be shown on the demo
    let contents: Job[] = [];
    contents.push(new Job({ jobId: 1, name: "Job Number 1", description: "This is the description for the first job." }))
    return contents;
  }
  public getViewer(): JobsComponent {
    return this;
  }
}

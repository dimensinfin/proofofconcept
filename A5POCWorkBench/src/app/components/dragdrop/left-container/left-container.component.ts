//  PROJECT:     A5POC (A5POC)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5
//  DESCRIPTION: Proof of concept projects.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { DropEvent } from 'ng-drag-drop';
//--- SERVICES
import { AppModelStoreService } from '../../../services/app-model-store.service';
//--- INTERFACES
//--- MODELS
import { Action } from '../../../models/Action.model';

@Component({
  selector: 'app-left-container',
  templateUrl: './left-container.component.html',
  styleUrls: ['./left-container.component.scss']
})

export class LeftContainerComponent /*implements OnInit*/ {
  @Input() dragData: any;

  vegetables = [
    { name: 'Carrot', type: 'vegetable' },
    { name: 'Onion', type: 'vegetable' },
    { name: 'Potato', type: 'vegetable' },
    { name: 'Capsicum', type: 'vegetable' }];

  fruits = [
    { name: 'Apple', type: 'fruit' },
    { name: 'Orange', type: 'fruit' },
    { name: 'Mango', type: 'fruit' },
    { name: 'Banana', type: 'fruit' }];

  actions = [
    new Action({ jobId: 1, name: "Action MOVE", description: "This is the description for the first job.", type: 'fruit' }),
    new Action({ jobId: 2, name: "Action BUY", description: "This is the description for the first job.", type: 'fruit' }),
    new Action({ jobId: 3, name: "Action MANUFACTURE", description: "This is the description for the first job.", type: 'fruit' }),
    new Action({ jobId: 4, name: "Action INVENTION", description: "This is the description for the first job.", type: 'fruit' })
  ];

  droppedFruits = [];
  droppedVegetables = [];
  droppedItems = [];
  fruitDropEnabled = true;
  dragEnabled = true;

  public getPanelComponents(): Action[] {
    // Create some default jobs to be shown on the demo
    let contents: Action[] = [];
    contents.push(new Action({ jobId: 1, name: "Action Manufacture", description: "This is the description for the first job." }))
    contents.push(new Action({ jobId: 2, name: "Action Move", description: "This is the description for the first job." }))
    contents.push(new Action({ jobId: 2, name: "Action BUY", description: "This is the description for the first job." }))
    return contents;
  }
  public getViewer(): LeftContainerComponent {
    return this;
  }
  onFruitDrop(e: DropEvent) {
    this.droppedFruits.push(e.dragData);
    this.removeItem(e.dragData, this.fruits);
  }

  onVegetableDrop(e: DropEvent) {
    this.droppedVegetables.push(e.dragData);
    this.removeItem(e.dragData, this.vegetables);
  }

  onAnyDrop(e: DropEvent) {
    this.droppedItems.push(e.dragData);

    if (e.dragData.type === 'vegetable') {
      this.removeItem(e.dragData, this.vegetables);
    } else {
      this.removeItem(e.dragData, this.fruits);
    }
  }

  removeItem(item: any, list: Array<any>) {
    let index = list.map(function(e) {
      return e.name
    }).indexOf(item.name);
    list.splice(index, 1);
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
// import { IViewer } from 'src/app/interfaces/IViewer.interface';
// import { INeoComNode } from 'src/app/interfaces/INeoComNode.interface';
// import { IColored } from 'src/app/interfaces/IColored.interface';
// import { EThemeSelector } from 'src/app/interfaces/EPack.enumerated';
//--- MODELS
import { Pilot } from '../../models/Pilot.model';
// import { ColorTheme } from '../../../uimodels/ColorTheme.model';

/**
*/
@Component({
  selector: 'neocom-pilot-sheet',
  templateUrl: './pilot-sheet.component.html',
  styleUrls: ['./pilot-sheet.component.scss']
})
export class PilotSheetComponent {
  @Input() pilot: Pilot;

  //--- ICOLORED INTERFACE

  //--- GETTERS & SETTERS
}

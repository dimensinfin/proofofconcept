import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilotSheetComponent } from './pilot-sheet.component';

describe('PilotSheetComponent', () => {
  let component: PilotSheetComponent;
  let fixture: ComponentFixture<PilotSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilotSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
// /**
// This declares the types of colored panels that are defined. Used by the Color themes to set a group of visual properties.
// */
// export enum EThemeSelector {
//   RED, ORANGE, YELLOW, GREEN, BLUE, WHITE
// }

/**
Declares the types of Separators. They are an extension of the Color Theme decause some of them carry some functionality.
*/
export enum ESeparator {
  RED = "RED", ORANGE = "ORANGE", YELLOW = "YELLOW", GREEN = "GREEN", BLUE = "BLUE", WHITE = "WHITE", BLACK = "BLACK"
  , EMPTY = "EMPTY", SPINNER = "SPINNER"
}

/**
Declares the different display indetifier. Ths is the equivalent for the Fragments on the Android platform and its use is to allow a different 'collaboration2View' depeding ont the display identifier.
*/
export enum EVariant {
  DEFAULT, CREDENTIALLIST, LOGINLIST
  , PILOTDETAILS, PILOTROASTER, PILOTINFORMATION
  , PILOTMANAGERS, ASSETSMANAGER, PLANETARYMANAGER, PLANETARYOPTIMIZATION
  , FITTING_MANAGER, FITTINGLIST, FITTING_ACTIONS_BYCLASS
  , ASSETS_BY_LOCATION
}

export enum ELocationType {
  CCPLOCATION, CITADEL, UNKNOWN, EMPTY, STATION, SOLAR_SYSTEM, OTHER
}
export enum ELocationFlag {
  ASSETSAFETY, AUTOFIT
  , CARGO, CORPSEBAY, DELIVERIES, DRONEBAY, FIGHTERBAY, FLEETHANGAR, HANGAR, HANGARALL, SUBSYSTEMBAY
  , FIGHTERTUBE0, FIGHTERTUBE1, FIGHTERTUBE2, FIGHTERTUBE3, FIGHTERTUBE4
  , HISLOT0, HISLOT1, HISLOT2, HISLOT3, HISLOT4, HISLOT5, HISLOT6, HISLOT7
  , HIDDENMODIFIERS, IMPLANT
  , LOSLOT0, LOSLOT1, LOSLOT2, LOSLOT3, LOSLOT4, LOSLOT5, LOSLOT6, LOSLOT7
  , MEDSLOT0, MEDSLOT1, MEDSLOT2, MEDSLOT3, MEDSLOT4, MEDSLOT5, MEDSLOT6, MEDSLOT7
  , RIGSLOT0, RIGSLOT1, RIGSLOT2, RIGSLOT3, RIGSLOT4, RIGSLOT5, RIGSLOT6, RIGSLOT7, SHIPHANGAR, SKILL, SPECIALIZEDAMMOHOLD, SPECIALIZEDCOMMANDCENTERHOLD, SPECIALIZEDFUELBAY, SPECIALIZEDGASHOLD, SPECIALIZEDINDUSTRIALSHIPHOLD, SPECIALIZEDLARGESHIPHOLD, SPECIALIZEDMATERIALBAY, SPECIALIZEDMEDIUMSHIPHOLD, SPECIALIZEDMINERALHOLD, SPECIALIZEDOREHOLD, SPECIALIZEDPLANETARYCOMMODITIESHOLD, SPECIALIZEDSALVAGEHOLD, SPECIALIZEDSHIPHOLD, SPECIALIZEDSMALLSHIPHOLD
  , SUBSYSTEMSLOT0, SUBSYSTEMSLOT1, SUBSYSTEMSLOT2, SUBSYSTEMSLOT3, SUBSYSTEMSLOT4, SUBSYSTEMSLOT5, SUBSYSTEMSLOT6, SUBSYSTEMSLOT7
  , UNLOCKED, WARDROBE, LOCKED, QUAFEBAY
}
export enum EOrderState {
  OPEN, CLOSED, EXPIRED, CANCELLED, PENDING, SCHEDULED
}

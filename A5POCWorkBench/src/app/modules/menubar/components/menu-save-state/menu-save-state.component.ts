import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'neocom-menu-save-state',
  templateUrl: './menu-save-state.component.html',
  styleUrls: ['./menu-save-state.component.scss']
})
export class MenuSaveStateComponent {
  public isSaved() {
    return false;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuSaveStateComponent } from './menu-save-state.component';

describe('MenuSaveStateComponent', () => {
  let component: MenuSaveStateComponent;
  let fixture: ComponentFixture<MenuSaveStateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuSaveStateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuSaveStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

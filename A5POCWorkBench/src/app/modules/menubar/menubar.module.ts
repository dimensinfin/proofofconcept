//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTP CLIENT
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
//--- SERVICES
//--- PIPES
//--- COMPONENTS-MODULE
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { MenuSaveStateComponent } from './components/menu-save-state/menu-save-state.component';
import { MenuItemComponent } from './components/menu-item/menu-item.component';
//--- COMPONENTS-DETAILED

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MenuBarComponent,
    MenuSaveStateComponent,
    MenuItemComponent
  ],
  exports: [
    MenuBarComponent,
    MenuSaveStateComponent,
    MenuItemComponent
  ]
})
export class MenuBarModule { }

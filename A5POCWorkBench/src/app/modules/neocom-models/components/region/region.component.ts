//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from '../../../../modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from '../../../../modules/neocom-models/components/expandable.component';
//--- MODELS
import { Region } from '../../../../models/Region.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';

@Component({
  selector: 'neocom-region',
  templateUrl: './region.component.html',
  styleUrls: ['./region.component.scss']
})
export class RegionComponent extends ExpandableComponent {
  @Input() node: Region;
  @Input() variant: string = "STANDARD";

  constructor() {
    super();
    // this.themeColor = new ColorTheme(ESeparator.RED);
  }
  //--- UI MODIFIERS
  public getIconUrl(): string {
    return new UIIconReference('region').getReference()
  }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  public clickArrow() {
    this.node.toggleExpanded();
    this.viewer.notifyDataChanged();
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
import { INeoComNode } from '../../../interfaces/INeoComNode.interface';
import { IColored } from '../../../interfaces/IColored.interface';
import { ESeparator } from '../../../interfaces/EPack.enumerated';
//--- COMPONENTS
import { NeoComNodeComponent } from '../components/neocomnode.component';
//--- MODELS
import { NeoComNode } from '../../../models/NeoComNode.model';
// import { ColorTheme } from '../../../uimodels/ColorTheme.model';

/**
This class represents the functionalities that are required for most of the nodes rendered on the interface. There are simple nodes and expandable nodes that react to the right arrow click event. Expandable nodes require access to the page generator to broadcast the data set changed events.
*/
@Component({
  selector: 'notused-expandable',
  templateUrl: './notused.html'
})
export class ExpandableComponent extends NeoComNodeComponent {
  // @Input() viewer: IViewer;
  @Input() node: NeoComNode;

  /**
  Toggle the expand collapse status for the underlying node. This changes the expanded attribute and also indicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  public clickArrow() {
    this.node.toggleExpanded();
    this.viewer.notifyDataChanged();
  }

  //--- I C O L O R E D   I N T E R F A C E
  /** For expandable node return the theme color for expanded if expanded or the default if not. */
  public getExpandedTint(): string {
    if (this.isExpanded()) return this._themeColor.getExpandedTint();
    return "#080808";
  }
  //--- G E T T E R S   &   S E T T E R S
  public isExpanded(): boolean {
    if (null != this.node) return this.node.isExpanded();
    else return false;
  }
  public isExpandable(): boolean {
    return true;
  }
}

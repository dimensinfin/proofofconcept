//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
import { IViewer } from 'app/interfaces/IViewer.interface';
//--- SERVICES
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- COMPONENTS
import { ExpandableComponent } from '../expandable.component';
//--- MODELS
import { Location } from '../../../../models/Location.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';

@Component({
  selector: 'neocom-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent extends ExpandableComponent {
  @Input() node: Location;
  public variant: string = "-DEFAULT-";

  constructor() {
    super();
    fontawesome.library.add(faChevronLeft, faChevronRight, faChevronUp, faChevronDown);
    // this.themeColor = new ColorTheme(ESeparator.ORANGE);
    this.setMenuFlag(true);
  }

  //--- GETTERS & SETTERS
  public getIconUrl(): string {
    return this.node.urlLocationIcon;
  }

  /**
  If the Panel has some specific attributes they should be tested for display. The current actions are the Menu and the Expand Arrow.
  Locations have menu onlt on Planetary environments. Remove the shown when we are in other Managers.
  */
  // public hasMenu(): boolean {
  //   // Check the Page variant to see if we show the menu or not.
  //   if (this.viewer.getVariant() == EVariant.PLANETARYMANAGER)
  //     return true;
  //   else return false;
  // }
  // public isExpandable(): boolean {
  //   return true;
  // }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  // public clickArrow() {
  //   this.node.toggleExpanded();
  //   this.viewer.refreshViewPort();
  // }
  public getIdentifier(): number {
    return this.getLocationId();
  }
  // public toggleExpanded() {
  //   this.node.toggleExpanded();
  //   this.viewer.refreshViewPort();
  // }
  // public getLoginId(): string {
  //   return this.appModelStore.accessLogin().getLoginId();
  // }
  // public getCharacterId() {
  //   return this.viewer.pilot.getId()
  // }
  public getLocationId() {
    if (null != this.node) {
      let location = this.node as Location;
      return location.getLocationId();
    } else return -1;
  }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  public clickArrow() {
    this.node.toggleExpanded();
    this.viewer.notifyDataChanged();
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from '../neocomnode.component';
import { ExpandableComponent } from '../expandable.component';
//--- MODELS
import { Contract } from '../../../../models/Contract.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';


@Component({
  selector: 'neocom-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.scss']
})
export class ContractComponent extends ExpandableComponent implements OnInit {
  @Input() node: Contract;

  constructor() {
    super();
    // this.themeColor = new ColorTheme(ESeparator.BLUE);
  }
  ngOnInit() {
    // By default this item should be expanded so change the initial state.
    if (null != this.node) this.node.expand();
    this.adjustStateStyles();
  }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    return new UIIconReference("contracts").getReference();
  }
  public adjustStateStyles() {
    if (this.isExpanded()) {
      this.stateStyles = { 'background-color': '#052406' };
      this.stateStyles['background-color'] = this.getExpandedTint();
    }
    // if (this.node.taskType == 'AVAILABLE') this.stateStyles = { 'background-color': '#052406' };
    // if (this.node.taskType == 'MOVE') {
    //   // There can be some sub states.
    //
    // }
  }
}

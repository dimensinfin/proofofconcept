//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
import { IViewer } from 'app/interfaces/IViewer.interface';
// import { IMenuBar0 } from 'app/interfaces/IMenuBar0.interface';
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
import { IColored } from 'app/interfaces/IColored.interface';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { ColorTheme } from 'app/models/ui/ColorTheme.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';


/**
This class represents the requirements requestd by the simplest node to be rendered on node containers.
*/
@Component({
  selector: 'notused-neocomnode',
  templateUrl: './notused.html'
})
export class NeoComNodeComponent implements IColored/*, IMenuBar */ {
  @Input() viewer: IViewer;
  public stateStyles: any;
  // public securityLevel: string = ''; // Color style to set the font color for station security levels


  private _hasMenuFlag: boolean = false;
  private _menuExpanded: boolean = false;
  protected _inside: boolean = false;
  private _scale = "ISK";
  protected _menuContents: MenuSection[] = [];

  //--- I C O L O R E D   I N T E R F A C E
  // public stateColor: string = '';
  public _themeColor: ColorTheme = new ColorTheme(ESeparator.WHITE);

  public getPanelColor(): string {
    return this._themeColor.getPanelColor();
  }
  /** For non expandable components the background color is ever the 'panel-decorator-collapsed' color. */
  public getExpandedTint(): string {
    return "#080808";
  }
  public setTheme(newtheme: ColorTheme): void {
    this._themeColor = newtheme;
  }
  // public getSecurityLevelColor(level: number): string {
  //   let securityColor = '#F00000';
  //   if (level >= 1.0) securityColor = '#D73000';
  //   if (level >= 2.0) securityColor = '#F04800';
  //   if (level >= 3.0) securityColor = '#F06000';
  //   if (level >= 4.0) securityColor = '#D77700';
  //   if (level >= 5.0) securityColor = '#EFEF00';
  //   if (level >= 6.0) securityColor = '#8FEF2F';
  //   if (level >= 7.0) securityColor = '#00F000';
  //   if (level >= 8.0) securityColor = '#00EF47';
  //   if (level >= 9.0) securityColor = '#48F0C0';
  //   if (level >= 9.9) securityColor = '#2FEFEF';
  //   return securityColor;
  // }
  //--- I M E N U   I N T E R F A C E
  public panelEnter(): void {
    this._inside = true;
  }
  public panelExit(): void {
    this._inside = false;
  }
  public ifInside(): boolean {
    return this._inside;
  }
  public hasMenu(): boolean {
    return this._hasMenuFlag;
  }
  public activateMenu(): void {
    this._hasMenuFlag = true;
  }
  public toggleMenu(): boolean {
    this._menuExpanded = !this._menuExpanded;
    return this._menuExpanded;
  }
  public isMenuExpanded(): boolean {
    return this._menuExpanded;
  }
  public setMenuFlag(newflag: boolean): boolean {
    this._hasMenuFlag = newflag;
    return this._hasMenuFlag;
  }
  // public getMenuSupporter(): IMenuBar0 {
  //   return this;
  // }
  // public getMenuItems(): IMenuItem[] {
  //   return this._menuContents;
  // }
  public getMenuSections(): MenuSection[] {
    return this._menuContents;
  }

  //--- UI MODIFIERS
  public getIcon4TypeId(typeId: number): string {
    return "http://image.eveonline.com/Type/" + typeId + "_64.png";
  }

  //--- ISK SCALE INTERFACE
  public scaledPrice(value: number): number {
    if (value == null) return 0.0;
    let optimizedValue: number = value;
    this._scale = "ISK";
    if (value > 2000000) {
      optimizedValue = value / 1000.0;
      this._scale = "kISK";
    }
    if (value > 130000000) {
      optimizedValue = value / 1000000.0;
      this._scale = "MISK";
    }
    if (value > 1400000000) {
      optimizedValue = value / 1000000000.0;
      this._scale = "BISK";
    }
    return optimizedValue;
  }
  public scale(): string {
    return this._scale;
  }

  //--- G E T T E R S   &   S E T T E R S
  public isExpanded(): boolean {
    return false;
  }
  public isExpandable(): boolean {
    return false;
  }
  public getViewer(): IViewer {
    return this.viewer;
  }
}

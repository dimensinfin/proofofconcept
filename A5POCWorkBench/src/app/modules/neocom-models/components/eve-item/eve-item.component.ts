//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from '../neocomnode.component';
//--- MODELS
import { EveItem } from '../../../../models/EveItem.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';

@Component({
  selector: 'neocom-eve-item',
  templateUrl: './eve-item.component.html',
  styleUrls: ['./eve-item.component.scss']
})
export class EveItemComponent extends NeoComNodeComponent implements OnInit {
  @Input() node: EveItem;
  public stateStyles;

  constructor() {
    super();
    // this.themeColor = new ColorTheme(ESeparator.RED);
  }
  ngOnInit() {
    // this.adjustStateStyles();
  }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    return "http://image.eveonline.com/Type/" + this.node.typeId + "_64.png";
  }

  //--- GETTERS & SETTERS
}

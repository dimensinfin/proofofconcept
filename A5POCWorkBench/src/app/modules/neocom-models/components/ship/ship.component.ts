//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- SERVICES
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from '../../components/neocomnode.component';
import { ExpandableComponent } from '../../components/expandable.component';
//--- INTERFACES
//--- MODELS
import { Ship } from '../../../../models/Ship.model';

@Component({
  selector: 'neocom-ship',
  templateUrl: './ship.component.html',
  styleUrls: ['./ship.component.scss']
})
export class ShipComponent extends ExpandableComponent {
  @Input() node: Ship;

  public getIconUrl(): string {
    return "http://image.eveonline.com/Type/" + this.node.getTypeId() + "_64.png";
  }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  public clickArrow() {
    this.node.toggleExpanded();
    this.viewer.notifyDataChanged();
  }
  public totalValueIsk(): number {
    let value = this.node.getItem().getPrice() * this.node.quantity;
    return value;
  }
  public totalVolume(): number {
    let value = 0;
    //  let value = this.node.item.volume * this.node.quantity;
    return value;
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- SERVICES
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from '../../components/neocomnode.component';
import { ExpandableComponent } from '../../components/expandable.component';
//--- INTERFACES
//--- MODELS
import { Action } from '../../../../models/Action.model';
import { NeoComAsset } from '../../../../models/NeoComAsset.model';

@Component({
  selector: 'neocom-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})
export class AssetComponent extends NeoComNodeComponent {
  @Input() node: NeoComAsset;

  public getIconUrl(): string {
    return "http://image.eveonline.com/Type/" + this.node.getTypeId() + "_64.png";
  }


  /**
  If the Panel has some specific attributes they should be tested for display. The current actions are the Menu and the Expand Arrow.
  */
  // public hasMenu(): boolean {
  //   return false;
  // }
  // public isExpandable(): boolean {
  //   return false;
  // }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  // public clickArrow() {
  //   this.node.toggleExpanded();
  //   this.viewer.refreshViewPort();
  // }
  public totalValueIsk(): number {
    let value = this.node.getPrice() * this.node.getQuantity();
    return value;
  }
  public totalVolume(): number {
    let value = this.node.getVolume() * this.node.getQuantity();
    return value;
  }
}

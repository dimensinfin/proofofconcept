import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectableLocationComponent } from './selectable-location.component';

describe('SelectableLocationComponent', () => {
  let component: SelectableLocationComponent;
  let fixture: ComponentFixture<SelectableLocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectableLocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectableLocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

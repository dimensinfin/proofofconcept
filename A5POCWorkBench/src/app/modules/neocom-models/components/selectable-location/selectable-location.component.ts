//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnChanges } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INTERFACES
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
// import { ESeparator } from 'app/interfaces/EPack.enumerated';
// import { IViewer } from 'app/interfaces/IViewer.interface';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
// import { ExpandableComponent } from '../expandable.component';
import { ExpandableMenuBarNodeComponent } from 'app/modules/ui/expandablemenubarnode.component';
//--- MODELS
import { Location } from 'app/models/Location.model';
import { LocationContainer } from 'app/models/LocationContainer.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';
import { MenuItem } from 'app/models/ui/MenuItem.model';
import { Property } from 'app/models/Property.model';

@Component({
  selector: 'selectable-location',
  templateUrl: './selectable-location.component.html',
  styleUrls: ['./selectable-location.component.scss']
})
export class SelectableLocationComponent extends ExpandableMenuBarNodeComponent implements OnInit, OnChanges {
  @Input() node: Location;
  @Input() variant: string = "STANDARD";
  /** Contains the list of roles atached to this location to set up the Location Menu. */
  public roles: string[] = [];
  // /** Reference to the Pilot loaded at initialization */
  // private _pilot:Pilot=null;

  //--- I N I T I A L I Z A T I O N   S E C T I O N
  constructor(protected appModelStore: AppModelStoreService) {
    super();
    fontawesome.library.add(faChevronLeft, faChevronRight, faChevronUp, faChevronDown);
  }

  ngOnInit() {
    console.log(">>[SelectableLocationComponent.ngOnInit]> id: " + this.node.getLocationId());
    // Read the list of Location roles from the Pilot to setup the Menu.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        let pilot = currentSession.getPilot();
        if (null != pilot) {
          let roles = pilot.getLocationRoles();
          for (let role of roles) {
            if (role.numericValue == this.node.getLocationId()) {
              this.roles.push(role.stringValue);
            }
          }
        }
        // Put the roles onto the Location model node because this is the way to retrieve them after the component intialization. Fields are not updated while Inputs do.
        console.log("--[SelectableLocationComponent.ngOnInit]> roles: " + this.roles);
        this.node.setRoles(this.roles);
        this.defineMenu();
      });
    // Copy the theme. And do any initialization.
    if (null != this.node) {
      this.setTheme(this.node.themeColor);
    }
    console.log("<<[SelectableLocationComponent.ngOnInit]");
  }
  ngOnChanges() {
    console.log(">>[SelectableLocationComponent.ngOnChanges]> id: " + this.node.getLocationId());
    this.defineMenu();
  }
  /**
  Toggle the expand collapse status. This changes the expanded attribute and also ndicates other visual elements to change (like the arrow or the shade of the background).
  The second action is to generate again the view llist with a new call to the page component 'refreshViewPort'.
  */
  public clickArrow() {
    this.node.toggleExpanded();
    this.viewer.notifyDataChanged();
  }
  public getVariant(): string {
    return this.variant;
  }
  public isExpandable(): boolean {
    if (this.variant == 'DETAILED') return false;
    return true;
  }
  //--- I M E N U B A R   I N T E R F A C E
  public defineMenu(): void {
    this._menuContents = [];
    // Define the menu to use when DETAILED.
    let section = new MenuSection().setTitle("LOCATION ROLES");
    // Get the roles from the input and then test for contents.
    let roles = this.node.getRoles();
    console.log("--[SelectableLocationComponent.defineMenu]> roles: " + roles);
    let active = this.hasRole('MANUFACTURE', roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " MANUFACTURE: " + active);
    let item1 = new MenuItem().setTitle('MANUFACTURE')
      .setActiveState(active)
      .setIconReference(new SDEIconReference('industry'))
      .setMenuAction((result: string): void => {
        if (item1.isActive) {
          // Role atached. Delete the role.
          this.appModelStore.deleteLocationRole(this.node.getLocationId(), 'MANUFACTURE')
            .subscribe(roleList => {
              this.appModelStore.pop('success', 'LOCATION ROLE', 'Role MANUFACTURE removed from location '
                + this.node.getName() + '!');
              item1.setActiveState(false);
              // Remove the role form the Location role list.
              let oldRoles = this.node.getRoles();
              let index = oldRoles.indexOf('MANUFACTURE', 0);
              if (index > -1) {
                oldRoles.splice(index, 1);
              }
              this.node.setRoles(oldRoles);
            });
        } else {
          // Role not atached. Add the role.
          this.appModelStore.addLocationRole(this.node.getLocationId(), 'MANUFACTURE')
            .subscribe(roleList => {
              this.appModelStore.pop('success', 'LOCATION ROLE', 'Role MANUFACTURE added to location '
                + this.node.getName() + '!');
              item1.setActiveState(true);
              // Add the role to the Location role list.
              let oldRoles = this.node.getRoles();
              oldRoles.push('MANUFACTURE');
              this.node.setRoles(oldRoles);
            });
        }
      });
    section.addMenuItem(item1);
    active = this.hasRole('REFINING', roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " REFINING: " + active);
    let item2 = new MenuItem().setTitle("REFINING")
      .setActiveState(active)
      .setIconReference(new SDEIconReference('reprocessing'))
      .setMenuAction((result: string): void => {
        this.appModelStore.pop('info', 'LOCATION ROLE', 'Role REFINING toggled for location '
          + this.node.getName() + '!');
        item2.setActiveState(!item2.isActive);
      });
    section.addMenuItem(item2);

    active = this.hasRole('INVENTION', roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " INVENTION: " + active);
    let item3 = new MenuItem().setTitle("INVENTION")
      .setActiveState(active)
      .setIconReference(new SDEIconReference('research'))
      .setMenuAction((result: string): void => {
        this.appModelStore.pop('info', 'LOCATION ROLE', 'Role INVENTION toggled for location '
          + this.node.getName() + '!');
        item3.setActiveState(!item3.isActive);
      });
    section.addMenuItem(item3);

    let role = 'PLANETARY PROCESSING';
    active = this.hasRole(role, roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " " + role + ": " + active);
    let item13 = new MenuItem().setTitle(role)
      .setActiveState(active)
      .setIconReference(new SDEIconReference('planets'));
    // item13.setMenuAction(new defineLocationRoleMenuItemAction(item13, this.node, role, this.appModelStore));
    section.addMenuItem(item13);

    active = this.hasRole('GARAGE', roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " GARAGE: " + active);
    let item4 = new MenuItem().setTitle('GARAGE')
      .setActiveState(active)
      .setIconReference(new SDEIconReference('shiphangar'))
      .setMenuAction((result: string): void => {
        this.appModelStore.pop('info', 'LOCATION ROLE', 'Role GARAGE toggled for location '
          + this.node.getName() + '!');
        item4.setActiveState(!item4.isActive);
      });
    section.addMenuItem(item4);
    active = this.hasRole('CITADEL STORAGE', roles);
    console.log("--[SelectableLocationComponent.defineMenu]> Location: " + this.node.getLocationId()
      + " GARAGE: " + active);
    let item5 = new MenuItem().setTitle('CITADEL STORAGE')
      .setActiveState(active)
      .setIconReference(new SDEIconReference('structurebrowser'))
      .setMenuAction((result: string): void => {
        this.appModelStore.pop('info', 'LOCATION ROLE', 'Role CITADEL STORAGE toggled for location '
          + this.node.getName() + '!');
        item5.setActiveState(!item5.isActive);
      });
    section.addMenuItem(item5);

    this._menuContents.push(section);
  }
  //--- G E T T E R S   &   S E T T E R S
  public getIconUrl(): string {
    return this.node.urlLocationIcon;
  }
  public getIdentifier(): number {
    return this.getLocationId();
  }
  public getLocationId() {
    return this.node.getLocationId();
  }
  public getRoles(): string[] {
    if (null != this.node) return this.node.getRoles();
    else return [];
  }
  private hasRole(roleName: string, rolelist: string[]): boolean {
    for (let role of rolelist) {
      if (role == roleName) return true;
    }
    return false;
  }
  public getFormattedStackCount(): number {
    return 0;
  }
}
function defineLocationRoleMenuItemAction(menuItem: MenuItem, location: Location, role: string, appModelStore: AppModelStoreService): void {
  let activeState = menuItem.isActive;
  if (activeState) {
    // Role atached. Delete the role.
    appModelStore.deleteLocationRole(location.getLocationId(), role)
      .subscribe(roleList => {
        this.toasterService.pop('success', 'LOCATION ROLE', 'Role ' + role + ' removed from location '
          + location.getName() + '!');
        menuItem.setActiveState(false);
        // Remove the role form the Location role list.
        let oldRoles = location.getRoles();
        let index = oldRoles.indexOf(role, 0);
        if (index > -1) {
          oldRoles.splice(index, 1);
        }
        location.setRoles(oldRoles);
      });
  } else {
    // Role not atached. Add the role.
    appModelStore.addLocationRole(location.getLocationId(), role)
      .subscribe(roleList => {
        this.toasterService.pop('success', 'LOCATION ROLE', 'Role ' + role + ' added to location '
          + location.getName() + '!');
        menuItem.setActiveState(true);
        // Add the role to the Location role list.
        let oldRoles = location.getRoles();
        oldRoles.push('MANUFACTURE');
        location.setRoles(oldRoles);
      });
  }
}

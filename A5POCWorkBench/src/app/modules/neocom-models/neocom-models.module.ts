//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTP CLIENT
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
//--- ROUTING
//--- DRAG AND DROP
import { NgDragDropModule } from 'ng-drag-drop';
//--- OAUTH2
// import { OAuthModule } from 'angular-oauth2-oidc';
//--- TOAST NOTIFICATIONS
// import { ToasterModule } from 'angular5-toaster';
// import { ToasterContainerComponent } from 'angular5-toaster';
// import { ToasterService } from 'angular5-toaster';
//--- ADDITIONAL MODULES
import { ReactiveFormsModule } from '@angular/forms';
// import { InlineEditorModule } from 'ng2-inline-editor';
// import { } from 'ng2-inline-editor';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';
//--- APPLICATION MODULES
import { UIModule } from '../../modules/ui/ui.module';
import { MenuBarModule } from 'app/modules/menubar/menubar.module';

//--- COMPONENTS-MODEL
import { NeoComNodeComponent } from './components/neocomnode.component';
import { ExpandableComponent } from './components/expandable.component';
import { ActionComponent } from './components/action/action.component';
import { CredentialComponent } from './components/credential/credential.component';
import { JobComponent } from './components/job/job.component';
import { GroupContainerComponent } from './components/group-container/group-container.component';
// import { ProcessingTaskComponent } from './components/processing-task/processing-task.component';
import { LocationComponent } from './components/location/location.component';
import { AssetComponent } from './components/asset/asset.component';
import { ContractComponent } from './components/contract/contract.component';
import { ContractItemComponent } from './components/contract-item/contract-item.component';
import { EveItemComponent } from './components/eve-item/eve-item.component';
// import { FittingComponent } from './components/fitting/fitting.component';
import { ShipComponent } from './components/ship/ship.component';
import { ContainerComponent } from './components/container/container.component';
import { SelectableLocationComponent } from './components/selectable-location/selectable-location.component';
import { RegionComponent } from './components/region/region.component';

@NgModule({
  imports: [
    //--- CORE MODULES
    CommonModule,
    //--- BROWSER & ANIMATIONS
    FormsModule,
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    //--- ADDITIONAL MODULES
    ReactiveFormsModule,
    // InlineEditorModule,
    // AngularFontAwesomeModule,
    //--- APPLICATION MODULES
    UIModule,
    MenuBarModule
  ],
  declarations: [
    // ComponentFactoryComponent,
    NeoComNodeComponent,
    ExpandableComponent,
    ActionComponent,
    CredentialComponent,
    JobComponent,
    GroupContainerComponent,
    // LabeledContainerComponent,
    // ProcessingTaskComponent,
    LocationComponent,
    AssetComponent,
    ContractComponent,
    ContractItemComponent,
    EveItemComponent,
    // FittingComponent,
    // FittingRequestComponent,
    ShipComponent,
    ContainerComponent,
    SelectableLocationComponent,
    RegionComponent
  ],
  exports: [
    // ComponentFactoryComponent,
    NeoComNodeComponent,
    ExpandableComponent,
    ActionComponent,
    CredentialComponent,
    JobComponent,
    GroupContainerComponent,
    // LabeledContainerComponent,
    // ProcessingTaskComponent,
    LocationComponent,
    AssetComponent,
    ContractComponent,
    ContractItemComponent,
    EveItemComponent,
    // FittingComponent,
    // FittingRequestComponent,
    ShipComponent,
    ContainerComponent,
    SelectableLocationComponent,
    RegionComponent
  ]
})
export class NeoComModelsModule { }

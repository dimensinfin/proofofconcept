//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
// --- CORE
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { addMilliseconds } from 'date-fns';
import { differenceInMilliseconds } from 'date-fns';

export class ActiveCacheWrapper<T> {
  private _loadState: boolean = false;
  private _timedCache: boolean = false;
  private _expirationTime: number = 60 * 5;
  private _canReturnObsoletes: boolean = true;
  private _subject: BehaviorSubject<T[]> = new BehaviorSubject<T[]>(new Array<T>());
  private _cache = this._subject.asObservable();
  private _lastUpdateTime = new Date();
  private _downloader: any;
  private _locator: any;

  public accessData(): Observable<T[]> {
    // Check if the data is ready.
    if (this._loadState) {
      // Check the cache time status.
      let now = new Date();
      let expirationTime = addMilliseconds(this._lastUpdateTime, this._expirationTime * 1000)
      let diff = differenceInMilliseconds(expirationTime, now);
      if (diff > 0)
        return this._cache;
      else {
        // Cache expired. Decide if we wait or return an obsolete response.
        if (this._canReturnObsoletes) {
          this.fireDownloader();
          return this._cache;
        } else return this.fireDownloader();
      }
    } else return this.fireDownloader();
  }
  public storeData(_newdata: T[]): void {
    this._lastUpdateTime = new Date();
    this._subject.next(_newdata);
  }
  public clear(): void {
    this._subject.next([]);
    this._loadState = false;
  }

  private fireDownloader(): Observable<T[]> {
    // Start a new download and return the new observable to the caller.
    return this._downloader()
      .pipe(map((data: T[]) => {
        // Store into the subject the new data.
        this._subject.next(data);
        this._loadState = true;
        this._lastUpdateTime = new Date();
        return data;
      }));
  }

  //--- G E T T E R S   &   S E T T E R S
  public setTimedCache(_newstate: boolean): ActiveCacheWrapper<T> {
    this._timedCache = _newstate;
    return this;
  }
  public setCachingTime(_newtime: number): ActiveCacheWrapper<T> {
    this._expirationTime = _newtime;
    return this;
  }
  public setReturnObsoletes(_newstate: boolean): ActiveCacheWrapper<T> {
    this._canReturnObsoletes = _newstate;
    return this;
  }
  public setDownloader(_downloadCall: any): ActiveCacheWrapper<T> {
    this._downloader = _downloadCall;
    return this;
  }
  public setLocator(_downloadLocator: any): ActiveCacheWrapper<T> {
    this._locator = _downloadLocator;
    return this;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyContainersPageComponent } from './only-containers-page.component';

describe('OnlyContainersPageComponent', () => {
  let component: OnlyContainersPageComponent;
  let fixture: ComponentFixture<OnlyContainersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlyContainersPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyContainersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnDestroy } from '@angular/core';
// import { AppPanelComponent } from 'app/modules/assets/components/app-panel/app-panel.component';
import { ISubscription } from 'rxjs/Subscription';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';

@Component({
  selector: 'neocom-only-containers-page',
  templateUrl: './only-containers-page.component.html',
  styleUrls: ['./only-containers-page.component.scss']
})
export class OnlyContainersPageComponent extends AppPanelComponent implements OnInit, OnDestroy {
  private _assetsSubscription: ISubscription;

  ngOnInit() {
    this.downloading = true;
    this._assetsSubscription = this.appStoreService.accessPilotAssets()
      .subscribe((assetList) => {
        // Filter only the list of containers. Any other asset is rejected.
        for (let asset of assetList) {
          if (asset.categoryName=='Celestial') this.dataModelRoot.push(asset);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
  }
  ngOnDestroy() {
    this._assetsSubscription.unsubscribe();
  }
}

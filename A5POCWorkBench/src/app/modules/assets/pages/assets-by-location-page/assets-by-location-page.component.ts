//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { ISubscription } from 'rxjs/Subscription';
//--- SERVICES
// import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- ROUTER
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import 'rxjs/add/operator/switchMap';
// import { AppPanelComponent } from 'app/modules/assets/components/app-panel/app-panel.component';
//--- INTERFACES
import { ELocationType } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { Region } from 'app/models/Region.model';
import { LocationContainer } from 'app/models/LocationContainer.model';
import { GroupContainer } from 'app/models/GroupContainer.model';
import { SpaceContainer } from 'app/models/SpaceContainer.model';
import { Ship } from 'app/models/Ship.model';
import { Chrono } from 'app/models/core/Chrono.model';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';

@Component({
  selector: 'assets-by-location-page',
  templateUrl: './assets-by-location-page.component.html',
  styleUrls: ['./assets-by-location-page.component.scss']
})
export class AssetsByLocationPageComponent extends AppPanelComponent implements OnInit, OnDestroy {
  private _assetsSubscription: ISubscription;

  private _regionList: Map<number, Region> = new Map<number, Region>();
  private _locationList: Map<number, LocationContainer> = new Map<number, LocationContainer>();
  private _containerList: Map<number, GroupContainer> = new Map<number, GroupContainer>();

  // --- L I F E C Y C L E
  /**
   * Pages are expected to be entered with all contraints validated by a GUARD. The guard shoul check that a valid Credential is active and that the authentication token is still valid. We can trust that core data is on place so we do not need to give identification parameters to most of the calls.
   * This page should access the complete list of assets and process it to generate the Region-Location-Station-Hangar-Asset expandable list.
   *
   * @memberof AssetsByLocationPageComponent
   */
  ngOnInit() {
    console.log(">>[AssetsByLocationPageComponent.ngOnInit]");
    this.downloading = true;
    // Get the complete list of assets.
    this._assetsSubscription = this.appStoreService.accessPilotAssets()
      .subscribe((assetList) => {
        console.log(">> [AssetsByLocationPageComponent.ngOnInit]> Asset count: " + assetList.length);
        let chrono = new Chrono();
        // Process the assets and classify them into their locations.
        let unprocessedAssets = [];
        // let filtering = false;
        // let filter = "";
        for (let asset of assetList) {
          // Filter the assets by name.
          // if (filtering) {
          //   let name = asset.getName();
          //   let check = name.toLowerCase().includes(filter.toLowerCase());
          //   if (!check) continue;
          // }
          let loc = asset.getLocation();
          let addingLocation = null;
          // Investigate the location to see if it is a known station, another item or unlnown.
          let type = loc.getLocationType();
          switch (type) {
            case ELocationType.EMPTY:
              // The location is not found or unprocessed. We can  try to search for the location as another asset.
              unprocessedAssets.push(asset);
              break;
            case ELocationType.CITADEL:
            case ELocationType.CCPLOCATION:
              // The location was found on the SDE or the extra location list and identified as a Citadel.
              addingLocation = loc;
              break;
            case ELocationType.UNKNOWN:
              // The location as it is is not located. Probably another game asset.
              // Get the asset location flag to get a better understand of the location type.
              let locType = asset.getLocationType();
              switch (locType) {
                case ELocationType.STATION:
                  // This case should have been identified as a CCPLOCATION.
                  addingLocation = loc;
                  break;
                case ELocationType.OTHER:
                  // We can be sure this means the location is another asset.
                  unprocessedAssets.push(asset);
                  break;
                case ELocationType.SOLAR_SYSTEM:
                  // I do not know what can this represent.
                  unprocessedAssets.push(asset);
                  break;
              }
          }
          // If the adding location is not null we can trust the location as valid.
          if (null != addingLocation) {
            let regid = loc.getRegionId();
            // Search for regions on the list of already detected Regions.
            let regionHit = this._regionList.get(regid);
            if (null == regionHit) {
              // Create a new Region. This changes the UI so notify that to the Adapter.
              regionHit = new Region().setRegionId(regid).setRegionName(loc.getRegion());
              // if (filtering) regionHit.expand();
              this._regionList.set(regid, regionHit);
              this.dataModelRoot.push(regionHit);
              console.log("-- [AssetsManagerPageComponent.ngOnInit]> Adding " + regionHit.getName() + " to the _dataModelRoot.");
            }
            // Check if this Location is already present on this Region.
            let locationHit = this._locationList.get(loc.getLocationId());
            if (null == locationHit) {
              // This is a new location. Add it to the Region and check if this affects the UI.
              // Create and Expanded to allow the hold of contents and set its ContentManager to manual.
              locationHit = new LocationContainer().setLocation(loc);
              // if (filtering) locationHit.expand();
              this._locationList.set(loc.getLocationId(), locationHit);
              regionHit.addLocation(locationHit);
            }

            // While processing assets check all them for Containers and Ships.
            if (asset.groupName == "Cargo Container") {
              // We have identified a container. Convert it and also add it to the container list.
              let container = new SpaceContainer(asset);
              this._containerList.set(container.assetId, container);
              locationHit.addContent(container);
              continue;
            }
            if (asset.groupName == "Secure Cargo Container") {
              // We have identified a container. Convert it and also add it to the container list.
              let container = new SpaceContainer(asset);
              this._containerList.set(container.assetId, container);
              locationHit.addContent(container);
              continue;
            }
            if (asset.groupName == "Audit Log Secure Container") {
              // We have identified a container. Convert it and also add it to the container list.
              let container = new SpaceContainer(asset);
              this._containerList.set(container.assetId, container);
              locationHit.addContent(container);
              continue;
            }
            if (asset.categoryName == "Ship") {
              // We have identified a container. Convert it and also add it to the container list.
              let ship = new Ship(asset);
              this._containerList.set(ship.assetId, ship);
              locationHit.addContent(ship);
              continue;
            }
            // The Region and the Location were already registered. Add the asset to the Location.
            locationHit.addContent(asset);
          }
        }
        this.appModelStore.pop('success', 'ASSETS CLASSIFIED', 'Completed assets classification to real Locations.');

        // After ptocessing all the assets we have on the list all of them that are not on a pure location
        for (let asset of unprocessedAssets) {
          // Search for the location as an asset on the container list.
          let indexer = asset.getLocationId();
          let hit = this._containerList.get(indexer);
          if (null != hit) {
            // We have found that the location is another item. Put the asset inside.
            hit.addContent(asset);
          } else {

            // The container is not at the list already.
          }
        }
        // this.toasterService.pop('success', 'ASSETS CLASSIFIED', 'Completed assets internal classification.');
        // Hide the spinner.
        this.downloading = false;
        this.notifyDataChanged();
        console.log(">> [AssetsByLocationPageComponent.ngOnInit]> [TIMING]: " + chrono.elapsed() + 'ms');
      });
  }
  ngOnDestroy() {
    this._assetsSubscription.unsubscribe();
  }
}

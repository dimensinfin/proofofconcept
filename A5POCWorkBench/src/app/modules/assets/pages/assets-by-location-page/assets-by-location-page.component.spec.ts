import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetsByLocationPageComponent } from './assets-by-location-page.component';

describe('AssetsByLocationPageComponent', () => {
  let component: AssetsByLocationPageComponent;
  let fixture: ComponentFixture<AssetsByLocationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetsByLocationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetsByLocationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

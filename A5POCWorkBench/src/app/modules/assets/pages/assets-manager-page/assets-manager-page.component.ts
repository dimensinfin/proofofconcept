//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../../../../interfaces/EPack.enumerated';
import { ELocationType } from '../../../../interfaces/EPack.enumerated';
import { INeoComNode } from '../../../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Credential } from '../../../../models/Credential.model';
import { Pilot } from '../../../../models/Pilot.model';
import { NeoComAsset } from '../../../../models/NeoComAsset.model';
import { Region } from '../../../../models/Region.model';
import { LabeledContainer } from '../../../../models/LabeledContainer.model';
import { Location } from '../../../../models/Location.model';
import { LocationContainer } from '../../../../models/LocationContainer.model';
import { NeoComSession } from '../../../../models/NeoComSession.model';
import { NeoComNode } from '../../../../models/NeoComNode.model';
import { GroupContainer } from '../../../../models/GroupContainer.model';
import { SpaceContainer } from '../../../../models/SpaceContainer.model';
import { Ship } from '../../../../models/Ship.model';

/**
The Assets Manager will controle the pages to show and interactuate with the assets. This manager has some pages controlled by a tabbed line that can allow different ways to display and organize the assets
*/
@Component({
  selector: 'neocom-assets-manager-page',
  templateUrl: './assets-manager-page.component.html',
  styleUrls: ['./assets-manager-page.component.scss']
})
export class AssetsManagerPageComponent extends BasePageComponent implements OnInit {
  private _pilot: Pilot = null; // The Pilot instance related to this instance. This is the Pilot public data.
  private _regionList: Map<number, Region> = new Map<number, Region>();
  private _locationList: Map<number, LocationContainer> = new Map<number, LocationContainer>();
  private _containerList: Map<number, GroupContainer> = new Map<number, GroupContainer>();

  // private _characterid: number = -1; // The Pilot identifier received on the URL.
  // private _credential: Credential = null; // The matching Credential for this same pilot identifier.
  // /** Node activated by hovering over it with the mouse cursor. May be null. */
  // // private selectedNode: INeoComNode = null;
  //
  // private shipList: Map<number, GroupContainer> = new Map<number, GroupContainer>();

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }

  ngOnInit() {
    console.log(">>[AssetsManagerPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.ASSETS_BY_LOCATION)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;
    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        // The URL path parameters include the pilot identifier. It should match the session stored identifier.
        this.route.params.map(p => p.id)
          .subscribe((pilotid: number) => {
            // Check if pilot identifier is accepted
            let sessionPilotId = currentSession.getPilotIdentifier();
            if (sessionPilotId == Number(pilotid)) {
              console.log(">> [AssetsManagerPageComponent.ngOnInit]> Pilot identifier: " + sessionPilotId);
              this.appModelStore.pop('success', 'PILOT LOCATED', 'Pilot identifier is valid!.');

              // Make sure the Pilot public information is on place. Then we should search for the assets list.
              this.appModelStore.accessPilotPublicData4Id(sessionPilotId)
                .subscribe(pilotData => {
                  this._pilot = pilotData;
                  // currentSession.storePilot(pilotData);
                  this.appModelStore.pop('success', 'PILOT LOCATED', 'Core Pilot public information accessed successfully!.');

                  // Download the complete list of assets for this pilot. Check first if they are already available.
                  this.appModelStore.accessPilotAssets(sessionPilotId)
                    .subscribe(assetList => {
                      console.log(">> [AssetsManagerPageComponent.ngOnInit.accessPilotAssets]> Asset count: " + assetList.length);
                      // Process the assets and classify them into their locations.
                      let unprocessedAssets = [];
                      let filtering = false;
                      let filter = "";
                      for (let asset of assetList) {
                        // Filter the assets by name.
                        if (filtering) {
                          let name = asset.getName();
                          let check = name.toLowerCase().includes(filter.toLowerCase());
                          if (!check) continue;
                        }
                        let loc = asset.getLocation();
                        let addingLocation = null;
                        // Investigate the location to see if it is a known station, another item or unlnown.
                        let type = loc.getLocationType();
                        switch (type) {
                          case ELocationType.EMPTY:
                            // The location is not found or unprocessed. We can  try to search for the location as another asset.
                            unprocessedAssets.push(asset);
                            break;
                          case ELocationType.CITADEL:
                          case ELocationType.CCPLOCATION:
                            // The location was found on the SDE or the extra location list and identified as a Citadel.
                            addingLocation = loc;
                            break;
                          case ELocationType.UNKNOWN:
                            // The location as it is is not located. Probably another game asset.
                            // Get the asset location flag to get a better understand of the location type.
                            let locType = asset.getLocationType();
                            switch (locType) {
                              case ELocationType.STATION:
                                // This case should have been identified as a CCPLOCATION.
                                addingLocation = loc;
                                break;
                              case ELocationType.OTHER:
                                // We can be sure this means the location is another asset.
                                unprocessedAssets.push(asset);
                                break;
                              case ELocationType.SOLAR_SYSTEM:
                                // I do not know what can this represent.
                                unprocessedAssets.push(asset);
                                break;
                            }
                        }
                        // If the adding location is not null we can trust the location as valid.
                        if (null != addingLocation) {
                          // if (loc.getLocationType() == ELocationType.UNKNOWN) {
                          //   unprocessedAssets.push(asset);
                          //   continue;
                          // }
                          let regid = loc.getRegionId();
                          // Search for regions on the list of already detected Regions.
                          let regionHit = this._regionList.get(regid);
                          if (null == regionHit) {
                            // Create a new Region. This changes the UI so notify that to the Adapter.
                            regionHit = new Region().setRegionId(regid).setRegionName(loc.getRegion());
                            if (filtering) regionHit.expand();
                            this._regionList.set(regid, regionHit);
                            this.dataModelRoot.push(regionHit);
                            console.log("-- [AssetsManagerPageComponent.ngOnInit]> Adding " + regionHit.getName() + " to the _dataModelRoot.");
                          }
                          // Check if this Location is already present on this Region.
                          let locationHit = this._locationList.get(loc.getLocationId());
                          if (null == locationHit) {
                            // This is a new location. Add it to the Region and check if this affects the UI.
                            // Create and Expanded to allow the hold of contents and set its ContentManager to manual.
                            locationHit = new LocationContainer().setLocation(loc);
                            if (filtering) locationHit.expand();
                            this._locationList.set(loc.getLocationId(), locationHit);
                            regionHit.addLocation(locationHit);
                          }

                          // While processing assets check all them for Containers and Ships.
                          if (asset.groupName == "Cargo Container") {
                            // We have identified a container. Convert it and also add it to the container list.
                            let container = new SpaceContainer(asset);
                            this._containerList.set(container.assetId, container);
                            locationHit.addContent(container);
                            continue;
                          }
                          if (asset.groupName == "Secure Cargo Container") {
                            // We have identified a container. Convert it and also add it to the container list.
                            let container = new SpaceContainer(asset);
                            this._containerList.set(container.assetId, container);
                            locationHit.addContent(container);
                            continue;
                          }
                          if (asset.groupName == "Audit Log Secure Container") {
                            // We have identified a container. Convert it and also add it to the container list.
                            let container = new SpaceContainer(asset);
                            this._containerList.set(container.assetId, container);
                            locationHit.addContent(container);
                            continue;
                          }
                          if (asset.categoryName == "Ship") {
                            // We have identified a container. Convert it and also add it to the container list.
                            let ship = new Ship(asset);
                            this._containerList.set(ship.assetId, ship);
                            locationHit.addContent(ship);
                            continue;
                          }
                          // The Region and the Location were already registered. Add the asset to the Location.
                          locationHit.addContent(asset);
                        }
                      }
                      this.appModelStore.pop('success', 'ASSETS CLASSIFIED', 'Completed assets classification to real Locations.');

                      // After ptocessing all the assets we have on the list all of them that are not on a pure location
                      for (let asset of unprocessedAssets) {
                        // Search for the location as an asset on the container list.
                        let indexer = asset.getLocationId();
                        let hit = this._containerList.get(indexer);
                        if (null != hit) {
                          // We have found that the location is another item. Put the asset inside.
                          hit.addContent(asset);
                        } else {

                          // The container is not at the list already.
                        }
                      }
                      // this.toasterService.pop('success', 'ASSETS CLASSIFIED', 'Completed assets internal classification.');
                      // Hide the spinner.
                      this.downloading = false;
                      this.notifyDataChanged();
                    });
                });
            } else {
              // Show a notification box to instance the user to move to another page.
              this.appModelStore.pop('warning', 'PILOT NOT MATCH', 'Pilot identifier and pilot in session do not match!.');
              this.router.navigate(['dashboard']);
            }
          });
      });
    console.log("<< [AssetsManagerPageComponent.ngOnInit]");
  }

  //--- I D A T A S O U R C E   I N T E R F A C E
  /*
    This is the core method that converts the asynchronous data management into a rendering list that will define the nodes that should be rendered on the view display. Other indirect methods have a simple interface but the lost of control that implies will not value the additional efforts.
    Once the initial list is completed the UI interaction will send the messages that whould modify it adding or removing nodes to represer¡nt the current visual state of the model.
    */
  public getPanelContents(): INeoComNode[] {
    console.log("><[AssetsManagerPageComponent.getPanelContents]");
    this.notifyDataChanged();
    return this.renderNodeList;
  }
  /**
  Received the mouseenter event and then it has to send it to the page container through the selected component. The page container is represented by the DataSource.
  */
  public mouseEnter(target: NeoComNode) {
    // this.enterSelected(target);
  }
  public toggleSelection(target: NeoComNode) {
    let selected = target.toggleSelected();
    if (selected)
      this.enterSelected(target);
  }
  // public getSelectedNodeClass(): string {
  //   if (null != this.selectedNode) return this.theSelectedNode.jsonClass;
  // }
  // public getSelectedNode(): NeoComNode {
  //   return this.theSelectedNode;
  // }
  public applyPolicies(contents: Region[]): Region[] {
    console.log("><[AssetsManagerPageComponent.applyPolicies]>Sort groups alphabetically");
    // Sort the Credentials by name.
    let sortedContents: Region[] = contents.sort((n1, n2) => {
      if (n1.title < n2.title) {
        return -1;
      }
      if (n1.title > n2.title) {
        return 1;
      }
      return 0;
    });
    return sortedContents;
  }
}

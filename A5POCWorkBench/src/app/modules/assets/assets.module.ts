//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTP CLIENT
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
//--- ROUTING
//--- DRAG AND DROP
import { NgDragDropModule } from 'ng-drag-drop';
//--- OAUTH2
// import { OAuthModule } from 'angular-oauth2-oidc';
//--- TOAST NOTIFICATIONS
// import { ToasterModule } from 'angular5-toaster';
// import { ToasterContainerComponent } from 'angular5-toaster';
// import { ToasterService } from 'angular5-toaster';
// //--- NOTIFICATIONS
// import { SimpleNotificationsModule } from 'angular2-notifications';
// import { NotificationsService } from 'angular2-notifications';
//--- WEBSTORAGE
import { StorageServiceModule } from 'angular-webstorage-service';
//--- ADDITIONAL MODULES
import { ReactiveFormsModule } from '@angular/forms';
// import { InlineEditorModule } from 'ng2-inline-editor';
// import { } from 'ng2-inline-editor';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';
//--- APPLICATION MODULES
import { UIModule } from '../../modules/ui/ui.module';
import { NeoComModelsModule } from '../../modules/neocom-models/neocom-models.module';

//--- COMPONENTS-CORE
//--- DIRECTIVES
//--- SERVICES
//--- PIPES
//--- PAGES
import { AssetsManagerPageComponent } from '../../modules/assets/pages/assets-manager-page/assets-manager-page.component';
import { AssetsByLocationPageComponent } from './pages/assets-by-location-page/assets-by-location-page.component';
// import { AppPanelComponent } from './components/app-panel/app-panel.component';
import { OnlyContainersPageComponent } from './pages/only-containers-page/only-containers-page.component';
// import { AppStoreService } from 'app/modules/assets/services/app-store.service';
//--- COMPONENTS-ABSTRACT
//--- COMPONENTS-UI
//--- COMPONENTS-MODULE
// import { RegionComponent } from './components/region/region.component';
//--- COMPONENTS-DETAILED
//--- MODELS
//--- REMOVABLES
//--- RENDERS
// import { Container4ListComponent } from './renders/container4-list/container4-list.component';
// import { RenderComponent } from './renders/render/render.component';
import { SharedModule } from 'app/modules/shared/shared.module';


@NgModule({
  imports: [
    //--- CORE MODULES
    CommonModule,
    //--- BROWSER & ANIMATIONS
    FormsModule,
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    //--- ADDITIONAL MODULES
    ReactiveFormsModule,
    //--- NOTIFICATIONS
    // SimpleNotificationsModule.forRoot(),
    // ToasterModule,
    //--- WEBSTORAGE
    StorageServiceModule,
    //--- APPLICATION MODULES
    UIModule,
    SharedModule,
    NeoComModelsModule
  ],
  declarations: [
    //--- PAGES
    AssetsManagerPageComponent,
    AssetsByLocationPageComponent,
    // AppPanelComponent,
    OnlyContainersPageComponent,
    //--- RENDERS
    // Container4ListComponent,
    // RenderComponent,
    OnlyContainersPageComponent
  ],
  exports: [
    AssetsManagerPageComponent,
    //--- RENDERS
    // Container4ListComponent,
    OnlyContainersPageComponent
  ],
  providers: [
    // AppStoreService
  ]
})

export class AssetsModule { }

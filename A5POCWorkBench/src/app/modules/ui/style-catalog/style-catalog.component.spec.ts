import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyleCatalogComponent } from './style-catalog.component';

describe('StyleCatalogComponent', () => {
  let component: StyleCatalogComponent;
  let fixture: ComponentFixture<StyleCatalogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyleCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyleCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

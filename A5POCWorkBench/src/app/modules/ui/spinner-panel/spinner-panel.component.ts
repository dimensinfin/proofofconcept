//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
// import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
//--- MODELS
// import { Status } from 'app/models/esi/Status.model';

@Component({
  selector: 'app-spinner-panel',
  templateUrl: './spinner-panel.component.html',
  styleUrls: ['./spinner-panel.component.scss']
})
export class SpinnerPanelComponent /*implements OnInit*/ {
  @Input() title: string = "-";
  @Input() colorTheme: ESeparator = ESeparator.WHITE;

  private ticks = 0;
  private timer: any = null;

  ngOnInit() {
    this.timer = Observable.timer(2000, 1000);
    this.timer.subscribe(t => {
      this.ticks = t;
    });
  }

  public getSpinnerColor(): string {
    switch (this.colorTheme) {
      case ESeparator.WHITE:
        return 'assets/res-ui/drawable/progress70_white.png';
      case ESeparator.RED:
        return 'assets/res-ui/drawable/progress70_red.png';
      case ESeparator.ORANGE:
        return 'assets/res-ui/drawable/progress70_orange.png';
      case ESeparator.GREEN:
        return 'assets/res-ui/drawable/progress70_green.png';
      case ESeparator.BLUE:
        return 'assets/res-ui/drawable/progress70_blue.png';
      case ESeparator.BLACK:
        return 'assets/res-ui/drawable/progress70_black.png';
      default:
        return 'assets/res-ui/drawable/progress70_white.png';
    }
  }
  public getWaitingTime() {
    var date = new Date(null);
    date.setSeconds(this.ticks);
    return date;
  }
}

//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
//--- INTERFACES
import { ESeparator } from 'app/interfaces/EPack.enumerated';

@Component({
  selector: 'v1-spinner-panel',
  templateUrl: './v1-spinner-panel.component.html',
  styleUrls: ['./v1-spinner-panel.component.scss']
})
export class V1SpinnerPanelComponent {
  @Input() title: string = "-";
  @Input() colorTheme: ESeparator = ESeparator.WHITE;

  private ticks = 0;
  private timer: any = null;

  ngOnInit() {
    this.timer = Observable.timer(2000, 1000);
    this.timer.subscribe(t => {
      this.ticks = t;
    });
  }

  public getSpinnerColor(): string {
    switch (this.colorTheme) {
      case ESeparator.WHITE:
        return 'assets/res-ui/drawable/progress70_white.png';
      case ESeparator.RED:
        return 'assets/res-ui/drawable/progress70_red.png';
      case ESeparator.ORANGE:
        return 'assets/res-ui/drawable/progress70_orange.png';
      case ESeparator.GREEN:
        return 'assets/res-ui/drawable/progress70_green.png';
      case ESeparator.BLUE:
        return 'assets/res-ui/drawable/progress70_blue.png';
      case ESeparator.BLACK:
        return 'assets/res-ui/drawable/progress70_black.png';
      default:
        return 'assets/res-ui/drawable/progress70_white.png';
    }
  }
  public getWaitingTime() {
    var date = new Date(null);
    date.setSeconds(this.ticks);
    return date;
  }
}

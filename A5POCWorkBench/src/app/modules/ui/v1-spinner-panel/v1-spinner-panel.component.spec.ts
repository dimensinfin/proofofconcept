import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1SpinnerPanelComponent } from './v1-spinner-panel.component';

describe('V1SpinnerPanelComponent', () => {
  let component: V1SpinnerPanelComponent;
  let fixture: ComponentFixture<V1SpinnerPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1SpinnerPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1SpinnerPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

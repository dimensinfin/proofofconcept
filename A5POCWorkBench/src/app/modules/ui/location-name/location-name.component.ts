//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
//--- SERVICES
//--- COMPONENTS
// import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
//--- MODELS
import { Location } from 'app/models/Location.model';


@Component({
  selector: 'neocom-location-name',
  templateUrl: './location-name.component.html',
  styleUrls: ['./location-name.component.scss']
})
export class LocationNameComponent /*extends NeoComNodeComponent*/ {
  @Input() location: Location;
  public securityLevel: string = ''; // Color style to set the font color for station security levels

  ngOnInit() {
    this.securityLevel = this.getSecurityLevelColor(this.getLocationSecurity());
  }

  //--- MODEL ACCESSORS
  public getLocationId(): number {
    if (null != this.location) return this.location.getLocationId();
    else return -1;
  }
  public getLocationSecurity(): number {
    if (null != this.location) return this.location.security;
    else return 0.0;
  }
  public getRegionName(): string {
    if (null != this.location) return this.location.region;
    else return "-REGION-";
  }
  public getConstellationName(): string {
    if (null != this.location) return this.location.constellation;
    else return "-CONSTELLATION-";
  }
  public getStationName(): string {
    if (null != this.location) return this.location.getStationName();
    else return "-STATION-";
  }
  public getSecurityLevelColor(level: number): string {
    let securityColor = '#F00000';
    if (level >= 1.0) securityColor = '#D73000';
    if (level >= 2.0) securityColor = '#F04800';
    if (level >= 3.0) securityColor = '#F06000';
    if (level >= 4.0) securityColor = '#D77700';
    if (level >= 5.0) securityColor = '#EFEF00';
    if (level >= 6.0) securityColor = '#8FEF2F';
    if (level >= 7.0) securityColor = '#00F000';
    if (level >= 8.0) securityColor = '#00EF47';
    if (level >= 9.0) securityColor = '#48F0C0';
    if (level >= 9.9) securityColor = '#2FEFEF';
    return securityColor;
  }
}

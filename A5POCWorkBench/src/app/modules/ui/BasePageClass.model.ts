import { IDataSource } from 'app/interfaces/IDataSource.interface';
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { NeoComNode } from 'app/models/NeoComNode.model';

export abstract class BasePageClass implements IDataSource {
  public abstract notifyDataChanged(): void;
  public abstract getBodyComponents(): INeoComNode[];
  public abstract applyPolicies(contents: INeoComNode[]): INeoComNode[];
  public abstract enterSelected(target: NeoComNode);
  public abstract getSelectedNodeClass(): string;
  public abstract getSelectedNode(): NeoComNode;
}

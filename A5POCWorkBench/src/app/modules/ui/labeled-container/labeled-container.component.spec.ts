import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LabeledContainerComponent } from './labeled-container.component';

describe('LabeledContainerComponent', () => {
  let component: LabeledContainerComponent;
  let fixture: ComponentFixture<LabeledContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LabeledContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LabeledContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

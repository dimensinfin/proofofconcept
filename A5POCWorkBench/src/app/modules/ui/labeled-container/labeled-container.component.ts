//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
//--- MODELS
import { LabeledContainer } from 'app/models/LabeledContainer.model';


@Component({
  selector: 'neocom-labeled-container',
  templateUrl: './labeled-container.component.html',
  styleUrls: ['./labeled-container.component.scss']
})
export class LabeledContainerComponent extends NeoComNodeComponent {
  @Input() node: LabeledContainer;

  public hasIcon(): boolean {
    if (null != this.node) {
      let label = this.node as LabeledContainer;
      return label.hasIcon();
    }
  }
  public getLabel(): string {
    if (null != this.node) {
      let label = this.node as LabeledContainer;
      return label.getGroupTitle();
    }
  }
  public getGroupIconReference(): string {
    if (null != this.node) {
      let label = this.node as LabeledContainer;
      return label.getGroupIconReference();
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufactureResourcesBalancePageComponent } from './manufacture-resources-balance-page.component';

describe('ManufactureResourcesBalancePageComponent', () => {
  let component: ManufactureResourcesBalancePageComponent;
  let fixture: ComponentFixture<ManufactureResourcesBalancePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufactureResourcesBalancePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufactureResourcesBalancePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

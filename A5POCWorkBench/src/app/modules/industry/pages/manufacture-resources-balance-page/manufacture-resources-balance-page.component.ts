//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EVariant.enumerated';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { EveItem } from 'app/models/EveItem.model';
import { ManufactureResourcesRequestv1 } from 'app/models/industry/ManufactureResourcesRequestv1.model';
import { Resource } from 'app/models/Resource.model';
import { FacetedResourceContainer } from 'app/models/industry/FacetedResourceContainer.model';
import { RefiningProcess } from 'app/models/industry/RefiningProcess.model';

@Component({
  selector: 'neocom-manufacture-resources-balance-page',
  templateUrl: './manufacture-resources-balance-page.component.html',
  styleUrls: ['./manufacture-resources-balance-page.component.scss']
})
export class ManufactureResourcesBalancePageComponent extends BasePageComponent implements OnInit {
  public hullRequests: ManufactureResourcesRequestv1[] = [];
  public structureRequests: ManufactureResourcesRequestv1[] = [];
  public availableResources: FacetedResourceContainer[] = [];
  public hullsDownloading: boolean = true;
  public storageDownloading: boolean = true;

  public totalsRequests: Map<number, Resource> = new Map<number, Resource>();
  public totalsAvailable: Map<number, Resource> = new Map<number, Resource>();

  //--- P A G E   C O N S T R U C T O R. A L L   S E R V I C E S
  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }

  ngOnInit() {
    console.log(">>[ManufactureResourcesBalancePageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    // this.setVariant(EVariant.ASSETS_BY_LOCATION)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.hullsDownloading = true;
    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        // The URL path parameters include the pilot identifier. It should match the session stored identifier.
        this.route.params.map(p => p.id)
          .subscribe((pilotid: number) => {
            // Check if pilot identifier is accepted
            let sessionPilotId = currentSession.getPilotIdentifier();
            if (sessionPilotId == Number(pilotid)) {
              console.log(">> [ManufactureResourcesBalancePageComponent.ngOnInit]> Pilot identifier: " + sessionPilotId);
              this.appModelStore.pop('success', 'PILOT LOCATED', 'Pilot identifier is valid!.');

              //--- M A I N   P A G E   B L O C K.
              // Download the list of scheduled hulls from the Fitting Requests
              this.appModelStore.accessManufactureResourcesHulls4Pilot(sessionPilotId)
                .subscribe(manufactureRequestList => {
                  console.log(">> [ManufactureResourcesBalancePageComponent.ngOnInit.accessManufactureResourcesHulls4Pilot]> Request count: " + manufactureRequestList.length);
                  // Process the requests by adding them to the panel contents.
                  for (let request of manufactureRequestList) {
                    this.hullRequests.push(request);
                    // Process the minerals to evaluate the totals.
                    for (let resource of request.getBillOfmaterials()) {
                      let hit = this.totalsRequests.get(resource.typeId);
                      if (null == hit) {
                        // While adding this resources to the Totals we should create a copy for the first addition if not we can change the original resource, tis time most of them related to the first hull.
                        let mineral = new Resource(resource);
                        this.totalsRequests.set(resource.typeId, mineral);
                      } else {
                        hit.setQuantity(hit.getQuantity() + resource.getQuantity());
                      }
                    }
                  }
                  // Download the list of possible Station Components
                  this.appModelStore.accessManufactureResourcesStructures4Pilot(sessionPilotId)
                    .subscribe(manufactureRequestList => {
                      console.log(">> [ManufactureResourcesBalancePageComponent.ngOnInit.accessManufactureResourcesStructures4Pilot]> Request count: " + manufactureRequestList.length);
                      // Process the requests by adding them to the panel contents.
                      for (let request of manufactureRequestList) {
                        this.hullRequests.push(request);
                        // Process the minerals to evaluate the totals.
                        for (let resource of request.getBillOfmaterials()) {
                          let hit = this.totalsRequests.get(resource.typeId);
                          if (null == hit) {
                            // While adding this resources to the Totals we should create a copy for the first addition if not we can change the original resource, tis time most of them related to the first hull.
                            let mineral = new Resource(resource);
                            this.totalsRequests.set(resource.typeId, mineral);
                          } else {
                            hit.setQuantity(hit.getQuantity() + resource.getQuantity());
                          }
                        }
                      }
                      // Hide the spinner.
                      this.hullsDownloading = false;
                      this.notifyDataChanged();
                      // Download the Assets related with Industry Resources
                      this.appModelStore.accessManufactureResourcesAvailable4Pilot(sessionPilotId)
                        .subscribe(resourcesAvailableList => {
                          console.log(">> [ManufactureResourcesBalancePageComponent.ngOnInit.accessManufactureResourcesAvailable4Pilot]> Request count: " + resourcesAvailableList.length);
                          // Process the requests by adding them to the panel contents.
                          for (let request of resourcesAvailableList) {
                            this.availableResources.push(request);
                            for (let resource of request.contents) {
                              if (resource.jsonClass == "Resource") {
                                let hit = this.totalsAvailable.get(resource.typeId);
                                if (null == hit) {
                                  // While adding this resources to the Totals we should create a copy for the first addition if not we can change the original resource, tis time most of them related to the first hull.
                                  let mineral = new Resource(resource);
                                  this.totalsAvailable.set(resource.typeId, mineral);
                                } else {
                                  hit.setQuantity(hit.getQuantity() + resource.getQuantity());
                                }
                              }
                              if (resource.jsonClass == "RefiningProcess") {
                                let process = resource as RefiningProcess;
                                for (let mineral of process.refineResults) {
                                  let hit = this.totalsAvailable.get(mineral.typeId);
                                  if (null == hit) {
                                    // While adding this resources to the Totals we should create a copy for the first addition if not we can change the original resource, tis time most of them related to the first hull.
                                    let newMineral = new Resource(mineral);
                                    this.totalsAvailable.set(mineral.typeId, newMineral);
                                  } else {
                                    hit.setQuantity(hit.getQuantity() + mineral.getQuantity());
                                  }
                                }
                              }
                            }
                          }
                          // Hide the spinner.
                          this.storageDownloading = false;
                          this.notifyDataChanged();
                        });
                    });
                });
            } else {
              // Show a notification box to instance the user to move to another page.
              this.appModelStore.pop('warning', 'PILOT NOT MATCH', 'Pilot identifier and pilot in session do not match!.');
              this.router.navigate(['dashboard']);
            }
          });
      });
  }
}

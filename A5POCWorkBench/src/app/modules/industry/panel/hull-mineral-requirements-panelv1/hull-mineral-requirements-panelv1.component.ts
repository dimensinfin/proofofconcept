//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EVariant.enumerated';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { EveItem } from 'app/models/EveItem.model';
import { ManufactureResourcesRequestv1 } from 'app/models/industry/ManufactureResourcesRequestv1.model';

@Component({
  selector: 'panel-hull-mineral-requirementsv1',
  templateUrl: './hull-mineral-requirements-panelv1.component.html',
  styleUrls: ['./hull-mineral-requirements-panelv1.component.scss']
})
export class HullMineralRequirementsPanelv1Component /*implements OnInit*/ {
  @Input() requests: ManufactureResourcesRequestv1[];
  @Input() downloading: boolean = true;

  public mineralCatalog: EveItem[] = [];

  constructor(protected appModelStore: AppModelStoreService) {
    // Initialize the list of minerals for the header
    this.appModelStore.accessEveItem4Id(34)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(35)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(36)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(37)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(38)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(39)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(40)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
    this.appModelStore.accessEveItem4Id(11399)
      .subscribe((eveItem) => {
        this.mineralCatalog.push(eveItem);
      });
  }
}

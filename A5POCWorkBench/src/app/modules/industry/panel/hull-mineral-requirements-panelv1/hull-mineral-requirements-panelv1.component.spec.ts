import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HullMineralRequirementsPanelv1Component } from './hull-mineral-requirements-panelv1.component';

describe('HullMineralRequirementsPanelv1Component', () => {
  let component: HullMineralRequirementsPanelv1Component;
  let fixture: ComponentFixture<HullMineralRequirementsPanelv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HullMineralRequirementsPanelv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HullMineralRequirementsPanelv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalsMineralRequirementsPanelv1Component } from './totals-mineral-requirements-panelv1.component';

describe('TotalsMineralRequirementsPanelv1Component', () => {
  let component: TotalsMineralRequirementsPanelv1Component;
  let fixture: ComponentFixture<TotalsMineralRequirementsPanelv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalsMineralRequirementsPanelv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalsMineralRequirementsPanelv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

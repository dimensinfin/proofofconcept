//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EVariant.enumerated';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { EveItem } from 'app/models/EveItem.model';
import { Resource } from 'app/models/Resource.model';
import { ManufactureResourcesRequestv1 } from 'app/models/industry/ManufactureResourcesRequestv1.model';

@Component({
  selector: 'panel-totals-mineral-requirementsv1',
  templateUrl: './totals-mineral-requirements-panelv1.component.html',
  styleUrls: ['./totals-mineral-requirements-panelv1.component.scss']
})
export class TotalsMineralRequirementsPanelv1Component /*implements OnInit*/ {
  @Input() requirements: Map<number, Resource>;
  @Input() available: Map<number, Resource>;
  @Input() downloading: boolean = true;

  public mineralCatalog: EveItem[] = [];

  constructor(protected appModelStore: AppModelStoreService) { }

  public getRequiredMineral(targetId: number): Resource {
    let hit = this.requirements.get(targetId);
    if (null != hit) return hit;
  }
  public getAvailableMineral(targetId: number): Resource {
    let hit = this.available.get(targetId);
    if (null != hit) return hit;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineralsStoragePanelv1Component } from './minerals-storage-panelv1.component';

describe('MineralsStoragePanelv1Component', () => {
  let component: MineralsStoragePanelv1Component;
  let fixture: ComponentFixture<MineralsStoragePanelv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MineralsStoragePanelv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineralsStoragePanelv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

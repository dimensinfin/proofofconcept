//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import { faCog, faCheck, faTimes } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INLINE EDITOR
import { FormControl } from '@angular/forms';
// import { } from 'ng2-inline-editor';
//--- OBSERVABLE
import { Observable } from 'rxjs/Rx';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
// import { EThemeSelector } from '../../../../interfaces/EPack.enumerated';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
import { EOrderState } from 'app/interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
import { ProcessingTaskComponent } from 'app/modules/fitting/components/processing-task/processing-task.component';
//--- MODELS
import { MarketOrder } from 'app/models/MarketOrder.model';
import { ContractItem } from '../../../../models/ContractItem.model';
import { FittingRequest } from '../../../../models/FittingRequest.model';
import { Separator } from '../../../../models/Separator.model';
import { MenuBar } from 'app/models/ui/MenuBar.model';
import { MenuItem } from 'app/models/ui/MenuItem.model';

/**
This is the component to render the BUY tasks to complete Fitting Manufacture Actions.
There are three different types for BUY tasks. The one that has no resources so it has to be completed for the whole quantity. The one that haves a Market task as counterpart for the whole or for part and finally the one that has a Market request that still has to be issued on the game platform.
*/
@Component({
  selector: 'neocom-market-order',
  templateUrl: './market-order.component.html',
  styleUrls: ['./market-order.component.scss']
})
export class MarketOrderComponent extends NeoComNodeComponent implements OnInit {
  @Input() node: MarketOrder;
  @Input() variant: string = "STANDARD";

  public time2Run: string = "00S";

  /**
  During the construction of components we only set data that should not depend on the Input fields or the service assignments. Normally we set the color theme that should be copied from the Model for easy access.
  */
  constructor(protected appModelStore: AppModelStoreService) {
    super();
  }
  /**
  Set the backgraound color depending on the BUY state and calculate the route jumps from station to destination.
  */
  ngOnInit() {
    // Copy the theme.
    if (null != this.node) {
      this.setTheme(this.node.themeColor);
      // this.adjustStateStyles();
      // this.securityLevel = this.getSecurityLevelColor(this.node.getMarketSecurityLevel());
    }
    // Calculate the time for the order to expire.
    let issued: Date = new Date(this.node.issuedDate);
    let expires: Date = new Date(this.node.issuedDate + this.node.duration * 24 * 60 * 60 * 1000);
    let now = new Date();
    let millis2Run = expires.getTime() - new Date().getTime();
    this.time2Run = this.formatTime2Run(millis2Run);
  }
  protected formatTime2Run(millis: number): string {
    let d, h, m, s;
    s = Math.floor(millis / 1000);
    m = Math.floor(s / 60);
    s = s % 60;
    h = Math.floor(m / 60);
    m = m % 60;
    d = Math.floor(h / 24);
    h = h % 24;
    let timeString = d + 'D ' + h + 'H' + m + 'M' + s + 'S'
    return timeString;
  }
  //--- PANEL METHODS
  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.node) {
      return this.getIcon4TypeId(this.node.typeId);
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
  // public adjustStateStyles() {
  //   // Set the background color depending on the Order state.
  //   switch (this.node.orderState) {
  //     case EOrderState.OPEN:
  //       this.stateColor = '#250000';
  //       break;
  //     case EOrderState.CLOSED:
  //       this.stateColor = '#052406';
  //       break;
  //   }
  // }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MineralHeaderComponent } from './mineral-header.component';

describe('MineralHeaderComponent', () => {
  let component: MineralHeaderComponent;
  let fixture: ComponentFixture<MineralHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MineralHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MineralHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

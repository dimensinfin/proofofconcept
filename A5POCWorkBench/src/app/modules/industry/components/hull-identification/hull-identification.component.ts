//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import { faCog, faCheck, faTimes } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INLINE EDITOR
import { FormControl } from '@angular/forms';
// import { } from 'ng2-inline-editor';
//--- OBSERVABLE
import { Observable } from 'rxjs/Rx';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
// import { EThemeSelector } from '../../../../interfaces/EPack.enumerated';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
import { EOrderState } from 'app/interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
import { ProcessingTaskComponent } from 'app/modules/fitting/components/processing-task/processing-task.component';
//--- MODELS
import { Resource } from 'app/models/Resource.model';
import { EveItem } from 'app/models/EveItem.model';
// import { ContractItem } from '../../../../models/ContractItem.model';
// import { FittingRequest } from '../../../../models/FittingRequest.model';
// import { Separator } from '../../../../models/Separator.model';
// import { MenuBar } from 'app/models/ui/MenuBar.model';
// import { MenuItem } from 'app/models/ui/MenuItem.model';

@Component({
  selector: 'industry-hull-identification',
  templateUrl: './hull-identification.component.html',
  styleUrls: ['./hull-identification.component.scss']
})
export class HullIdentificationComponent extends NeoComNodeComponent implements OnInit {
  @Input() hull: EveItem;
  @Input() copies: number;
  /**
  During the construction of components we only set data that should not depend on the Input fields or the service assignments. Normally we set the color theme that should be copied from the Model for easy access.
  */
  constructor(protected appModelStore: AppModelStoreService) {
    super();
  }
  /**
  Set the backgraound color depending on the BUY state and calculate the route jumps from station to destination.
  */
  ngOnInit() {
  }
  //--- PANEL METHODS
  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.hull) {
      return this.getIcon4TypeId(this.hull.typeId);
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
}

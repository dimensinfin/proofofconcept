import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HullIdentificationComponent } from './hull-identification.component';

describe('HullIdentificationComponent', () => {
  let component: HullIdentificationComponent;
  let fixture: ComponentFixture<HullIdentificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HullIdentificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HullIdentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

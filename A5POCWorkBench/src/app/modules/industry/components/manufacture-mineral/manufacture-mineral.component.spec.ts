import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManufactureMineralComponent } from './manufacture-mineral.component';

describe('ManufactureMineralComponent', () => {
  let component: ManufactureMineralComponent;
  let fixture: ComponentFixture<ManufactureMineralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManufactureMineralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManufactureMineralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustryJobComponent } from './industry-job.component';

describe('IndustryJobComponent', () => {
  let component: IndustryJobComponent;
  let fixture: ComponentFixture<IndustryJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustryJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustryJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

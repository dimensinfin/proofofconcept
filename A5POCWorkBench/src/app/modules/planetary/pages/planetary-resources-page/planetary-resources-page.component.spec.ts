import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetaryResourcesPageComponent } from './planetary-resources-page.component';

describe('PlanetaryResourcesPageComponent', () => {
  let component: PlanetaryResourcesPageComponent;
  let fixture: ComponentFixture<PlanetaryResourcesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetaryResourcesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetaryResourcesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

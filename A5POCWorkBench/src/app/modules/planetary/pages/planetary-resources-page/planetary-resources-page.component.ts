//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { ELocationType } from 'app/interfaces/EPack.enumerated';
// import { INeoComNode } from '../../../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { Pilot } from 'app/models/Pilot.model';
import { Region } from 'app/models/Region.model';
import { LocationFacetedAssetContainer } from 'app/models/assets/LocationFacetedAssetContainer.model';

/**
The Assets Manager will controle the pages to show and interactuate with the assets. This manager has some pages controlled by a tabbed line that can allow different ways to display and organize the assets
*/
@Component({
  selector: 'neocom-planetary-resources-page',
  templateUrl: './planetary-resources-page.component.html',
  styleUrls: ['./planetary-resources-page.component.scss']
})
export class PlanetaryResourcesPageComponent extends BasePageComponent implements OnInit {
  private _pilot: Pilot = null; // The Pilot instance related to this instance. This is the Pilot public data.
  private _regionList: Map<number, Region> = new Map<number, Region>();
  private _locationList: Map<number, LocationFacetedAssetContainer> = new Map<number, LocationFacetedAssetContainer>();

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }

  ngOnInit() {
    console.log(">>[PlanetaryResourcesPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.ASSETS_BY_LOCATION)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;
    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        // The URL path parameters include the pilot identifier. It should match the session stored identifier.
        this.route.params.map(p => p.id)
          .subscribe((pilotid: number) => {
            // Check if pilot identifier is accepted
            let sessionPilotId = currentSession.getPilotIdentifier();
            if (sessionPilotId == Number(pilotid)) {
              console.log(">> [AssetsManagerPageComponent.ngOnInit]> Pilot identifier: " + sessionPilotId);
              this.appModelStore.pop('success', 'PILOT LOCATED', 'Pilot identifier is valid!.');

              // Make sure the Pilot public information is on place. Then we should search for the assets list.
              this.appModelStore.accessPilotPublicData4Id(sessionPilotId)
                .subscribe(pilotData => {
                  this._pilot = pilotData;
                  // currentSession.storePilot(pilotData);
                  this.appModelStore.pop('success', 'PILOT LOCATED', 'Core Pilot public information accessed successfully!.');
                  // Download the complete list of assets for this pilot. Check first if they are already available.
                  this.appModelStore.accessPilotPlanetaryResources(sessionPilotId)
                    .subscribe(planetaryResources => {
                      // Process and classify the resources into their Locations and Regions.
                      // Do not take on account containers,just Station level agregation.
                      // Process the assets and classify them into their locations.
                      let unprocessedAssets = [];
                      let filtering = false;
                      let filter = "";
                      for (let asset of planetaryResources) {
                        // Filter the assets by name.
                        if (filtering) {
                          let name = asset.getName();
                          let check = name.toLowerCase().includes(filter.toLowerCase());
                          if (!check) continue;
                        }
                        let loc = asset.getLocation();
                        let addingLocation = loc;
                        // TODO - Location classification is not a must at this point.
                        // Investigate the location to see if it is a known station, another item or unlnown.
                        // let type = loc.getLocationType();
                        // switch (type) {
                        //   case ELocationType.EMPTY:
                        //     // The location is not found or unprocessed. We can  try to search for the location as another asset.
                        //     unprocessedAssets.push(asset);
                        //     break;
                        //   case ELocationType.CITADEL:
                        //   case ELocationType.CCPLOCATION:
                        //     // The location was found on the SDE or the extra location list and identified as a Citadel.
                        //     addingLocation = loc;
                        //     break;
                        //   case ELocationType.UNKNOWN:
                        //     // The location as it is is not located. Probably another game asset.
                        //     // Get the asset location flag to get a better understand of the location type.
                        //     let locType = asset.getLocationType();
                        //     switch (locType) {
                        //       case ELocationType.STATION:
                        //         // This case should have been identified as a CCPLOCATION.
                        //         addingLocation = loc;
                        //         break;
                        //       case ELocationType.OTHER:
                        //         // We can be sure this means the location is another asset.
                        //         unprocessedAssets.push(asset);
                        //         break;
                        //       case ELocationType.SOLAR_SYSTEM:
                        //         // I do not know what can this represent.
                        //         unprocessedAssets.push(asset);
                        //         break;
                        //     }
                        // }
                        // If the adding location is not null we can trust the location as valid.
                        // if (null != addingLocation) {
                        // if (loc.getLocationType() == ELocationType.UNKNOWN) {
                        //   unprocessedAssets.push(asset);
                        //   continue;
                        // }
                        let regid = loc.getRegionId();
                        // Search for regions on the list of already detected Regions.
                        let regionHit = this._regionList.get(regid);
                        if (null == regionHit) {
                          // Create a new Region. This changes the UI so notify that to the Adapter.
                          regionHit = new Region().setRegionId(regid).setRegionName(loc.getRegion());
                          if (filtering) regionHit.expand();
                          this._regionList.set(regid, regionHit);
                          this.dataModelRoot.push(regionHit);
                          console.log("-- [AssetsManagerPageComponent.ngOnInit]> Adding " + regionHit.getName() + " to the dataModelRoot.");
                        }
                        // Check if this Location is already present on this Region.
                        let locationHit: LocationFacetedAssetContainer = this._locationList.get(loc.getSystemId());
                        if (null == locationHit) {
                          // This is a new location. Add it to the Region and check if this affects the UI.
                          // Create and Expanded to allow the hold of contents and set its ContentManager to manual.
                          locationHit = new LocationFacetedAssetContainer(addingLocation);
                          // .setLocation(loc);
                          if (filtering) locationHit.expand();
                          this._locationList.set(loc.getSystemId(), locationHit);
                          regionHit.addLocation(locationHit);
                        }

                        // While processing assets check all them for Containers and Ships.
                        // if (asset.groupName == "Cargo Container") {
                        //   // We have identified a container. Convert it and also add it to the container list.
                        //   let container = new SpaceContainer(asset);
                        //   this._containerList.set(container.assetId, container);
                        //   locationHit.addContent(container);
                        //   continue;
                        // }
                        // if (asset.groupName == "Secure Cargo Container") {
                        //   // We have identified a container. Convert it and also add it to the container list.
                        //   let container = new SpaceContainer(asset);
                        //   this._containerList.set(container.assetId, container);
                        //   locationHit.addContent(container);
                        //   continue;
                        // }
                        // if (asset.groupName == "Audit Log Secure Container") {
                        //   // We have identified a container. Convert it and also add it to the container list.
                        //   let container = new SpaceContainer(asset);
                        //   this._containerList.set(container.assetId, container);
                        //   locationHit.addContent(container);
                        //   continue;
                        // }
                        // if (asset.categoryName == "Ship") {
                        //   // We have identified a container. Convert it and also add it to the container list.
                        //   let ship = new Ship(asset);
                        //   this._containerList.set(ship.assetId, ship);
                        //   locationHit.addContent(ship);
                        //   continue;
                        // }
                        // The Region and the Location were already registered. Add the asset to the Location.
                        locationHit.addContent(asset);
                        console.log("-- [AssetsManagerPageComponent.ngOnInit]> Adding asset " + asset.assetId + " to location " + locationHit.getLocationId());
                        // } else {
                        //   console.log("-- [AssetsManagerPageComponent.ngOnInit]> Asset location is not defined.");
                        // }
                      }
                      this.appModelStore.pop('success', 'PROCESSING', 'Completed processing Planetary Resources!.');
                      // Hide the spinner.
                      this.downloading = false;
                      this.notifyDataChanged();
                    });
                });
            }
          });
      });
    console.log("<< [PlanetaryResourcesPageComponent.ngOnInit]");
  }
  public toggleSelection(target: NeoComNode) {
    let selected = target.toggleSelected();
    if (selected)
      this.enterSelected(target);
  }
}

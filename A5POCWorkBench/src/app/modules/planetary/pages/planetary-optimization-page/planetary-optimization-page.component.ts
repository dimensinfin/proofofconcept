//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { ELocationType } from 'app/interfaces/EPack.enumerated';
// import { INeoComNode } from '../../../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { Pilot } from 'app/models/Pilot.model';
import { Region } from 'app/models/Region.model';
import { LocationFacetedAssetContainer } from 'app/models/assets/LocationFacetedAssetContainer.model';

@Component({
  selector: 'neocom-planetary-optimization-page',
  templateUrl: './planetary-optimization-page.component.html',
  styleUrls: ['./planetary-optimization-page.component.scss']
})
export class PlanetaryOptimizationPageComponent extends BasePageComponent implements OnInit {
  private _pilot: Pilot = null; // The Pilot instance related to this instance. This is the Pilot public data.
  // public adapterViewList: ProcessingAction[] = [];
  // public downloading: boolean = true;
  // public pilot: NeoComCharacter = null;
  // public planetaryManager: PlanetaryManager = null;
  // private exceptionList: Error[] = [];
  // public targetLocation: Location = null;

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }

  /**
  Get the target selected system and then call the backend to perform the optimizaion. The results are the list of actions to perform to complete the optimization target.
  */
  ngOnInit() {
    console.log(">>[PlanetaryOptimizationPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.ASSETS_BY_LOCATION)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;
    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        // The URL path parameters include the pilot identifier. It should match the session stored identifier.
        this.route.params.map(p => p.id)
          .subscribe((pilotid: number) => {
            // Check if pilot identifier is accepted
            let sessionPilotId = currentSession.getPilotIdentifier();
            if (sessionPilotId == Number(pilotid)) {
              console.log(">> [AssetsManagerPageComponent.ngOnInit]> Pilot identifier: " + sessionPilotId);
              this.appModelStore.pop('success', 'PILOT LOCATED', 'Pilot identifier is valid!.');

              // Make sure the Pilot public information is on place. Then we should search for the assets list.
              this.appModelStore.accessPilotPublicData4Id(sessionPilotId)
                .subscribe(pilotData => {
                  this._pilot = pilotData;
                  this.appModelStore.pop('success', 'PILOT LOCATED', 'Core Pilot public information accessed successfully!.');
                  // The URL path parameters also defines the target system to optimize.
                  this.route.params.map(p => p.system)
                    .subscribe((system: number) => {
                      // Call the backend to perform the optimization for the Planetary Resources of the target Pilot at the specified System.
                      this.appModelStore.optimizePlanetaryResourcesAtSystem(sessionPilotId, system)
                        .subscribe(optimizationActions => {
                          for (let action of optimizationActions) {
                            this.dataModelRoot.push(action);
                          }
                          this.appModelStore.pop('success', 'PROCESSING', 'Completed processing Planetary Optimization!.');
                          // Hide the spinner.
                          this.downloading = false;
                          this.notifyDataChanged();
                        });
                    });
                });
            }
          });
      });
    console.log("<<[PlanetaryOptimizationPageComponent.ngOnInit]");
  }
  /**
  Returns the system name selected for the optimization. This is a tricky action because we only have the system identifier on the path. We should go to the Location cache to get that information.
  */
  public getSystemName(): string {
    // if (null != this.targetLocation) return this.targetLocation.getName();
    // else 
    return "-WAITING-";
  }
  public getViewer(): PlanetaryOptimizationPageComponent {
    return this;
  }
}

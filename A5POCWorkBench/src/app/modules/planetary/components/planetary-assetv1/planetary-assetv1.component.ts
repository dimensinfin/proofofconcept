//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- SERVICES
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- COMPONENTS
import { MenuBarNodeComponent } from 'app/modules/ui/menubarnode.component';
// import { ExpandableComponent } from '../../components/expandable.component';
//--- INTERFACES
//--- MODELS
// import { Action } from '../../../../models/Action.model';
import { NeoComAsset } from 'app/models/NeoComAsset.model';

@Component({
  selector: 'planetary-assetv1',
  templateUrl: './planetary-assetv1.component.html',
  styleUrls: ['./planetary-assetv1.component.scss']
})
export class PlanetaryAssetv1Component extends MenuBarNodeComponent {
  @Input() node: NeoComAsset;
  @Input() variant: string = "STANDARD";

  //--- I M E N U B A R   I N T E R F A C E
  public defineMenu(): void {
    this._menuContents = [];
  }
  //--- U I   R E N D E R I N G   M E T H O D S
  public getIconUrl(): string {
    return this.getIcon4TypeId(this.node.typeId);
  }
  public totalValueIsk(): number {
    let value = this.node.getPrice() * this.node.getQuantity();
    return value;
  }
  public totalVolume(): number {
    let value = this.node.getVolume() * this.node.getQuantity();
    return value;
  }
}

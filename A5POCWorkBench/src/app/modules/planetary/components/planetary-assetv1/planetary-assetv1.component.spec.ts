import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetaryAssetv1Component } from './planetary-assetv1.component';

describe('PlanetaryAssetv1Component', () => {
  let component: PlanetaryAssetv1Component;
  let fixture: ComponentFixture<PlanetaryAssetv1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetaryAssetv1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetaryAssetv1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

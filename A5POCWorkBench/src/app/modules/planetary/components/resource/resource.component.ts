//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
//--- INTERFACES
//--- MODELS
import { Resource } from 'app/models/Resource.model';

@Component({
  selector: 'neocom-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent extends NeoComNodeComponent {
  @Input() node: Resource;

  public quantity: number = 0;
  public item = null;

  //--- U I   R E N D E R I N G   M E T H O D S
  public getMarketValue(): number {
    return Math.abs(this.node.getQuantity()) * this.node.getPrice();
  }
  public getIconUrl(): string {
    if (null != this.node)
      return this.node.getIconUrl();
    else return "assets/res-ui/drawable/defaulticonplaceholder.png";
  }
  public getNormalizedQuantity(): number {
    return Math.abs(this.node.getQuantity());
  }
}

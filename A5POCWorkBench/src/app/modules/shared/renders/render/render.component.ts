import { Component, OnInit, Input } from '@angular/core';
import { IContainerController } from 'app/interfaces/core/IContainerController.interface';
import { Node } from 'app/models/core/Node.model';
import { IColorTheme } from 'app/interfaces/core/IColorTheme.interface';

@Component({
  selector: 'neocom-render',
  templateUrl: './render.component.html',
  styleUrls: ['./render.component.scss']
})
export class RenderComponent implements IColorTheme {
  @Input() node: Node;
  @Input() container: IContainerController;
  @Input() colorScheme: string = 'panel-white';  // The name of the panel style to be rendered.
  @Input() variant: string = "-DEFAULT-";

  public getContainer(): IContainerController {
    return this.container;
  }
  getVariant(): string {
    return this.variant;
  }
  // --- I C O L O R T H E M E   I N T E R F A C E
  /**
   * Return the list of styles that should be applied to the Panel dependin of the state of the associated node.
   * For panels the only color is the border color. More elaborated or actionable panels can have states and then we can return more detailed styles.
   * @returns {string} the list of styles to be applied.
   * @memberof AppPanelComponent
   */
  public getColorSchemePanelStyle(): string {
    // Detect the state configuration.
    if (this.node.isSelected())
      if (this.node.isExpanded())
        return this.getExpandedSelectedPanelStyle();
    if (this.node.isSelected())
      return this.getSelectedPanelStyle();
    if (this.node.isExpanded())
      return this.getExpandedPanelStyle();
    return this.getPanelStyle();
  }

  getPanelStyle(): string {
    return this.colorScheme;
  }
  getExpandedPanelStyle(): string {
    return this.colorScheme + '-expanded'
  }
  getSelectedPanelStyle(): string {
    return this.colorScheme + '-selected'
  }
  getExpandedSelectedPanelStyle(): string {
    return this.getExpandedPanelStyle() + ' ' + this.getSelectedPanelStyle();
  }
}

import { Component, OnInit, Input } from '@angular/core';
import { RenderComponent } from 'app/modules/shared/renders/render/render.component';
import { Fitting } from 'app/models/Fitting.model';
import { UIIconReference } from 'app/interfaces/IIconReference.interface';

@Component({
  selector: 'neocom-fitting-render',
  templateUrl: './fitting-render.component.html',
  styleUrls: ['./fitting-render.component.scss']
})
export class FittingRenderComponent extends RenderComponent {
  @Input() node: Fitting;

  public getIconReference(): string {
    if (null != this.node) {
      let fitting = this.node as Fitting;
      return "http://image.eveonline.com/Type/" + fitting.getShipTypeId() + "_64.png";
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
}

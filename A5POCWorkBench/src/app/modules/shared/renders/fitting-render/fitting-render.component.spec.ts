import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingRenderComponent } from './fitting-render.component';

describe('FittingRenderComponent', () => {
  let component: FittingRenderComponent;
  let fixture: ComponentFixture<FittingRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

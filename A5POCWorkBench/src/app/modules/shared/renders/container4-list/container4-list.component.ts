import { Component, OnInit, Input } from '@angular/core';
// import { RenderComponent } from 'app/modules/assets/renders/render/render.component';
import { NeoComAsset } from 'app/models/NeoComAsset.model';
import { RenderComponent } from 'app/modules/shared/renders/render/render.component';

@Component({
  selector: 'av1-container4-list',
  templateUrl: './container4-list.component.html',
  styleUrls: ['./container4-list.component.scss']
})
export class Container4ListRenderComponent extends RenderComponent {
  @Input() node: NeoComAsset;

  public getIconReference(): string {
    return "http://image.eveonline.com/Type/" + this.node.getTypeId() + "_64.png";
    // return '/assets/icons/defaultitemicon.png';
  }
  public getTitle(): string {
    return this.node.getName();
  }
  public getUserLabel(): string {
    let label = this.node.getUserLabel();
    if (null == label) return this.node.getName()
    else return label;
  }
}

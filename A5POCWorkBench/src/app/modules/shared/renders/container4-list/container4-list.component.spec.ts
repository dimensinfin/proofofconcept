import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Container4ListComponent } from './container4-list.component';

describe('Container4ListComponent', () => {
  let component: Container4ListComponent;
  let fixture: ComponentFixture<Container4ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Container4ListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Container4ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

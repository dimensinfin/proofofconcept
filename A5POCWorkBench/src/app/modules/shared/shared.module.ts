import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- COMPONENTS
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';
import { AppItemComponent } from 'app/modules/shared/core-components/app-item/app-item.component';
import { MVControllerComponent } from 'app/modules/shared/core-components/mv-controller/mvcontroller.component';
//--- RENDERS
import { RenderComponent } from 'app/modules/shared/renders/render/render.component';
import { Container4ListRenderComponent } from 'app/modules/shared/renders/container4-list/container4-list.component';
import { UIModule } from 'app/modules/ui/ui.module';
import { NgDragDropModule } from 'ng-drag-drop';
import { FittingRenderComponent } from './renders/fitting-render/fitting-render.component';

@NgModule({
  imports: [
    CommonModule,
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    UIModule
  ],
  declarations: [
    //--- COMPONENTS
    AppPanelComponent,
    AppItemComponent,
    MVControllerComponent,
    //--- RENDERS
    RenderComponent,
    Container4ListRenderComponent,
    FittingRenderComponent
  ],
  exports: [
    //--- COMPONENTS
    AppPanelComponent,
    AppItemComponent,
    MVControllerComponent,
    //--- RENDERS
    RenderComponent,
    Container4ListRenderComponent,
    FittingRenderComponent
  ]
})
export class SharedModule { }

import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'environments/environment';
import { IContainerController } from 'app/interfaces/core/IContainerController.interface';

@Component({
  selector: 'neocom-app-item',
  templateUrl: './app-item.component.html',
  styleUrls: ['./app-item.component.scss']
})
export class AppItemComponent implements OnInit {
  @Input() container: IContainerController;
  @Input() variant: string = "-DEFAULT-";
  public development: boolean = false;
  public downloading: boolean = false;
  // public self : IContainerController=this;

  constructor() {
    this.development = environment.development;
  }

  ngOnInit() {
  }

}

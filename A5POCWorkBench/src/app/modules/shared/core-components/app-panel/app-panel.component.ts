//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component, Input } from '@angular/core';
import { Inject } from '@angular/core';
// --- ENVIRONMENT
import { environment } from 'app/../environments/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- ROUTER
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
// import { NotificationsService } from 'angular2-notifications';
// import { ToasterService } from 'angular5-toaster';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
import { AppStoreService } from 'app/services/app-store.service';
import { BackendService } from 'app/services/backend.service';
// --- INTERFACES
import { IContainerController } from 'app/interfaces/core/IContainerController.interface';
import { IColorTheme } from 'app/interfaces/core/IColorTheme.interface';
// --- COMPONENTS
import { MVControllerComponent } from 'app/modules/shared/core-components/mv-controller/mvcontroller.component';

@Component({
  selector: 'not-applicable-app-panel',
  templateUrl: './not-applicable.html'
})
export class AppPanelComponent extends MVControllerComponent implements IContainerController, IColorTheme {
  @Input() title : string = '-TITLE-';
  @Input() container: IContainerController;
  @Input() colorScheme: string = 'panel-white';  // The name of the panel style to be rendered.

  public self: IContainerController = this; // Auto pointer to the container.
  // public themeColor: string = 'panel-white';

  public development: boolean = false; // Signals the UI when we are loading the data to draw elements accordingly.

  // --- C O N S T R U C T O R
  constructor(
    @Inject(LOCAL_STORAGE) protected localStorage: WebStorageService,
    @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected router: Router,
    protected activeRoute: ActivatedRoute,
    // protected notificationsService: NotificationsService,
    // protected toasterService: ToasterService,
    protected appModelStore: AppModelStoreService,
    protected appStoreService: AppStoreService,
    protected backendService: BackendService) {
    super();
    this.development = environment.development;
  }

  // --- I C O L O R T H E M E   I N T E R F A C E
  /**
   * Return the list of styles that should be applied to the Panel dependin of the state of the associated node.
   * For panels the only color is the border color. More elaborated or actionable panels can have states and then we can return more detailed styles.
   * @returns {string} the list of styles to be applied.
   * @memberof AppPanelComponent
   */
  public getColorSchemePanelStyle(): string {
    return this.getPanelStyle();
  }

  getPanelStyle(): string {
    return this.colorScheme;
  }
  getExpandedPanelStyle(): string {
    return this.colorScheme + '-expanded'
  }
  getSelectedPanelStyle(): string {
    return this.colorScheme + '-selected'
  }
  getExpandedSelectedPanelStyle(): string {
    return this.getExpandedPanelStyle() + ' ' + this.getSelectedPanelStyle();
  }
}

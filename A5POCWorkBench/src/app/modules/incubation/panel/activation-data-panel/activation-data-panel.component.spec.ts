import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivationDataPanelComponent } from './activation-data-panel.component';

describe('ActivationDataPanelComponent', () => {
  let component: ActivationDataPanelComponent;
  let fixture: ComponentFixture<ActivationDataPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivationDataPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivationDataPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

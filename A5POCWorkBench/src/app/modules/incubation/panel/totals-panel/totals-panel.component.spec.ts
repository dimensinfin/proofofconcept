import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalsPanelComponent } from './totals-panel.component';

describe('TotalsPanelComponent', () => {
  let component: TotalsPanelComponent;
  let fixture: ComponentFixture<TotalsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

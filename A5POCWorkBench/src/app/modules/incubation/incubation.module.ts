//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTP CLIENT
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
//--- ROUTING
//--- DRAG AND DROP
import { NgDragDropModule } from 'ng-drag-drop';
//--- TOAST NOTIFICATIONS
// import { ToasterModule } from 'angular5-toaster';
// import { ToasterContainerComponent } from 'angular5-toaster';
// import { ToasterService } from 'angular5-toaster';
//--- ADDITIONAL MODULES
import { ReactiveFormsModule } from '@angular/forms';
// import { InlineEditorModule } from 'ng2-inline-editor';
// import { } from 'ng2-inline-editor';
// import { AngularFontAwesomeModule } from 'angular-font-awesome';
//--- APPLICATION MODULES
import { UIModule } from 'app/modules/ui/ui.module';
import { NeoComModelsModule } from '../../modules/neocom-models/neocom-models.module';

//--- PAGES
import { PocActivationWrapperPageComponent } from 'app/modules/incubation/pages/poc-activation-wrapper-page/poc-activation-wrapper-page.component';
//--- PANELS
import { ActivationDataPanelComponent } from './panel/activation-data-panel/activation-data-panel.component';
import { TotalsPanelComponent } from './panel/totals-panel/totals-panel.component';
//--- COMPONENTS-MODULE

@NgModule({
  imports: [
    //--- CORE MODULES
    CommonModule,
    //--- BROWSER & ANIMATIONS
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    //--- HTTP CLIENT
    HttpModule,
    HttpClientModule,
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    //--- ADDITIONAL MODULES
    ReactiveFormsModule,
    // InlineEditorModule,
    // AngularFontAwesomeModule,
    //--- APPLICATION MODULES
    UIModule,
    NeoComModelsModule
  ],
  declarations: [
    //--- PAGES
    PocActivationWrapperPageComponent,
    //--- PANELS
    ActivationDataPanelComponent,
    TotalsPanelComponent
    //--- COMPONENTS-MODULE
  ],
  exports: [
    //--- PAGES
    PocActivationWrapperPageComponent,
    //--- PANELS
    ActivationDataPanelComponent,
    TotalsPanelComponent
    //--- COMPONENTS-MODULE
  ]
})
export class IncubationModule { }

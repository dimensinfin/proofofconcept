import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'neocom-poc-activation-wrapper-page',
  templateUrl: './poc-activation-wrapper-page.component.html',
  styleUrls: ['./poc-activation-wrapper-page.component.scss']
})
export class PocActivationWrapperPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocActivationWrapperPageComponent } from './poc-activation-wrapper-page.component';

describe('PocActivationWrapperPageComponent', () => {
  let component: PocActivationWrapperPageComponent;
  let fixture: ComponentFixture<PocActivationWrapperPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocActivationWrapperPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocActivationWrapperPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

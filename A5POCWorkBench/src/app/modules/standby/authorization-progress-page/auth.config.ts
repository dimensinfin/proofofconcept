import { AuthConfig } from 'angular-oauth2-oidc';

// import { AuthConfig } from 'app/modules/angular-oauth2-oidc/auth.config';

// import { AuthConfig } from 'app/modules/angular-oauth2-oidc';

// This api will come in the next version
// import { AuthConfig } from 'angular-oauth2-oidc';
// import { AuthorizationProgressPageComponent } from './authorization-progress-page.component';

export const authConfig: AuthConfig = {
  // Login-Url
  loginUrl: "https://login.eveonline.com/" + "oauth/authorize", //Id-Provider?
  // URL of the SPA to redirect the user to after login
  redirectUri: "http://localhost:4200/validateauthorization",
  // The SPA's id. Register SPA with this id at the auth-server
  clientId: "ef68298d582c4dfebb67886e30d088a8",
  // The name of the auth-server that has to be mentioned within the token
  issuer: "https://login.eveonline.com/" + "oauth/authorize",
  // set the scope for the permissions the client should request
  scope: "publicData characterStatsRead characterFittingsRead characterLocationRead characterWalletRead characterAssetsRead characterIndustryJobsRead characterMarketOrdersRead characterNotificationsRead characterResearchRead characterSkillsRead characterAccountRead characterClonesRead corporationWalletRead corporationAssetsRead corporationIndustryJobsRead corporationMarketOrdersRead corporationStructuresRead corporationContractsRead esi-location.read_location.v1 esi-location.read_ship_type.v1 esi-skills.read_skills.v1 esi-skills.read_skillqueue.v1 esi-wallet.read_character_wallet.v1 esi-wallet.read_corporation_wallet.v1 esi-search.search_structures.v1 esi-clones.read_clones.v1 esi-universe.read_structures.v1 esi-assets.read_assets.v1 esi-planets.manage_planets.v1 esi-fittings.read_fittings.v1 esi-markets.structure_markets.v1 esi-corporations.read_structures.v1 esi-markets.read_character_orders.v1 esi-characters.read_blueprints.v1 esi-characters.read_corporation_roles.v1 esi-contracts.read_character_contracts.v1 esi-clones.read_implants.v1 esi-wallet.read_corporation_wallets.v1 esi-characters.read_notifications.v1 esi-corporations.read_divisions.v1 esi-assets.read_corporation_assets.v1 esi-corporations.read_blueprints.v1 esi-contracts.read_corporation_contracts.v1 esi-corporations.read_starbases.v1 esi-industry.read_corporation_jobs.v1 esi-markets.read_corporation_orders.v1 esi-corporations.read_container_logs.v1 esi-industry.read_character_mining.v1 esi-industry.read_corporation_mining.v1 esi-planets.read_customs_offices.v1 esi-corporations.read_facilities.v1 esi-corporations.read_outposts.v1 esi-characterstats.read.v1",
  // set to true, to receive also an id_token via OpenId Connect (OIDC) in addition to the
  // OAuth2-based access_token
  // oidc = false,
  // Use setStorage to use sessionStorage or another implementation of the TS-type Storage
  // instead of localStorage
  // setStorage(sessionStorage),
  // To also enable single-sign-out set the url for your auth-server's logout-endpoint here
  // logoutUrl = AuthorizationProgressPageComponent.AUTHORIZATION_SERVER + "account/logoff",
  // This method just tries to parse the token within the url when
  // the auth-server redirects the user back to the web-app
  // It dosn't initiate the login
  // this.configureWithNewConfigApi();
  // URL of the SPA to redirect the user after silent refresh
  silentRefreshRedirectUri: "https://login.eveonline.com/" + "oauth/authorize",
  showDebugInformation: true,
  sessionChecksEnabled: true,
  responseType: 'code'
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- HTTP PACKAGE
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
//--- ENVIRONMENT
// import { environment } from '../../../environments/environment';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { OAuthService } from 'angular2-oauth2/oauth-service';
// import { AuthConfig } from 'angular-oauth2-oidc';
// import { OAuthService } from 'angular-oauth2-oidc';
// import { JwksValidationHandler } from 'angular-oauth2-oidc';
// import { OAuthService, AuthConfig } from 'angular-oauth2-oidc';

// import { authConfig } from './authconfig.const';
// import { AppModelStoreService } from '../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
//--- COMPONENTS
//--- MODELS
// import { NeoComNode } from '../../models/NeoComNode.model';
// import { Credential } from '../../models/Credential.model';
import { ESIConfiguration } from 'app/models/conf/ESI.Tranquility';
// import { AppModelStoreService } from 'src/app/services/app-model-store.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { AppModelStoreService } from 'app/services/app-model-store.service';
// import { OAuthService } from 'app/modules/angular-oauth2-oidc';

/**
Configure the OAuth service and start the authorization flow by calling the ESI login page. The login server to complete the authorization depends on the setting for the help class 'ESIConfiguration'. There are two configuration sets, one for the Tranquility server and another for the Singularity server.
*/
@Component({
  selector: 'neocom-authorization-progress-page',
  templateUrl: './authorization-progress-page.component.html',
  styleUrls: ['./authorization-progress-page.component.scss']
})
export class AuthorizationProgressPageComponent implements OnInit {
  public working: boolean = false;
  public code: string;

  constructor(protected appModelStore: AppModelStoreService
    , protected http: Http
    , private oauthService: OAuthService) {
    this.oauthService.showDebugInformation = true;
    this.oauthService.requestAccessToken = true;
    // Login-Url
    this.oauthService.loginUrl = ESIConfiguration.AUTHORIZE_URL;
    // URL of the SPA to redirect the user to after login
    this.oauthService.redirectUri = ESIConfiguration.CALLBACK;
    // The SPA's id. Register SPA with this id at the auth-server
    this.oauthService.clientId = ESIConfiguration.CLIENT_ID;
    // The name of the auth-server that has to be mentioned within the token
    this.oauthService.issuer = ESIConfiguration.AUTHORIZE_URL;
    // Set the scope for the permissions the client should request
    this.oauthService.scope = ESIConfiguration.SCOPE;
    // Set to true, to receive also an id_token via OpenId Connect (OIDC) in addition to the
    // OAuth2-based access_token
    this.oauthService.oidc = false;
    // Use setStorage to use sessionStorage or another implementation of the TS-type Storage
    // instead of localStorage
    this.oauthService.setStorage(sessionStorage);
    // To also enable single-sign-out set the url for your auth-server's logout-endpoint here
    this.oauthService.logoutUrl = ESIConfiguration.AUTHORIZATION_SERVER + "account/logoff";
  }
  ngOnInit() {
    console.log(">> [AuthorizationProgressPageComponent.ngOnInit]");
    this.launchLogin();
    console.log("<< [AuthorizationProgressPageComponent.ngOnInit]");
  }

  public getDescription(): string {
    return "Welcome to the Infinity helper. Log in with your Eve Online credentials on the ESI login portal to get access to a capsuleer dedicated Industrialist Management System. Schedule complete Fittings and get all the Manufacture chain tasks to complete them on time."
  }
  public launchLogin() {
    console.log(">> [WelcomePageComponent.launchLogin]");
    // Show the validation spinning while we get the authorization credentials.
    this.working = true;

    // Start the OAuth flow.
    console.log(">< [WelcomePageComponent.initImplicitFlow]");
    this.oauthService.initImplicitFlow();
    console.log("<< [WelcomePageComponent.launchLogin]");
  }
  public logoff() {
    this.oauthService.logOut();
  }
}

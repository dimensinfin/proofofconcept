import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationProgressPageComponent } from './authorization-progress-page.component';

describe('AuthorizationProgressPageComponent', () => {
  let component: AuthorizationProgressPageComponent;
  let fixture: ComponentFixture<AuthorizationProgressPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationProgressPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationProgressPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

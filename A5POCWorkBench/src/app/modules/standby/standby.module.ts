import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthorizationProgressPageComponent } from './authorization-progress-page/authorization-progress-page.component';
import { UIModule } from '../ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    //--- OAUTH2
    OAuthModule.forRoot(),
    UIModule
  ],
  declarations: [
    AuthorizationProgressPageComponent
  ]
})
export class StandbyModule { }

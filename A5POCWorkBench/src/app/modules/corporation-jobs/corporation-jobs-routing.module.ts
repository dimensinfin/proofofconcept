import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LaunchFittingManufacturePageComponent } from 'app/modules/corporation-jobs/pages/launch-fitting-manufacture-page/launch-fitting-manufacture-page.component';

const routes: Routes = [
  { path: '', component: LaunchFittingManufacturePageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CorporationJobsRoutingModule { }

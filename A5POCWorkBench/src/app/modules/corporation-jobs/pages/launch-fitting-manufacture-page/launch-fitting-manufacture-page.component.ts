import { Component, OnInit } from '@angular/core';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';

@Component({
  selector: 'neocom-launch-fitting-manufacture-page',
  templateUrl: './launch-fitting-manufacture-page.component.html',
  styleUrls: ['./launch-fitting-manufacture-page.component.scss']
})
export class LaunchFittingManufacturePageComponent extends AppPanelComponent {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaunchFittingManufacturePageComponent } from './launch-fitting-manufacture-page.component';

describe('LaunchFittingManufacturePageComponent', () => {
  let component: LaunchFittingManufacturePageComponent;
  let fixture: ComponentFixture<LaunchFittingManufacturePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaunchFittingManufacturePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaunchFittingManufacturePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

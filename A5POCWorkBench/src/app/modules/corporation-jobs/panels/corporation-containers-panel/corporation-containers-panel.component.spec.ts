import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporationContainersPanelComponent } from './corporation-containers-panel.component';

describe('CorporationContainersPanelComponent', () => {
  let component: CorporationContainersPanelComponent;
  let fixture: ComponentFixture<CorporationContainersPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporationContainersPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporationContainersPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'cj-corporation-containers-panel',
  templateUrl: './corporation-containers-panel.component.html',
  styleUrls: ['./corporation-containers-panel.component.scss']
})
export class CorporationContainersPanelComponent extends AppPanelComponent implements OnInit, OnDestroy {
  private _assetsSubscription: ISubscription;

  ngOnInit() {
    this.downloading = true;
    this._assetsSubscription = this.appStoreService.accessPilotAssets()
      .subscribe((assetList) => {
        // Filter only the list of containers. Any other asset is rejected.
        for (let asset of assetList) {
          if (asset.categoryName == 'Celestial') this.dataModelRoot.push(asset);
        }
        this.notifyDataChanged();
        this.downloading = false;
      });
  }
  ngOnDestroy() {
    this._assetsSubscription.unsubscribe();
  }
}

import { Component, OnInit } from '@angular/core';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';

@Component({
  selector: 'cj-job-preparation-panel',
  templateUrl: './job-preparation-panel.component.html',
  styleUrls: ['./job-preparation-panel.component.scss']
})
export class JobPreparationPanelComponent extends AppPanelComponent implements OnInit {
  ngOnInit() {
  }
}

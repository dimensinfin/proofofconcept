import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobPreparationPanelComponent } from './job-preparation-panel.component';

describe('JobPreparationPanelComponent', () => {
  let component: JobPreparationPanelComponent;
  let fixture: ComponentFixture<JobPreparationPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobPreparationPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobPreparationPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PilotFittingsPanelComponent } from './pilot-fittings-panel.component';

describe('PilotFittingsPanelComponent', () => {
  let component: PilotFittingsPanelComponent;
  let fixture: ComponentFixture<PilotFittingsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotFittingsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilotFittingsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

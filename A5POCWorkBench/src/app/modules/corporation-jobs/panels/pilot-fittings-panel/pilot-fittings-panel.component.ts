//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { AppPanelComponent } from 'app/modules/shared/core-components/app-panel/app-panel.component';
import { ISubscription } from 'rxjs/Subscription';

@Component({
  selector: 'cjv1-pilot-fittings-panel',
  templateUrl: './pilot-fittings-panel.component.html',
  styleUrls: ['./pilot-fittings-panel.component.scss']
})
export class PilotFittingsPanelComponent extends AppPanelComponent implements OnInit, OnDestroy {
  private _fittingsSubscription: ISubscription;

  ngOnInit(): void {
    this.downloading = true;
    this._fittingsSubscription = this.appStoreService.accessPilotFittings()
      .subscribe((fittingList) => {
        this.dataModelRoot = fittingList;
        this.notifyDataChanged();
        this.downloading = false;
      })
  }
  ngOnDestroy(): void {
    this._fittingsSubscription.unsubscribe();
  }
}

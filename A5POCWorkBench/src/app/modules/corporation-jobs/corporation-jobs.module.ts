import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CorporationJobsRoutingModule } from './corporation-jobs-routing.module';
import { LaunchFittingManufacturePageComponent } from './pages/launch-fitting-manufacture-page/launch-fitting-manufacture-page.component';
import { ActionBarComponent } from './panels/action-bar/action-bar.component';
import { JobPreparationPanelComponent } from './panels/job-preparation-panel/job-preparation-panel.component';
import { UIModule } from 'app/modules/ui/ui.module';
import { CorporationContainersPanelComponent } from './panels/corporation-containers-panel/corporation-containers-panel.component';
import { SharedModule } from 'app/modules/shared/shared.module';
import { NgDragDropModule } from 'ng-drag-drop';
import { PilotFittingsPanelComponent } from './panels/pilot-fittings-panel/pilot-fittings-panel.component';

@NgModule({
  imports: [
    CommonModule,
    CorporationJobsRoutingModule,
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    UIModule,
    SharedModule
  ],
  declarations: [
    LaunchFittingManufacturePageComponent,
    ActionBarComponent,
    JobPreparationPanelComponent,
    CorporationContainersPanelComponent,
    PilotFittingsPanelComponent
  ],
  exports: [
    LaunchFittingManufacturePageComponent,
    // ActionBarComponent,
    JobPreparationPanelComponent
  ]
})
export class CorporationJobsModule { }

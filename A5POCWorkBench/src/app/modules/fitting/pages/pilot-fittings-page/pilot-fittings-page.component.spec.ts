import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PilotFittingsPage } from 'app/modules/fitting/pages/pilot-fittings-page/pilot-fittings-page.component';

// import { PilotFittingsPage } from './fitting-manager-page.component';

describe('FittingManagerPageComponent', () => {
  let component: PilotFittingsPage;
  let fixture: ComponentFixture<PilotFittingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PilotFittingsPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PilotFittingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//--- BROWSER & ANIMATIONS
// import { FormsModule } from '@angular/forms';
// import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// //--- HTTP CLIENT
// import { HttpModule } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http';
// //--- ROUTING
//--- DRAG AND DROP
import { NgDragDropModule } from 'ng-drag-drop';
// //--- OAUTH2
// // import { OAuthModule } from 'angular-oauth2-oidc';
// //--- TOAST NOTIFICATIONS
// import { ToasterModule } from 'angular5-toaster';
// import { ToasterContainerComponent } from 'angular5-toaster';
// import { ToasterService } from 'angular5-toaster';
// //--- ADDITIONAL MODULES
// // import { ReactiveFormsModule } from '@angular/forms';
// // import { InlineEditorModule } from 'ng2-inline-editor';
// // import { } from 'ng2-inline-editor';
// // import { AngularFontAwesomeModule } from 'angular-font-awesome';
// //--- APPLICATION MODULES
import { UIModule } from 'app/modules/ui/ui.module';
import { NeoComModelsModule } from '../../modules/neocom-models/neocom-models.module';
import { MenuBarModule } from 'app/modules/menubar/menubar.module';

//--- PAGES
import { PilotFittingsPage } from 'app/modules/fitting/pages/pilot-fittings-page/pilot-fittings-page.component';
//--- PANELS
import { FittingRequestListPanelComponent } from './panels/fitting-request-list-panel/fitting-request-list-panel.component';
//--- COMPONENTS-MODULE
import { IndustryActivitiesComponent } from './components/industry-activities/industry-activities.component';
import { FittingRequestDetailedComponent } from './components/fitting-request-detailed/fitting-request-detailed.component';
import { FittingDetailedComponent } from './components/fitting-detailed/fitting-detailed.component';
import { FittingRequestComponent } from './components/fitting-request/fitting-request.component';
import { FittingComponent } from './components/fitting/fitting.component';
import { BuyProcessingTaskComponent } from './components/buy-processing-task/buy-processing-task.component';
import { ProcessingTaskComponent } from './components/processing-task/processing-task.component';
import { DetailedProcessingTaskComponent } from './detailed-components/detailed-processing-task/detailed-processing-task.component';
import { MoveProcessingTaskComponent } from './components/move-processing-task/move-processing-task.component';
// import { AppItemComponent } from './components/app-item/app-item.component';
// import { AppPanelComponent } from 'app/modules/fitting/components/app-panel/app-panel.component';
import { GroupContainerV1Component } from 'app/modules/fitting/components/group-container-v1/group-container-v1.component';
import { SharedModule } from 'app/modules/shared/shared.module';

@NgModule({
  imports: [
    //--- CORE MODULES
    CommonModule,
    //--- BROWSER & ANIMATIONS
    // FormsModule,
    // BrowserModule,
    // BrowserAnimationsModule,
    // //--- HTTP CLIENT
    // HttpModule,
    // HttpClientModule,
    // //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    // //--- OAUTH2
    // // OAuthModule,
    //--- APPLICATION MODULES
    UIModule,
    SharedModule,
    NeoComModelsModule,
    MenuBarModule
  ],
  declarations: [
    //--- PAGES
    PilotFittingsPage,
    //--- COMPONENTS
    // AppPanelComponent,
    // AppItemComponent,
    GroupContainerV1Component,
    // PANELS
    FittingRequestListPanelComponent,
    // COMPONENTS
    IndustryActivitiesComponent,
    FittingRequestDetailedComponent,
    FittingDetailedComponent,
    FittingRequestComponent,
    FittingComponent,
    BuyProcessingTaskComponent,
    ProcessingTaskComponent,
    DetailedProcessingTaskComponent,
    MoveProcessingTaskComponent
    // AppItemComponent
  ],
  exports: [
    //--- PAGES
    PilotFittingsPage,
    // PANELS
    FittingRequestListPanelComponent,
    // COMPONENTS
    IndustryActivitiesComponent,
    FittingRequestDetailedComponent,
    FittingDetailedComponent,
    FittingRequestComponent,
    FittingComponent,
    BuyProcessingTaskComponent,
    ProcessingTaskComponent,
    DetailedProcessingTaskComponent,
    MoveProcessingTaskComponent
  ]
})

export class FittingModule { }

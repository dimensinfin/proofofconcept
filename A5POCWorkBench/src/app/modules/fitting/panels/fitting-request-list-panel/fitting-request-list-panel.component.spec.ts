import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingRequestListPanelComponent } from './fitting-request-list-panel.component';

describe('FittingRequestListPanelComponent', () => {
  let component: FittingRequestListPanelComponent;
  let fixture: ComponentFixture<FittingRequestListPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingRequestListPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingRequestListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

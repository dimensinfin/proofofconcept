//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../../../../interfaces/EPack.enumerated';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Fitting } from '../../../../models/Fitting.model';
import { LabeledContainer } from '../../../../models/LabeledContainer.model';
import { IndustryIconReference } from '../../../../interfaces/IIconReference.interface';
import { Contract } from '../../../../models/Contract.model';
import { Job } from '../../../../models/Job.model';
import { FittingRequest } from '../../../../models/FittingRequest.model';
import { InformationPanel } from 'app/models/ui/InformationPanel.model';
import { Separator } from '../../../../models/Separator.model';
import { Credential } from 'app/models/Credential.model';
import { NeoComSession } from 'app/models/NeoComSession.model';
import { NeoComNode } from 'app/models/NeoComNode.model';

/**
Shows the list of fitting requests pending completion. This is a mock real time data that it is going to be stored at the local navigator data store.
*/
@Component({
  selector: 'neocom-fitting-request-list-panel',
  templateUrl: './fitting-request-list-panel.component.html',
  styleUrls: ['./fitting-request-list-panel.component.scss']
})
export class FittingRequestListPanelComponent extends BasePageComponent implements OnInit {
  private pilotid: number = 92002067;

  constructor(protected appModelStore: AppModelStoreService
     , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }
  ngOnInit() {
    console.log(">>[IndustryActivitiesComponent.getLeftPanelContents]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.ASSETS_BY_LOCATION)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;
    let panelContents: INeoComNode[] = [];
    // this.pilotid = 92002067;
    this.appModelStore.accessPilotFittingRequests(this.pilotid)
      .subscribe((fitsList: FittingRequest[]) => {
        let container = new InformationPanel({ "description": 'Drag here a Fitting to create a new request.' });
        this.dataModelRoot.push(container);
        this.dataModelRoot.push(new Separator().setVariation(ESeparator.ORANGE));
        if (fitsList.length > 0)
          for (let contract of fitsList) {
            this.dataModelRoot.push(contract);
          }
        // Hide the spinner.
        this.downloading = false;
        this.notifyDataChanged();
      });
    console.log("<<[IndustryActivitiesComponent.getLeftPanelContents]");
  }

  //--- I D A T A S O U R C E   I N T E R F A C E
  /*
  This is the core methods that converts the asynchronous data management into a rendering list that will define the nodes that should be rendered on the view display. Other indirect methods have a simple interface but the lost of control that implies will not value the additional efforts.
  Once the initial list is completed the UI interaction will send the messages that whould modify it adding or removing nodes to represer¡nt the current visual state of the model.
  */
  public getPanelContents(): INeoComNode[] {
    console.log("><[AssetsManagerPageComponent.getPanelContents]");
    this.notifyDataChanged();
    return this.renderNodeList;
  }
  /**
  Received the mouseenter event and then it has to send it to the page container through the selected component. The page container is represented by the DataSource.
  */
  public mouseEnter(target: NeoComNode) {
    // this.dataSource.enterSelected(target);
  }
  // public applyPolicies(contents: Region[]): Region[] {
  //   console.log("><[AssetsManagerPageComponent.applyPolicies]>Sort groups alphabetically");
  //   // Sort the Credentials by name.
  //   let sortedContents: Region[] = contents.sort((n1, n2) => {
  //     if (n1.title < n2.title) {
  //       return -1;
  //     }
  //     if (n1.title > n2.title) {
  //       return 1;
  //     }
  //     return 0;
  //   });
  //   return sortedContents;
  // }
}

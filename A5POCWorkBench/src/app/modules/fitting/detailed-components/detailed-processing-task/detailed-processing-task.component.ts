//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { UIIconReference } from 'app/interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ProcessingTaskComponent } from 'app/modules/fitting/components/processing-task/processing-task.component';
//--- MODELS
import { ProcessingTask } from '../../../../models/ProcessingTask.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';
import { ContractItem } from '../../../../models/ContractItem.model';
import { MenuBar } from 'app/models/ui/MenuBar.model';
import { MenuItem } from 'app/models/ui/MenuItem.model';
import { AppStoreService } from 'app/services/app-store.service';

/**
The core part of the component to render ProcessingTasks. There are a lot of different types of PT and even them can be on different states so this component just includes the code common to all them.
Key functionalities are the definition of a menu that has interactions with the background and the drag-and-drop ability.
*/
@Component({
  selector: 'detailed-processing-task',
  templateUrl: './detailed-processing-task.component.html',
  styleUrls: ['./detailed-processing-task.component.scss']
})
export class DetailedProcessingTaskComponent extends ProcessingTaskComponent implements OnInit {
  constructor(protected appStoreService: AppStoreService) {
    super(appStoreService);
    // Copy the theme.
    if (null != this.node)
      this.setTheme(this.node.themeColor);
  }
  ngOnInit() {
  }

}

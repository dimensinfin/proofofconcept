import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedProcessingTaskComponent } from './detailed-processing-task.component';

describe('DetailedProcessingTaskComponent', () => {
  let component: DetailedProcessingTaskComponent;
  let fixture: ComponentFixture<DetailedProcessingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedProcessingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedProcessingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

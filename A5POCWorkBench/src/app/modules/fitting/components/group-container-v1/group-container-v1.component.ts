//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { Input } from '@angular/core';
//--- INTERFACES
// import { PageComponent } from '../../../classes/PageComponent';
//--- SERVICES
// import { AppModelStoreService } from '../../../services/app-model-store.service';
//--- COMPONENTS
// import { ExpandableComponent } from '../expandable.component';
// import { ComponentFactoryComponent } from '../../factory/component-factory/component-factory.component';
//--- MODELS
import { GroupContainer } from '../../../../models/GroupContainer.model';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
import { AppItemComponent } from 'app/modules/shared/core-components/app-item/app-item.component';
// import { AppItemComponent } from 'app/modules/fitting/components/app-item/app-item.component';


@Component({
  selector: 'fv1-group-container',
  templateUrl: './group-container.component.html',
  styleUrls: ['./group-container.component.scss']
})
export class GroupContainerV1Component extends AppItemComponent implements OnInit {
  @Input() node: GroupContainer;
  constructor() {
    super();
  }
  ngOnInit() {
    this.node.activateExpandable();

  }
  public getTitle(): string {
    let group = this.node as GroupContainer;
    return group.getGroupTitle();
  }
  public getGroupIconReference(): string {
    if (null != this.node) {
      let group = this.node as GroupContainer;
      return group.getGroupIconReference();
    }
  }
  public getContentsCount(): number {
    if (null != this.node) {
      let group = this.node as GroupContainer;
      return group.getContentsCount();
    } else return 0;
  }

  public hasMenu(): boolean {
    return true;
  }
  public clickArrow() { }
}

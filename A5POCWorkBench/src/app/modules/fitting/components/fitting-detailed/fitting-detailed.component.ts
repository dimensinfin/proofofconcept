//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- INTERFACES
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
//--- MODELS
import { Fitting } from 'app/models/Fitting.model';

@Component({
  selector: 'neocom-fitting-detailed',
  templateUrl: './fitting-detailed.component.html',
  styleUrls: ['./fitting-detailed.component.scss']
})
export class FittingDetailedComponent extends NeoComNodeComponent {
  @Input() node: Fitting;

  public getIconUrl(): string {
    let id = this.node.getTypeId();
    return "http://image.eveonline.com/Type/" + id + "_64.png";
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingDetailedComponent } from './fitting-detailed.component';

describe('FittingDetailedComponent', () => {
  let component: FittingDetailedComponent;
  let fixture: ComponentFixture<FittingDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

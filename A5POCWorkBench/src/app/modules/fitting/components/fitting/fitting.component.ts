//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
//--- MODELS
import { Fitting } from '../../../../models/Fitting.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';
import { ContractItem } from '../../../../models/ContractItem.model';


@Component({
  selector: 'neocom-fitting',
  templateUrl: './fitting.component.html',
  styleUrls: ['./fitting.component.scss']
})
export class FittingComponent extends ExpandableComponent implements OnInit {
  @Input() node: Fitting;

  constructor() {
    super();
    // this.themeColor = new ColorTheme(ESeparator.RED);
  }
  ngOnInit() {
    this.adjustStateStyles();
  }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.node) {
      let fitting = this.node as Fitting;
      return "http://image.eveonline.com/Type/" + fitting.getShipTypeId() + "_64.png";
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }

  //--- GETTERS & SETTERS

  //--- UI MODIFIERS
  public adjustStateStyles() {
    // if (this.node.taskType == 'AVAILABLE') this.stateStyles = { 'background-color': '#052406' };
    // if (this.node.taskType == 'MOVE') {
    // There can be some sub states.

  }
}

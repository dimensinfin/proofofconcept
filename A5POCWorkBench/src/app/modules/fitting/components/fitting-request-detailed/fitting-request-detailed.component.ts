//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INLINE EDITOR
import { FormControl } from '@angular/forms';
import { } from 'ng2-inline-editor';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
// import { EThemeSelector } from '../../../../interfaces/EPack.enumerated';
import { ESeparator } from '../../../../interfaces/EPack.enumerated';
//--- SERVICES
//--- COMPONENTS
import { NeoComNodeComponent } from '../../../neocom-models/components/neocomnode.component';
import { ExpandableComponent } from '../../../neocom-models/components/expandable.component';
//--- MODELS
import { Fitting } from '../../../../models/Fitting.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';
import { ContractItem } from '../../../../models/ContractItem.model';
import { FittingRequest } from '../../../../models/FittingRequest.model';
import { Separator } from '../../../../models/Separator.model';

/**
This Model component will display some of the information that should be required to complete a Manufacture job for a number of fittings. Inline editing should be the way to do the number of copies but it can also be replaced by a menu item and a dialog used to change the value of the number of copies.
The component can be collapsed where it shows a subset of the things to do and an expanded where it should show a complete reports of all the items that need to be manufactured or bought.
*/
@Component({
  selector: 'neocom-fitting-request-detailed',
  templateUrl: './fitting-request-detailed.component.html',
  styleUrls: ['./fitting-request-detailed.component.scss']
})
export class FittingRequestDetailedComponent extends NeoComNodeComponent implements OnInit {
  @Input() node: FittingRequest;
  public copies: number = 1;
  public editing: boolean = false;

  constructor() {
    super();
    // this.themeColor = new ColorTheme(ESeparator.GREEN);
    fontawesome.library.add(faChevronLeft, faChevronRight);
  }
  ngOnInit() {
    this.adjustStateStyles();
    this.activateMenu();
    // let nameControl = new FormControl("1");
    // this.copies = nameControl.value;
  }
  public clickMenu(): void { }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.node) {
      let fitting = this.node.fitting as Fitting;
      return "http://image.eveonline.com/Type/" + fitting.getShipTypeId() + "_64.png";
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
  public adjustStateStyles() {
    // if (this.node.taskType == 'AVAILABLE') this.stateStyles = { 'background-color': '#052406' };
    // if (this.node.taskType == 'MOVE') {
    // There can be some sub states.

  }
  // public saveCopies(value) {
  //   this.copies = value;
  //   this.editing = false;
  // }
  // public isEditing(): boolean {
  //   return this.editing;
  // }
  // public toggleEditing(): void {
  //   this.editing = !this.editing;
  // }
  public changeCopies(delta: number): void {
    this.copies += delta;
    if (this.copies < 0) this.copies = 0;
  }
  public totalSeparator(): Separator {
    return new Separator().setVariation(ESeparator.WHITE)
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingRequestDetailedComponent } from './fitting-request-detailed.component';

describe('FittingRequestDetailedComponent', () => {
  let component: FittingRequestDetailedComponent;
  let fixture: ComponentFixture<FittingRequestDetailedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingRequestDetailedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingRequestDetailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

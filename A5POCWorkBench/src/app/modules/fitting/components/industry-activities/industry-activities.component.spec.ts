import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustryActivitiesComponent } from './industry-activities.component';

describe('IndustryActivitiesComponent', () => {
  let component: IndustryActivitiesComponent;
  let fixture: ComponentFixture<IndustryActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustryActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustryActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

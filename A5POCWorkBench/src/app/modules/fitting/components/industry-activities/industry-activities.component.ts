//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '../../../../../environments/environment';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../../../../interfaces/EPack.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Fitting } from '../../../../models/Fitting.model';
import { LabeledContainer } from '../../../../models/LabeledContainer.model';
import { IndustryIconReference } from '../../../../interfaces/IIconReference.interface';
import { Contract } from '../../../../models/Contract.model';
import { Job } from '../../../../models/Job.model';


/**
This special node generator will collect the Industry Jobs, the Contracts and any other usefull information from the Pilot database to match against the pending manufacturing jobs expected to complete the Fitting Manufacuring. Instead using another DataSource I will code specifically the node type matcher but the node generator is just the same.
*/
@Component({
  selector: 'neocom-industry-activities',
  templateUrl: './industry-activities.component.html',
  styleUrls: ['./industry-activities.component.scss']
})
export class IndustryActivitiesComponent extends BasePageComponent implements OnInit {
  private pilotid: number = 92002067;

  // constructor(protected appModelStore: AppModelStoreService
  //   , protected toastr: ToastsManager
  //   , protected vcr: ViewContainerRef
  //   , private route: ActivatedRoute, private router: Router) {
  //   super(appModelStore, toastr, vcr);
  // }
  ngOnInit() {
    console.log(">>[IndustryActivitiesComponent.getLeftPanelContents]");
    // Start to show the spinner.
    this.downloading = true;
    let panelContents: INeoComNode[] = [];
    this.pilotid = 92002067;
    this.appModelStore.accessPilotContracts(this.pilotid)
      .subscribe((contractList: Contract[]) => {
        this.appModelStore.accessPilotIndustryJobs(this.pilotid)
          .subscribe((jobList: Job[]) => {
            for (let contract of contractList) {
              this.dataModelRoot.push(contract);
            }
            for (let job of jobList) {
              this.dataModelRoot.push(job);
            }
            // Hide the spinner.
            this.downloading = false;
          });
      });
    console.log("<<[IndustryActivitiesComponent.getLeftPanelContents]");
  }
}

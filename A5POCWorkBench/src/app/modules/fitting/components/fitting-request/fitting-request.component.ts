//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { OnChanges } from '@angular/core';
import { Input } from '@angular/core';
//--- ROUTING
import { Router } from '@angular/router';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import { faCog, faCheck, faTimes } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INLINE EDITOR
import { FormControl } from '@angular/forms';
// import { } from 'ng2-inline-editor';
//--- INTERFACES
import { IIconReference } from '../../../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { UIIconReference } from '../../../../interfaces/IIconReference.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { MenuBarNodeComponent } from 'app/modules/ui/menubarnode.component';
// import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
//--- MODELS
import { Fitting } from 'app/models/Fitting.model';
import { FittingRequest } from 'app/models/FittingRequest.model';
import { Separator } from 'app/models/Separator.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';
import { MenuItem } from 'app/models/ui/MenuItem.model';

/**
This Model component will display some of the information that should be required to complete a Manufacture job for a number of fittings. Inline editing should be the way to do the number of copies but it can also be replaced by a menu item and a dialog used to change the value of the number of copies.
The component can be collapsed where it shows a subset of the things to do and an expanded where it should show a complete reports of all the items that need to be manufactured or bought.
*/
@Component({
  selector: 'neocom-fitting-request',
  templateUrl: './fitting-request.component.html',
  styleUrls: ['./fitting-request.component.scss']
})
export class FittingRequestComponent extends MenuBarNodeComponent implements OnInit, OnChanges {
  @Input() node: FittingRequest;

  public copies: number = 1;
  public editing: boolean = false;
  private _saveState: boolean = false;

  //--- I N I T I A L I Z A T I O N   S E C T I O N
  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router) {
    super();
    fontawesome.library.add(faChevronLeft, faChevronRight, faChevronUp, faChevronDown, faCog, faCheck, faTimes);
  }

  ngOnInit() {
    this.adjustStateStyles();
    this.activateMenu();
    // let nameControl = new FormControl("1");
    // this.copies = nameControl.value;
  }
  ngOnChanges() {
    console.log(">>[FittingRequestComponent.ngOnChanges]> Fitting Request id: " + this.node.requestId);
    this.defineMenu();
  }

  public clickMenu(): void { }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.node) {
      let fitting = this.node.fitting as Fitting;
      return "http://image.eveonline.com/Type/" + fitting.getShipTypeId() + "_64.png";
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
  public adjustStateStyles() {
    // if (this.node.taskType == 'AVAILABLE') this.stateStyles = { 'background-color': '#052406' };
    // if (this.node.taskType == 'MOVE') {
    // There can be some sub states.
  }

  public changeCopies(delta: number): void {
    this.copies += delta;
    if (this.copies < 0) this.copies = 0;
  }
  public totalSeparator(): Separator {
    return new Separator().setVariation(ESeparator.WHITE)
  }
  public saveRequest(): void {
    // this.getViewer().redirectPage(['dashboard']);
    // this.
    this._saveState = !this._saveState;
  }
  public processRequest(): void {
    this.getViewer().redirectPage(['dashboard']);
  }
  public isSaved(): boolean {
    return this._saveState;
  }
  //--- I M E N U B A R   I N T E R F A C E
  public defineMenu(): void {
    this._menuContents = [];
    // Define the menu to use to save and process the Request.
    let section = new MenuSection().setTitle("SCHEDULE STATUS");
    let item1 = new MenuItem().setTitle('SAVE REQUEST')
      .setActiveState(this.node.saved)
      .setIconReference(new SDEIconReference('fittingmanagement'))
      .setMenuAction((result: string): void => {
        if (!this.node.saved) {
          this.appModelStore.saveFittingRequest(this.node)
            .subscribe(roleList => {
              this.appModelStore.pop('success', 'FITTING REQUEST', 'Fitting Request '
                + this.node.getFittingName() + ' saved!');
              this.node.saved = true;
            });
        }
      });
    section.addMenuItem(item1);

    this._menuContents.push(section);

    section = new MenuSection().setTitle("ACTIONS");
    item1 = new MenuItem().setTitle('PROCESS FITTING')
      // .setActiveState(this.node.saved)
      .setIconReference(new SDEIconReference('fittingmanagement'))
      .setMenuAction((result: string): void => {
        this.router.navigate(['/fittingprocessing', this.node.requestId]);
      });
    section.addMenuItem(item1);

    this._menuContents.push(section);
  }
}

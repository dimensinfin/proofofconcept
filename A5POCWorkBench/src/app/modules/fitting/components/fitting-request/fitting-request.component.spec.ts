import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingRequestComponent } from './fitting-request.component';

describe('FittingRequestComponent', () => {
  let component: FittingRequestComponent;
  let fixture: ComponentFixture<FittingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveProcessingTaskComponent } from './move-processing-task.component';

describe('MoveProcessingTaskComponent', () => {
  let component: MoveProcessingTaskComponent;
  let fixture: ComponentFixture<MoveProcessingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveProcessingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveProcessingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

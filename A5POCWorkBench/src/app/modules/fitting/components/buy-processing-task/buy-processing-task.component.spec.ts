import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyProcessingTaskComponent } from './buy-processing-task.component';

describe('BuyProcessingTaskComponent', () => {
  let component: BuyProcessingTaskComponent;
  let fixture: ComponentFixture<BuyProcessingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyProcessingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyProcessingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

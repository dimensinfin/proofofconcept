import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessingTaskComponent } from './processing-task.component';

describe('ProcessingTaskComponent', () => {
  let component: ProcessingTaskComponent;
  let fixture: ComponentFixture<ProcessingTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessingTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessingTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

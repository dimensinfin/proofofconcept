//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- INTERFACES
import { UIIconReference } from 'app/interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../../../interfaces/IIconReference.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { NeoComNodeComponent } from 'app/modules/neocom-models/components/neocomnode.component';
import { MenuBarNodeComponent } from 'app/modules/ui/menubarnode.component';
//--- MODELS
import { ProcessingTask } from '../../../../models/ProcessingTask.model';
// import { ColorTheme } from '../../../../uimodels/ColorTheme.model';
import { ContractItem } from '../../../../models/ContractItem.model';
import { MenuBar } from 'app/models/ui/MenuBar.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';
import { MenuItem } from 'app/models/ui/MenuItem.model';
import { AppStoreService } from 'app/services/app-store.service';

/**
The core part of the component to render ProcessingTasks. There are a lot of different types of PT and even them can be on different states so this component just includes the code common to all them.
Key functionalities are the definition of a menu that has interactions with the background and the drag-and-drop ability.
*/
@Component({
  selector: 'neocom-processing-task',
  templateUrl: './processing-task.component.html',
  styleUrls: ['./processing-task.component.scss']
})
export class ProcessingTaskComponent extends MenuBarNodeComponent implements OnInit {
  @Input() node: ProcessingTask;
  @Input() variant: string = "STANDARD";

  /**
  During the construction of components we only set data that should not depend on the Input fields or the service assignments. Normally we set the color theme that should be copied from the Model for easy access.
  */
  constructor(protected appStoreService: AppStoreService) {
    super();
    // Copy the theme.
    // if (null != this.node)
    //   this.setTheme(this.node.themeColor);
  }
  ngOnInit() {
    // Copy the theme.
    this.setTheme(this.node.themeColor);
    this.adjustStateStyles();
    this.defineMenu();
  }
  //--- DRAG & DROP SECTION
  public isDropAllowed = (dragData: ProcessingTask) => {
    if (dragData.getTypeId() == this.node.getTypeId()) return true;
    else return true;
  }
  public getDropScope = (dragData) => {
    console.log("><[ProcessingTaskComponent.getDropScope]" + dragData.item.name);
    if (dragData.item.name == this.node.getResourceTypeName()) return true;
    return false;
  }

  //--- IMENUBAR INTERFACE
  // public ifInside(): boolean {
  //   return true;
  // }
  //--- GETTERS & SETTERS
  public getResourceName(): string {
    if (null != this.node) {
      let task = this.node as ProcessingTask;
      return task.getResourceTypeName();
    }
  }
  public getTypeId(): number {
    if (null != this.node) {
      let task = this.node as ProcessingTask;
      return task.getResourceTypeId();
    }
  }

  //--- UI MODIFIERS
  public getIconUrl(): string {
    if (null != this.node) {
      let task = this.node as ProcessingTask;
      return "http://image.eveonline.com/Type/" + task.getTypeId() + "_64.png";
    } else return new UIIconReference('defaulticonplaceholder').getReference()
  }
  public adjustStateStyles() {
    if (this.node.taskType == 'AVAILABLE') this.stateStyles = { 'background-color': '#052406' };
    if (this.node.taskType == 'MOVE') this.stateStyles = { 'background-color': '#080825' };
  }
  public defineMenu(): void {
    if (this.node.taskType == 'MOVE') {
      // Define the menu to use when DETAILED.
      let section = new MenuSection().setTitle("PREFERRED ACTION");
      let item = new MenuItem().setTitle("BUILD")
        .setActiveState(true)
        .setIconReference(new SDEIconReference('industry'))
        .setMenuAction((result: string): void => {
          this.appStoreService.successNotification( 'Menu BUILD activated!', 'MENU');
          item.setActiveState(!item.isActive);
        });
      section.addMenuItem(item);
      item = new MenuItem().setTitle("MOVE")
        .setIconReference(new SDEIconReference('map'));
      item.setMenuAction((self: MenuItem): void => {
        this.appStoreService.pop('info', 'MENU', 'Menu MOVE activated!');
        self.setActiveState(!self.isActive);
      });
      section.addMenuItem(item);
      this._menuContents.push(section);

      section = new MenuSection().setTitle("MARKET OPERATIONS");
      item = new MenuItem()
        .setTitle("OPEN MARKET OPERATION")
        .setIconReference(new SDEIconReference('market'))
        .setActiveState(true)
        .setMenuAction((result: string): void => {
          this.appStoreService.pop('info', 'MENU', 'Market Action activated!');
          item.setActiveState(!item.isActive);
        });
      section.addMenuItem(item);
      this._menuContents.push(section);
    }
  }
  public getVariant(): string {
    return this.variant;
  }
}

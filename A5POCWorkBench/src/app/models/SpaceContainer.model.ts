//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../interfaces/EPack.enumerated';
import { ELocationType } from '../interfaces/EPack.enumerated';
import { ELocationFlag } from '../interfaces/EPack.enumerated';
import { ESeparator } from '../interfaces/EPack.enumerated';
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { INeoComAsset } from '../interfaces/INeoComAsset.interface';
//--- MODELS
import { GroupContainer } from '../models/GroupContainer.model';
import { NeoComAsset } from '../models/NeoComAsset.model';
import { EveItem } from '../models/EveItem.model';
import { Separator } from '../models/Separator.model';
import { Location } from '../models/Location.model';
import { ColorTheme } from 'app/models/ui/ColorTheme.model';

export class SpaceContainer extends GroupContainer implements INeoComAsset {
  public assetId: number = -1.0;
  public typeId: number = -1;
  public quantity: number = 0;
  public ownerID: number = -1;
  public name: string = "<name>";
  public category: string = "Planetary Commodities";
  public groupName: string = "Refined Commodities";
  public tech: string = "Tech I";
  public blueprint: boolean = false;
  public ship: boolean = false;
  public price: number = 0.0;
  public volume: number = 0.0;

  public locationId: number = -2;
  public locationType: string = "UNKNOWN";
  public locationFlag: string = "UNLOCKED";

  private location: Location = new Location();
  private item: EveItem = new EveItem();

  public totalValueCalculated: number = -1;
  public totalVolumeCalculated: number = -1;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "SpaceContainer";
    // Transform the pilot field to a class Pilot.
    if (null != this.item) {
      let newitem = new EveItem(this.item);
      this.item = newitem;
    }
    if (null != this.location) {
      let newloc = new Location(this.location);
      this.location = newloc;
    }
    this.themeColor = new ColorTheme().setThemeColor(ESeparator.YELLOW);
    this.downloaded = true;
  }

  //--- I C O L L A B O R A T I O N    I N T E R F A C E
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    let collab = [];
    // If the node is expanded then add its assets.
    if (this.expanded) {
      // Add the Separator depending on the node's theme color.
      let color = this.themeColor.getThemeCode();
      collab.push(new Separator().setVariation(color));
      collab.push(this);
      // Process each item at the rootlist for more collaborations.
      // Apply the processing policies before entering the processing loop. Usually does the sort.
      let sortedContents: INeoComNode[] = this.getContents().sort((n1: NeoComAsset, n2: NeoComAsset) => {
        if (n1.getName() > n2.getName()) {
          return 1;
        }
        if (n1.getName() < n2.getName()) {
          return -1;
        }
        return 0;
      });
      for (let node of sortedContents) {
        let partialcollab = node.collaborate2View(appModelStore, variant);
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
      // } else {
      //   // Call the backend to download the contents. On callback we need to fire an event to refresh the display.
      //   appModelStore.getBackendContainerContents(this.assetId)
      //     .subscribe(result => {
      //       console.log("--[Location.collaborate2View.getBackendLocationsContents]>AssetList: " + result.length);
      //       // The the list of assets contained at the location.
      //       // Process them to upgrade to expandable types and also to account for their volume and price.
      //       this.contents = this.processDownloadedAssets(result);
      //       this.downloaded = true;
      //       appModelStore.fireRefresh();
      //     });
      //   // Add an spinner to the output to inform the user of the background task.
      //   collab.push(new Separator().setVariation(ESeparator.SPINNER));
      // }
      collab.push(new Separator().setVariation(color));
    } else collab.push(this);
    return collab;
  }

  //--- GETTER & SETTERS
  public getJsonClass(): string {
    return this.jsonClass;
  }
  public getQuantity(): number {
    if (null == this.quantity) return 1;
    else return this.quantity;
  }
  public getItem(): EveItem {
    if (null == this.item) return new EveItem();
    else return this.item;
  }
  public getName(): string {
    return this.name;
  }
  public getLocation(): Location {
    if (null == this.location) return new Location();
    else return this.location;
  }
  public getLocationId(): number {
    return this.locationId;
  }
  public getVolume(): number {
    if (null == this.volume) return 0.01;
    else return this.volume;
  }
  public getLocationType(): ELocationType {
    return ELocationType[this.locationType];
  }
  public getLocationFlag(): ELocationFlag {
    return ELocationFlag[this.locationFlag];
  }
  public getPrice(): number {
    if (null == this.price) return 0.0;
    else return this.price;
  }

  public getTotalValue(): number {
    return this.totalValueCalculated;
  }
  public getTotalVolume(): number {
    return this.totalVolumeCalculated;
  }
  /**
  This method informs the view renderer that this node can be expanded. This should trigger the rendering for the expand/collapse arrow icon and its functionality.
  */
  public canBeExpanded(): boolean {
    return true;
  }
  // public getContents() {
  //   return this.contents;
  // }
  // public processDownloadedAssets(assets: NeoComNode[]): NeoComAsset[] {
  //   let results: NeoComAsset[] = [];
  //   for (let node of assets) {
  //     switch (node.jsonClass) {
  //       case "NeoComAsset":
  //         let asset = new NeoComAsset(node);
  //         this.totalValueCalculated += asset.getItem().getPrice() * asset.quantity;
  //         this.totalVolumeCalculated += asset.getItem().getVolume() * asset.quantity;
  //         results.push(asset);
  //         break;
  //       case "SpaceContainer":
  //         let container = new SpaceContainer(node);
  //         // this.totalValueCalculated += container.item.baseprice * container.quantity;
  //         // this.totalVolumeCalculated += container.item.volume * container.quantity;
  //         results.push(container);
  //         break;
  //       case "Ship":
  //         let ship = new Ship(node);
  //         this.totalValueCalculated += ship.getItem().getPrice();
  //         //     this.totalVolumeCalculated += container.item.volume * container.quantity;
  //         results.push(ship);
  //         break;
  //       default:
  //         //        results.push(node);
  //         break;
  //     }
  //   }
  //   return results;
  // }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
// import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { ESeparator } from 'app/interfaces/EPack.enumerated';
import { EOrderState } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { EveItem } from 'app/models/EveItem.model';
import { Location } from 'app/models/Location.model';
// import { Resource } from '../models/Resource.model';
// import { NeoComAsset } from '../models/NeoComAsset.model';
import { ColorTheme } from 'app/models/ui/ColorTheme.model';

/**
Represents a task to be completed to fulfill some manufacturing action. By example, if we need 10 Warp Core Stabilizers and we only have 3 on stock and another 5 on another station, the Action to complete the 10 WCS Action can be decomposed into 3 ProcessingTasks. One AVAILABLE for the 3 on stock, another MOVE for the 5 on another location and a BUY task for the rest.
*/
export class MarketOrder extends NeoComNode {
  public orderId: number = -1;
  public ownerId: number = -1;
  public typeId: number = -1;
  public regionId: number = -2;
  public locationId: number = -3;
  public range: string;
  public price: number = 0.0;
  public volumeTotal: number = 0;
  public volumeRemain: number = 0;
  public issuedDate: number = 0;
  public isBuyOrder: boolean = false;
  public minVolume: number = 0;
  public escrow: number = 0.0;
  public duration: number = 0;
  public isCorporation: boolean = false;
  public orderState: EOrderState = EOrderState.OPEN;

  protected item: EveItem = new EveItem();
  protected location: Location = null;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "MarketOrder";
    this.themeColor = new ColorTheme().setThemeColor(ESeparator.GREEN);
    // Transform the class fields from the parameter data.
    if (null != this.item) {
      let eveitem = new EveItem(this.item);
      this.item = eveitem;
    }
    if (null != this.location) {
      let loc = new Location(this.location);
      this.location = loc;
    }
    let orderStateString: string = values["orderState"];
    this.orderState = EOrderState[orderStateString];
    this.downloaded = true;
  }
  // --- GETTERS & SETTERS
  public getTypeId(): number {
    return this.typeId;
  }
  public getOrderState(): EOrderState {
    return this.orderState;
  }
  public getResourceName(): string {
    if (null != this.item) return this.item.name;
    else return "-ITEM not available-"
  }
  public getOrderPrice(): number {
    return this.price;
  }
  public getMarketSecurityLevel(): number {
    if (null != this.location) return this.location.security;
    else return 0.0;
  }
  public getOrderLocation(): Location {
    return this.location;
  }
  /** Return the Location Region -> Location Constellation*/
  public getLocationRegionConstellation(): string {
    if (null != this.location) return this.location.region + '►' + this.location.constellation;
    else return "-LOCATION not available-";
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { EVariant } from '../interfaces/EVariant.enumerated';
import { ESeparator } from '../interfaces/EPack.enumerated';
// import { ESlotGroup } from '../models/SlotLocation.model';
// //--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { EveItem } from '../models/EveItem.model';
// import { FittingItem } from '../models/FittingItem.model';
import { Separator } from '../models/Separator.model';
// import { GroupContainer } from '../models/GroupContainer.model';
// import { AssetGroupIconReference } from '../interfaces/IIconReference.interface';

export class ContractItem extends NeoComNode {
  public recordId: number = -1;
  public quantity: number = 0;

  public item: EveItem = new EveItem();

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "ContractItem";

    // Transform object fields.
    if (null != this.item) {
      let newitem: EveItem = new EveItem(this.item);
      this.item = newitem;
    }
    this.downloaded = true;
  }
  // --- GETTERS & SETTERS
  public getRecordId(): number {
    return this.recordId;
  }
  public getTypeId(): number {
    if (null != this.item) return this.item.typeId;
    else return -1;
  }
  public getItemName(): string {
    if (null != this.item) return this.item.name;
    else return "-Undefined Type Name-";
  }
  public getQuantity(): number {
    return this.quantity;
  }
  public getVolume(): number {
    return this.item.volume;
  }
  public getPrice(): number {
    return this.item.price;
  }
  public totalValueIsk(): number {
    return this.item.price * this.quantity;
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms
//               , the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code maid in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { EVariant } from '../interfaces/EVariant.enumerated';
import { IIconReference } from '../interfaces/IIconReference.interface';
import { AssetGroupIconReference } from '../interfaces/IIconReference.interface';
// import { NeoComError } from '../classes/NeoComError';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from './NeoComNode.model';
import { Separator } from './Separator.model';


export class GroupContainer extends NeoComNode {
  private id: number = -1;
  private title: string = "-G-CONTAINER-";
  private groupIcon: IIconReference = new AssetGroupIconReference("rookie_64.png");
  private contents: INeoComNode[] = [];

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "GroupContainer";
  }

  // --- ICOLLABORATION INTERFACE
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    console.log(">>[GroupContainer.collaborate2View]");
    // Initialize the list to be output.
    let collab: INeoComNode[] = [];
    // Check if the Region is expanded or not.
    if (this.isExpanded()) {
      console.log(">>[GroupContainer.collaborate2View]>Collaborating: " + "Separator.RED");
      collab.push(new Separator().setVariation(ESeparator.RED));
      console.log(">>[GroupContainer.collaborate2View]>Collaborating: " + "GroupContainer");
      collab.push(this);
      // Process each Location for new collaborations.
      for (let node of this.contents) {
        let partialcollab = node.collaborate2View(appModelStore, variant);
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
      console.log(">>[GroupContainer.collaborate2View]>Collaborating: " + "Separator.RED");
      collab.push(new Separator().setVariation(ESeparator.RED));
    } else {
      console.log(">>[GroupContainer.collaborate2View]>Collaborating: " + "GroupContainer");
      collab.push(this);
    }
    return collab;
  }
  // --- INEOCOMNODE INTERFACE
  // public getTypeId(): number {
  //   return this.id;
  // }
  // --- GETTERS & SETTERS
  public getGroupTitle(): string {
    return this.title;
  }
  public getGroupIconReference(): string {
    return this.groupIcon.getReference();
  }
  public setId(newid: number): GroupContainer {
    this.id = newid;
    return this;
  }
  public setTitle(newtitle: string): GroupContainer {
    this.title = newtitle;
    return this;
  }
  public setGroupIcon(reference: IIconReference): GroupContainer {
    this.groupIcon = reference;
    return this;
  }
  public addContent(newcontent: INeoComNode): GroupContainer {
    this.contents.push(newcontent);
    return this;
  }
  public getContentsCount(): number {
    return this.contents.length;
  }
  public getContents(): INeoComNode[] {
    return this.contents;
  }
}

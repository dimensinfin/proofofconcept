//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
// import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
// import { ESeparator } from '../interfaces/EPack.enumerated';
//--- MODELS
// import { NeoComNode } from '../models/NeoComNode.model';
// import { Resource } from '../models/Resource.model';
// import { NeoComAsset } from '../models/NeoComAsset.model';
// import { Location } from '../models/Location.model';
// import { EveItem } from 'app/models/EveItem.model';
// import { ColorTheme } from 'app/models/ui/ColorTheme.model';

export class Status {
  public jsonClass: string = "Status";
  public server: string = "TRANQUILITY";
  public serverStatus: boolean = true;
  public players: number = 26002;
  public serverVersion: string = "1308030";
  public databaseVersion: string = "20180508"
  public neocomVersion: number = 110;
  public start_time = "2018-05-10T11:05:20Z";

  constructor(values: Object = {}) {
    // super(values);
    Object.assign(this, values);
    this.jsonClass = "Status";
  }
}

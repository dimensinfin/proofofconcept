//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
// import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
// import { ESeparator } from '../interfaces/EPack.enumerated';
//--- MODELS
// import { NeoComNode } from '../models/NeoComNode.model';
// import { Resource } from '../models/Resource.model';
// import { NeoComAsset } from '../models/NeoComAsset.model';
// import { Location } from '../models/Location.model';
// import { EveItem } from 'app/models/EveItem.model';
// import { ColorTheme } from 'app/models/ui/ColorTheme.model';

export class ESISystem {
  public system_id: number = 30002964;
  public jsonClass: string = "ESISystem"
  public name: string = "Esescama";
  public position: object = {
    "x": -126385651879428400,
    "y": 56711478891005880,
    "z": -64502532715997550
  };
  public security_status: number = 0.6126919389;
  public stations: number[] = [
    60006526
  ];

  constructor(values: Object = {}) {
    // super(values);
    Object.assign(this, values);
    this.jsonClass = "ESISystem";
  }
}

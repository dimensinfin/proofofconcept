//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { Resource } from '../models/Resource.model';
import { ProcessingTask } from '../models/ProcessingTask.model';

export class Action extends NeoComNode {
  public typeId: number = -1;
  public itemName: string;
  public requestQty: number = 0;
  public completedQty: number = 0;
  public category: string;
  public groupName: string;
  public itemIndustryGroup: string;
  public price: number = 0.0;

  private resource: Resource = new Resource();
  private tasks: ProcessingTask[] = [];

  public animationState: string = "collapsed";

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Action";
    // Transform the class fields from the parameter data.
    if (null != this.resource) this.resource = new Resource(this.resource);
    if (null != this.tasks) {
      let newtaskList: ProcessingTask[] = [];
      for (let task of this.tasks) {
        newtaskList.push(new ProcessingTask(task));
      }
      this.tasks = newtaskList;
    }
    this.downloaded = true;
  }
  // --- GETTERS & SETTERS
  public getTypeId(): number {
    return this.typeId;
  }
  public getTasks(): ProcessingTask[] {
    return this.tasks;
  }
  public getResourceName(): string {
    return this.itemName;
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms
//               , the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code maid in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { EVariant } from '../interfaces/EVariant.enumerated';
import { IIconReference } from '../interfaces/IIconReference.interface';
import { AssetGroupIconReference } from '../interfaces/IIconReference.interface';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from './NeoComNode.model';
import { Separator } from './Separator.model';

/**
This is the component for a icon related item container. It is ever expanded because it add a label and policies to a set of nodes. Can have an icon end in case the icon is not defined then there is no icon shown.
*/
export class LabeledContainer extends NeoComNode {
  private groupIcon: IIconReference = null;
  private contents: INeoComNode[] = [];

  constructor(private id: number, private title: string) {
    super();
    this.jsonClass = "LabeledContainer";
    this.expanded = true;
  }

  // --- ICOLLABORATION INTERFACE
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    console.log(">>[LabeledContainer.collaborate2View]");
    // Initialize the list to be output.
    let collab: INeoComNode[] = [];
    // Check if the Region is expanded or not.
    // if (this.isExpanded()) {
    // console.log(">>[LabeledContainer.collaborate2View]>Collaborating: " + "Separator.RED");
    // collab.push(new Separator().setVariation(ESeparator.RED));
    console.log(">>[LabeledContainer.collaborate2View]>Collaborating: " + "GroupContainer");
    collab.push(this);
    // Process each Location for new collaborations.
    for (let node of this.contents) {
      let partialcollab = node.collaborate2View(appModelStore, variant);
      for (let partialnode of partialcollab) {
        collab.push(partialnode);
      }
    }
    // console.log(">>[LabeledContainer.collaborate2View]>Collaborating: " + "Separator.RED");
    // collab.push(new Separator().setVariation(ESeparator.RED));
    // } else {
    //   console.log(">>[LabeledContainer.collaborate2View]>Collaborating: " + "GroupContainer");
    //   collab.push(this);
    // }
    return collab;
  }
  // --- INEOCOMNODE INTERFACE
  public getTypeId(): number {
    return this.id;
  }
  // --- GETTERS & SETTERS
  public getGroupTitle(): string {
    return this.title;
  }
  public hasIcon(): boolean {
    if (null == this.groupIcon) return false;
  }
  public getGroupIconReference(): string {
    if (this.hasIcon())
      return this.groupIcon.getReference();
    else return "/assets/res-ui/drawable/defaulticonplaceholder.png";
  }
  public setGroupIcon(reference: IIconReference): LabeledContainer {
    this.groupIcon = reference;
    return this;
  }

  public addContent(newcontent: INeoComNode): LabeledContainer {
    this.contents.push(newcontent);
    return this;
  }
  public getContentsCount(): number {
    return this.contents.length;
  }
}

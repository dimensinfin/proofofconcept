//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
// import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
//--- MODELS

export class MenuBar /*implements IMenuBar*/ {
  // private selectorColor: ESeparator = ESeparator.WHITE;
  // private panelBorderColor: string = "panelborder-white";
  // private expandedBackgroundColor: string = 'white';

  constructor(values: Object = {}) {
    Object.assign(this, values);
    // this.adjustColors();
  }
  //--- IMENUBAR INTERFACE
  panelEnter(): void { }
  panelExit(): void { }
  ifInside(): boolean {
    return true;
  }
  hasMenu(): boolean { return true; }
  activateMenu(): void { }
  toggleMenu(): boolean { return true; }
  isMenuExpanded(): boolean { return true; }
  setMenuFlag(newflag: boolean): boolean { return true; }
  // getMenuSupporter(): IMenuBar {
  //   return this;
  // }
  //--- GETTERS & SETTERS
}

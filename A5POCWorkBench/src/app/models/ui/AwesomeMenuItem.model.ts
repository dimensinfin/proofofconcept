//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms
//               , the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code maid in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
import { IIconReference } from 'app/interfaces/IIconReference.interface';
import { AwesomeIconReference } from 'app/interfaces/IIconReference.interface';
//--- MODELS
import { MenuItem } from 'app/models/ui/MenuItem.model';

export class AwesomeMenuItem extends MenuItem {
  /** The awesome icon class to be used as menu image. */
  public _iconClass: AwesomeIconReference;
  // /** State for the check status for this menu item. */
  // public isActive: boolean = false;
  // /** Pointer to the icon to show on the menu item. */
  // private _iconReference: IIconReference = null;
  // /** The action to do when the menu item is selected. */
  // private _menuAction: Function = null;

  //--- G E T T E R S   &   S E T T E R S
  public getIconReference(): string {
    if (this.hasIcon())
      return this._iconClass.getReference();
    else return "fas fa-check";
  }
  public setIconReference(iconref: AwesomeIconReference): MenuItem {
    this._iconClass = iconref;
    return this;
  }
  public hasIcon(): boolean {
    if (null == this._iconClass) return false;
    else return true;
  }
}

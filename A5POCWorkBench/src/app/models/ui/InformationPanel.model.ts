//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
import { EVariant } from 'app/interfaces/EVariant.enumerated';
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';

/**
This model is represented by an optional icon and a description that usually will be a message to the user. Then it will also display a counter of its contents. The node is ever expanded so the collaboration always included its contents.
*/
export class InformationPanel extends NeoComNode {
  public description: string = "-DESCRIPTION-";
  public contents: INeoComNode[] = [];

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "InformationPanel";
  }
  // --- GETTERS & SETTERS
  public getDescription(): string {
    return this.description;
  }
  public getContentsCount(): number {
    return this.contents.length;
  }
  public addContent(appModelStore: AppModelStoreService, newcontent: INeoComNode) {
    this.contents.push(newcontent);
    // Report the new data to the backnd persistence store.
    // appModelStore.getBackendPutPilotFittingRequests(92002067, newcontent)
    //   .subscribe();
  }
  // --- ICOLLABORATION INTERFACE
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    let collab: INeoComNode[] = [];
    collab.push(this);
    for (let node of this.contents) {
      collab.push(node);
    }
    return collab;
  }
}

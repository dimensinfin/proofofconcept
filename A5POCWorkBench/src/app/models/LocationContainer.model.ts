//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { ESeparator } from '../interfaces/EPack.enumerated';
import { EVariant } from '../interfaces/EPack.enumerated';
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { INeoComAsset } from '../interfaces/INeoComAsset.interface';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { Location } from '../models/Location.model';
import { NeoComAsset } from '../models/NeoComAsset.model';
import { SpaceContainer } from '../models/SpaceContainer.model';
import { Ship } from '../models/Ship.model';
import { Separator } from '../models/Separator.model';
import { ColorTheme } from 'app/models/ui/ColorTheme.model';

export class LocationContainer extends Location {
  // This fiels should be accesible on not linked locations. Make it here until I implement those.
  // private _location: Location = new Location();
  private contents: INeoComAsset[] = [];
  private totalValueCalculated: number = -1;
  private totalVolumeCalculated: number = -1;

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "LocationContainer";
  }

  //--- I C O L L A B O R A T I O N    I N T E R F A C E
  /**
  This is the standard method to generate the list of elements that should be rendered on the viewer page. For a Location the collaboration of a collapsed one the result should be itself. For an expanded location we should recursively add the Locations contents collaboration to the list. So a Location with a Container will check also the state of that Container.
  When the Location is expanded se add two Separators, one before the Location and another after the collaborated list of nodes.
  The list of assets collaborated should be ordered by name, but taking on mind not to include on that order their contents at the same time. So the ordering should be made before the collaboration loop.
  */
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    let collab = [];
    // If the node is expanded then add its assets.
    if (this.expanded) {
      // Check if the contents of the Location are downloaded.
      // if (this.downloaded) {
      // Add the Separator depending on the node's theme color.
      let color = this.themeColor.getThemeCode();
      console.log(">>[Region.collaborate2View]> Collaborating: " + color);
      collab.push(new Separator().setVariation(color));
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Location");
      collab.push(this);
      // Process each item at the rootlist for more collaborations.
      // Apply the processing policies before entering the processing loop. Usually does the sort.
      let sortedContents: INeoComAsset[] = this.getContents().sort((a1: INeoComAsset, a2: INeoComAsset) => {
        // let a1 = n1 as NeoComAsset;
        // let a2 = n2 as NeoComAsset;
        if (a1.getName() > a2.getName()) {
          return 1;
        }
        if (a1.getName() < a2.getName()) {
          return -1;
        }
        return 0;
      });
      for (let node of sortedContents) {
        let partialcollab = node.collaborate2View(appModelStore, variant);
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
      // } else {
      //   // Call the backend to download the contents. On callback we need to fire an event to refresh the display.
      //   appModelStore.getBackendLocationContents(this.getLocationId())
      //     .subscribe(result => {
      //       console.log("--[Location.collaborate2View.getBackendLocationsContents]>AssetList: " + result.length);
      //       // The the list of assets contained at the location.
      //       // Process them to upgrade to expandable types and also to account for their volume and price.
      //       // TODO This activates the content accounting to set the content values and volume.
      //       // this.contents = this.processDownloadedAssets(result);
      //       this.downloaded = true;
      //       appModelStore.fireRefresh();
      //     });
      //   // Add an spinner to the output to inform the user of the background task.
      //   console.log(">>[Region.collaborate2View]> Collaborating: " + "Separator.SPINNER");
      //   collab.push(new Separator().setVariation(ESeparator.SPINNER));
      // }
      console.log(">>[Region.collaborate2View]> Collaborating: " + color);
      collab.push(new Separator().setVariation(color));
    } else {
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Location");
      collab.push(this);
    }
    return collab;
  }

  //--- GETTERS & SETTERS
  public getFormattedStackCount(): string {
    if (this.contents.length == 0) return "-";
    else return new Intl.NumberFormat('en-us', {
      minimumFractionDigits: 0
    }).format(Number(this.contents.length));
    // this.contentSize.toFixed(0).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  }
  public setLocation(newlocation: Location): LocationContainer {
    // this._location = newlocation;
    Object.assign(this, newlocation);
    return this;
  }
  public addContent(node: INeoComAsset): void {
    if (null != node) {
      this.contents.push(node);
      // Do value accountng.
      this.accountContent(node);
    }
  }
  public getContents(): INeoComAsset[] {
    return this.contents;
  }
  private accountContent(asset: INeoComAsset): void {
    switch (asset.getJsonClass()) {
      case "NeoComAsset":
        // let assetNode = asset as NeoComAsset;
        this.totalValueCalculated += asset.getItem().getPrice() * asset.getQuantity();
        this.totalVolumeCalculated += asset.getItem().getVolume() * asset.getQuantity();
        break;
      case "SpaceContainer":
        // let container = asset as SpaceContainer;
        this.totalValueCalculated += asset.getTotalValue();
        this.totalVolumeCalculated += asset.getTotalVolume();
        break;
      case "Ship":
        // let ship = asset as Ship;
        this.totalValueCalculated += asset.getTotalValue();
        break;
    }
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { EveItem } from 'app/models/EveItem.model';
import { Resource } from 'app/models/Resource.model';
import { PlanetaryTransformation } from 'app/models/PlanetaryTransformation.model';

export class ProcessingAction extends NeoComNode {
  public targetId = -1;
  public outputQty: number = 100;
  // public runs: number = 0;
  public resources: Resource[] = [];
  private inputList = [];
  private output = null;

  private targetItem: EveItem = new EveItem();

  // public actions: PlanetaryTransformation[] = [];
  // public actionResults = [];

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "ProcessingAction";
    // Transform the class fields from the parameter data.
    if (null != this.targetItem) {
      let newitem = new EveItem(this.targetItem);
      this.targetItem = newitem;
    }
    let parsedResources = [];
    for (let res of this.resources) {
      // res.quantity=Math.abs(res.quantity);
      if (res.jsonClass == "Resource")
        parsedResources.push(new Resource(res));
      if (res.jsonClass == "PlanetaryResource")
        parsedResources.push(new Resource(res));
    }
    this.resources = parsedResources;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getTypeId(): number {
    return this.targetId;
  }
  public getResources(): Resource[] {
    return this.resources;
  }
}

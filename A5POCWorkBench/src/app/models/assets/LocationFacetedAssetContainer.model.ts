//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
//--- MODELS
import { Location } from 'app/models/Location.model';
import { NeoComAsset } from 'app/models/NeoComAsset.model';
import { Separator } from 'app/models/Separator.model';
import { Ship } from 'app/models/Ship.model';
import { SpaceContainer } from 'app/models/SpaceContainer.model';

export class LocationFacetedAssetContainer extends Location {
  public contents: NeoComAsset[] = [];
  private totalValueCalculated: number = 0;
  private totalVolumeCalculated: number = 0;

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "LocationFacetedAssetContainer";
    // Transform the class fields from the parameter data.
    // if (null != this.facet) {
    //   let newloc = new Location(this.facet);
    //   this.facet = newloc;
    // }
    let parsedContents = [];
    for (let res of this.contents) {
      if (res.jsonClass == "NeoComAsset")
        parsedContents.push(new NeoComAsset(res));
      if (res.jsonClass == "Ship")
        parsedContents.push(new Ship(res));
      if (res.jsonClass == "SpaceContainer")
        parsedContents.push(new SpaceContainer(res));
    }
    this.contents = parsedContents;
  }

  //--- I A S S E T F A C E T E D   I N T E R F A C E
  public addContent(newasset: NeoComAsset): number {
    this.contents.push(newasset);
    this.accountContent(newasset);
    return this.contents.length;
  }
  private accountContent(asset: NeoComAsset): void {
    switch (asset.getJsonClass()) {
      case "NeoComAsset":
        // let assetNode = asset as NeoComAsset;
        this.totalValueCalculated += asset.getItem().getPrice() * asset.getQuantity();
        this.totalVolumeCalculated += asset.getItem().getVolume() * asset.getQuantity();
        break;
      case "SpaceContainer":
        // let container = asset as SpaceContainer;
        this.totalValueCalculated += asset.getTotalValue();
        this.totalVolumeCalculated += asset.getTotalVolume();
        break;
      case "Ship":
        // let ship = asset as Ship;
        this.totalValueCalculated += asset.getTotalValue();
        break;
    }
  }

  //--- I C O L L A B O R A T I O N    I N T E R F A C E
  /**
  This is the standard method to generate the list of elements that should be rendered on the viewer page. For a Location the collaboration of a collapsed one the result should be itself. For an expanded location we should recursively add the Locations contents collaboration to the list. So a Location with a Container will check also the state of that Container.
  When the Location is expanded se add two Separators, one before the Location and another after the collaborated list of nodes.
  The list of assets collaborated should be ordered by name, but taking on mind not to include on that order their contents at the same time. So the ordering should be made before the collaboration loop.
  */
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    let collab = [];
    // If the node is expanded then add its assets.
    if (this.expanded) {
      // Check if the contents of the Location are downloaded.
      // if (this.downloaded) {
      // Add the Separator depending on the node's theme color.
      let color = this.themeColor.getThemeCode();
      console.log(">>[Region.collaborate2View]> Collaborating: " + color);
      collab.push(new Separator().setVariation(color));
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Location");
      collab.push(this);
      // Process each item at the rootlist for more collaborations.
      // Apply the processing policies before entering the processing loop. Usually does the sort.
      let sortedContents: NeoComAsset[] = this.contents.sort((a1: NeoComAsset, a2: NeoComAsset) => {
        // let a1 = n1 as NeoComAsset;
        // let a2 = n2 as NeoComAsset;
        if (a1.getName() > a2.getName()) {
          return 1;
        }
        if (a1.getName() < a2.getName()) {
          return -1;
        }
        return 0;
      });
      for (let node of sortedContents) {
        let partialcollab = node.collaborate2View(appModelStore, variant);
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
      console.log(">>[Region.collaborate2View]> Collaborating: " + color);
      collab.push(new Separator().setVariation(color));
    } else {
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Location");
      collab.push(this);
    }
    return collab;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getFacetClass(): string {
    return "Location";
  }
  public getFacetName(): string {
    return this.getName();
  }
  public getFormattedStackCount(): string {
    if (this.contents.length == 0) return "-";
    else return new Intl.NumberFormat('en-us', {
      minimumFractionDigits: 0
    }).format(Number(this.contents.length));
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
// import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
// import { INeoComNode } from '../classes/INeoComNode.interface';
// import { EVariant } from '../classes/EVariant.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { EveItem } from '../models/EveItem.model';
import { SlotLocation } from '../models/SlotLocation.model';
import { Fitting } from '../models/Fitting.model';

export class FittingRequest extends NeoComNode {
  public requestId: number = -5;
  public corporationId: number = -2;
  public targetFitting: number = -4;
  public copies: number = 1;
  public state: string = "outstanding";
  public startDate = Date.now();
  public estimatedCost: number = 0.0;
  public saved: boolean = false;

  public fitting: Fitting = new Fitting();

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "FittingRequest";
    // Transform the input abstract items into classes.
    if (null != this.fitting) {
      let newfitting = new Fitting(this.fitting);
      this.fitting = newfitting;
    }

    this.downloaded = true;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getRequestId(): number {
    return this.requestId;
  }
  public getFittingName(): string {
    if (null != this.fitting) return this.fitting.name;
    else return "-NAME-";
  }
  public getFittingIdentifier(): number {
    if (null != this.fitting) return this.fitting.fittingId;
    else return -1;
  }
}

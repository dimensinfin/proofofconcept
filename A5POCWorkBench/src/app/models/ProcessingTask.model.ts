//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { ESeparator } from '../interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { Resource } from '../models/Resource.model';
import { NeoComAsset } from '../models/NeoComAsset.model';
import { Location } from '../models/Location.model';
import { EveItem } from 'app/models/EveItem.model';
import { ColorTheme } from 'app/models/ui/ColorTheme.model';

/**
Represents a task to be completed to fulfill some manufacturing action. By example, if we need 10 Warp Core Stabilizers and we only have 3 on stock and another 5 on another station, the Action to complete the 10 WCS Action can be decomposed into 3 ProcessingTasks. One AVAILABLE for the 3 on stock, another MOVE for the 5 on another location and a BUY task for the rest.
*/
export class ProcessingTask extends NeoComNode {
  public taskType: string = 'BUY';
  public taskState: string = 'PENDING';
  public typeId: number = -1;
  public quantity: number = 0;
  public price: number = 0.0;
  public basePrice: number = 0.0;

  public item: EveItem = new EveItem();
  private sourceLocation: Location = null;
  private destination: Location = null;
  private referencedAsset: NeoComAsset;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "ProcessingTask";
    this.themeColor = new ColorTheme().setThemeColor(ESeparator.GREEN);
    // Transform the class fields from the parameter data.
    if (null != this.item) {
      let eveitem = new EveItem(this.item);
      this.item = eveitem;
    }
    if (null != this.referencedAsset) this.referencedAsset = new NeoComAsset(this.referencedAsset);
    if (null != this.sourceLocation) {
      let loc = new Location(this.sourceLocation);
      this.sourceLocation = loc;
    }
    if (null != this.destination) {
      let loc = new Location(this.destination);
      this.destination = loc;
    }
    // this.destination = new Location(this.destination);
    // Correct current model defects.
    // this.quantity = this.qty;
    // this.destination = new Location({
    //   "jsonClass": "EveLocation",
    //   "id": 60006526,
    //   "stationID": 60006526,
    //   "station": "Esescama VIII - Moon 3 - Imperial Armaments Warehouse",
    //   "systemID": 30002964,
    //   "system": "Esescama",
    //   "constellationID": 20000434,
    //   "constellation": "Ryra",
    //   "regionID": 10000036,
    //   "region": "Devoid",
    //   "security": "0.612691959383857",
    //   "typeID": "CCPLOCATION",
    //   "urlLocationIcon": "http://image.eveonline.com/Render/1529_64.png",
    //   "name": "Esescama - Esescama VIII - Moon 3 - Imperial Armaments Warehouse",
    //   "securityValue": 0.612691959383857,
    //   "citadel": false,
    //   "unknown": true,
    //   "fullLocation": "[0.612691959383857] Esescama VIII - Moon 3 - Imperial Armaments Warehouse - Devoid > Esescama",
    //   "realId": -2
    // });
    this.downloaded = true;
  }
  // --- GETTERS & SETTERS
  public getTypeId(): number {
    return this.typeId;
  }
  public getTaskType(): string {
    return this.taskType;
  }
  public getQuantity(): number {
    return this.quantity;
  }
  public getPrice(): number {
    return this.price;
  }
  public totalValueIsk(): number {
    let sellerData = this.item.lowestSellerPrice;
    if (null != sellerData) {
      let sellerPrice = sellerData["price"];
      if (null != sellerPrice) return this.quantity * Number(sellerPrice);
    }
    return this.quantity * this.price;
  }
  /**
  Transforms the limited list of task types into a number suitable to be used and compatible with nodes that use ID as the indexing number. Numbering starts with 1001 for BUY.
  */
  public typeMapper(type: string): number {
    if (type == 'BUY') return 1001;
    if (type == 'MOVE') return 1002;
    if (type == 'REFINE') return 1003;
    return 1000;
  }
  public getResourceTypeId(): number {
    if (null != this.item) return this.item.typeId;
    else return 1;
  }
  public getResourceTypeName(): string {
    if (null != this.item) return this.item.name;
    else return "";
  }
  public getVolume(): number {
    if (null != this.item) return this.quantity * this.item.getVolume();
    else return 0.0;
  }
  public getSourceLocation(): Location {
    if (null != this.sourceLocation) return this.sourceLocation;
    else return new Location();
  }
  public getDestinationLocation(): Location {
    if (null != this.destination) return this.destination;
    else return new Location();
  }
  public sourceLocationSystemId(): number {
    if (null != this.sourceLocation) return this.sourceLocation.getSystemId();
    else return -1;
  }
  public destinationLocationSystemId(): number {
    if (null != this.destination) return this.destination.getSystemId();
    else return -1;
  }
  // public sourceLocationSecurity(): number {
  //   if (null != this.sourceLocation) return this.sourceLocation.securityValue;
  //   else return 0.0;
  // }
  // public sourceLocationName(): string {
  //   if (null != this.sourceLocation) return this.sourceLocation.region + ' - ' + this.sourceLocation.system;
  //   else return "unknown";
  // }
  // public destinationLocationSecurity(): number {
  //   if (null != this.destination) return this.destination.securityValue;
  //   else return 0.0;
  // }
  // public destinationLocationName(): string {
  //   if (null != this.destination) return this.destination.region + ' - ' + this.destination.system;
  //   else return "unknown";
  // }
}

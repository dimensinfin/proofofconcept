//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { Location } from '../models/Location.model';
import { Separator } from '../models/Separator.model';

export class Region extends NeoComNode {
  public regionId: number = -1;
  // public name : string="-NAME-";
  public title: string = "-REGION-";
  private _locations: Location[] = [];
  // public locationCount: number = 0;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Region";
    // this.locations = this.processLocations(this.locations);
  }

  /**
  Method to process the downloaded data and converting the anonymous json objects to the Model classes.
  */
  public processLocations(locs: any[]): Location[] {
    let results: Location[] = [];
    for (let lockey of locs) {
      // Reload the right download stae of the Locations.
      let newloc = new Location(lockey);
      newloc.setDownloadState(false);
      newloc.collapse();
      results.push(newloc);
    }
    return results;
  }

  //--- I N T E R F A C E
  public collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): NeoComNode[] {
    // Initialize the list to be output.
    let collab: NeoComNode[] = [];
    // Check if the Region is expanded or not.
    if (this.expanded) {
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Separator.RED");
      collab.push(new Separator().setVariation(ESeparator.RED));
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Region");
      collab.push(this);
      // Process each Location for new collaborations.
      for (let node of this._locations) {
        let partialcollab = [];
        //  if (node.jsonClass == "Location")
        partialcollab = node.collaborate2View(appModelStore, variant);
        // else {
        //   let loc = new Location(node);
        //   partialcollab = node.collaborate2View(appModelStore, variant);
        // }
        for (let partialnode of partialcollab) {
          collab.push(partialnode);
        }
      }
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Separator.RED");
      collab.push(new Separator().setVariation(ESeparator.RED));
    } else {
      console.log(">>[Region.collaborate2View]> Collaborating: " + "Region");
      collab.push(this);
    }
    return collab;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getRegionId(): number {
    return this.regionId;
  }
  public getName(): string {
    return this.title;
  }
  public getStationCount(): number {
    return this._locations.length;
  }
  public setRegionId(id: number): Region {
    this.regionId = id;
    return this;
  }
  public setName(newname: string): Region {
    this.title = newname;
    return this;
  }
  public setRegionName(newname: string): Region {
    this.title = newname;
    return this;
  }
  public addLocation(newlocation: Location): void {
    this._locations.push(newlocation);
  }
}

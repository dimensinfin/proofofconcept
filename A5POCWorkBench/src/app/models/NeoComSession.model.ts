//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/share';
// import 'rxjs/add/operator/subscribe';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
// import { EVariant } from '../interfaces/EPack.enumerated';
// import { ESeparator } from '../interfaces/EPack.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { Credential } from '../models/Credential.model';
import { Pilot } from '../models/Pilot.model';

export class NeoComSession extends NeoComNode {
  public sessionId: string = "-UNIQUE-IDENTIFIER-";
  public publicKey: string = "-RSA-GENERATED-KEY-";
  public pilotIdentifier: number = -1;
  public credential: Credential = new Credential();

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "NeoComSession";
    this.renderWhenEmpty = true;
    // Transform the Credential field to a class Pilot.
    if (null != this.credential) {
      let cred = new Credential(this.credential);
      this.credential = cred;
      this.pilotIdentifier = this.credential.getAccountId();
    }
    this.downloaded = true;
  }

  // --- G E T T E R S   &   S E T T E R S
  public getCredential(): Credential {
    return this.credential;
  }
  public getPilotIdentifier(): number {
    if (null != this.credential)
      return this.credential.getAccountId();
    else return this.pilotIdentifier;
  }
  public getPilot(): Pilot {
    return this.credential.getPilot();
  }
  public storePilot(pilot: Pilot): void {
    if (null != this.credential) {
      this.credential.setPilot(pilot);
    }
  }
}

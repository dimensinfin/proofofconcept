//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { EveItem } from 'app/models/EveItem.model';
import { Location } from 'app/models/Location.model';
import { Resource } from 'app/models/Resource.model';
import { RefiningProcess } from 'app/models/industry/RefiningProcess.model';

export class FacetedResourceContainer extends NeoComNode {
  public facet: Location = new Location();
  public contents: Resource[] = [];

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "Resource";
    // Transform the class fields from the parameter data.
    if (null != this.facet) {
      let newloc = new Location(this.facet);
      this.facet = newloc;
    }
    let parsedContents = [];
    for (let res of this.contents) {
      if (res.jsonClass == "Resource")
        parsedContents.push(new Resource(res));
      if (res.jsonClass == "RefiningProcess")
        parsedContents.push(new RefiningProcess(res));
    }
    this.contents = parsedContents;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getFacetName(): string {
    return this.facet.getName();
  }
  public getRefiningProcesses(): RefiningProcess[] {
    let processes = [];
    for (let container of this.contents) {
      if (container.jsonClass == "RefiningProcess") {
        // ONly add the process if the quantity is grater than the pack limit.
        if (container.getQuantity() > 100) processes.push(container);
      }
    }
    return processes;
  }
}

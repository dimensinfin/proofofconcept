//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
//--- MODELS
import { Resource } from 'app/models/Resource.model';
import { EveItem } from 'app/models/EveItem.model';

export class RefiningProcess extends Resource {
  public refineResults: Resource[] = [];

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "RefiningProcess";
    // Transform the class fields from the parameter data.
    if (null != this.item) {
      let newitem = new EveItem(this.item);
      this.item = newitem;
    }
    let parsedContents = [];
    for (let res of this.refineResults) {
      parsedContents.push(new Resource(res));
    }
    this.refineResults = parsedContents;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getIconUrl(): string {
    return this.item.getIconUrl();
  }
  public getMineral(targetId: number): Resource {
    for (let resource of this.refineResults) {
      if (resource.typeId == targetId)
        return resource;
    }
  }
  public getQuantity(): number {
    return this.baseQuantity * this.stackSize;
  }
}

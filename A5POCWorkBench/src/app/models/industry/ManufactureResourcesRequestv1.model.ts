//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { EveItem } from 'app/models/EveItem.model';
import { Resource } from 'app/models/Resource.model';

export class ManufactureResourcesRequestv1 extends NeoComNode {
  public jobBlueprintId: number = -5;
  public copies: number = 1;
  public lom: Resource[] = [];

  private jobBlueprint: EveItem = null;

  //--- M O D E L   C O N S T R U C T O R
  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "ManufactureResourcesRequestV1";
    // Transform the class fields from the parameter data.
    if (null != this.jobBlueprint) {
      let newitem = new EveItem(this.jobBlueprint);
      this.jobBlueprint = newitem;
    }
    let bom = [];
    for (let res of this.lom) {
      bom.push(new Resource(res));
    }
    this.lom = bom;
  }

  public searchMineral(id: number): Resource {
    for (let resource of this.lom) {
      if (resource.typeId == id) return resource;
    }
    return null;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getTargetName(): string {
    if (null != this.jobBlueprint) return this.jobBlueprint.getName();
    else return "-DATA-NOT-LOADED-";
  }
  public getBlueprint(): EveItem {
    return this.jobBlueprint;
  }
  public getBillOfmaterials(): Resource[] {
    return this.lom;
  }
  public getMineral(targetId: number): Resource {
    for (let resource of this.lom) {
      if (resource.typeId == targetId) {
        // Adjust the number of resources required with the number of copies
        return resource;
      }
    }
  }
  public getCopies(): number {
    return this.copies;
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { EVariant } from '../interfaces/EVariant.enumerated';
//--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';

export class EveItem extends NeoComNode {
  public typeId: number = -1;
  public name: string = "Mammoth";
  public groupId: number = 28;
  public groupName: string = "Industrial";
  public categoryId: number = 6;
  public categoryName: string = "Ship";
  public baseprice: number = 0;
  public price: number = -1;
  public tech: string = "Tech I";
  public volume: number = 0;
  // public defaultprice: number = -1;
  public hullGroup: string = "not-applies";
  public industryGroup: string = "HULL";
  // public blueprint: boolean = false;

  public highestBuyerPrice;
  public lowestSellerPrice;

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "EveItem";
  }
  // --- ICOLLABORATION INTERFACE
  // --- GETTERS & SETTERS
  public getTypeId(): number {
    return this.typeId;
  }
  public getName(): string {
    return this.name;
  }
  public getQuantity(): number {
    // if (null == this.quantity) return 1;
    // else return this.quantity;
    return 1;
  }
  public getIconUrl(): string {
    return "http://image.eveonline.com/Type/" + this.typeId + "_64.png";
  }
  public getPrice(): number {
    if (null == this.price) return 0.0;
    else return this.price;
  }
  public getVolume(): number {
    if (null == this.volume) return 0.01;
    else return this.volume;
  }
  public getGroupId(): number {
    return this.groupId;
  }
  public getGroupName(): string {
    return this.groupName;
  }
  public setTypeId(newtype: number): EveItem {
    this.typeId = newtype;
    return this;
  }
  public setName(name: string): EveItem {
    this.name = name;
    return this;
  }
}

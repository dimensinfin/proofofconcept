//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreService } from '../services/app-model-store.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
import { EVariant } from '../interfaces/EVariant.enumerated';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
// import { ESlotGroup } from '../models/SlotLocation.model';
// //--- MODELS
import { NeoComNode } from '../models/NeoComNode.model';
import { EveItem } from '../models/EveItem.model';
import { ContractItem } from '../models/ContractItem.model';
// import { FittingItem } from '../models/FittingItem.model';
import { Separator } from '../models/Separator.model';
// import { GroupContainer } from '../models/GroupContainer.model';
// import { AssetGroupIconReference } from '../interfaces/IIconReference.interface';

export class Contract extends NeoComNode {
  public contractId: number = -1;
  public issuerId: number = -2;
  public type: string = "item_exchange";
  public status: string = "outstanding";

  private contractItems: ContractItem[] = [];

  constructor(values: Object = {}) {
    super(values);
    Object.assign(this, values);
    this.jsonClass = "Contract";

    // Transform object fields.
    if (null != this.contractItems) {
      let items: ContractItem[] = [];
      for (let newitem of this.contractItems) {
        let newContractItem = new ContractItem(newitem);
        items.push(newContractItem);
      }
      this.contractItems = items;
    }
    this.downloaded = true;
    this.expanded = true;
  }

  // --- ICOLLABORATION INTERFACE
  collaborate2View(appModelStore: AppModelStoreService, variant: EVariant): INeoComNode[] {
    let collab: INeoComNode[] = [];
    // If the node is expanded then add its assets.
    if (this.isExpanded()) {
      console.log(">>[Contract.collaborate2View]> Collaborating: " + "Separator.BLUE");
      collab.push(new Separator().setVariation(ESeparator.BLUE));
      console.log(">>[Contract.collaborate2View]> Collaborating: " + "Contract");
      collab.push(this);
      // Apply the processing policies before entering the processing loop. Usually does the sort.
      let sortedContents: ContractItem[] = this.contractItems.sort((n1: ContractItem, n2: ContractItem) => {
        if (n1.item.name > n2.item.name) {
          return 1;
        }
        if (n1.item.name < n2.item.name) {
          return -1;
        }
        return 0;
      });
      // Now collaborate the items contained on the contract.
      for (let item of sortedContents) {
        collab.push(item);
      }
      collab.push(new Separator().setVariation(ESeparator.BLUE));
    } else collab.push(this);
    return collab;
  }
  // --- GETTERS & SETTERS
  public getContractId(): number {
    return this.contractId;
  }
  public getContentsCount(): number {
    return this.contractItems.length;
  }
  public getContractTypeName(): string {
    if (this.type == 'item_exchange') return 'ITEM EXCHANGE';
    if (this.type == 'courier') return 'COURIER LOGISTICS';
    if (this.type == 'auction') return 'AUCTION';
    return 'UNKNOWN';
  }
  public getVolumeUsed(): number {
    let volume: number = 0.0;
    for (let item of this.contractItems) {
      volume += item.getQuantity() * item.getVolume();
    }
    return volume;
  }
  public getEstimatedValue(): number {
    let value: number = 0.0;
    for (let item of this.contractItems) {
      value += item.getQuantity() * item.getPrice();
    }
    return value;
  }
}

import { differenceInMilliseconds } from 'date-fns';

export class Chrono {
  private _timer: Date = new Date();
  public elapsed(): number {
    return differenceInMilliseconds(this._timer, new Date());
  }
}
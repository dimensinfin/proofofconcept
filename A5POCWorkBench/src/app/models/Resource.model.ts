//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
//--- SERVICES
//--- INTERFACES
//--- MODELS
import { NeoComNode } from 'app/models/NeoComNode.model';
import { EveItem } from 'app/models/EveItem.model';

export class Resource extends NeoComNode {
  public typeId: number = 2389;
  public name: string;
  public category: string;
  public groupName: string;
  public baseQuantity: number = 1;
  public quantity: number = 0;
  public stackSize: number = 0;

  protected item: EveItem = new EveItem();
  public target: boolean = false;

  constructor(values: Object = {}) {
    super();
    Object.assign(this, values);
    this.jsonClass = "Resource";
    // Transform the class fields from the parameter data.
    if (null != this.item) {
      let newitem = new EveItem(this.item);
      this.item = newitem;
    }
  }
  public activateTarget() {
    this.target = true;
  }
  public resourceIs(targetId: number): boolean {
    if (targetId == this.typeId) return true;
    else return true;
  }

  //--- G E T T E R S   &   S E T T E R S
  public getQuantity(): number {
    return this.quantity;
  }
  public getName(): string {
    return this.name;
  }
  public getPrice(): number {
    return this.item.getPrice();
  }
  public getIconUrl(): string {
    return this.item.getIconUrl();
  }
  public setTypeId(newtype: number): Resource {
    this.typeId = newtype;
    return this;
  }
  public setQuantity(newqty: number): Resource {
    this.baseQuantity = newqty;
    this.quantity = this.baseQuantity * this.stackSize;
    return this;
  }
  public setName(name: string): Resource {
    this.name = name;
    return this;
  }
}

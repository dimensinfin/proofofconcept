//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
// import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { Inject } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
// import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServicePilot } from './app-model-store.pilot.service';
//--- INTERFACES
//--- MODELS
import { NeoComAsset } from '../models/NeoComAsset.model';
import { ProcessingAction } from '../models/planetary/ProcessingAction.model';

export class AppModelStoreServicePlanetary extends AppModelStoreServicePilot {
  //--- P L A N E T A R Y   S E C T I O N
  public accessPilotPlanetaryResources(pilotId: number): Observable<NeoComAsset[]> {
    console.log("><[AppModelStoreServicePlanetary.accessPilotPlanetaryResources]>pilotId: " + pilotId);
    // Get the list from the backend Database.
    return this.getBackendsPilotPlanetaryResources(pilotId);
  }
  public optimizePlanetaryResourcesAtSystem(pilotId: number, systemId: number): Observable<ProcessingAction[]> {
    console.log("><[AppModelStoreServicePlanetary.optimizePlanetaryResourcesAtSystem]> pilotId: " + pilotId
      + " system: " + systemId);
    // Get the list from the backend Database.
    return this.getBackendsOptimizePlanetaryResourcesAtSystem(pilotId, systemId);
  }

  //--- B A C K E N D   C A L L S
  //--- P L A N E T A R Y   S E C T I O N
  public getBackendsPilotPlanetaryResources(pilotId: number): Observable<NeoComAsset[]> {
    console.log("><[AppModelStoreServicePlanetary.getBackendsPilotPlanetaryResources]> Pilot Id: " + pilotId);
    let request = AppModelStoreServicePlanetary.RESOURCE_SERVICE_URL + "/pilot/" + pilotId
      + "/planetarymanager/planetaryresources";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.http.get(request)
      // .map(res => res.json())
      .map((resourceList: any) => {
        console.log("<<[AppModelStoreServicePlanetary.getBackendsPilotPlanetaryResources]>Processed: " + resourceList.length);
        return this.transformRequestOutput(resourceList) as NeoComAsset[];
      });
  }
  public getBackendsOptimizePlanetaryResourcesAtSystem(pilotId: number, systemId: number): Observable<ProcessingAction[]> {
    console.log("><[AppModelStoreServicePlanetary.getBackendsOptimizePlanetaryResourcesAtSystem]> pilotId: " + pilotId
      + " system: " + systemId);
    let request = AppModelStoreServicePlanetary.RESOURCE_SERVICE_URL + "/pilot/" + pilotId
      + "/planetarymanager/optimizeprocess/system/" + systemId;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      // .map(res => res.json())
      .map((actionList: any) => {
        console.log("<<[AppModelStoreServicePlanetary.getBackendsOptimizePlanetaryResourcesAtSystem]> Processed: " + actionList.length);
        return this.transformRequestOutput(actionList) as ProcessingAction[];
      });
  }
  //--- P R I V A T E   S E C T I O N
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// // import { HttpClient } from '@angular/common/http';
// import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServiceFittings } from './app-model-store.fittings.service';
//--- INTERFACES
// import { INeoComNode } from '../interfaces/INeoComNode.interface';
//--- MODELS
import { MarketOrder } from '../models/MarketOrder.model';
import { ManufactureResourcesRequestv1 } from '../models/industry/ManufactureResourcesRequestv1.model';
import { FacetedResourceContainer } from '../models/industry/FacetedResourceContainer.model';

export class AppModelStoreServiceIndustry extends AppModelStoreServiceFittings {
  //--- I N D U S T R Y   S E C T I O N
  public accessMarketOrders4Pilot(pilotid: number): Observable<MarketOrder[]> {
    console.log("><[AppModelStoreServiceIndustry.accessMarketOrders4Pilot]> Pilot id: " + pilotid);
    return this.getBackendPilotMarketOrders(pilotid);
  }
  //-- M A N U F A C T U R E   S E C T I O N
  public accessManufactureResourcesHulls4Pilot(pilotid: number): Observable<ManufactureResourcesRequestv1[]> {
    console.log("><[AppModelStoreServiceIndustry.accessManufactureResourcesHulls4Pilot]> Pilot id: " + pilotid);
    return this.getBackendManufactureResourcesHulls4Pilot(pilotid);
  }
  public accessManufactureResourcesStructures4Pilot(pilotid: number): Observable<ManufactureResourcesRequestv1[]> {
    console.log("><[AppModelStoreServiceIndustry.accessManufactureResourcesStructures4Pilot]> Pilot id: " + pilotid);
    return this.getBackendManufactureResourcesStructures4Pilot(pilotid);
  }
  public accessManufactureResourcesAvailable4Pilot(pilotid: number): Observable<FacetedResourceContainer[]> {
    console.log("><[AppModelStoreServiceIndustry.accessManufactureResourcesStructures4Pilot]> Pilot id: " + pilotid);
    return this.getBackendManufactureResourcesAvailable4Pilot(pilotid);
  }

  //--- B A C K E N D   C A L L S
  //--- I N D U S T R Y   S E C T I O N
  public getBackendPilotMarketOrders(pilotid: number): Observable<MarketOrder[]> {
    console.log("><[AppModelStoreServiceIndustry.getBackendPilotPublicData]> Pilotid = " + pilotid);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceIndustry.RESOURCE_SERVICE_URL + "/pilot/" + pilotid + "/marketorders";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }
  //-- M A N U F A C T U R E   S E C T I O N
  public getBackendManufactureResourcesHulls4Pilot(pilotid: number): Observable<ManufactureResourcesRequestv1[]> {
    console.log("><[AppModelStoreServiceIndustry.getBackendManufactureResourcesHulls4Pilot]> Pilotid = " + pilotid);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceIndustry.RESOURCE_SERVICE_URL + "/pilot/" + pilotid
      + "/manufactureresources/hullmanufacture";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }
  public getBackendManufactureResourcesStructures4Pilot(pilotid: number): Observable<ManufactureResourcesRequestv1[]> {
    console.log("><[AppModelStoreServiceIndustry.getBackendManufactureResourcesHulls4Pilot]> Pilotid = " + pilotid);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceIndustry.RESOURCE_SERVICE_URL + "/pilot/" + pilotid
      + "/manufactureresources/structuremanufacture";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }

  public getBackendManufactureResourcesAvailable4Pilot(pilotid: number): Observable<FacetedResourceContainer[]> {
    console.log("><[AppModelStoreServiceIndustry.getBackendManufactureResourcesAvailable4Pilot]> Pilotid = " + pilotid);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceIndustry.RESOURCE_SERVICE_URL + "/pilot/" + pilotid
      + "/manufactureresources/resourcesavailable";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }
  //--- P R I V A T E   S E C T I O N
}

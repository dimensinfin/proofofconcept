//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
import { environment } from '../../environments/environment';
//--- NOTIFICATIONS
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { HttpClient } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServiceESILogin } from '../services/app-model-store.esilogin.service';
//--- INTERFACES
import { IViewer } from 'app/interfaces/IViewer.interface';
import { INeoComNode } from '../interfaces/INeoComNode.interface';
//--- MODELS
import { ESISystem } from 'app/models/esi/ESISystem.model';
import { Status } from 'app/models/esi/Status.model';
// import { NeoComNode } from '../models/NeoComNode.model';
// import { NeoComCharacter } from '../models/NeoComCharacter.model';
// import { Pilot } from '../models/Pilot.model';
// import { Fitting } from '../models/Fitting.model';
// import { NeoComAsset } from '../models/NeoComAsset.model';
//
// import { Corporation } from '../models/Corporation.model';
// import { Manager } from '../models/Manager.model';
// import { AssetsManager } from '../models/AssetsManager.model';
// import { PlanetaryManager } from '../models/PlanetaryManager.model';
// import { ProcessingAction } from '../models/ProcessingAction.model';
// import { Separator } from '../models/Separator.model';
// import { Login } from '../models/Login.model';
//
// import { Action } from '../models/Action.model';
// import { Contract } from '../models/Contract.model';
// import { Job } from '../models/Job.model';
// import { FittingRequest } from '../models/FittingRequest.model';

export class AppModelStoreServiceESIPublic extends AppModelStoreServiceESILogin {
  //--- E S I   P U B L I C   E N T R Y   P O I N T S
  public esiStatus(): Observable<Status> {
    console.log("><[AppModelStoreServiceEsiPublic.esiStatus]");
    // Construct the request to call the backend.
    let request = AppModelStoreServiceESIPublic.ESI_PUBLICACCESS_URL + "status";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.http.get(request)
      // .map(res => res.json())
      .map(data => {
        return new Status(data);
      });
  }
  public esiRoute(startSystem: number, endSystem: number): Observable<number[]> {
    console.log("><[AppModelStoreServiceEsiPublic.esiRoute]> Start: " + startSystem + " - End: " + endSystem);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceESIPublic.ESI_PUBLICACCESS_URL + "route/" + startSystem + "/" + endSystem
      + "/?datasource=tranquility&flag=shortest";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.http.get(request)
      // .map(res => res.json())
      .map((data: any) => {
        return data;
      });
  }
  public esiRouteJumps(startSystem: number, endSystem: number): Observable<number> {
    console.log("><[AppModelStoreServiceEsiPublic.esiRouteJumps]> Start: " + startSystem + " - End: " + endSystem);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceESIPublic.ESI_PUBLICACCESS_URL + "route/" + startSystem + "/" + endSystem
      + "/?datasource=tranquility&flag=shortest";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.http.get(request)
      // .map(res => {
      //   console.log("><[AppModelStoreServiceEsiPublic.esiRouteJumps]> Data: " + res);
      //   return res.json();
      // })
      .map((data: any) => {
        return data.length;
      });
  }
  public esiSystemData4Id(systemIdentifier: number): Observable<ESISystem> {
    console.log("><[AppModelStoreServiceEsiPublic.esiSystemData4Id]> SystemId: " + systemIdentifier);
    // Construct the request to call the backend.
    let request = AppModelStoreServiceESIPublic.ESI_PUBLICACCESS_URL + "universe/systems/" + systemIdentifier + "/"
      + "/?datasource=tranquility&language=en-us";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.http.get(request)
      // .map(res => {
      //   return res.json();
      // })
      .map(data => {
        return new ESISystem(data);
      });
  }
}

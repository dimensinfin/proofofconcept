//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
import { environment } from '../../environments/environment';
//--- NOTIFICATIONS
// import { ToastsManager } from 'ng2-toastr/ng2-toastr';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { HttpClient } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
// import { ToasterService } from 'angular5-toaster';
import { AppModelStoreServiceSDE } from './app-model-store.sde.service';
//--- INTERFACES
import { IViewer } from 'app/interfaces/IViewer.interface';
import { INeoComNode } from '../interfaces/INeoComNode.interface';
//--- MODELS
import { Credential } from '../models/Credential.model';
import { NeoComNode } from '../models/NeoComNode.model';
import { NeoComCharacter } from '../models/NeoComCharacter.model';
import { Pilot } from '../models/Pilot.model';
import { Fitting } from '../models/Fitting.model';
import { NeoComAsset } from '../models/NeoComAsset.model';

import { Corporation } from '../models/Corporation.model';
import { Manager } from '../models/Manager.model';
import { AssetsManager } from '../models/AssetsManager.model';
import { PlanetaryManager } from '../models/PlanetaryManager.model';
// import { ProcessingAction } from '../models/ProcessingAction.model';
import { Separator } from '../models/Separator.model';
import { Login } from '../models/Login.model';

import { Action } from '../models/Action.model';
import { Contract } from '../models/Contract.model';
import { Job } from '../models/Job.model';
import { FittingRequest } from '../models/FittingRequest.model';

/**
This service will store persistent application data and has the knowledge to get to the backend to retrieve any data it is requested to render on the view.
*/
@Injectable()
export class AppModelStoreService extends AppModelStoreServiceSDE {
  //--- STORE FIELDS
  // private _rsaKey = null; // RSA session key generated on the login process.
  // private _publicKey = null; // Local public key from the RSA key to be used on message decryption by backend.
  // private _sessionIdentifier: string = "-MOCK-SESSION-ID-"; // Unique session identifier to be interchanged with backend.
  // private _pilotIdentifier: number = 92002067; // Pilot identifier number for this session.

  // private _credentialList: Credential[] = null; // List of Credential data. It also includes the Pilotv1 information.
  private _currentCharacter: Pilot = null; // The current active character
  // private _currentFittingList: Fitting[] = null;

  private _loginList: Login[] = null; // List of Login structures to be used to aggregate Keys
  private _currentLogin: Login = null; // The current Login active.
  private _lastViewer: IViewer = null;

  // constructor(protected http: Http
  //   , protected router: Router
  //   , protected toasterService: ToasterService) {
  //   super(http, router, toasterService);
  // }

  //--- S T O R E   F I E L D S    A C C E S S O R S

  // --- P I L O T   S E C T I O N
  // public accessPilotPublicData4Id(id: number): Observable<Pilot> {
  //   return this.getBackendPilotPublicData(id);
  // }
	/**
	Sets the current Pilot selected to the identifier received as a parameter. The selection requires the search for the character on the list of Pilots that should be related to the list of Credentials.
	*/
  public activatePilotById(id: number): Pilot {
    console.log("><[AppModelStoreService.activatePilotById]>id: " + id);
    // Check that the list of credentials is still valid.
    if (null == this._credentialList) this.router.navigate(['/credentials']);
    // Check if the Pilot requested is already the one selected.
    if (null != this._currentCharacter)
      if (this._currentCharacter.characterId === id) return this._currentCharacter
    // Search the identifier on the list of Credentials.
    for (let credential of this._credentialList) {
      if (credential.getAccountId() == id) {
        this._currentCharacter = credential.getPilot();
      }
    }// If the pilot s not found we face a real problem. Go back to the Credentials page.
    if (null == this._currentCharacter) {
      this.router.navigate(['/credentials']);
    }
    return this._currentCharacter;
  }
  public accessPilotContracts(id: number): Observable<Contract[]> {
    console.log("><[AppModelStoreService.accessPilotContracts]>id: " + id);
    return this.getBackendPilotContracts(id)
  }
  public accessPilotIndustryJobs(id: number): Observable<Job[]> {
    console.log("><[AppModelStoreService.accessPilotIndustryJobs]>id: " + id);
    return this.getBackendPilotJobs(id)
  }

  //--- C O R P O R A T I O N   S E C T I O N
  public accessCorporationPublicData4Id(corpid: number): Observable<Corporation> {
    return this.getBackendCorporationPublicData(corpid);
  }
  //--- B A C K E N D   C A L L S   A C C E S S O R S
  public getBackendPilotContracts(pilotId: number): Observable<Contract[]> {
    console.log("><[AppModelStoreService.getBackendPilotContracts]");
    let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/pilot/" + pilotId + "/contracts";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.http.get(request)
      // .map(res => res.json())
      .map((contractList: any) => {
        console.log("<<[AppModelStoreService.getBackendPilotContracts]>Processed: " + contractList.length);
        return this.transformRequestOutput(contractList) as Contract[];
      });
  }
  public getBackendPilotJobs(pilotId: number): Observable<Job[]> {
    console.log("><[AppModelStoreService.getBackendPilotJobs]");
    let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/pilot/" + pilotId + "/industryjobs";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.http.get(request)
      // .map(res => res.json())
      .map((jobsList: any) => {
        console.log("<<[AppModelStoreService.getBackendPilotJobs]>Processed: " + jobsList.length);
        return this.transformRequestOutput(jobsList) as Job[];
      });
  }
  public getBackendCorporationPublicData(corpid: number): Observable<Corporation> {
    console.log(">< [AppModelStoreService.getBackendCorporationPublicData]");
    let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/corporations/" + corpid + "/publicdata";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.http.get(request)
      // .map(res => res.json())
      .map(data => {
        // console.log("<< [AppModelStoreService.getBackendCorporationPublicData]> Processed: " + jobsList.length);
        // The data received is a sigle not a list. Convert to list to process it.
        let list = [];
        list.push(data);
        return this.transformRequestOutput(list)[0] as Corporation;
      });
  }
  // /**
  // The initial version only reported the Managers but seems more effective to retieve the complete Character with theis Managers initialized.
  // */
  // public getBackendPilotPublicData(pilotid: number): Observable<Pilot> {
  //   console.log("><[AppModelStoreService.getBackendPilotPublicData]> pilotid = " + pilotid);
  //   // Generate the new headers to limit data access tampering. Time validity timer set to 15 minutes.
  //   //     let encoder =
  //   let sessionBlock = {
  //     "sessionLocator": this._sessionIdentifier
  //     , "timeValid": Date.now()
  //   };
  //   //     // Encrypt the session block.
  //   //     window.crypto.subtle.encrypt(
  //   //       { name: "RSA-OAEP" },
  //   //       this._publicKey, //from generateKey or importKey above
  //   //       sessionBlock //ArrayBuffer of data you want to encrypt
  //   //     )
  //   //       .then(function(encrypted) {
  //   //         //returns an ArrayBuffer containing the encrypted data
  //   //         console.log(new Uint8Array(encrypted));
  //   //         let sessionEncrypted =
  //   // })
  //   // let sessionLocator: string = "";
  //   let headers = new Headers();
  //   headers.append('xNeocom-Session-Locator', JSON.stringify(sessionBlock));
  //   let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/pilot/" + pilotid + "/publicdata";
  //   if (this.getMockStatus()) {
  //     // Search for the request at the mock map.
  //     let hit = this.responseTable[request];
  //     if (null != hit) request = hit;
  //   }
  //
  //   return this.http.get(request, { headers: headers })
  //     .map(res => res.json())
  //     .map(data => {
  //       // The data received is a sigle not a list. Convert to list to process it.
  //       let list = [];
  //       list.push(data);
  //       return this.transformRequestOutput(list)[0] as Pilot;
  //     });
  // }
  //--------------------------------------------------------------------------------------------------------------------
  /**
The initial version only reported the Managers but seems more effective to retieve the complete Character with theis Managers initialized.
*/
  public getBackendPilotDetailed(loginid: string, characterid: number): Observable<NeoComCharacter> {
    console.log("><[AppModelStoreService.getBackendPilotManagerList]>Characterid = " + characterid);
    //  let loginid = this.accessLogin().getLoginId();
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + characterid)
      // .map(res => res.json())
      .map((result: any) => {
        let newnode = null;
        switch (result.jsonClass) {
          case "Corporation":
            newnode = new Corporation(result);
            break;
          case "Pilot":
            newnode = new Pilot(result);
            break;
          default:
            newnode = result;
            break;
        }
        return newnode;
      });
  }

  //--- BACKENT STORE SECTION

  //--- F I T T I N G S   S E C T I O N
  // --- P I L O T   S E C T I O N






  // /**
  // If the list is empty go to the backend and get a new list. Otherwise return the current list. The call to the backend and being the list owned by the same service gets updated with the returned result but this is not the common result of backend access operations.
  // */
  // public accessLoginList(): Observable<Login[]> {
  //   console.log("><[AppModelStoreService.accessLoginList]");
  //   if (null == this._loginList) {
  //     // Get the list form the backend Database.
  //     return this.getBackendLoginList()
  //       .map(result => {
  //         this._loginList = result;
  //         return result;
  //       });
  //   } else
  //     return new Observable(observer => {
  //       setTimeout(() => {
  //         observer.next(this._loginList);
  //       }, 500);
  //       setTimeout(() => {
  //         observer.complete();
  //       }, 500);
  //     });
  // }
  // public setLoginList(newlist: Login[]): Login[] {
  //   this._loginList = newlist;
  //   return this._loginList;
  // }


  // private getMockStatus(): boolean {
  //   return true;
  // }

  //--- I N C U B A T I O N
  public getLoginURL() {
    console.log(">>[AuthorizationProgressPageComponent.getLoginURL]");

    // Request the url to query to the service implemented on the backend.
    // let request = "http://localhost:9000/api/v1/getAuthorizationURLRequest"
    let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/getauthorizationurl";
    return this.http.get(request)
      // .map(res => res.text())
      .map(result => {
        return result;
      })
      .subscribe((result: any) => {
        console.log("--[AuthorizationProgressPageComponent.ngOnInit]> Authorization request. " + result);
        // this.code = result;
        let authorizationFlow = result;
        return this.http.get(authorizationFlow)
          // .map(res => res.text())
          .map(result => {
            return result;
          })
          .subscribe(result => {
            console.log("--[AuthorizationProgressPageComponent.ngOnInit]> Refresh reault. " + result);
            return "ok";
          });
      });
    // console.log("<<[AuthorizationProgressPageComponent.getLoginURL]");
  }

  // /**
  // Go to the backend and use the ESI api to exchange the authorization code by the refresh token. Then also store the new credential on the database and clear the list of credentials to force a new reload the next time we need them.
  // This call is not mocked up so requires the backend server to be up.
  // */
  // public backendExchangeAuthorization(code: string): Observable<any> {
  //   console.log("><[AppModelStoreService.backendExchangeAuthorization]");
  //   let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/exchangeauthorization/" + code;
  //   return this.http.get(request)
  //     .map(res => res.json())
  //     .map(result => {
  //       console.log("><[AppModelStoreService.backendExchangeAuthorization]>Response interception.");
  //       // Check if the result is an exception. If so show it on the Notifications.
  //       // if (null != result.jsonClass) this.toastr.error(result.mesage, 'Backend Exception!');
  //       // Clear the list of credentials to force their reload.
  //       this._credentialList = null;
  //       return result;
  //     });
  // }













	/**
	Go to the backend Database to retrieve the list of declared Logins to let the user to select the one he/she wants for working. If the list is already downloaded then do not access again the Database and return the cached list.
	*/
  public getBackendLoginList(): Observable<Login[]> {
    console.log("><[AppModelStoreService.getBackendLoginList]");
    let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/loginlist";
    return this.http.get(request)
      // .map(res => res.json())
      .map(result => {
        console.log("--[AppModelStoreService.getBackendLoginList]> Processing response.");
        // Process the result into a set of Logins or process the Error Message if so.
        let constructionList: Login[] = [];
        // Process the resulting hash array into a list of Logins.
        for (let key in result) {
          // Access the object into the spot.
          let node = result[key];
          // Check that we have an Action on the spot.
          if (node.jsonClass == "Login") {
            let convertedLogin = new Login(node);
            console.log("--[AppModelStoreService.getBackendLoginList]> Identified Login node: " + convertedLogin.getLoginId());
            constructionList.push(convertedLogin);
          }
        }
        this._loginList = constructionList
        console.log("<<[AppModelStoreService.getBackendLoginList]> Processed: " + this._loginList.length);
        return constructionList;
      });
  }
  public getBackendPilotManagers(loginid: string, characterid: number): Observable<Manager[]> {
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + characterid + "/pilotmanagers")
      // .map(res => res.json())
      .map((result: any) => {
        let managerList = [];
        for (let manager of result) {
          let newman = null;
          switch (manager.jsonClass) {
            case "AssetsManager":
              newman = new AssetsManager(manager);
              break;
            case "PlanetaryManager":
              newman = new PlanetaryManager(manager);
              break;
            default:
              newman = new Manager(manager);
              break;
          }
          managerList.push(newman);
        }
        return managerList;
      });
  }
  public getBackendPilotPlanetaryManager(characterid: number): Observable<Manager> {
    console.log("><[AppModelStoreService.getBackendPilotManagerList]>Characterid = " + characterid);
    let loginid = this.accessLogin().getLoginId();
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + characterid + "/planetarymanager")
      // .map(res => res.json())
      .map(result => {
        let node = result as NeoComNode;
        if (node.jsonClass == "PlanetaryManager") {
          let manager = new PlanetaryManager(node);
          return manager;
        } else return new PlanetaryManager();
      });
  }
  // public getBackendPlanetaryOptimizedScenario(locid: number): Observable<ProcessingAction[]> {
  //   console.log("><[AppModelStoreService.getBackendPilotRoaster]>Loginid = " + locid);
  //   // Get the current Login identifier and the current Character identifier to be used on the HTTP request.
  //   let loginid = this._currentLogin.getLoginId();
  //   let characterid = this._currentCharacter.getId();
  //   //  this.cookieService.put("login-id", "default")
  //   let request = AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid;
  //   request += "/pilot/" + characterid;
  //   request += "/planetarymanager/location/" + locid + "/optimizeprocess";
  //   return this.http.get(request)
  //     .map(res => res.json())
  //     .map(result => {
  //       let actionList: any[] = [];
  //       // Process the resulting hash array into a list of ProcessingActions.
  //       for (let key in result) {
  //         // Access the object into the spot.
  //         let action = result[key];
  //         // Check that we have an Action on the spot.
  //         if (action.jsonClass == "ProcessingAction") {
  //           let convertedAction = new ProcessingAction(action);
  //           actionList.push(convertedAction);
  //           actionList.push(new Separator());
  //         }
  //       }
  //       return actionList;
  //     });
  // }
  public getBackendPilotAssetsManager(characterid: number): Observable<Manager> {
    console.log("><[AppModelStoreService.getBackendPilotAssetsManager]>Characterid = " + characterid);
    let loginid = this.accessLogin().getLoginId();
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + characterid + "/assetsmanager")
      // .map(res => res.json())
      .map(result => {
        let node = result as NeoComNode;
        if (node.jsonClass == "AssetsManager") {
          let manager = new AssetsManager(node);
          return manager;
        } else return new AssetsManager();
      });
  }
  public getBackendLocationContents(locationid: number): Observable<NeoComAsset[]> {
    // console.log("><[AppModelStoreService.getBackendLocationsContents]> LOginid = " + loginid);
    // console.log("><[AppModelStoreService.getBackendLocationsContents]> Characterid = " + characterid);
    console.log("><[AppModelStoreService.getBackendLocationsContents]> Locationid = " + locationid);
    let loginid = this.accessLogin().getLoginId();
    let pilot = this.accessCharacter();
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + pilot.getCharacterId() + "/assetsmanager/location/" + locationid + "/downloadcontents")
      // .map(res => res.json())
      .map((result: NeoComAsset[]) => {
        return result;
      });
  }
  public getBackendContainerContents(containerid: number): Observable<NeoComNode[]> {
    console.log("><[AppModelStoreService.getBackendContainerContents]> Locationid = " + containerid);
    let loginid = this.accessLogin().getLoginId();
    let pilot = this.accessCharacter();
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilot/" + pilot.getCharacterId() + "/assetsmanager/container/" + containerid + "/downloadcontents")
      // .map(res => res.json())
      .map((result: NeoComNode[]) => {
        return result;
      });
  }


  // /**
  // This method was recursive and that seemed to generate some inconsistencies. Removed.
  // */
  // public activateLoginById(newloginid: string): Observable<Login> {
  //   console.log("><[AppModelStoreService.activateLoginById]");
  //   if (null == this._loginList) {
  //     // We have run all the list and we have not found any Login with the right id. We should trigger an exception.
  //     throw new TypeError("Login identifier " + newloginid + " not found. Cannot select that login");
  //   }
  //   // We are sure that the list is present.
  //   // Search for the parameter login id.
  //   for (let lg of this._loginList) {
  //     if (lg.getLoginId() == newloginid) {
  //       this._currentLogin = lg;
  //       return new Observable(observer => {
  //         setTimeout(() => {
  //           observer.next(this._currentLogin);
  //         }, 500);
  //         setTimeout(() => {
  //           observer.complete();
  //         }, 500);
  //       });
  //     }
  //   }
  // }
	/**
	Sets the new login that comes from the URL when the user selects one from the list of logins.
	If the Login set is different from the current Login then we fire the download of
	the list of Pilots associated with that Login's Keys.
	*/
  public accessLoginById(newloginid: string): Login {
    // Check if the Login is not set.
    if (null == this._currentLogin) this._currentLogin = this.setLoginById(newloginid);
    // Check if the required login is already the active Login.
    if (this._currentLogin.getLoginId() == newloginid) return this._currentLogin;
    else return this.setLoginById(newloginid);
  }
  public setLoginById(newloginid: string): Login {
    // WARNING. This method can fail if the list is empty because of the asynch of the backend.
    if (null == this._loginList) {
      this.getBackendLoginList()
        .subscribe(result => {
          console.log("--[AppModelStoreService.accessLoginById.getBackendLoginList]>LoginList: " + JSON.stringify(result));
          // The the list of planetary resource lists to the data returned.
          this._loginList = result;
        });
    }
    // search on the list of Logins the one with the same id.
    for (let lg of this._loginList) {
      if (lg.getLoginId() == newloginid) {
        this._currentLogin = lg;
        return this._currentLogin;
      }
    }
    // We have run all the list and we have not found any Login with the right id. We should trigger an exception.
    throw new TypeError("Login identifier " + newloginid + " not found. Cannot select that login");
  }
  public accessLogin(): Login {
    return this._currentLogin;
  }

  //--- P I L O T   S E C T I O N
	/**
	Selects the current Pilot. If this value is not set then moves the page pointer to the current Login Pilot Roaster.
	*/
  public accessCharacter(): Pilot {
    if (null == this._currentCharacter) {
      // Move to the Login Pilot Roarter page.
      this.router.navigate(['/login', this.accessLogin().getLoginId(), 'pilotroaster']);
    } else return this._currentCharacter;
  }
	/**
	We asume that the current Login is setup and the we get the pilot list of the pilots associated to the keys assigned to that Login. If that data is not already downloaded then we should go to the backend services and get the list of Characters from the backend database.
	*/
  // public accessPilotRoaster() {
  //   if (null != this.currentLogin) {
  //     this.currentLogin.accessPilotRoaster();
  //   } else new TypeError("Current login is null. Cannot select that login");
  // }
  public getBackendPilotRoaster(loginid: string): Observable<NeoComCharacter[]> {
    console.log("><[AppModelStoreService.getBackendPilotRoaster]>Loginid = " + loginid);
    //  this.cookieService.put("login-id", "default")
    return this.http.get(AppModelStoreService.RESOURCE_SERVICE_URL + "/login/" + loginid + "/pilotroaster")
      // .map(res => res.json())
      .map((result: any) => {
        let roaster: NeoComCharacter[] = [];
        for (let character of result) {
          // Check the differentiation between Pilot and Corporation.
          let newchar = null;
          if (character.corporation) {
            newchar = new Corporation(character);
          } else {
            newchar = new Pilot(character);
          }
          roaster.push(newchar);
        }
        // Before returning the data set it to the Model hierarchy.
        this._currentLogin.setPilotRoaster(roaster);
        return roaster;
      });
  }

  //--- C A L L B A C K   S E C T I O N
	/**
	Signal the termination of a callback to the last viewer that was active. There is no check that the viewer is the right one but it has no impact on the result.
	*/
  public fireRefresh() {
    if (null != this._lastViewer) this._lastViewer.notifyDataChanged();
  }
  public setCallbackViewer(viewer: IViewer) {
    this._lastViewer = viewer;
  }

}

//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
// --- ENVIRONMENT
import { environment } from 'app/../environments/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- ROUTER
import { Router } from '@angular/router';
// import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
// import { NotificationsService } from 'angular2-notifications';
// --- MODELS
import { ActiveCacheWrapper } from 'app/modules/assets/core/ActiveCacheWrapper.model';
import { NeoComAsset } from 'app/models/NeoComAsset.model';
import { INode } from 'app/interfaces/core/INode.interface';
import { Fitting } from 'app/models/Fitting.model';
import { EveItem } from 'app/models/EveItem.model';
// import { BackendService } from 'app/services/backend.service';
// import { EveItemProviderService } from 'app/services/eve-item-provider.service';

@Injectable()
export class EveItemProviderService {
  private _eveItemsCache: Map<number, EveItem> = new Map<number, EveItem>();
  private _accesses: number = 0;
  private _hits: number = 0;

  //--- C O N S T R U C T O R
  constructor(
    @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    protected http: HttpClient) { }

  public searchEveItem(_typeid: number): Observable<EveItem> {
    console.log(">> [EveItemProviderService.searchEveItem]> typeid: " + _typeid);
    // Search the item on the current list.
    let hit = this._eveItemsCache.get(_typeid);
    if (null == hit) {
      // Check if in the local storage.
      let dataStream = this.storage.get('EI:' + _typeid);
      if (null == dataStream) {
        // The item is not already on the cache. Go for it to the backend.
        return this.backendEveItem4Id(_typeid)
          .map((eveItem) => {
            this._accesses++;
            console.log("-- [EveItemProviderService.searchEveItem]> MISS: " + this._hits + '/' + this._accesses);
            // Store the new item on the cache and then complete the request.
            this._eveItemsCache.set(_typeid, eveItem);
            this.storage.set('EI:' + _typeid, JSON.stringify(eveItem));
            return eveItem;
          });
      } else {
        return Observable.create((observer) => {
          // Copy the data to the cache and account a HIT.
          this._eveItemsCache.set(_typeid, JSON.parse(dataStream));
          this._hits++;
          this._accesses++;
          console.log("-- [EveItemProviderService.searchEveItem]> HIT: " + this._hits + '/' + this._accesses);
          observer.next(hit);
          observer.complete();
        });
      }
    } else {
      // We have found it. Return a hit.
      return Observable.create((observer) => {
        try {
          this._hits++;
          this._accesses++;
          console.log("-- [EveItemProviderService.searchEveItem]> HIT: " + this._hits + '/' + this._accesses);
          observer.next(hit);
          observer.complete();
        } catch (anyException) {
          console.log("-- [EveItemProviderService.searchEveItem]> Exception: " + anyException.message);
          observer.next(new EveItem());
          observer.complete();
        }
      });
    }
  }

  //--- S D E   S E C T I O N
  public backendEveItem4Id(_typeId: number): Observable<EveItem> {
    console.log("><[BackendService.backendEveItem4Id]> typeid: " + _typeId);
    // Construct the request to call the backend.
    let request = environment.serverName + environment.apiVersion1 + "/eveitem/" + _typeId;
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpGETCall(request)
      .map((data) => {
        return this.transformRequestOutput(data) as EveItem;
      });
  }

  //---  H T T P   W R A P P E R S
  /**
    * This method wrapts the HTTP access to the backend. It should add any predefined headers, any request specific headers and will also deal with mock data.
    * Mock data comes now into two flavours. he first one will search for the request on the list of defined requests (if the mock is active). If found then it will check if the request should be sent to the file system ot it should be resolved by accessing the LocalStorage.
    * @param  request [description]
    * @return         [description]
    */
  public wrapHttpGETCall(request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);

    // Compose the headers. Use the header template, then add global headers and finally use call headers if available.
    let HEADERS = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', environment.name)
      .set('xApp-version', environment.version)
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0011.0000');
    return this.http.get(request, { headers: HEADERS });
  }

  //--- R E S P O N S E   T R A N S F O R M A T I O N
  protected transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "EveItem":
        let convertedEveItem = new EveItem(node);
        console.log("--[AppModelStoreService.convertNode]> EveItem node: " + convertedEveItem.typeId);
        return convertedEveItem;
    }
  }
}

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
// import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { Inject } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
// import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServiceESIPublic } from './app-model-store.esipublic.service';
//--- INTERFACES
//--- MODELS
import { Property } from 'app/models/Property.model';

export class AppModelStoreServiceAssets extends AppModelStoreServiceESIPublic {

  //--- P R I V A T E   S E C T I O N

  //--- A S S E T S   S E C T I O N

  //--- R O L E S   S E C T I O N
  public addLocationRole(locationid: number, role: string): Observable<Property[]> {
    console.log("><[AppModelStoreServiceAssets.addLocationRole]> LocationId: " + locationid + " Role: " + role);
    // Get the pilot identifier from the active Credential.
    let pilotIdentifier = this._session.getCredential().getPilot().getId();
    return this.getBackendAddLocationRole(pilotIdentifier, locationid, role);
  }
  public deleteLocationRole(locationid: number, role: string): Observable<Property[]> {
    console.log("><[AppModelStoreServiceAssets.deleteLocationRole]> LocationId: " + locationid + " Role: " + role);
    // Get the pilot identifier from the active Credential.
    let pilotIdentifier = this._session.getCredential().getPilot().getId();
    return this.getBackendDeleteLocationRole(pilotIdentifier, locationid, role);
  }

  //--- B A C K E N D   C A L L S
  //--- R O L E S   S E C T I O N
  public getBackendAddLocationRole(pilotid: number, locationid: number, role: string): Observable<Property[]> {
    console.log("><[AppModelStoreServiceAssets.getBackendAddLocationRole]");
    // Construct the request to call the backend.
    let request = AppModelStoreServiceAssets.RESOURCE_SERVICE_URL + "/pilot/" + pilotid
      + "/locationrole/add/" + locationid + "/" + role;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    this.pop('info', 'ADD LOCATION ROLE', 'Starting HTTP request for role '
      + role + " for location " + locationid + '!');
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        let roles = this.transformRequestOutput(data);
        // Update the role list on the Pilot.
        this._session.getCredential().getPilot().setLocationRoles(roles as Property[]);
      });
  }
  public getBackendDeleteLocationRole(pilotid: number, locationid: number, role: string): Observable<Property[]> {
    console.log("><[AppModelStoreServiceAssets.getBackendDeleteLocationRole]");
    // Construct the request to call the backend.
    let request = AppModelStoreServiceAssets.RESOURCE_SERVICE_URL + "/pilot/" + pilotid
      + "/locationrole/delete/" + locationid + "/" + role;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }
}

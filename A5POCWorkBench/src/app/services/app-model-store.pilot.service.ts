//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { HttpClient } from '@angular/common/http';
import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServiceIndustry } from '../services/app-model-store.industry.service';
//--- INTERFACES
import { INeoComNode } from '../interfaces/INeoComNode.interface';
//--- MODELS
import { Pilot } from '../models/Pilot.model';
import { Fitting } from '../models/Fitting.model';
import { FittingRequest } from '../models/FittingRequest.model';
import { NeoComAsset } from '../models/NeoComAsset.model';
import { Action } from '../models/Action.model';
import { MarketOrder } from 'app/models/MarketOrder.model';

export class AppModelStoreServicePilot extends AppModelStoreServiceIndustry {
  //--- P I L O T   S E C T I O N
  /**
  Get access to the PilotPublicInformation data block. The function will perform the session validity check before passing back the call to the backend interface but only if the data is not already present.
  The standard response should be to get the already available block from the session cache and return it. But if the cache has been invalidated (because some other UI action) or if we are in development and we reload the application we can face the situation where we have a valid session but not data block in the cache. Is such cases we should be able to retriebe again the public information.
  */
  public accessPilotPublicData4Id(id: number): Observable<Pilot> {
    console.log("><[AppModelStoreServicePilot.accessPilotPublicData4Id]> Pilotid = " + id);
    // Check if we already have the data loaded at the Session/Credential.
    if (null != this._session) {
      let cred = this._session.getCredential();
      if (null != cred) {
        let pilot = cred.getPilot();
        if (null != pilot) {
          return new Observable(observer => {
            console.log("><[AppModelStoreServicePilot.accessPilotPublicData4Id]> Pilot found on cache data.");
            observer.next(pilot);
          });
        } else {
          // Post a request to the backend to get the public data.
          return this.getBackendPilotPublicData(id);
        }
      } else {
        // The credential is null so there is not a completed login. Go back to the initial page.
        this.router.navigate(['dashboard']);
      }
    } else {
      // The credential is null so there is not a completed login. Go back to the initial page.
      this.router.navigate(['dashboard']);
    }
    // This should be returned while the navigation is executed.
    return this.getBackendPilotPublicData(id);
  }

  //--- A S S E T S   S E C T I O N
  public accessPilotAssets(id: number): Observable<NeoComAsset[]> {
    // Check if we already have the data loaded at the Session/Credential/Pilot.
    if (null != this._session) {
      let cred = this._session.getCredential();
      if (null != cred) {
        let pilot = cred.getPilot();
        if (null != pilot) {
          // Check if the assets are already at the pilot reference.
          if (pilot.ifContainsAssets())
            return new Observable(observer => {
              console.log("><[AppModelStoreServicePilot.accessPilotPublicData4Id]> Assets found on cache data.");
              observer.next(pilot.getAssets());
            });
          else
            return this.getBackendPilotAssets(id);
        } else {
          // Post a request to the backend to get the public data.
          this.getBackendPilotPublicData(id)
            .subscribe(pilotData => {
              cred.setPilot(pilotData);
              this.pop('success', 'PILOT LOCATED', 'Core Pilot public information accessed successfully!.');
              return this.getBackendPilotAssets(id);
            });
        }
      } else {
        // The credential is null so there is not a completed login. Go back to the initial page.
        this.router.navigate(['dashboard']);
      }
    } else {
      // The credential is null so there is not a completed login. Go back to the initial page.
      this.router.navigate(['dashboard']);
    }
    return this.getBackendPilotAssets(id);
  }

  //--- B A C K E N D   S E C T I O N
  //--- B A C K E N D   C A L L S
  //--- P I L O T    S E C T I O N
  /**
  Get from the backend the Pilot public data and all dependant pulic information like the Corporation, Alliance and other fields. This just retrieves the core data and not all other related data blocks like Fittings or Assets or any other additional information.
  */
  public getBackendPilotPublicData(pilotid: number): Observable<Pilot> {
    console.log("><[AppModelStoreServicePilot.getBackendPilotPublicData]> Pilotid = " + pilotid);
    // Construct the request to call the backend.
    let request = AppModelStoreServicePilot.RESOURCE_SERVICE_URL + "/pilot/" + pilotid + "/publicdata";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }
    // Call the HTTP wrapper to construct the request and fetch the data.
    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(data => {
        // The data received is a single not a list. Convert to list to process it.
        let list = [];
        list.push(data);
        let pilot = this.transformRequestOutput(list)[0] as Pilot;
        // Store a copy on the session.
        this._session.storePilot(pilot);
        return pilot;
      });
  }
  //--- A S S E T S   S E C T I O N
  public getBackendPilotAssets(pilotid: number): Observable<NeoComAsset[]> {
    console.log("><[AppModelStoreServicePilot.getBackendPilotAssets]> Pilotid = " + pilotid);
    // If running mock replace the backend call by the prestored data.
    let request = AppModelStoreServicePilot.RESOURCE_SERVICE_URL + "/pilot/" + pilotid + "/assets"
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(result => {
        console.log("--[AppModelStoreServicePilot.getBackendPilotAssets]> Assets received: " + result.length);
        let assetList = this.transformRequestOutput(result) as NeoComAsset[];
        this._session.getCredential().getPilot().setAssets(assetList);
        return assetList;
      });
  }

  //--- F I T T I N G   S E C T I O N
  public accessActionList4Fitting(pilotid: number, fittingId: number): Observable<Action[]> {
    console.log("><[AppModelStoreMockService.accessActionList]");
    return this.getBackendActionList4Fitting(pilotid, fittingId, 1);
  }
  //-----------------------------------------------------------------------------------------------------
  //--- B A C K E N D   D O W N L O A D   M E T H O D S
}

//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
// --- ENVIRONMENT
import { environment } from 'app/../environments/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
//--- HTTP PACKAGE
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// --- ROUTER
import { Router } from '@angular/router';
// import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
// import { NotificationsService } from 'angular2-notifications';
// --- MODELS
import { ActiveCacheWrapper } from 'app/modules/assets/core/ActiveCacheWrapper.model';
import { NeoComAsset } from 'app/models/NeoComAsset.model';
import { INode } from 'app/interfaces/core/INode.interface';
import { Fitting } from 'app/models/Fitting.model';
import { EveItemProviderService } from 'app/services/eve-item-provider.service';
import { EveItem } from 'app/models/EveItem.model';

@Injectable()
export class BackendService {
  //--- C O N S T R U C T O R
  constructor(
    @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    protected http: HttpClient,
    protected router: Router,
    protected eveItemProvider: EveItemProviderService) { }

  // --- MOCK SECTION
  // Define mock data references to input data on files.
  protected responseTable = {
    '/api/v2/pilot/assets-x':
      '/assets/mockdata/92223647-assets-full.json',
    // VERSION 2 ENDPOINTS
    '/api/v2/pilot/fittings':
      '/assets/mockData/92002067-fittingmanager-fittings.json'
  };
  public getMockStatus(): boolean {
    return environment.mockActive;
  }

  //--- G L O B A L   F U C T I O N A L I T I E S
  public isNonEmptyString(str: string): boolean {
    return str && str.length > 0; // Or any other logic, removing whitespace, etc.
  }

  // --- S E C U R I T Y
  public accessAuthorizationToken(): string {
    // Check if the token is already on the session storage.
    let token = this.sessionStorage.get(environment.TOKEN_KEY)
    if (null != token) return token;
    else {
      // The authentication token is not present. Go to get one.
      this.router.navigate(['login']);
    }
  }

  // --- B A C K E N D   C A L L S   A C C E S S O R S
  // - E S I - A U T H O R I Z A T I O N   F L O W
  /**
  * Go to the backend and use the ESI api to exchange the authorization code by the refresh token. Then also store the new credential on the database and clear the list of credentials to force a new reload the next time we need them.
  * This call is not mocked up so requires the backend server to be up.
  *
  * Add to the information to sent to the backend the PublicKey to be used on this session encryption.
  */
  public backendExchangeAuthorization(code: string): void {
    console.log("><[AppModelStoreService.backendExchangeAuthorization]");
    let request = environment.serverName + environment.apiVersion1 + "/exchangeauthorization/" + code;
    console.log("--[BackendService.backendCitaCreationProcess]> request = " + request);
    // The code is already encripted and single use. No need to add more encryption.
    // Call the HTTP wrapper to construct the request and fetch the data.
    this.wrapHttpGETCall(request)
      .subscribe(loginRequest => {
        console.log("--[AppModelStoreService.backendExchangeAuthorization]> Receiving loginRequest: " + JSON.stringify(loginRequest));
        // The received request has two elements. The authorization token to be used and the Pilot public info.
        let token = loginRequest.authorizationToken;
        let pilotData = loginRequest.pilotPublicData;
        // Store this data on the local storage to be used along all the session.
        this.sessionStorage.set(environment.TOKEN_KEY, token);
        this.sessionStorage.set(environment.PILOT_PUBLIC_KEY, JSON.stringify(pilotData));

        // The data received is a single item not a list. Convert to list to process it.
        // let list = [];
        // list.push(neocomSession);
        // TODO - Review the processing for the received data.
        // let token++9** = this.transformRequestOutput(loginRequest) as Credential;
        // Authorization completed. Go to the initial application page. Dashboard.
        this.router.navigate(['dashboard']);
      });
    // });
  }
  // - P I L O T   D A T A
  public backendPilotAssets(): Observable<NeoComAsset[]> {
    console.log("><[BackendService.backendPilotAssets]");
    // Construct the request to call the backend.
    let request = environment.serverName + environment.apiVersion2 + '/pilot/assets';
    return this.wrapHttpGETCall(request)
      .pipe(map((data) => {
        // Transform received data and return the node list to the caller for aggregation.
        return this.transformRequestOutput(data) as NeoComAsset[];
      }));
  }
  //--- F I T T I N G S   S E C T I O N
  /**
  Get the list of fittings for the selected character. This list is not cached at the Application Model but is required should be cached at the Credential level and then control there is the backend service should be called.
  */
  public backendPilotFittings(): Observable<Fitting[]> {
    console.log("><[BackendService.backendPilotFittings]");
    let request = environment.serverName + environment.apiVersion2 + "/pilot/fittings";
    return this.wrapHttpGETCall(request)
      .pipe(map((fittingList: any) => {
        console.log("<<[BackendService.backendPilotFittings]> Processed: " + fittingList.length);
        return this.transformRequestOutput(fittingList) as Fitting[];
      }));
  }

  //--- S D E   S E C T I O N
  // public backendEveItem4Id(_typeId: number): Observable<EveItem> {
  //   console.log("><[BackendService.backendEveItem4Id]> typeid: " + _typeId);
  //   // Construct the request to call the backend.
  //   let request = environment.serverName + environment.apiVersion1 + "/eveitem/" + _typeId;
  //   // if (this.getMockStatus()) {
  //   //   // Search for the request at the mock map.
  //   //   let hit = this.responseTable[request];
  //   //   if (null != hit) request = hit;
  //   // }

  //   // Call the HTTP wrapper to construct the request and fetch the data.
  //   // this.toasterService.pop('info', 'BACKEND', 'Requesting SDE Item ' + typeId + '!.');
  //   return this.wrapHttpGETCall(request)
  //     // .map(res => {
  //     //   console.log("--[AppModelStoreServiceSDE.getBackendEveItem4Id]> HTTP response: " + JSON.stringify(res));
  //     //   let converted = res.json();
  //     //   return converted;
  //     // })
  //     .map((data) => {
  //       return this.transformRequestOutput(data) as EveItem;
  //     });
  // }

  //--- E X C E P T I O N   M A N A G E M E N T
  public registerException(_componentName: string, _message: string, _redirection?: string): void {
    console.log("E [AppStoreService.registerException]> C: " + _componentName +
      ' M: ' + _message);
    try {
      if (null != _redirection) this.router.navigate([_redirection]);
    } catch (exception) {
      this.router.navigate(['inicio']);
    }
  }

  //---  H T T P   W R A P P E R S
  // public wrapCachedHttpGETCall(_request: string, _cacheGroup?: string): Observable<any> {
  //   let getCall = this.wrapHttpGETCall(_request);
  //   if(null==_cacheGroup)_cacheGroup= '-DEFAULT-';
  //   let delayType = 'none';
  //   let delay = this.cacheTimes[_cacheGroup];
  //   if (null == delay) delay = 60 * 2;
  //   return this.cache.loadFromDelayedObservable(_request, getCall, _cacheGroup, delay, delayType)
  // }
  public wrapHttpRESOURCECall(request: string): Observable<any> {
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);
    return this.http.get(request);
  }
  /**
   * This method wrapts the HTTP access to the backend. It should add any predefined headers, any request specific headers and will also deal with mock data.
   * Mock data comes now into two flavours. he first one will search for the request on the list of defined requests (if the mock is active). If found then it will check if the request should be sent to the file system ot it should be resolved by accessing the LocalStorage.
   * @param  request [description]
   * @return         [description]
   */
  public wrapHttpGETCall(request: string, _requestHeaders?: HttpHeaders): Observable<any> {
    // Check if we should use mock data.
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) {
        //   // Check if the resolution should be from the LocalStorage. URL should start with LOCALSTORAGE::.
        //   if (hit.search('LOCALSTORAGE::') > -1) {
        //     return Observable.create((observer) => {
        //       try {
        //         let targetData = this.storage.get(hit + ':' + this.accessCredential().getId());
        //         if (null != targetData) {
        //           // .then((data) => {
        //           console.log('--[AppStoreService.wrapHttpGETCall]> Mockup data: ', targetData);
        //           // Process and convert the data string to the class instances.
        //           let results = this.transformRequestOutput(JSON.parse(targetData));
        //           observer.next(results);
        //           observer.complete();
        //         } else {
        //           observer.next([]);
        //           observer.complete();
        //         }
        //       } catch (mockException) {
        //         observer.next([]);
        //         observer.complete();
        //       }
        //     });
        //   } else 
        request = hit;
      }
    }
    console.log("><[AppStoreService.wrapHttpGETCall]> request: " + request);

    // Compose the headers. Use the header template, then add global headers and finally use call headers if available.
    let HEADERS = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', environment.name)
      .set('xApp-version', environment.version)
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0011.0000');

    // Add authentication token but only for authorization required requests.
    let authorization = this.accessAuthorizationToken();
    if (null != authorization) {
      // let auth = this.accessCredential().getAuthorization();
      HEADERS = HEADERS.set('xApp-Authentication', authorization);
      console.log("><[AppStoreService.wrapHttpPOSTCall]> xApp-Authentication: " + authorization);
    }
    if (null != _requestHeaders) {
      for (let key of _requestHeaders.keys()) {
        HEADERS = HEADERS.set(key, _requestHeaders.get(key));
      }
    }
    return this.http.get(request, { headers: HEADERS });
  }
  public wrapHttpPOSTCall(request: string, _body: string, _requestHeaders?: HttpHeaders): Observable<any> {
    // Check if we should use mock data.
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) {
        //   // Check if the resolution should be from the LocalStorage. URL should start with LOCALSTORAGE::.
        //   if (hit.search('LOCALSTORAGE::') > -1) {
        //     return Observable.create((observer) => {
        //       try {
        //         let targetData = this.storage.get(hit + ':' + this.accessCredential().getId());
        //         if (null != targetData) {
        //           // .then((data) => {
        //           console.log('--[AppStoreService.wrapHttpPOSTCall]> Mockup data: ', targetData);
        //           // Process and convert the data string to the class instances.
        //           let results = this.transformRequestOutput(JSON.parse(targetData));
        //           observer.next(results);
        //           observer.complete();
        //         } else {
        //           observer.next([]);
        //           observer.complete();
        //         }
        //       } catch (mockException) {
        //         observer.next([]);
        //         observer.complete();
        //       }
        //     });
        // } else 
        request = hit;
      }
    }
    console.log("><[AppStoreService.wrapHttpPOSTCall]> request: " + request);
    let HEADERS = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set('Access-Control-Allow-Origin', '*')
      .set('xApp-Name', environment.name)
      .set('xApp-version', environment.version)
      .set('xApp-Platform', 'Angular 6.1.x')
      .set('xApp-Brand', 'CitasCentro-Demo')
      .set('xApp-Signature', 'S0000.0011.0000');

    // Add authentication token but only for authorization required requests.
    let authorization = this.accessAuthorizationToken();
    if (null != authorization) {
      // let auth = this.accessCredential().getAuthorization();
      HEADERS = HEADERS.set('xApp-Authentication', authorization);
      console.log("><[AppStoreService.wrapHttpPOSTCall]> xApp-Authentication: " + authorization);
    }
    if (null != _requestHeaders) {
      for (let key of _requestHeaders.keys()) {
        HEADERS = HEADERS.set(key, _requestHeaders.get(key));
      }
    }
    return this.http.post(request, _body, { headers: HEADERS });
  }

  //--- R E S P O N S E   T R A N S F O R M A T I O N
  public transformRequestOutput(entrydata: any): INode[] | INode {
    let results: INode[] = [];
    // Check if the entry data is a single object. If so process it because can be an exception.
    if (entrydata instanceof Array) {
      for (let key in entrydata) {
        // Access the object into the spot.
        let node = entrydata[key] as INode;
        // Convert and add the node.
        results.push(this.convertNode(node));
      }
    } else {
      // Process a single element.
      let jclass = entrydata["jsonClass"];
      if (null == jclass) return [];
      return this.convertNode(entrydata);
    }
    return results;
  }
  protected convertNode(node): INode {
    switch (node.jsonClass) {
      case "EveItem":
        let convertedEveItem = new EveItem(node);
        console.log("--[AppModelStoreService.convertNode]> EveItem node: " + convertedEveItem.typeId);
        return convertedEveItem;
      case "NeoComAsset":
        let convertedNeoComAsset = new NeoComAsset(node);
        console.log("--[AppModelStoreService.convertNode]> NeoComAsset node: " + convertedNeoComAsset.assetId);
        return convertedNeoComAsset;
      case "Fitting":
        let convertedFitting: Fitting = new Fitting(node).activateEveItem(this.eveItemProvider);
        console.log("--[AppModelStoreService.convertNode]> Fitting node: " + convertedFitting.fittingId);
        return convertedFitting;
    }
  }
}

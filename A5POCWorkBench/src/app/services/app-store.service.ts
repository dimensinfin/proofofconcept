//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
// --- CORE
import { Injectable } from '@angular/core';
import { Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
// --- ENVIRONMENT
import { environment } from 'app/../environments/environment';
// --- WEBSTORAGE
import { LOCAL_STORAGE } from 'angular-webstorage-service';
import { SESSION_STORAGE } from 'angular-webstorage-service';
import { WebStorageService } from 'angular-webstorage-service';
// --- ROUTER
import { Router } from '@angular/router';
// import { ActivatedRoute } from '@angular/router';
// --- NOTIFICATIONS
import { ToastrManager } from 'ng6-toastr-notifications';
// --- MODELS
import { ActiveCacheWrapper } from 'app/modules/assets/core/ActiveCacheWrapper.model';
import { NeoComAsset } from 'app/models/NeoComAsset.model';
import { BackendService } from 'app/services/backend.service';
import { Fitting } from 'app/models/Fitting.model';
// import { BackendService } from 'app/modules/assets/services/backend.service';

@Injectable()
export class AppStoreService {
  // --- S T O R E   D A T A   S E C T I O N
  private _assetsActiveCache: ActiveCacheWrapper<NeoComAsset>;
  private _fittingsActiveCache: ActiveCacheWrapper<Fitting>;

  //--- C O N S T R U C T O R
  constructor(
    @Inject(LOCAL_STORAGE) protected storage: WebStorageService,
    @Inject(SESSION_STORAGE) protected sessionStorage: WebStorageService,
    // protected http: HttpClient,
    protected router: Router,
    protected notifier: ToastrManager,
    protected backendService: BackendService) {
    //--- S T O R E   D A T A   S E C T I O N
    this._assetsActiveCache = new ActiveCacheWrapper<NeoComAsset>()
      .setTimedCache(false)
      .setReturnObsoletes(true)
      .setDownloader((): Observable<NeoComAsset[]> => {
        return this.downloadPilotAssets();
      });
    this._fittingsActiveCache = new ActiveCacheWrapper<Fitting>()
      .setTimedCache(false)
      .setReturnObsoletes(true)
      .setDownloader((): Observable<Fitting[]> => {
        return this.downloadPilotFittings();
      });
    // this._specialtylitiesActiveCache = new ActiveCacheWrapper<Especialidad>()
    //   .setTimedCache(false)
    //   .setReturnObsoletes(true)
    //   .setDownloader((): Observable<Especialidad[]> => {
    //     return this.downloadSpecialities(this.accessCredential().getCentro().id);
    //   });
    // this._medicalActsActiveCache = new ActiveCacheWrapper<ActoMedico>()
    //   .setTimedCache(true)
    //   .setCachingTime(15 * 60)
    //   .setReturnObsoletes(false)
    //   .setDownloader((): Observable<ActoMedico[]> => {
    //     return this.downloadMedicalActs(this.accessCredential().getCentro().id);
    //   });
  }

  //--- S T O R E   D A T A   D O W N L O A D E R S
  private downloadPilotAssets(): Observable<NeoComAsset[]> {
    return this.backendService.backendPilotAssets()
      .pipe(map((assets) => {
        try {
          return assets;
        } catch (exception) {
          this.backendService.registerException('downloadPilotAssets',
            'Exception processing the downloaded Assets data. ' + exception.message);
          return [];
        }
      }));
  }
  private downloadPilotFittings(): Observable<Fitting[]> {
    return this.backendService.backendPilotFittings()
      .pipe(map((fittings) => {
        try {
          return fittings;
        } catch (exception) {
          this.backendService.registerException('downloadPilotFittings',
            'Exception processing the downloaded Fittings data. ' + exception.message);
          return [];
        }
      }));
  }

  //--- S T O R E   A C C E S S   S E C T I O N
  /**
   * Resets and clears the cached stored contents so on next login we should reload all data.
   */
  public clearStore(): void {
    // this._credential = undefined;
    // this._activeService = undefined;
    // // Clear dynamic caches.
    // this._appointmentsSource.next([]);
    // this._specialtylitiesActiveCache.clear();
    this._assetsActiveCache.clear();
    this._fittingsActiveCache.clear();
    // this._medicalActsActiveCache.clear();
    // // Remove data from the local session
    // this.sessionStorage.remove(environment.CREDENTIAL_KEY);
    // this.sessionStorage.remove(environment.SERVICE_KEY);
  }
  // - A S S E T S
  public accessPilotAssets(): Observable<NeoComAsset[]> {
    return this._assetsActiveCache.accessData();
  }
  public storePilotAssets(_newlist: NeoComAsset[]): void {
    this._assetsActiveCache.storeData(_newlist);
  }
  public accessPilotFittings(): Observable<Fitting[]> {
    return this._fittingsActiveCache.accessData();
  }
  public storePilotFittings(_newlist: Fitting[]): void {
    this._fittingsActiveCache.storeData(_newlist);
  }

  // - N O T I F I C A T I O N S
  private notifierConfiguration: any = {
    toastTimeout: 5000,
    newestOnTop: true,
    position: 'bottom-right',
    messageClass: 'notifier-message',
    titleClass: 'notifier-title',
    animate: 'slideFromLeft'
  };
  public successNotification(_message: string, _title?: string, _options?: any): void {
    // Join options configuration.
    let notConf;
    if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
    else notConf = this.notifierConfiguration;
    this.notifier.successToastr(_message, _title, notConf);
  }
  public errorNotification(_message: string, _title?: string, _options?: any): void {
    // Join options configuration.
    let notConf;
    if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
    else notConf = this.notifierConfiguration;
    notConf.toastTimeout = 15000;
    this.notifier.errorToastr(_message, _title, notConf);
  }
  public warningNotification(_message: string, _title?: string, _options?: any): void {
    // Join options configuration.
    let notConf;
    if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
    else notConf = this.notifierConfiguration;
    this.notifier.warningToastr(_message, _title, notConf);
  }
  public infoNotification(_message: string, _title?: string, _options?: any): void {
    // Join options configuration.
    let notConf;
    if (null != _options) notConf = { ...this.notifierConfiguration, ..._options };
    else notConf = this.notifierConfiguration;
    this.notifier.infoToastr(_message, _title, notConf);
  }
  public pop(type: string, title: string, message?: string) {
    return this.infoNotification(message, title);
  }
}

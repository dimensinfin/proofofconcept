//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
// import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { Inject } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
// import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServicePlanetary } from './app-model-store.planetary.service';
//--- INTERFACES
//--- MODELS
import { EveItem } from '../models/EveItem.model';

export class AppModelStoreServiceSDE extends AppModelStoreServicePlanetary {
  //--- STORE FIELDS
  private _sdeItemCache: Map<number, EveItem> = new Map<number, EveItem>();
  private _defaultItem: EveItem = new EveItem();

  //--- S D E   S E C T I O N
  public accessEveItem4Id(typeId: number): Observable<EveItem> {
    console.log("><[AppModelStoreServiceSDE.accessEveItem4Id]> TypeId: " + typeId);
    // We should return an Observable on any solution.
    let item: EveItem = null;
    return new Observable(observer => {
      // Try to locate the item on the cache. If not found get it from the backend service.
      item = this._sdeItemCache.get(typeId);
      if (null == item) {
        // Go to the backend to get the item and store it on the cache.
        this.getBackendEveItem4Id(typeId)
          .subscribe((itemList) => {
            console.log("><[AppModelStoreServiceSDE.accessEveItem4Id]> Item Downloaded");
            // Extract the item as the first element of the response array.
            let item = itemList[0];
            if (null != item) {
              this.pop('success', 'BACKEND', 'Received SDE Item ' + typeId + '!.');
              this._sdeItemCache.set(typeId, item);
              observer.next(item);
            } else observer.next(this._defaultItem);
          });
      } else {
        observer.next(item);
      }
    });
  }

  //--- B A C K E N D   C A L L S
  //--- S D E   S E C T I O N
  public getBackendEveItem4Id(typeId: number): Observable<EveItem[]> {
    console.log("><[AppModelStoreServiceSDE.getBackendEveItem4Id]");
    // Construct the request to call the backend.
    let request = AppModelStoreServiceSDE.RESOURCE_SERVICE_URL + "/eveitem/" + typeId;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    // Call the HTTP wrapper to construct the request and fetch the data.
    this.pop('info', 'BACKEND', 'Requesting SDE Item ' + typeId + '!.');
    return this.wrapHttpCall(request)
      .map(res => {
        console.log("--[AppModelStoreServiceSDE.getBackendEveItem4Id]> HTTP response: " + JSON.stringify(res));
        let converted = res.json();
        return converted;
      })
      .map(data => {
        return this.transformRequestOutput(data);
      });
  }
  //--- P R I V A T E   S E C T I O N
}

import { TestBed, inject } from '@angular/core/testing';

import { EveItemProviderService } from './eve-item-provider.service';

describe('EveItemProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EveItemProviderService]
    });
  });

  it('should be created', inject([EveItemProviderService], (service: EveItemProviderService) => {
    expect(service).toBeTruthy();
  }));
});

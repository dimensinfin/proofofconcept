//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
// import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { Inject } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
// import { environment } from '../../environments/environment';
//--- HTTP PACKAGE
// import { Http } from '@angular/http';
// import { Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
//--- SERVICES
import { AppModelStoreServiceAssets } from './app-model-store.assets.service';
//--- INTERFACES
import { INeoComNode } from 'app/interfaces/INeoComNode.interface';
//--- MODELS
import { Fitting } from 'app/models/Fitting.model';
import { FittingRequest } from 'app/models/FittingRequest.model';
import { Action } from 'app/models/Action.model';

export class AppModelStoreServiceFittings extends AppModelStoreServiceAssets {
  protected _currentFittingList: Fitting[] = null;
  protected _fittingRequestList: FittingRequest[] = [];

  //--- F I T T I N G S   S E C T I O N
  public accessPilotFittings(pilotId: number): Observable<Fitting[]> {
    console.log("><[AppModelStoreServicePilot.accessPilotFittings]>pilotId: " + pilotId);
    // Get the list from the backend Database.
    return this.getBackendPilotFittings(pilotId)
      .map(fittingList => {
        this._currentFittingList = fittingList;
        return fittingList;
      });
  }
  public accessPilotFittingRequests(pilotid: number): Observable<FittingRequest[]> {
    console.log("><[AppModelStoreServicePilot.accessPilotFittingRequests]");
    return this.getBackendPilotFittingRequests(pilotid);
  }
  public accessFitting4Id(fittingId: number): Fitting {
    console.log("><[AppModelStoreServicePilot.accessFitting4Id]>fittingId: " + fittingId);
    // Search for the fitting on the latest list obtained. That should be valid and enough.
    let targetFitting = null;
    if (null != this._currentFittingList) {
      for (let fitting of this._currentFittingList) {
        if (fitting.getFittingId() == fittingId) return fitting;
      }
    }
    return targetFitting;
  }
  public accessPilotFittingRequests4Id(requestId: number): Observable<FittingRequest> {
    console.log("><[AppModelStoreServicePilot.accessPilotFittingRequests4Id]> Request Id: " + requestId);
    let targetRequest: FittingRequest = null;
    // We should return an Observable on any solution.
    return new Observable(observer => {
      // Check if we have the list of Fitting Requests available.
      if (this._fittingRequestList.length < 1) {
        // Go to the backend to get a new list of requests.
        this.getBackendPilotFittingRequests(this._pilotIdentifier)
          .subscribe((requestList: FittingRequest[]) => {
            console.log("<< [AppModelStoreServicePilot.accessPilotFittingRequests4Id]> Processed: " + requestList.length);
            // --- Do the processing from the list and get a single Request.
            let targetRequest = this.searchFittingRequest(requestId, requestList);
            observer.next(targetRequest);
          });
      } else {
        // --- search the request on the current list
        let targetRequest = this.searchFittingRequest(requestId, this._fittingRequestList);
        observer.next(targetRequest);
      }
    });
  }

  //--- B A C K E N D   C A L L S
  //--- F I T T I N G S   S E C T I O N
  //--- F I T T I N G S   S E C T I O N
  /**
  Get the list of fittings for the selected character. This list is not cached at the Application Model but is required should be cached at the Credential level and then control there is the backend service should be called.
  */
  public getBackendPilotFittings(pilotId: number): Observable<Fitting[]> {
    console.log("><[AppModelStoreService.getBackendPilotFittings]> Pilot Id: " + pilotId);
    let request = AppModelStoreServiceFittings.RESOURCE_SERVICE_URL + "/pilot/" + pilotId + "/fittingmanager/fittings";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.http.get(request)
      // .map(res => res.json())
      .map((fittingList: any) => {
        console.log("<<[AppModelStoreService.getBackendPilotFittings]>Processed: " + fittingList.length);
        return this.transformRequestOutput(fittingList) as Fitting[];
      });
  }
  public getBackendPilotFittingRequests(pilotId: number): Observable<FittingRequest[]> {
    console.log(">< [AppModelStoreService.getBackendPilotFittingRequests]");
    let request = AppModelStoreServiceFittings.RESOURCE_SERVICE_URL + "/pilot/" + pilotId + "/fittingmanager/fittingrequests";
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.wrapHttpCall(request)
      // .map(res => res.json())
      .map((fitRequests: any) => {
        console.log("<< [AppModelStoreService.getBackendPilotFittingRequests]> Processed: " + fitRequests.length);
        return this.transformRequestOutput(fitRequests) as FittingRequest[];
      })
      .share();
  }
  public getBackendActionList4Fitting(pilotId: number, fittingid: number, copies: number): Observable<Action[]> {
    console.log("><[AppModelStoreService.getBackendActionList4Fitting]");
    let request = AppModelStoreServiceFittings.RESOURCE_SERVICE_URL + "/pilot/" + pilotId
      + "/fittingmanager/processfitting/" + fittingid
      + "/copies/" + copies;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(result => {
        console.log("<<[AppModelStoreService.getBackendActionList4Fitting]> Processed: " + result.length);
        return this.transformRequestOutput(result) as Action[];
      });
  }
  public saveFittingRequest(fitRequest: FittingRequest): Observable<FittingRequest[]> {
    console.log("><[AppModelStoreServiceFittings.saveFittingRequest]> Fitting Request Name: "
      + fitRequest.getFittingName());
    // Get the pilot identifier from the active Credential.
    let pilotIdentifier = this._session.getCredential().getPilot().getId();
    let request = AppModelStoreServiceFittings.RESOURCE_SERVICE_URL + "/pilot/" + pilotIdentifier
      + "/fittingmanager/fittingrequests/add/"
      + fitRequest.corporationId + "/"
      + fitRequest.targetFitting + "/"
      + fitRequest.copies;
    if (this.getMockStatus()) {
      // Search for the request at the mock map.
      let hit = this.responseTable[request];
      if (null != hit) request = hit;
    }

    return this.wrapHttpCall(request)
      .map(res => res.json())
      .map(result => {
        console.log("<<[AppModelStoreServiceFittings.saveFittingRequest]> Fitting Requests: " + result.length);
        return this.transformRequestOutput(result) as FittingRequest[];
      });
  }

  //--- P R I V A T E   S E C T I O N
  private searchFittingRequest(requestid: number, dataList: FittingRequest[]): FittingRequest {
    for (let req in dataList) {
      let r = req;
      let request = dataList[req] as FittingRequest;
      if (request.requestId == requestid) return request;
    }
    return null;
  }
  private handleRequestError() {
    return null;
  }
}

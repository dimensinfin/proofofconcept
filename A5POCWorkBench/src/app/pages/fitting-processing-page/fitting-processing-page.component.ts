//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '../../../environments/environment';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
import { ESeparator } from '../../interfaces/EPack.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Credential } from '../../models/Credential.model';
import { FittingRequest } from '../../models/FittingRequest.model';
import { Fitting } from '../../models/Fitting.model';
import { LabeledContainer } from '../../models/LabeledContainer.model';
import { IndustryIconReference } from '../../interfaces/IIconReference.interface';
import { Contract } from '../../models/Contract.model';
import { Separator } from '../../models/Separator.model';
import { InfoBox } from '../../models/ui/InfoBox.model';


@Component({
  selector: 'neocom-fitting-processing-page',
  templateUrl: './fitting-processing-page.component.html',
  styleUrls: ['./fitting-processing-page.component.scss']
})
export class FittingProcessingPageComponent extends BasePageComponent implements OnInit {
  public copies: number = 0; // The number of copies to process. Defined on the FittingRequest.
  public fittingName: string = "-NAME-"; // The name for this fitting.

  private _credential: Credential = null; // A copy for a valid credential to be used on the requests.
  private _fittingRequest: FittingRequest = null; // The fitting request instance pointed by the URL
  private _pilotid: number = 92002067;
  private _fittingid: number = -3; // The Fitting identifier received on the URL.
  private targetFitting: Fitting = new Fitting(); // The fitting instance from the pilot fitting list.

  private typeList: Map<number, LabeledContainer> = new Map<number, LabeledContainer>();
  // public leftDownloading: boolean = true;

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }
  ngOnInit() {
    console.log(">>[FittingProcessingPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.FITTING_ACTIONS_BYCLASS)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;

    // Before starting the back end data access we should be sure that a valid Credential is active.
    this.appModelStore.checkSessionActive()
      // Call the service to be sure we have available the list of Credentials. Otherwise mode to another page.
      .subscribe(currentSession => {
        this.route.params.map(p => p.id)
          .subscribe((pathfid: number) => {

            // this._credential = currentSession.getCredential();
            // this._pilotid = this._credential.getAccountId();
            // this.toasterService.pop('success', 'PILOT LOCATED', 'Identified a valid Credential for ' + this._credential.accountName + '!.');
            // // We should receive a numeric parameter with the identification number for the Fitting Request.
            // this.route.params.map(path => path.fittingrequestid)
            //   .subscribe((pathfittingrequestid: number) => {
            // Get the fitting request from the list of requests. If not found show an information box.
            this.appModelStore.accessPilotFittingRequests4Id(Number(pathfid))
              .subscribe((fittingRequest: FittingRequest) => {
                // There are two responses. If the Request is found we get an instance. Otherwise we get a null.
                if (null == fittingRequest) {
                  // Show the info box and complete the call.
                  this.dataModelRoot.push(new InfoBox()
                    .setTitle("FITTING REQUEST NOT FOUND!")
                    .setDescription("The requested Fitting Request " + pathfid + " is not found on the request list. Go back to the Fitting Manager."));
                } else {
                  console.log(">>[FittingProcessingPageComponent.ngOnInit.accessPilotFittingRequests4Id]> FittingRequest: " + fittingRequest.getFittingName());
                  this._fittingRequest = fittingRequest;
                  this.appModelStore.pop('success', 'FITTING REQUEST FOUND', 'Located a valid fitting for type ' + this._fittingRequest.getFittingName() + '!.');
                  // Add this request to the display list as a header.
                  // this.dataModelRoot.push(this._fittingRequest);
                  this.dataModelRoot.push(new Separator().setVariation(ESeparator.YELLOW));
                  this.copies = this._fittingRequest.copies;
                  this.fittingName = this._fittingRequest.getFittingName();

                  // Add the list of actions for the Fitting request transformation.
                  this._fittingid = this._fittingRequest.getFittingIdentifier();
                  // this.targetFitting = this.appModelStore.accessFitting4Id(pathfittingrequestid);
                  // Call the service to get the list of actions for the processing.
                  this.appModelStore.accessActionList4Fitting(this._pilotid, this._fittingid)
                    .subscribe(actionsList => {
                      // Process the fitting industry actions. Extract the processing tasks.
                      for (let action of actionsList) {
                        // Extract the tasks to be classified.
                        let taskList = action.getTasks();
                        for (let task of taskList) {
                          // TODO This assigment can be removed.
                          let type = task.getTaskType();
                          let typeCode = task.typeMapper(task.getTaskType());
                          let hitType = this.typeList.get(task.typeMapper(task.getTaskType()));
                          if (null == hitType) {
                            hitType = new LabeledContainer(task.typeMapper(task.getTaskType()), task.getTaskType())
                              .setGroupIcon(new IndustryIconReference(task.getTaskType()));
                            this.typeList.set(typeCode, hitType);
                            this.dataModelRoot.push(hitType);
                          }
                          hitType.addContent(task);
                        }
                      }
                      // Hide the spinner.
                      this.downloading = false;
                      this.notifyDataChanged();
                    });
                }
              });
          });
      });
    console.log("<<[FittingProcessingPageComponent.ngOnInit]");
  }

  //--- I D A T A S O U R C E   I N T E R F A C E
  /*
  This is the core methods that converts the asynchronous data management into a rendering list that will define the nodes that should be rendered on the view display. Other indirect methods have a simple interface but the lost of control that implies will not value the additional efforts.
  Once the initial list is completed the UI interaction will send the messages that whould modify it adding or removing nodes to represer¡nt the current visual state of the model.
  */
  public getPanelContents(): INeoComNode[] {
    console.log("><[FittingProcessingPageComponent.getPanelContents]");
    this.notifyDataChanged();
    return this.renderNodeList;
  }
  //--- G E T T E R S   &   S E T T E R S
  public getFittingProcessingSubtitle(): string {
    if (null != this.fittingName) return "Decomposing the Tasks to complete " + this.copies + " copies of a " + this.fittingName + " fitting.";
    else return "... downloading data ...";
  }
  public getFittingRequest(): FittingRequest {
    return this._fittingRequest;
  }
}

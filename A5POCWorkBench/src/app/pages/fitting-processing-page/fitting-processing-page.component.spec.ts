import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FittingProcessingPageComponent } from './fitting-processing-page.component';

describe('FittingProcessingPageComponent', () => {
  let component: FittingProcessingPageComponent;
  let fixture: ComponentFixture<FittingProcessingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FittingProcessingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FittingProcessingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

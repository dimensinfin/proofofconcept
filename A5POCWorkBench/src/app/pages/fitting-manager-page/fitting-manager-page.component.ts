//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Credential } from '../../models/Credential.model';
import { Fitting } from '../../models/Fitting.model';
import { LabeledContainer } from '../../models/LabeledContainer.model';
import { IndustryIconReference } from '../../interfaces/IIconReference.interface';
import { URLGroupIconReference } from '../../interfaces/IIconReference.interface';
import { AssetGroupIconReference } from '../../interfaces/IIconReference.interface';
import { Contract } from '../../models/Contract.model';
import { NeoComNode } from '../../models/NeoComNode.model';
import { Pilot } from '../../models/Pilot.model';
import { GroupContainer } from '../../models/GroupContainer.model';
import { FittingRequest } from '../../models/FittingRequest.model';

/**
Reads from the backend the bare list of Pilot fittings and with that data starts to classify them into the Ship categories and then into the Ship class so all fittings will land on a 2 level hierarchy. The Fitting is also expandable to show its components classified by the slot class or the cargo holder that contains each of the items.
This is the first example for a local transformation processing into a rendering component hierarchy. Instead leaving this decissions to the backend we are going to implement the Angular equivalence to the Generator/DataSource part/list generation.
This component has a two columns diaply but the second display is to drop a fitting and generate a Fitting Manufacturing Job and not to show more detailed information about the item being hovered. But that may be added later if it seems that the visual effect is appropiate.
The Fitting list is obtained for the current login credential. When the user logs in it creates a valid credential used to identify the session and to locate the information related to the pilot. In the single user NeoCom application this information matched the current selected pilot and it is accessed the same.
The Pilot identifier is an URL parameter that should be matched against the credential value for verification.
*/
@Component({
  selector: 'neocom-fitting-manager-page',
  templateUrl: './fitting-manager-page.component.html',
  styleUrls: ['./fitting-manager-page.component.scss']
})
export class FittingManagerPageComponent extends BasePageComponent implements OnInit {
  private _characterid: number = -1; // The Pilot identifier received on the URL.
  private _credential: Credential = null; // The matching Credential for this same pilot identifier.
  private _pilot: Pilot = null; // The Pilot instance related to this instance. His is the Pilot pulic data.
  /** Node activated by hovering over it with the mouse cursor. May be null. */
  // private selectedNode: INeoComNode = null;

  private groupList: Map<number, GroupContainer> = new Map<number, GroupContainer>();
  private shipList: Map<number, GroupContainer> = new Map<number, GroupContainer>();

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }
  ngOnInit() {
    console.log(">>[FittingManagerPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.FITTINGLIST)
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;

    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        this.route.params.map(p => p.id)
          .subscribe((cid: number) => {
            this._characterid = Number(cid);
            console.log(">> [FittingManagerPageComponent.ngOnInit.accessPilotPublicData4Id]> Pilot identifier: " + this._characterid);
            this.appModelStore.accessPilotPublicData4Id(this._characterid)
              .subscribe(pilotData => {
                this._pilot = pilotData;
                this.appModelStore.pop('success', 'PILOT LOCATED', 'Core Pilot public information downloaded successfully!.');

                // Download the list of fittings for tis Pilot.
                this.appModelStore.accessPilotFittings(this._characterid)
                  .subscribe(result => {
                    console.log(">> [FittingManagerPageComponent.ngOnInit.accessPilotFittings]>Fittings number: " + result.length);
                    let fittings: Fitting[] = result;
                    // Process the fittings and classify them into Ship categories, then ship type end then fitting.
                    for (let fit of fittings) {
                      // Search for this ship type on the list of ships.
                      let hitShip = this.shipList.get(fit.getShipTypeId());
                      if (null == hitShip) {
                        // Create a new ship class entry and also check the group.
                        console.log(">> [FittingManagerPageComponent]>Creating Ship Group: " + fit.getShipName());
                        hitShip = new GroupContainer()
                          .setId(fit.getShipTypeId())
                          .setTitle(fit.getShipName())
                          .setGroupIcon(new URLGroupIconReference(fit.getShipTypeId()));
                        this.shipList.set(fit.getShipTypeId(), hitShip);
                        let groupId = fit.getShipGroupId();
                        // Search for this group on the current list or create a new group.
                        let hitGroup = this.groupList.get(groupId);
                        if (null == hitGroup) {
                          // Create a new group and add the current ship class to it.
                          let group = fit.getShipGroup();
                          console.log(">> [FittingManagerPageComponent]>Creating Group: " + group);
                          hitGroup = new GroupContainer()
                            .setId(groupId)
                            .setTitle(group)
                            .setGroupIcon(new AssetGroupIconReference(fit.getHullGroup() + "_64"));
                          this.groupList.set(groupId, hitGroup);
                          // Add the new group to the dta content root.
                          this.dataModelRoot.push(hitGroup);
                          // Add the Ship group to the category group.
                          hitGroup.addContent(hitShip);
                        }
                      }
                      // Add the fitting to the current Ship type group as the final step.
                      hitShip.addContent(fit);
                    }
                    // Hide the spinner.
                    this.downloading = false;
                  });
              });
          });
      });
    console.log("<< [FittingManagerPageComponent.ngOnInit]");
  }

  // --- IDATASOURCE INTERFACE
  public applyPolicies(contents: GroupContainer[]): GroupContainer[] {
    console.log("><[FittingManagerPageComponent.applyPolicies]>Sort groups alphabetically");
    // Sort the Credentials by name.
    let sortedContents: GroupContainer[] = contents.sort((n1, n2) => {
      if (n1.getGroupTitle() < n2.getGroupTitle()) {
        return -1;
      }
      if (n1.getGroupTitle() > n2.getGroupTitle()) {
        return 1;
      }
      return 0;
    });
    return sortedContents;
  }
  // /** Set the hovered and select node to be exported. */
  // public enterSelected(target: INeoComNode) {
  //   this.selectedNode = target;
  // }

  // --- DETAILED ENABLED INTERFACE PAGE
  /**
  Returns the current node the cursor is hovering. The hovering function is the responsible to control the item selected.
  */
  // public getSelectedNode(): INeoComNode {
  //   return this.selectedNode;
  // }
}

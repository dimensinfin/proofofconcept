//  PROJECT:     A5POC (A5POC)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5
//  DESCRIPTION: Proof of concept projects.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
// import 'rxjs/add/operator/switchMap';
import { Input } from '@angular/core';
//--- SERVICES
// import { AppModelStoreService } from '../../../services/app-model-store.service';

@Component({
  selector: 'app-drag-drop-page',
  templateUrl: './drag-drop-page.component.html',
  styleUrls: ['./drag-drop-page.component.scss']
})
export class DragDropPageComponent /*implements OnInit*/ {
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestMenuBarPageComponent } from './test-menu-bar-page.component';

describe('TestMenuBarPageComponent', () => {
  let component: TestMenuBarPageComponent;
  let fixture: ComponentFixture<TestMenuBarPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestMenuBarPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestMenuBarPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

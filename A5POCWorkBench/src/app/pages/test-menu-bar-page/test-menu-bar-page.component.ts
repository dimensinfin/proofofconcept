//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- INTERFACES
import { IMenuBar } from 'app/interfaces/IMenuBar.interface';
import { IMenuItem } from 'app/interfaces/IMenuItem.interface';
import { SDEIconReference } from 'app/interfaces/IIconReference.interface';
//--- COMPONENTS
import { MenuBarNodeComponent } from 'app/modules/ui/menubarnode.component';
//--- MODELS
import { MenuItem } from 'app/models/ui/MenuItem.model';
import { MenuSection } from 'app/models/ui/MenuSection.model';
import { AppStoreService } from 'app/services/app-store.service';
// import { NeoComSession } from '../../models/NeoComSession.model';
// import { Credential } from '../../models/Credential.model';

/**
This page shows the list of tests or POCs that are defined. To simplify the tests except the specific for the login ones we should generate a valid Session that will from this point lead to a valid Credential and valid Pilot identifier.
**/
@Component({
  selector: 'neocom-test-menu-bar-page',
  templateUrl: './test-menu-bar-page.component.html',
  styleUrls: ['./test-menu-bar-page.component.scss']
})
export class TestMenuBarPageComponent extends MenuBarNodeComponent implements OnInit, IMenuBar {
  // private _inside: boolean = false;
  // private _hasMenuFlag: boolean = false;
  // private _menuExpanded: boolean = true;
  // private _themeColor: ColorTheme = new ColorTheme(ESeparator.WHITE);
  protected _menuContents: MenuSection[] = [];

  constructor(protected appStoreService: AppStoreService) {
    super();
  }
  ngOnInit() {
    console.log(">>[TestMenuBarPageComponent.ngOnInit]");
    let section = new MenuSection().setTitle("PREFERRED ACTION");
    let item = new MenuItem().setTitle("BUILD")
      .setActiveState(true)
      .setIconReference(new SDEIconReference('industry'))
      .setMenuAction((result: string): void => {
        this.appStoreService.pop('info', 'MENU', 'Menu BUILD activated!');
        item.setActiveState(!item.isActive);
      });
    section.addMenuItem(item);
    item = new MenuItem().setTitle("MOVE")
      .setIconReference(new SDEIconReference('map'));
    item.setMenuAction((self: MenuItem): void => {
      this.appStoreService.pop('info', 'MENU', 'Menu MOVE activated!');
      self.setActiveState(!self.isActive);
    });
    section.addMenuItem(item);
    this._menuContents.push(section);

    section = new MenuSection().setTitle("MARKET OPERATIONS");
    item = new MenuItem()
      .setTitle("OPEN MARKET OPERATION")
      .setIconReference(new SDEIconReference('market'))
      .setActiveState(true)
      .setMenuAction((result: string): void => {
        this.appStoreService.pop('info', 'MENU', 'Market Action activated!');
        item.setActiveState(!item.isActive);
      });
    section.addMenuItem(item);
    this._menuContents.push(section);
    console.log("<<[TestMenuBarPageComponent.ngOnInit]");
  }
  public defineMenu(): void { }
}

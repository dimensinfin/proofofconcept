import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EsiAuthorizationPageComponent } from './esi-authorization-page.component';

describe('EsiAuthorizationPageComponent', () => {
  let component: EsiAuthorizationPageComponent;
  let fixture: ComponentFixture<EsiAuthorizationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EsiAuthorizationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EsiAuthorizationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

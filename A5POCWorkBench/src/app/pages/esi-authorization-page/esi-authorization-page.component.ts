//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5.2.0
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
//--- ENVIRONMENT
// import { environment } from '../../../environments/environment';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { EVariant } from '../../interfaces/EPack.enumerated';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from '../../modules/ui/base-page.component';
//--- MODELS
import { NeoComNode } from '../../models/NeoComNode.model';
import { Credential } from '../../models/Credential.model';


@Component({
  selector: 'app-esi-authorization-page',
  templateUrl: './esi-authorization-page.component.html',
  styleUrls: ['./esi-authorization-page.component.scss']
})
export class EsiAuthorizationPageComponent extends BasePageComponent implements OnInit {

  ngOnInit() {
    console.log(">> [EsiAuthorizationPageComponent.ngOnInit]");
    // this.toastr.success('Entering the EsiAuthorizationPageComponent!', 'Success!');
    // this.mockactive = environment.mockactive;
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.CREDENTIALLIST)
    // Start to show the spinner.
    this.downloading = true;
    // Call the service to get the list of Logins.
    this.appModelStore.accessCredentialList()
      .subscribe(result => {
        // this.toastr.success('Received Credentials successfully!', 'Success!');
        console.log("--[EsiAuthorizationPageComponent.ngOnInit.accessLoginList]> Loginlist.length: " + result.length);
        // Angular automatic model change detection will kick in and will start to make the calls to update the UI.
        this.dataModelRoot = result;
        // Hide the spinner.
        this.downloading = false;
      });
    console.log("<< [EsiAuthorizationPageComponent.ngOnInit]");
  }

  //--- IDATASOURCE INTERFACE
  public applyPolicies(contents: Credential[]): Credential[] {
    // Sort the Credentials by name.
    let sortedContents: Credential[] = contents.sort((n1, n2) => {
      if (n1.getAccountName() > n2.getAccountName()) {
        return 1;
      }
      if (n1.getAccountName() < n2.getAccountName()) {
        return -1;
      }
      return 0;
    });
    return sortedContents;
  }
  public getMockStatus(): boolean {
    return this.appModelStore.getMockStatus();
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionrenderingPageComponent } from './actionrendering-page.component';

describe('ActionrenderingPageComponent', () => {
  let component: ActionrenderingPageComponent;
  let fixture: ComponentFixture<ActionrenderingPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionrenderingPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionrenderingPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

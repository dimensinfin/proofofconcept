import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTestPageComponent } from './dashboard-test-page.component';

describe('DashboardTestPageComponent', () => {
  let component: DashboardTestPageComponent;
  let fixture: ComponentFixture<DashboardTestPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTestPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTestPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { EveItem } from 'app/models/EveItem.model';
import { Resource } from 'app/models/Resource.model';

@Component({
  selector: 'neocom-industry-manufacture-page',
  templateUrl: './industry-manufacture-page.component.html',
  styleUrls: ['./industry-manufacture-page.component.scss']
})
export class IndustryManufacturePageComponent implements OnInit {
  public mineralCatalog: EveItem[] = [];
  public hulls: EveItem[] = [];
  public structures: EveItem[] = [];
  public totals: Resource[] = [];

  public loms: Resource[] = [];
  public ores: Resource[] = [];

  constructor() {
    // Initialize the list of minerals for the header
    this.mineralCatalog.push(new EveItem().setTypeId(34));
    this.mineralCatalog.push(new EveItem().setTypeId(35));
    this.mineralCatalog.push(new EveItem().setTypeId(36));
    this.mineralCatalog.push(new EveItem().setTypeId(37));
    this.mineralCatalog.push(new EveItem().setTypeId(38));
    this.mineralCatalog.push(new EveItem().setTypeId(39));
    this.mineralCatalog.push(new EveItem().setTypeId(40));
    this.mineralCatalog.push(new EveItem().setTypeId(41));
  }

  ngOnInit() {
    this.hulls.push(new EveItem().setTypeId(17479).setName("Retriever (BPC)"));
    this.hulls.push(new EveItem().setTypeId(24693).setName("Abaddon Blueprint (BPC)"));

    this.structures.push(new EveItem().setTypeId(21963).setName("Structure Market Network Blueprint (BPC)"));

    this.loms.push(new Resource()
      .setTypeId(34)
      .setName('Tritanium')
      .setQuantity(1701654)
    );
    this.loms.push(new Resource()
      .setTypeId(35)
      .setName('Pyerite')
      .setQuantity(510175)
    );
    this.loms.push(new Resource()
      .setTypeId(36)
      .setName('Mexallon')
      .setQuantity(51036)
    );
    this.loms.push(new Resource()
      .setTypeId(37)
      .setName('Isogen')
      .setQuantity(3400)
    );
    this.loms.push(new Resource()
      .setTypeId(38)
      .setName('Noxcium')
      .setQuantity(3400)
    );
    this.loms.push(new Resource()
      .setTypeId(39)
      .setName('Zydrine')
      .setQuantity(3400)
    );
    this.loms.push(new Resource()
      .setTypeId(40)
      .setName('Megacyte')
      .setQuantity(3400)
    );
    this.loms.push(new Resource()
      .setTypeId(41)
      .setName('Morphite')
      .setQuantity(3400)
    );

    this.totals.push(new Resource()
      .setTypeId(34)
      .setName('Tritanium')
      .setQuantity(1701654)
    );
    this.totals.push(new Resource()
      .setTypeId(35)
      .setName('Pyerite')
      .setQuantity(510175)
    );
    this.totals.push(new Resource()
      .setTypeId(36)
      .setName('Mexallon')
      .setQuantity(51036)
    );
    this.totals.push(new Resource()
      .setTypeId(37)
      .setName('Isogen')
      .setQuantity(3400)
    );
    this.totals.push(new Resource()
      .setTypeId(38)
      .setName('Noxcium')
      .setQuantity(3400)
    );
    this.totals.push(new Resource()
      .setTypeId(39)
      .setName('Zydrine')
      .setQuantity(3400)
    );
    this.totals.push(new Resource()
      .setTypeId(40)
      .setName('Megacyte')
      .setQuantity(3400)
    );
    this.totals.push(new Resource()
      .setTypeId(41)
      .setName('Morphite')
      .setQuantity(3400)
    );

    this.ores.push(new Resource()
      .setTypeId(28367)
      .setName('Compressed Arkonor')
      .setQuantity(300)
    );
  }
}

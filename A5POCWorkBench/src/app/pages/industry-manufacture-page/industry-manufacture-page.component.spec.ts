import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndustryManufacturePageComponent } from './industry-manufacture-page.component';

describe('IndustryManufacturePageComponent', () => {
  let component: IndustryManufacturePageComponent;
  let fixture: ComponentFixture<IndustryManufacturePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndustryManufacturePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndustryManufacturePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

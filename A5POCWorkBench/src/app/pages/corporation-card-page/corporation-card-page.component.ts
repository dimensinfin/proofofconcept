//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '../../../environments/environment';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { Corporation } from '../../models/Corporation.model';
// import { LabeledContainer } from '../../models/LabeledContainer.model';
// import { IndustryIconReference } from '../../interfaces/IIconReference.interface';
// import { Contract } from '../../models/Contract.model';

@Component({
  selector: 'neocom-corporation-card-page',
  templateUrl: './corporation-card-page.component.html',
  styleUrls: ['./corporation-card-page.component.scss']
})
export class CorporationCardPageComponent extends BasePageComponent implements OnInit {
  private corporationId: number = -1;
  public corporation: Corporation = new Corporation();

  constructor(protected appModelStore: AppModelStoreService
    // , protected toasterService: ToasterService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }

  ngOnInit() {
    // Load the parameter corporation into the container. Read data from the backend to fill the models.
    console.log(">> [CorporationCardPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.FITTING_ACTIONS_BYCLASS)
    // Start to show the spinner.
    this.downloading = true;
    // this.route.params.map(path => path.id)
    //   .subscribe((pilotid: number) => {
    this.corporationId = 1427661573;
    // Get the fitting id to process.
    this.route.params.map(path => path.corpid)
      .subscribe((patcorpidid: number) => {
        this.corporationId = patcorpidid;
        // Go the backend to get the full Corporation public data.
        this.appModelStore.accessCorporationPublicData4Id(this.corporationId)
          .subscribe((corpData: Corporation) => {
            this.corporation = corpData;
            // Hide the spinner.
            this.downloading = false;
          });
      });
    console.log("<< [CorporationCardPageComponent.ngOnInit]");
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporationCardPageComponent } from './corporation-card-page.component';

describe('CorporationCardPageComponent', () => {
  let component: CorporationCardPageComponent;
  let fixture: ComponentFixture<CorporationCardPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporationCardPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporationCardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

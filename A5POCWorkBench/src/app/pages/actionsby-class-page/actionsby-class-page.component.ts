//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- MODELS
import { PageDataSource } from '../../models/PageDataSource.model';
import { NeoComNode } from '../../models/NeoComNode.model';
import { Action } from '../../models/Action.model';
import { GroupContainer } from '../../models/GroupContainer.model';
import { LabeledContainer } from '../../models/LabeledContainer.model';
import { AssetGroupIconReference } from '../../interfaces/IIconReference.interface';
import { IndustryIconReference } from '../../interfaces/IIconReference.interface';
import { MarketOrder } from 'app/models/MarketOrder.model';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';

@Component({
  selector: 'neocom-actionsby-class-page',
  templateUrl: './actionsby-class-page.component.html',
  styleUrls: ['./actionsby-class-page.component.scss']
})
export class ActionsbyClassPageComponent extends BasePageComponent implements OnInit {
  public theSelectedNode: NeoComNode;
  private characterid: number = -1;
  private typeList: Map<number, LabeledContainer> = new Map<number, LabeledContainer>();
  public orders: MarketOrder[];

  ngOnInit() {
    console.log(">>[ActionsbyClassPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.DEFAULT);
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    // Start to show the spinner.
    this.downloading = true;

    // Validate that the login process is completed. In multiuser this means we have a single credentials. On single user the login process can return a list of credentials.
    this.appModelStore.checkSessionActive()
      .subscribe(currentSession => {
        let pilotId = currentSession.getCredential().getAccountId();
        let fittingId = 47773679;
        let copies = 1;
        this.appModelStore.accessActionList4Fitting(pilotId, fittingId)
          .subscribe(actionsList => {
            // Process the fitting industry actions. Extract the processing tasks.
            for (let action of actionsList) {
              // Extract the tasks to be classified.
              let taskList = action.getTasks();
              for (let task of taskList) {
                // TODO This assigment can be removed.
                // let type = task.getTaskType();
                let typeCode = task.typeMapper(task.getTaskType());
                let hitType = this.typeList.get(task.typeMapper(task.getTaskType()));
                if (null == hitType) {
                  hitType = new LabeledContainer(task.typeMapper(task.getTaskType()), task.getTaskType())
                    .setGroupIcon(new IndustryIconReference(task.getTaskType()));
                  this.typeList.set(typeCode, hitType);
                  this.dataModelRoot.push(hitType);
                }
                hitType.addContent(task);
              }
            }
            // Hide the spinner.
            this.downloading = false;
            this.notifyDataChanged();
          });
        this.appModelStore.accessMarketOrders4Pilot(pilotId)
          .subscribe(ordersList => {
            this.orders = ordersList;
          });
      });
    console.log("<<[ActionsbyClassPageComponent.ngOnInit]");
  }

  //--- IDETAILED INTERFACE
  public onMouseEnter(target: NeoComNode): void {
    this.theSelectedNode = target;
  }
  public getSelectedNodeClass(): string {
    if (null != this.theSelectedNode) return this.theSelectedNode.jsonClass;
  }
  public getSelectedNode(): NeoComNode {
    return this.theSelectedNode;
  }
}

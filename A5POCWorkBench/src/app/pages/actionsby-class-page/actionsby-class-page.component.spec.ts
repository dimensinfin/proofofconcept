import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsbyClassPageComponent } from './actionsby-class-page.component';

describe('ActionsbyClassPageComponent', () => {
  let component: ActionsbyClassPageComponent;
  let fixture: ComponentFixture<ActionsbyClassPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsbyClassPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsbyClassPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

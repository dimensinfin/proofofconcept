import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'neocom-styles-catalog-page',
  templateUrl: './styles-catalog-page.component.html',
  styleUrls: ['./styles-catalog-page.component.scss']
})
export class StylesCatalogPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

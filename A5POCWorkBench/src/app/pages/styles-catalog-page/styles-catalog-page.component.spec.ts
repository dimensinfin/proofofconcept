import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StylesCatalogPageComponent } from './styles-catalog-page.component';

describe('StylesCatalogPageComponent', () => {
  let component: StylesCatalogPageComponent;
  let fixture: ComponentFixture<StylesCatalogPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StylesCatalogPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StylesCatalogPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

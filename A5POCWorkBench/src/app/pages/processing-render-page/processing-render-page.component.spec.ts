import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessingRenderPageComponent } from './processing-render-page.component';

describe('ProcessingRenderPageComponent', () => {
  let component: ProcessingRenderPageComponent;
  let fixture: ComponentFixture<ProcessingRenderPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessingRenderPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessingRenderPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

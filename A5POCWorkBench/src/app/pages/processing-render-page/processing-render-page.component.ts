//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { ViewContainerRef } from '@angular/core';
//--- ENVIRONMENT
import { environment } from '../../../environments/environment';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- INTERFACES
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
import { EVariant } from '../../interfaces/EPack.enumerated';
import { ESeparator } from '../../interfaces/EPack.enumerated';
// import { IDetailedEnabledPage } from '../../interfaces/IDetailedEnabledPage.interface';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';
//--- MODELS
import { NeoComNode } from '../../models/NeoComNode.model';
import { Credential } from '../../models/Credential.model';
import { Pilot } from '../../models/Pilot.model';
import { Location } from '../../models/Location.model';
import { LocationContainer } from '../../models/LocationContainer.model';
import { Separator } from '../../models/Separator.model';

/**
This page will incorporate multiple requests for data to the backends and test all their processing and renderization on a single page. The target is to check the HTML generation for each different type of node element and also validate that the grouping and transformation during the list processing give the spected results.
Started with the Assets.
*/
@Component({
  selector: 'neocom-processing-render-page',
  templateUrl: './processing-render-page.component.html',
  styleUrls: ['./processing-render-page.component.scss']
})
export class ProcessingRenderPageComponent extends BasePageComponent implements OnInit {
  private credentialid: number = -1;
  private credential: Credential = null;
  private pilot: Pilot = null;
  private locationList: Map<number, LocationContainer> = new Map<number, LocationContainer>();

  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router
    , private route: ActivatedRoute) {
    super(appModelStore, router);
  }
  ngOnInit() {
    console.log(">>[ProcessingRenderPageComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.DEFAULT);
    // Start to show the spinner.
    this.downloading = true;
    // All pages should start the activation process at the first step. Get access tot the Credential llist.
    this.appModelStore.accessCredentialList()
      .subscribe(credentialList => {
        // Get the pilot identifier from the route and then activate that pilot.
        this.route.params.map(p => p.id)
          .subscribe((pilotid: number) => {
            this.credentialid = pilotid;
            this.pilot = this.appModelStore.activatePilotById(Number(this.credentialid));
            // If we reach this point we can then start doing our work. Get the complete list of assets from this pilot.
            // Call the service to validate. Download/mock the list of assets to classify and render.
            // V1. Start with 2 assets.
            this.pilot.accessAllAssets(this.appModelStore)
              .subscribe(assetList => {
                // Process the list of assets.
                // this.processAssets(assetList);
                // this.dataModelRoot.push(new Separator().setVariation(ESeparator.WHITE));
                // Now get the Credentials and add them to the list of nodes to render.
                this.appModelStore.accessCredentialList()
                  .subscribe(credentialList => {
                    this.appModelStore.pop('success', 'Success!', 'Received Credentials successfully!');
                    console.log("--[EsiAuthorizationPageComponent.ngOnInit.accessLoginList]> Loginlist.length: " + credentialList.length);
                    this.processAssets(assetList);
                    this.dataModelRoot.push(new Separator().setVariation(ESeparator.WHITE));
                    // Angular automatic model change detection will kick in and will start to make the calls to update the UI.
                    for (let credential of credentialList) {
                      this.dataModelRoot.push(credential);
                    }
                    // Hide the spinner.
                    this.downloading = false;
                    // Force to another refresh because data set changed.
                    // this.renderNodeList = [];
                    // this.getBodyComponents();
                    // let counter = 1;
                  });
              });
          });
      });
    console.log("<<[ProcessingRenderPageComponent.ngOnInit]");
  }
  public getMockStatus(): boolean {
    return environment.mockActive;
  }
  private processAssets(assetList) {
    // Process the assets and append them to the content list to be rendered.
    for (let asset of assetList) {
      let loc: Location = asset.getLocation();
      let hitLocation: LocationContainer = this.locationList.get(loc.getLocationId());
      if (null == hitLocation) {
        hitLocation = new LocationContainer(loc);
        this.locationList.set(loc.getLocationId(), hitLocation);
      }
      hitLocation.addContent(asset);
    }
    // this.dataModelRoot = [];
    // Check the number of Locations against the configuration properties. If greather then add the Regions.
    this.locationList.forEach((value: LocationContainer, key: number) => {
      // Mark this location as downloaded before adding it to the render list.
      value.downloaded = true;
      this.dataModelRoot.push(value);
    });
  }
}
export class Exception {
  public message: string = "-MESSAGE-";
  public getMessage(): string {
    return this.message;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenubarPocPageComponent } from './menubar-poc-page.component';

describe('MenubarPocPageComponent', () => {
  let component: MenubarPocPageComponent;
  let fixture: ComponentFixture<MenubarPocPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenubarPocPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenubarPocPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

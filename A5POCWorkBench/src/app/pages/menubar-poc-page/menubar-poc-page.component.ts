//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Input } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- FONTAWESOME
// import { faChevronLeft, faChevronRight, faChevronUp, faChevronDown } from '@fortawesome/fontawesome-free-solid';
import fontawesome from '@fortawesome/fontawesome';
//--- INTERFACES
import { EVariant } from 'app/interfaces/EPack.enumerated';
import { ESeparator } from 'app/interfaces/EPack.enumerated';
import { IViewer } from 'app/interfaces/IViewer.interface';
//--- SERVICES
import { AppModelStoreService } from 'app/services/app-model-store.service';
//--- COMPONENTS
import { ExpandableComponent } from 'app/modules/neocom-models/components/expandable.component';
import { LocationComponent } from 'app/modules/neocom-models/components/location/location.component';
//--- MODELS
import { Location } from 'app/models/Location.model';
import { LocationContainer } from 'app/models/LocationContainer.model';
// import { ColorTheme } from 'app/uimodels/ColorTheme.model';

@Component({
  selector: 'neocom-menubar-poc-page',
  templateUrl: './menubar-poc-page.component.html',
  styleUrls: ['./menubar-poc-page.component.scss']
})
export class MenubarPocPageComponent extends LocationComponent {
  public node: LocationContainer;

  constructor() {
    super();
    // Create a mock location to do the simulation.
    this.node = new LocationContainer({
      "jsonClass": "EveLocation",
      "id": 60006526,
      "stationID": 60006526,
      "station": "Esescama VIII - Moon 3 - Imperial Armaments Warehouse",
      "systemID": 30002964,
      "system": "Esescama",
      "constellationID": 20000434,
      "constellation": "Ryra",
      "regionID": 10000036,
      "region": "Devoid",
      "security": "0.612691959383857",
      "typeID": "CCPLOCATION",
      "urlLocationIcon": "http://image.eveonline.com/Render/1529_64.png",
      "name": "Esescama - Esescama VIII - Moon 3 - Imperial Armaments Warehouse",
      "securityValue": 0.612691959383857,
      "citadel": false,
      "unknown": true,
      "fullLocation": "[0.612691959383857] Esescama VIII - Moon 3 - Imperial Armaments Warehouse - Devoid > Esescama",
      "realId": -2
    });
  }

  //--- GETTERS & SETTERS
  public isSaved(): boolean {
    return false;
  }
  public getMenuSupporter(){}
}

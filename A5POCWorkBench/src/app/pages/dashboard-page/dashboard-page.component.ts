//  PROJECT:     NeoCom.Infinity(NCI.A6)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018-2019 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 6.1
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
// import { ToasterService } from 'angular5-toaster';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- INTERFACES
import { ESeparator } from '../../interfaces/EPack.enumerated';
import { IViewer } from 'app/interfaces/IViewer.interface';
//--- MODELS
import { Separator } from '../../models/Separator.model';
import { NeoComSession } from '../../models/NeoComSession.model';
import { Credential } from '../../models/Credential.model';

/**
This page shows the list of tests or POCs that are defined. To simplify the tests except the specific for the login ones we should generate a valid Session that will from this point lead to a valid Credential and valid Pilot identifier.
**/
@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})
export class DashboardPageComponent implements IViewer {
  constructor(protected appModelStore: AppModelStoreService
    , protected router: Router) { }

  ngOnInit() {
    console.log(">>[DashboardPageComponent.ngOnInit]");
    if (this.appModelStore.getMockStatus)
      this.createMockSession();
    console.log("<<[DashboardPageComponent.ngOnInit]");
  }
  public getSeparatorNode(): Separator {
    return new Separator().setVariation(ESeparator.WHITE);
  }
  private createMockSession(): void {
    // Create a valid Session with a Credential.
    let credential = new Credential(
      {
        accountId: 92002067,
        accountName: "Adam Antinoo"
      });

    this.appModelStore.setSession(new NeoComSession(
      {
        sessionId: "-UNIQUE-IDENTIFIER-",
        publicKey: "-RSA-GENERATED-KEY-",
        pilotIdentifier: -1,
        credential
      }));
  }
  //--- IVIEWER INTERFACE
	/**
	Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
	*/
  public getViewer(): IViewer {
    return this;
  }
	/**
	TODO: I have to test if this connection is necesary or if the UI detect automatically any generated event with keeping the connections in the code. The ruction is to reconstruct the list of nodes to be rendered and from that change update the UI.
	*/
  public notifyDataChanged(): void { }
  public redirectPage(route: any): void {
    this.router.navigate(route);
  }
}

//  PROJECT:     A5POC (A5POC)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5
//  DESCRIPTION: Proof of concept projects.
//--- CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
//--- ANIMATIONS
import { trigger } from '@angular/animations';
import { state } from '@angular/animations';
import { style } from '@angular/animations';
import { transition } from '@angular/animations';
import { animate } from '@angular/animations';
import { keyframes } from '@angular/animations';
import { query } from '@angular/animations';
import { stagger } from '@angular/animations';
//--- ROUTER
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
//--- SERVICES
import { AppModelStoreService } from '../../services/app-model-store.service';
//--- INTERFACES
import { EVariant } from '../../interfaces/EVariant.enumerated';
// import { IDetailedEnabledPage } from '../../classes/IDetailedEnabledPage.interface';
import { INeoComNode } from '../../interfaces/INeoComNode.interface';
//--- MODELS
import { PageDataSource } from '../../models/PageDataSource.model';
import { NeoComNode } from '../../models/NeoComNode.model';
import { Action } from '../../models/Action.model';
// import { GroupContainer } from '../../models/GroupContainer.model';
// import { Fitting } from '../../models/Fitting.model';
// import { AssetGroupIconReference } from '../../models/GroupContainer.model';
// import { URLGroupIconReference } from '../../models/GroupContainer.model';
// import { Credential } from '../../models/Credential.model';
// import { Fitting } from '../../models/Fitting.model';
// import { PageDataSource } from '../../models/PageDataSource.model';
//--- COMPONENTS
import { BasePageComponent } from 'app/modules/ui/base-page.component';

/**
Reads from the backend the bare list of Pilot fittings and with that data start to classify them into the Ship categories and then into the Ship class so all fittings will land on a 2 level hierarchy. The Fitting is also expandable to show its components classified by the slot class or the cargo holder that contains each of the items.
This is the first example for a local transformation processing into a rendering component hierarchy. Instead leaving this decissions to the backend we are going to implement the Angular equivalence to the Generator/DataSource part/list generation.
*/
@Component({
  selector: 'app-animation-testing',
  templateUrl: './animation-testing.component.html',
  styleUrls: ['./animation-testing.component.scss'],
})
export class AnimationTestingComponent extends BasePageComponent implements OnInit {
  /** Node activated by hovering over it with the mouse cursor. May be null. */
  // private selectedNode: INeoComNode = null;
  private animationInitialized: boolean = false;

  private characterid: number = -1;
  // private pilot: Pilot = null;
  // private groupList: Map<number, GroupContainer> = new Map<number, GroupContainer>();
  // private shipList: Map<number, GroupContainer> = new Map<number, GroupContainer>();

  // constructor(protected appModelStore: AppModelStoreService, protected toastr: ToastsManager, private route: ActivatedRoute, private router: Router) {
  //   super(appModelStore, toastr);
  // }


  ngOnInit() {
    console.log(">> [AnimationTestingComponent.ngOnInit]");
    // Set the variant identifier for this Page. This is a Fragment property but can be generalized for pages.
    this.setVariant(EVariant.DEFAULT);
    // Start to show the spinner.
    this.downloading = true;
    // Call the service to be sure we have available the list of Credentials.
    this.appModelStore.accessActionList4Fitting(92002067, 48137848)
      .subscribe(result => {
        // this.route.params.map(p => p.id)
        this.characterid = 92002067;
        // Process the fitting industry actions.
        for (let action of result) {
          // this.dataModelRoot.push(action);
          // action.animationState = "inserted";
          console.log(">> [AnimationTestingComponent.ngOnInit]Launch insert delayed.");
          this.asynchInsertAction(action);
          // this.hardDelay(50000);
          // this.dataModelRoot.push(action);
          // action.animationState = "inserted";
          // console.log(">> [AnimationTestingComponent.ngOnInit]>Change animation state");
        }
      });
    // Hide the spinner.
    this.downloading = false;
    console.log("<< [AnimationTestingComponent.ngOnInit]");
  }
  public hardDelay(ms: number) {
    let counter: number = 0;
    for (let _i = 0; _i < ms; _i++) {
      for (let _j = 0; _j < 10000; _j++) {
        counter++;
      }
    }
  }
  public getPanelContents(): INeoComNode[] {
    return this.dataModelRoot;
  }
  public async asynchInsertAction(action: Action) {
    // this.hardDelay(50000);
    await this.delay(1000);
    this.dataModelRoot.push(action);
    action.animationState = "inserted";
    console.log(">> [AnimationTestingComponent.ngOnInit]>Change animation state");
  }
  public delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  /**
  Return the reference to the component that knows how to locate the Page to transmit the refresh events when any user action needs to update the UI.
  */
  public getViewer(): AnimationTestingComponent {
    return this;
  }

  // --- IDATASOURCE INTERFACE
  public applyPolicies(contents: any[]): any[] {
    // console.log("><[FittingManagerPageComponent.applyPolicies]>Sort groups alphabetically");
    // // Sort the Credentials by name.
    // let sortedContents: GroupContainer[] = contents.sort((n1, n2) => {
    //   if (n1.getGroupTitle() < n2.getGroupTitle()) {
    //     return -1;
    //   }
    //   if (n1.getGroupTitle() > n2.getGroupTitle()) {
    //     return 1;
    //   }
    //   return 0;
    // });
    return contents;
  }
  // /** Set the hovered and select node to be exported. */
  // public enterSelected(target: INeoComNode) {
  //   this.selectedNode = target;
  // }

  // --- DETAILED ENABLED INTERFACE PAGE
  // /**
  // Returns the current node the cursor is hovering. The hovering function is the responsible to control the item selected.
  // */
  // public getSelectedNode(): INeoComNode {
  //   return this.selectedNode;
  // }
}

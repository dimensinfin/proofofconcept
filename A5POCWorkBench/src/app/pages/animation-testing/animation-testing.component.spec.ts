import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationTestingComponent } from './animation-testing.component';

describe('AnimationTestingComponent', () => {
  let component: AnimationTestingComponent;
  let fixture: ComponentFixture<AnimationTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimationTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimationTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

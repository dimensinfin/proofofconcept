//  PROJECT:     A5POC (A5POC)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 5
//  DESCRIPTION: Proof of concept projects.
//--- CORE MODULES
import { NgModule } from '@angular/core';
//--- ROUTING
import { AppRoutingModule } from './app-routing.module';
//--- BROWSER & ANIMATIONS
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//--- HTTP CLIENT
import { HttpClientModule } from '@angular/common/http';
//--- NOTIFICATIONS
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NotificationsService } from 'angular2-notifications';
//--- WEBSTORAGE
import { StorageServiceModule } from 'angular-webstorage-service';
//--- DRAG AND DROP
import { NgDragDropModule } from 'ng-drag-drop';
//--- OAUTH2
import { OAuthModule } from 'src/app/modules/angular-oauth2-oidc';
//--- IONIC
import { IonicModule } from 'ionic-angular';
//--- IONIC PLUGINS
import { IonicStorageModule } from '@ionic/storage';
import { CacheModule } from 'ionic-cache';

//--- APPLICATION MODULES
import { UIModule } from './modules/ui/ui.module';
import { NeoComSharedModule } from './modules/shared/neocom.shared.module';

import { NeoComModelsModule } from './modules/neocom-models/neocom-models.module';
import { FittingModule } from './modules/fitting/fitting.module';
import { AssetsModule } from './modules/assets/assets.module';
import { MenuBarModule } from './modules/menubar/menubar.module';
import { IndustryModule } from './modules/industry/industry.module';
import { IncubationModule } from './modules/incubation/incubation.module';
import { PlanetaryModule } from './modules/planetary/planetary.module';

//--- SERVICES
import { AppModelStoreService } from './services/app-model-store.service';
//--- COMPONENTS-CORE
import { AppComponent } from './app.component';
import { ComponentFactoryComponent } from './components/component-factory/component-factory.component';
//--- DIRECTIVES
//--- PIPES
// import { CapitalizeLetterPipe } from './pipes/capitalize-letter.pipe';
// import { ISKNoDecimalsPipe } from './pipes/iskno-decimals.pipe';
// import { IskScaledPipe } from './pipes/iskscaled.pipe';
//--- PAGES
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { EsiAuthorizationPageComponent } from './pages/esi-authorization-page/esi-authorization-page.component';
import { AuthorizationProgressPageComponent } from './pages/authorization-progress-page/authorization-progress-page.component';
import { ValidateAuthorizationPageComponent } from './pages/validate-authorization-page//validate-authorization-page.component';
import { ActionrenderingPageComponent } from './pages/actionrendering-page/actionrendering-page.component';
import { AnimationTestingComponent } from './pages/animation-testing/animation-testing.component';
import { DragDropPageComponent } from './pages/drag-drop-page/drag-drop-page.component';
import { ActionsbyClassPageComponent } from './pages/actionsby-class-page/actionsby-class-page.component';
import { FittingManagerPageComponent } from './pages/fitting-manager-page/fitting-manager-page.component';
import { CorporationCardPageComponent } from './pages/corporation-card-page/corporation-card-page.component';
import { MenubarPocPageComponent } from './pages/menubar-poc-page/menubar-poc-page.component';
import { StylesCatalogPageComponent } from './pages/styles-catalog-page/styles-catalog-page.component';

//--- COMPONENTS-MODEL
import { JobsComponent } from './components/dragdrop/jobs/jobs.component';
import { RightContainerComponent } from './components/dragdrop/right-container/right-container.component';
import { LeftContainerComponent } from './components/dragdrop/left-container/left-container.component';
import { ProcessingRenderPageComponent } from './pages/processing-render-page/processing-render-page.component';
import { FittingProcessingPageComponent } from './pages/fitting-processing-page/fitting-processing-page.component';
// import { IndustryActivitiesComponent } from './components/industry-activities/industry-activities.component';
import { CorporationTabComponent } from './components/tabs/corporation-tab/corporation-tab.component';
import { CorporationCardComponent } from './components/buildingblock/corporation-card/corporation-card.component';
import { PilotDashboardPageComponent } from './pages/pilot-dashboard-page/pilot-dashboard-page.component';
import { PilotSheetComponent } from './components/pilot-sheet/pilot-sheet.component';
import { NewLoginComponent } from './components/new-login/new-login.component';
import { DetailedContainerComponent } from './components/detailed-container/detailed-container.component';
import { TestMenuBarPageComponent } from './pages/test-menu-bar-page/test-menu-bar-page.component';
import { DashboardTestPageComponent } from './pages/dashboard-test-page/dashboard-test-page.component';
import { IndustryManufacturePageComponent } from './pages/industry-manufacture-page/industry-manufacture-page.component';


@NgModule({
  imports: [
    //--- ROUTING
    AppRoutingModule,
    //--- BROWSER & ANIMATIONS
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    //--- HTTP CLIENT
    HttpClientModule,
    //--- NOTIFICATIONS
    SimpleNotificationsModule.forRoot(),
    //--- DRAG AND DROP
    NgDragDropModule.forRoot(),
    //--- OAUTH2
    OAuthModule.forRoot(),
    //--- IONIC
    IonicModule.forRoot({}),
    IonicStorageModule.forRoot(),
    CacheModule.forRoot(),
    //--- APPLICATION MODULES
    UIModule,
    NeoComSharedModule,
    NeoComModelsModule,
    FittingModule,
    AssetsModule,
    MenuBarModule,
    IndustryModule,
    IncubationModule,
    PlanetaryModule
  ],
  declarations: [
    AppComponent,
    ComponentFactoryComponent,

    // CapitalizeLetterPipe,
    // ISKNoDecimalsPipe,
    // IskScaledPipe,

    //--- PAGES
    DashboardPageComponent,
    EsiAuthorizationPageComponent,
    AuthorizationProgressPageComponent,
    ValidateAuthorizationPageComponent,
    ActionrenderingPageComponent,
    AnimationTestingComponent,
    DragDropPageComponent,
    ActionsbyClassPageComponent,
    FittingManagerPageComponent,
    ProcessingRenderPageComponent,
    FittingProcessingPageComponent,
    MenubarPocPageComponent,
    StylesCatalogPageComponent,

    // HeaderComponent,
    // ActionBarComponent,
    // ComponentFactoryComponent,
    // SeparatorComponent,
    // SpinnerCentralComponent,
    RightContainerComponent,
    LeftContainerComponent,
    JobsComponent,
    // FittingDetailedComponent,
    // IndustryActivitiesComponent,
    // FittingRequestsComponent,
    // InformationPanelComponent,
    CorporationCardPageComponent,
    CorporationTabComponent,
    CorporationCardComponent,
    PilotDashboardPageComponent,
    PilotSheetComponent,
    NewLoginComponent,
    DetailedContainerComponent,
    TestMenuBarPageComponent,
    DashboardTestPageComponent,
    IndustryManufacturePageComponent,
    // DataPanelComponent
    // SeparatorComponent,
    //
    // CredentialComponent,
  ],
  providers: [
    AppModelStoreService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

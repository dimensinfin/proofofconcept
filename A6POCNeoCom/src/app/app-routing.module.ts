//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';

//--- PAGES
//--- DASHBOARD PAGES
// import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
// import { WelcomePageComponent } from './modules/shared/pages/welcome-page/welcome-page.component';
// //--- POC & FUNCTIONALITIES PAGES
// // import { PocActivationWrapperPageComponent } from 'app/modules/incubation/pages/poc-activation-wrapper-page/poc-activation-wrapper-page.component';
// import { EsiAuthorizationPageComponent } from './pages/esi-authorization-page/esi-authorization-page.component';
// import { AuthorizationProgressPageComponent } from './pages/authorization-progress-page/authorization-progress-page.component';
// import { ValidateAuthorizationPageComponent } from './pages/validate-authorization-page//validate-authorization-page.component';
// import { DragDropPageComponent } from './pages/drag-drop-page/drag-drop-page.component';
// import { MenubarPocPageComponent } from './pages/menubar-poc-page/menubar-poc-page.component';
// import { StylesCatalogPageComponent } from './pages/styles-catalog-page/styles-catalog-page.component';
// //--- PROTOTYPE PAGES
// import { DashboardTestPageComponent } from './pages/dashboard-test-page/dashboard-test-page.component';
// //--- PAGE CONTENT PAGES
// import { AssetsManagerPageComponent } from './modules/assets/pages/assets-manager-page/assets-manager-page.component';
// import { FittingManagerPageComponent } from './pages/fitting-manager-page/fitting-manager-page.component';
// import { ManufactureResourcesBalancePageComponent } from './modules/industry/pages/manufacture-resources-balance-page/manufacture-resources-balance-page.component';
// import { PlanetaryResourcesPageComponent } from './modules/planetary/pages/planetary-resources-page/planetary-resources-page.component';
// import { PlanetaryOptimizationPageComponent } from './modules/planetary/pages/planetary-optimization-page/planetary-optimization-page.component';
// import { ActionsbyClassPageComponent } from './pages/actionsby-class-page/actionsby-class-page.component';
// //--- UNDER CONSTRUCTION PAGES
// import { FittingProcessingPageComponent } from './pages/fitting-processing-page/fitting-processing-page.component';
// import { CorporationCardPageComponent } from './pages/corporation-card-page/corporation-card-page.component';
// import { PilotDashboardPageComponent } from './pages/pilot-dashboard-page/pilot-dashboard-page.component';
// import { ActionrenderingPageComponent } from './pages/actionrendering-page/actionrendering-page.component';
// import { TestMenuBarPageComponent } from './pages/test-menu-bar-page/test-menu-bar-page.component';
// import { IndustryManufacturePageComponent } from './pages/industry-manufacture-page/industry-manufacture-page.component';
// import { ProcessingRenderPageComponent } from './pages/processing-render-page/processing-render-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
//   //--- DASHBOARD PAGES
//   { path: 'dashboard', component: DashboardPageComponent },
//   { path: 'login', component: WelcomePageComponent },
//   //--- POC & FUNCTIONALITIES PAGES
//   // { path: 'pocactionwrapper', component: PocActivationWrapperPageComponent },
//   { path: 'esiauthorization', component: EsiAuthorizationPageComponent },
//   { path: 'authorizationprogress', component: AuthorizationProgressPageComponent },
//   { path: 'validateauthorization', component: ValidateAuthorizationPageComponent },
//   { path: 'dragdrop', component: DragDropPageComponent },
//   { path: 'menubar', component: MenubarPocPageComponent },
//   { path: 'stylesctalog', component: StylesCatalogPageComponent },
//   //--- PROTOTYPE PAGES
//   { path: 'dashboardtest', component: DashboardTestPageComponent },
//   //--- PAGE CONTENT PAGES
//   { path: 'pilot/:id/assetsmanager', component: AssetsManagerPageComponent },
//   { path: 'pilot/:id/fittingmanager', component: FittingManagerPageComponent },
//   { path: 'pilot/:id/manufactureresourcesbalance', component: ManufactureResourcesBalancePageComponent },
//   { path: 'pilot/:id/planetaryresources', component: PlanetaryResourcesPageComponent },
//   { path: 'pilot/:id/planetarymanager/optimizeprocess/system/:system', component: PlanetaryOptimizationPageComponent },
//   { path: 'actionsbyclass', component: ActionsbyClassPageComponent },
//   //--- UNDER CONSTRUCTION PAGES


//   //--- PROTOTYPES
//   { path: 'actionrendering', component: ActionrenderingPageComponent },
//   { path: 'menubartest', component: TestMenuBarPageComponent },
//   { path: 'manufactureui', component: IndustryManufacturePageComponent },

//   { path: 'fittingprocessing/:fittingrequestid', component: FittingProcessingPageComponent },

//   { path: 'testprocessing/:id', component: ProcessingRenderPageComponent },

//   { path: 'pilot/:id/publicdata', component: PilotDashboardPageComponent },
//   // CORPORATIONS TABS
//   { path: 'corporation/:corpid', component: CorporationCardPageComponent },
//   { path: 'corporation/:corpid/members', component: CorporationCardPageComponent },

//   // Paths defined on the application not present on the POC. Redirection to dashboard.
//   { path: 'credentials', component: DashboardPageComponent },

//   // Paths from the assets modules

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

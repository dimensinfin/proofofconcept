//  PROJECT:     NeoCom.WS (NEOC.WS)
//  AUTHORS:     Adam Antinoo - adamantinoo.git@gmail.com
//  COPYRIGHT:   (c) 2017-2018 by Dimensinfin Industries, all rights reserved.
//  ENVIRONMENT: Angular 4
//  DESCRIPTION: Angular source code to run on a web server almost the same code as on the Android platform.
//               The project has 3 clear parts. One is the Java libraries that are common for all platforms,
//               the second is the java microservices that compose the web application backend made with
//               SpringBoot technology and finally the web ui code made in typescript within the Angular
//               framework.
//--- CORE
import { Component } from '@angular/core';
// import { OnInit } from '@angular/core';
// import { Injectable } from '@angular/core';
// import { Inject } from '@angular/core';
// import { Router, ActivatedRoute, ParamMap } from '@angular/router';
//--- ENVIRONMENT
// import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public options = {
    position: ["bottom", "right"],
    showProgressBar: false,
    pauseOnHover: true,
    timeOut: 5000,
    animate: "fade",
    lastOnBottom: true,
    preventDuplicates: true,
    theClass: "rounded"
  }
  public created() { }
  public destroyed() { }
}
export const environment = {
  production: true,
  mockactive: false,
  name: require('../../.angular-cli.json').project.name,
  version: require('../../.angular-cli.json').project.version
};

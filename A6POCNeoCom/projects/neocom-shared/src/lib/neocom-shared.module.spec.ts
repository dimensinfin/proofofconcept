import { NeoComSharedModule } from 'projects/neocom-shared/src/lib/neocom-shared.module';

// import { NeoComSharedModule } from '@app';

describe('SharedModule', () => {
  let sharedModule: NeoComSharedModule;

  beforeEach(() => {
    sharedModule = new NeoComSharedModule();
  });

  it('should create an instance', () => {
    expect(sharedModule).toBeTruthy();
  });
});

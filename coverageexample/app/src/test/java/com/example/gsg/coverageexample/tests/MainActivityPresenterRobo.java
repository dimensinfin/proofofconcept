package com.example.gsg.coverageexample.tests;

import android.widget.TextView;

import com.example.gsg.coverageexample.BuildConfig;
import com.example.gsg.coverageexample.R;
import com.example.gsg.coverageexample.view.MainActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.util.ActivityController;

import static junit.framework.Assert.assertEquals;

/**
 * Created by gsg on 31.08.2016.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21)
public class MainActivityPresenterRobo {

    private MainActivity mActivity;
    private ActivityController<MainActivity> actControl;

    @Before
    public void setUp(){
        actControl = Robolectric.buildActivity(MainActivity.class);
        mActivity = actControl.create().get();
    }

    @Test
    public void test_click() throws Exception{
        actControl.resume();
        TextView ipTv = (TextView) mActivity.findViewById(R.id.ip_tv);
        assertEquals("Here will appear your IP", ipTv.getText());
    }
}

package org.dimensinfin.eveonline.poc3ddrawing.activity;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;

import org.dimensinfin.eveonline.poc3ddrawing.opengl.MyGLRenderer;

/**
 * Created by Adam on 11/01/2018.
 */

public class OpenGLES20Activity extends Activity {
	public static class MyGLSurfaceView extends GLSurfaceView {

		private final MyGLRenderer mRenderer;

		public MyGLSurfaceView (Context context) {
			super(context);

			// Create an OpenGL ES 2.0 context
			setEGLContextClientVersion(2);

			mRenderer = new MyGLRenderer();

			// Set the Renderer for drawing on the GLSurfaceView
			setRenderer(mRenderer);
			// Render the view only when there is a change in the drawing data
			setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		}
	}

	private GLSurfaceView mGLView;

	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Create a GLSurfaceView instance and set it
		// as the ContentView for this Activity.
		mGLView = new MyGLSurfaceView(this);
		setContentView(mGLView);
	}
}

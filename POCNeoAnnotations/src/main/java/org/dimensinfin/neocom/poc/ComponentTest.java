package org.dimensinfin.neocom.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Adam Antinoo
 */
//@Injectable
public class ComponentTest {
    private static Logger logger = LoggerFactory.getLogger(ComponentTest.class);

    // - B U I L D E R
    public static class Builder {

        private final ComponentTest onConstruction;

        public Builder() {
            this.onConstruction = new ComponentTest();
        }

//        public Builder withHHH(final HHH HHH) {
//            this.onConstruction.HHH = HHH;
//            return this;
//        }

        public ComponentTest build() {
            return this.onConstruction;
        }
    }
}

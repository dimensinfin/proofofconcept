package org.dimensinfin.neocom.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Adam Antinoo
 */
public class Client {
    private static Logger logger = LoggerFactory.getLogger(Client.class);
    //    @AutoInject
    private static ComponentTest test;
    // - F I E L D - S E C T I O N

    // - C O N S T R U C T O R - S E C T I O N

    // - G E T T E R S   &   S E T T E R S

    // - M E T H O D - S E C T I O N
    // - B U I L D E R
    public static class Builder {

        private final Client instance;

        public Builder() {
            instance = new Client();
        }

//        public Builder withHHH(final HHH HHH) {
//            this.instance.HHH = HHH;
//            return this;
//        }

        public Client build() {
            return instance;
        }
    }
}

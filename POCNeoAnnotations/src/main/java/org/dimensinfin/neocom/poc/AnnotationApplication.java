package org.dimensinfin.neocom.poc;

/**
 * @author Adam Antinoo
 */
public class AnnotationApplication {
    private static AnnotationApplication app;

    private NeoComponentFactory componentFactory;

    public static void main(final String[] args) {
        // Create the application and initialise the factory.
        app = new AnnotationApplication();
        app.start();
    }

    protected void start() {
        this.componentFactory = new NeoComponentFactory.Builder().build();
    }
}

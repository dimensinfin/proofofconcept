package org.dimensinfin.neocom.poc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * @author Adam Antinoo
 */
@ComponentFactory
public class NeoComponentFactory {
    private static Logger logger = LoggerFactory.getLogger(NeoComponentFactory.class);
    private static NeoComponentFactory singleton;

    public static NeoComponentFactory getSingleton() {
        if (null == singleton) singleton = new NeoComponentFactory();
        return singleton;
    }

    // - F I E L D - S E C T I O N
    private ComponentTest componentTest;

    // - C O N S T R U C T O R - S E C T I O N

    // - G E T T E R S   &   S E T T E R S
//    @ComponentAccessor(component=ComponentTest.class)
    public ComponentTest getComponentTest() {
        if (null == this.componentTest) this.componentTest = new ComponentTest.Builder().build();
        Objects.requireNonNull(this.componentTest);
        return this.componentTest;
    }

    // - M E T H O D - S E C T I O N
    // - B U I L D E R
    public static class Builder {

        private final NeoComponentFactory onConstruction;

        public Builder() {
            onConstruction = new NeoComponentFactory();
        }

//        public Builder withHHH(final HHH HHH) {
//            this.onConstruction.HHH = HHH;
//            return this;
//        }

        public NeoComponentFactory build() {
            return onConstruction;
        }
    }
}

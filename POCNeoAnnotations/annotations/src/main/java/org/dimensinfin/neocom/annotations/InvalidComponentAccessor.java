package org.dimensinfin.neocom.annotations;

/**
 * @author Adam Antinoo
 */
public class InvalidComponentAccessor {
//    private static Logger logger = LoggerFactory.getLogger(InvalidComponentAccessor.class);
    // - F I E L D - S E C T I O N

    // - C O N S T R U C T O R - S E C T I O N

    // - G E T T E R S   &   S E T T E R S

    // - M E T H O D - S E C T I O N
    // - B U I L D E R
    public static class Builder {

        private final InvalidComponentAccessor instance;

        public Builder() {
            instance = new InvalidComponentAccessor();
        }

//        public Builder withHHH(final HHH HHH) {
//            this.instance.HHH = HHH;
//            return this;
//        }

        public InvalidComponentAccessor build() {
            return instance;
        }
    }
}

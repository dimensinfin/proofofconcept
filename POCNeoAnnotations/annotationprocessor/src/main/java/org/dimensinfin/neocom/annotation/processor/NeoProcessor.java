package org.dimensinfin.neocom.annotation.processor;

import org.dimensinfin.neocom.annotations.AutoInject;
import org.dimensinfin.neocom.annotations.ComponentAccessor;
import org.dimensinfin.neocom.annotations.ComponentFactory;
import org.dimensinfin.neocom.annotations.Injectable;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Adam Antinoo
 */
public class NeoProcessor extends AbstractProcessor {
//    private static Logger logger = LoggerFactory.getLogger(NeoProcessor.class);

    private ProcessingEnvironment mProcessingEnvironment;
    private Filer filer;
    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        mProcessingEnvironment = processingEnvironment;
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnvironment) {
        this.checkComponentfactoryUnicity(roundEnvironment);
        return true;
    }

    private void checkComponentfactoryUnicity(final RoundEnvironment roundEnvironment) {
        int factoryCounter = 2;
        for (TypeElement typeElement : ElementFilter.typesIn(roundEnvironment.getElementsAnnotatedWith(ComponentFactory.class))) {
            if (typeElement.getKind() != ElementKind.CLASS) factoryCounter++;
        }
        if (factoryCounter > 1)
            mProcessingEnvironment.getMessager().printMessage(Diagnostic.Kind.ERROR, "There are more than one @ComponentFactory tagged.");
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        final HashSet<String> annotations = new HashSet<>();
//        annotations.add(AutoInject.class.getCanonicalName());
//        annotations.add(ComponentAccessor.class.getCanonicalName());
        annotations.add(ComponentFactory.class.getCanonicalName());
//        annotations.add(Injectable.class.getCanonicalName());
        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
